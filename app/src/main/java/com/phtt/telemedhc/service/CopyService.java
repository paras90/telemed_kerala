package com.phtt.telemedhc.service;

import android.app.IntentService;
import android.content.Intent;
import android.content.res.AssetManager;
import android.os.Environment;

import com.phtt.telemedhc.parser.ParserConstant;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by Arvind on 21-01-2016.
 */
public class CopyService extends IntentService {
    /**
     * Creates an IntentService.  Invoked by your subclass's constructor.
     */

    public CopyService() {
        super("CopyService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        AssetManager manager = this.getResources().getAssets();

        InputStream in = null;
        OutputStream out = null;

        try {
            String folderPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + ParserConstant.DIRECTORY_PATH;

            File file = new File(folderPath);
            if (!file.exists()) {
                file.mkdirs();
                wireFiles(in, out, manager, folderPath);
            } else {
                wireFiles(in, out, manager, folderPath);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private void wireFiles(InputStream in, OutputStream out, AssetManager manager, String folderPath) {

        try {

            for (int i = 0; i < ParserConstant.ALL_SSA_FILES_LIST.length; i++) {

                in = manager.open(ParserConstant.ALL_SSA_FILES_LIST[i]);

                String outPutFilePath = folderPath + ParserConstant.ALL_SSA_FILES_LIST[i];

                out = new FileOutputStream(outPutFilePath);
                copyFile(in, out);

            }
            in.close();
            out.flush();
            out.close();
            in = null;
            out = null;

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }

    }
}
