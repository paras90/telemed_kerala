package com.phtt.telemedhc.utils;

public class Constants {

    public static final String DOCTOR_DETAIL = "doctor_detail";
    public static final String TAB_ALLTIMING = "all_timing";
    public static final String TAB_ALLSERVICES = "all_services";


    public final static String PACKAGE_NAME = "com.phtt.telemedhc";

    public static final String name = "nameKey";
    public static final String passw = "passwordKey";

    public static final String MyPREFERENCES = "MoHealthPreferences";

    public final static String LOGIN_USERNAME = "username";
    public final static String LOGIN_PASSWORD = "password";
    public static final String PATIENT_DATA_INTENT = "patient_data";

    public static final String PARSER_DATA = "PARSER_REQUIRED_DATA";
    public final static String SYMPTOM_DATABASE_NAME = "symptom_ailment";


    public static final String SSA_NAME_REGISTRATION = "registration.ssa";
    public static final String SSA_NAME_BENEFICIARY_REPORT = "healthcube_data.ssa";
    public static final String SSA_NAME_ADD_SYMPTOMS = "addasymptom.ssa";

    // Exit result code for sypmtom class
    public static final int EXIT_CODE_SYMPTOM_SSA = 888;

    public static final String LOGIN_API = "https://healthcubed.com/telemed_services/login.php";
    public static final String REGISTARTION_API = "http://healthcubed.com/telemed_services/reg.php";
    public static final String UPLOAD_IMAGE_API = "https://healthcubed.com/telemed_services/uploadPic_telemedicine.php";
    public static final String SYNC_UPLOAD_API = "https://healthcubed.com/telemed_services/symptoms_data_upload.php";
    public static final String ALL_IMAGE_UPLOAD_API = "https://healthcubed.com/telemed_services/bodyimages_data_upload.php";
    public static final String BULK_REGISTRATION_UPLOAD_API = "https://healthcubed.com/telemed_services/telemed_bulk_reg.php";


    public static final String SEARCH_DOCTOR_SERVICES_API = "http://healthcubed.com/telemed_services/doctor_clinic_search.php";
    //	public static final String APPOINTMENT_SLOT_API = "https://healthcubed.com/webservices/patient_appointments_slot.php";
    public static final String APPOINTMENT_BOOK_API = "http://healthcubed.com/telemed_services/patient_appointments.php";
    public static final String GCM_API = "https://healthcubed.com/telemed_services/gcm_registration.php";
    public static final String DIAGNOSTIC_LIST_API = "https://healthcubed.com/telemed_services/returned_diagnostics.php";
    //	public static final String REPORT_API="http://healthcubed.com/telemed_services/get_patient_report.php";
    public static final String REPORT_API = "https://healthcubed.com/telemedicine/pdf_genrate.php?";


    public static final int CAMERA_PIC_REQUEST_SIDE = 101;

    public static final int CAMERA_EAR_PIC_REQUEST_SIDE = 102;
    public static final int CAMERA_NOSE_PIC_REQUEST_SIDE = 103;
    public static final int CAMERA_MOUTH_PIC_REQUEST_SIDE = 104;
    public static final int CAMERA_OTHER_PIC_REQUEST_SIDE = 105;

    // GCM and MAP Constants
    public static final String SENDER_ID = "722009276921";
    public static final String GCM_API_KEY = "AIzaSyByRZ0OF_hplk8ZpeygNr-iDMaNrAqfxV4";
    public static final String MAP_API_KEY = "AIzaSyCihI_Flwd98VzKmbxBphoq7egF_qQrcKg";

    // state and distric data constatnts
    public static final String TABLE_STATE_DISTRICT = "statedistrict";
    public static final String STATE_NAME = "statename";
    public static final String STATE_CODE = "statecode";
    public static final String DISTRICT_NAME = "districtname";
    public static final String DISTRICT_CODE = "districtcode";


}
