package com.phtt.telemedhc.utils;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class NotificationsDisplay extends View {
	
	public NotificationsDisplay(Context context) {
		super(context);
		// TODO Auto-generated constructor stub
	}

	public void customToast(Context context,String drawImageName, String text)
	{
		int drawImage=0;
		if (drawImageName.equalsIgnoreCase("info"));
		{
			 drawImage =android.R.drawable.ic_dialog_info;
		}
		  LinearLayout  layout=new LinearLayout(context);
          layout.setBackgroundColor(Color.rgb(64, 43, 18));
          
 
          TextView  tv=new TextView(context);
          // set the TextView properties like color, size etc
    
          tv.setTextColor(Color.WHITE);
          tv.setTextSize(20);
          tv.setTypeface(Typeface.DEFAULT_BOLD, 1);
          tv.setGravity(Gravity.CENTER_HORIZONTAL);
          // set the text you want to show in  Toast
          tv.setText("  "+text+"   ");  

          ImageView img=new ImageView(context);

          // give the drawble resource for the ImageView
          img.setImageResource(drawImage);

          // add both the Views TextView and ImageView in layout
          layout.addView(img);
          layout.addView(tv);
            
          Toast toast=new Toast(context);//context is object of Context write "this" if you are an Activity
          // Set The layout as Toast View
          toast.setView(layout);

            // Position you toast here toast position is 50 dp from bottom you can give any integral value   
           toast.setGravity(Gravity.CENTER, 0, 50);
          toast.setDuration(Toast.LENGTH_SHORT);
          toast.show();
	}

}
