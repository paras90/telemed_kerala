package com.phtt.telemedhc.utils;

/**
 * Created by HP on 12-10-2015.
 */
public class AppointmentConstants {

    public static final String BOOKING_STATUS_PENDING = "pending";
    public static final String BOOKING_STATUS_CANCELLED = "cancelled";
    public static final String BOOKING_STATUS_CONFIRMED = "confirm";
    public static final String VISIT_STATUS_COMPLETED = "completed";
    public static final String VISIT_STATUS_MISSED = "missed";
    public static final String VISIT_STATUS_FOLLOWUP = "followup";
    public static final String VISIT_STATUS_UP_COMING = "upcoming";
    public static final String BOOKING_STATUS_OPEN="open";
    public static final String BOOKING_STATUS_CLOSE="close";


}
