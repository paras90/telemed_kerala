package com.phtt.telemedhc.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.AssetManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.provider.Settings.Secure;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.phtt.telemedhc.R;
import com.phtt.telemedhc.parser.ParserConstant;
import com.phtt.telemedhc.parser.jsonparser.JsonParserConstant;
import com.phtt.telemedhc.ui.ActivityManagerForFinish;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.UUID;
import java.util.Vector;
import java.util.concurrent.atomic.AtomicReference;


public class Utils {

    private static final String TAG = Utils.class.getName();

    Dialog contDialog = null;
    Bitmap b;
    boolean smssentbool = false;

    private Utils() {
    }

    private static class SingletonHolder {
        private static final Utils mInstance = new Utils();
    }

    public static Utils getInstance() {
        return SingletonHolder.mInstance;
    }


    public void setUserPicToImageView(ImageView view, String path, int RoundRadius, Context mContext) {

        File imageFile = new File(path);

        BitmapFactory.Options bmOptions = new BitmapFactory.Options();

        Bitmap bitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath(), bmOptions);

        view.setImageBitmap(roundCornerImage(bitmap, RoundRadius));
    }


    public void setTypeFace(Typeface typeFace, ViewGroup parent) {
        for (int i = 0; i < parent.getChildCount(); i++) {
            View v = parent.getChildAt(i);
            if (v instanceof ViewGroup) {
                setTypeFace(typeFace, (ViewGroup) v);
            }
        }
    }

    public long calculateChildAgeGraph(String timestamp, int dobDays) {

        Log.v(TAG, "dob : " + timestamp);
        //  Date date = null;
        int childAgeDays = 0;
        int totalDays = 0;
        long month = 0;

        try {
            //Oct 9, 2014 7:48:21 AM

            SimpleDateFormat formatter = new SimpleDateFormat("MMM dd,yyyy hh:mm:ss");
            Date date = formatter.parse(timestamp);

            Calendar c = Calendar.getInstance();
            long curTime = c.getTimeInMillis();

            long dobMS = (date.getTime() - curTime);

            Log.v(TAG, "dobInMS : " + dobMS);
            childAgeDays = Math.abs((int) (dobMS / (1000 * 60 * 60 * 24)));

            //   childAgeDays =  TimeUnit.MILLISECONDS.toDays(dobMS);
            totalDays = childAgeDays - dobDays;

            month = Math.abs((totalDays / 30));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return month;
    }


    /**
     * upload alert dialog
     *
     * @param mContext
     */
    public void uploadAlert(final Context mContext) {

		/*AlertDialog.Builder exitAlert = new AlertDialog.Builder(mContext);
        notify = new NotificationsDisplay(mContext);
		exitAlert.setTitle(mContext.getString(R.string.UA_UPLOAD_MSG));
//		 ViewGroup vg = (ViewGroup)exitAlert.getWindow().getDecorView();
//		 ViewUtils.setTypeFace(Typeface.createFromAsset(mContext.getAssets(), Constants.FONT_PATH), vg);

		exitAlert.setPositiveButton(mContext.getString(R.string.YES),
				new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface arg0, int arg1) {

				notify.customToast(mContext, "info",  mContext.getString(R.string.UA_SERVER_NOT_READY));

			}
		});

		exitAlert.setNegativeButton(mContext.getString(R.string.NO),
				new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});

		exitAlert.show();*/
    }

    private static AtomicReference<Long> currentTime =
            new AtomicReference<>(System.currentTimeMillis());

    public String getNewTokenId() {
        Long prev;
        Long next = System.currentTimeMillis();
        do {
            prev = currentTime.get();
            next = next > prev ? next : prev + 1;
        } while (!currentTime.compareAndSet(prev, next));
        return next.toString();
    }

    public Bitmap roundCornerImage(Bitmap src, float round) {
        // Source image size
        int width = src.getWidth();
        int height = src.getHeight();
        // create result bitmap output
        Bitmap result = Bitmap.createBitmap(width, height, Config.ARGB_8888);
        // set canvas for painting
        Canvas canvas = new Canvas(result);
        canvas.drawARGB(0, 0, 0, 0);

        // configure paint
        final Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setStrokeWidth(33);
        paint.setColor(Color.GREEN);

        // configure rectangle for embedding
        final Rect rect = new Rect(0, 0, width, height);
        final RectF rectF = new RectF(rect);

        // draw Round rectangle to canvas
        canvas.drawRoundRect(rectF, round, round, paint);

        // create Xfer mode
        paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
        // draw source image to canvas
        canvas.drawBitmap(src, rect, rect, paint);

        // return final image
        return result;
    }


    /**
     * provide the device id of android phone
     *
     * @return
     */
    public String getDeviceID(Context mContext) {
        return Secure.getString(mContext.getContentResolver(),
                Secure.ANDROID_ID);
    }


    public String createNewTokenId() {

        String temp = UUID.randomUUID().toString();

        byte[] ba = temp.getBytes();
        StringBuilder str = new StringBuilder();

        for (int i = 0; i < ba.length; i++)
            str.append(String.format("%x", ba[i]));
        return str.toString();
    }

    public int calculateChildAge(String dob) {

        Log.v(TAG, "dob : " + dob);
        Date date = null;
        int childAgeDays = 0;
        try {
            date = new SimpleDateFormat("dd-MM-yyyy").parse(dob);
            long dobMS = date.getTime();

            Log.v(TAG, "dobInMS : " + dobMS);
            Date curDate = new Date();

            long curTimeMS = curDate.getTime();
            Log.v(TAG, "curTimeMS : " + curTimeMS);

            long curAgeMS = curTimeMS - dobMS;
            Log.v(TAG, "curAgeMS => " + curAgeMS);

            //  childAge = (curAgeMS*86400000);

            childAgeDays = Math.abs((int) (curAgeMS / (1000 * 60 * 60 * 24)));

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return childAgeDays;
    }


    public void loadUserPic(ImageView userPic, String picUrl) {

        File imageFile = new File(Environment.getExternalStorageDirectory() + "/ChildCarePic/" + picUrl);
        Log.v(TAG, "imageFile abs path" + imageFile.getAbsolutePath());

        if (imageFile.exists()) {
            Bitmap bitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath());
            Log.v(TAG, "Bitmap: " + bitmap);
            userPic.setImageBitmap(bitmap);

        }
    }

    /**
     * set mandatory field color
     *
     * @param view
     */
    public void setMandatoryFieldColor(TextView view) {

        if (view.getText().toString().contains("**")) {
            Log.d("check", "6,2");
            //dv.getText().toString().ge
            String arse = view.getText().toString();

            int apple = arse.indexOf("**");
            String next = "<font color='#FF0000'>" + arse.substring(apple) + "</font>";
            view.setText(Html.fromHtml(arse.substring(0, apple) + next));
        }

    }


    /**
     * get current date in milliseconds
     *
     * @return long
     */
    public long getLongDate() {
        Calendar c = Calendar.getInstance();
        return c.getTimeInMillis();
    }

    /**
     * @param timeStamp
     * @return Time
     */
    public String getDateValue(String timeStamp) {
        String ret = null;
        long s_time = getLongValue(timeStamp);
        try {
            //			if (s_time <= 0){
            //				return "Existing event still in progress";
            //			}

            //			return getDate(s_time);

        } catch (Throwable e) {
            ret = null;
        }
        return ret;
    }

    /**
     * @param
     * @return date & time in "hh:mm:ss aa dd-MM-yyyy" format
     */
    public String getDate(/*long time*/) {
        String date = null;
        try {
            Calendar c = Calendar.getInstance();
            System.out.println("Current time => " + c.getTime());

            SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
            date = df.format(c.getTime());
            //
            //			Calendar cal = Calendar.getInstance(Locale.ENGLISH);
            //			cal.setTimeInMillis(time);
            //			date = DateFormat.format("hh:mm:ss aa dd-MM-yyyy", cal).toString();

        } catch (Throwable e) {
            e.printStackTrace();
        }
        return date;
    }

    /**
     * @param value
     * @return long value
     */
    public long getLongValue(String value) {
        long ret = -1;

        try {
            ret = Long.parseLong(value);
        } catch (Throwable e) {
            ret = -1;
        }
        return ret;
    }


    /**
     * method to change app language
     *
     * @param baseContext
     * @param langToLoad
     */
    public void changeAppLocale(final Context baseContext, final String langToLoad) {

        try {
            Locale locale = new Locale(langToLoad);
            Locale.setDefault(locale);
            Configuration config = new Configuration();
            config.locale = locale;
            baseContext.getResources().updateConfiguration(config,
                    baseContext.getResources().getDisplayMetrics());

        } catch (Throwable e) {
            Log.v(TAG, "changeLanguage, Exception: " + e);
            e.printStackTrace();
        }
    }


    /**
     * check if Internet is connected or not
     *
     * @param mContext
     * @return data connectivity status
     */
    public boolean isInternetAvailable(Context mContext) {

        ConnectivityManager cm = (ConnectivityManager) mContext
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        return netInfo != null;
/*
		return null != netInfo && netInfo.isConnected();
*/
    }


    /**
     * method to get current time stamp
     *
     * @return current time stamp
     */
    public String getTimeStamp() {

        String timeStamp = null;
        try {
            timeStamp = java.text.DateFormat.getDateTimeInstance().format(
                    Calendar.getInstance().getTime());
            Log.v(TAG, "getTimeStamp(), Time stamp :" + timeStamp);

        } catch (Exception e) {
            Log.e(TAG, "getTimeStamp() :" + e);
            e.printStackTrace();
        }

        return timeStamp;

    }


    public Bitmap getBitmapFromView(View view, int totalHeight, int totalWidth) {

        Bitmap returnedBitmap = Bitmap.createBitmap(totalWidth, totalHeight, Config.ARGB_8888);
        //Bitmap.createScaledBitmap(view, 600, totalHeight, null);
//		Paint p = new Paint();
//	    p.setXfermode(new PorterDuffXfermode(Mode.CLEAR));
        Canvas canvas = new Canvas(returnedBitmap);
        //canvas.drawRect(30, 30, 100, 100, p);
        Drawable bgDrawable = view.getBackground();
        if (bgDrawable != null)
            bgDrawable.draw(canvas);
        else
            canvas.drawColor(Color.WHITE);
        view.draw(canvas);
        return returnedBitmap;
    }


    /**
     * set compulsory field color
     *
     * @param view
     */
    public void setCompulsoryColor(TextView view) {

        if (view.getText().toString().contains("**")) {
            String arse = view.getText().toString();

            int apple = arse.indexOf("**");
            String next = "<font color='#FF0000'>" + arse.substring(apple) + "</font>";
            view.setText(Html.fromHtml(arse.substring(0, apple) + next));
        }
    }

    public boolean isSimExists(Context mContext) {
        TelephonyManager telephonyManager = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
        int SIM_STATE = telephonyManager.getSimState();

        return SIM_STATE == TelephonyManager.SIM_STATE_READY;
    }

    public boolean sendSMS(String phoneNumber, String message, Context mContext) {
        smssentbool = false;
        String SENT = "SMS_SENT";
        String DELIVERED = "SMS_DELIVERED";
        Log.d("here", "hererh");
        PendingIntent sentPI = PendingIntent.getBroadcast(mContext, 0, new Intent(
                SENT), 0);

        PendingIntent deliveredPI = PendingIntent.getBroadcast(mContext, 0,
                new Intent(DELIVERED), 0);

        // ---when the SMS has been sent---
        mContext.registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
                        smssentbool = true;
//					Toast.makeText(getBaseContext(), "SMS sent",
//							Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
//					Toast.makeText(getBaseContext(), "Generic failure",
//							Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NO_SERVICE:
//					Toast.makeText(getBaseContext(), "No service",
//							Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_NULL_PDU:
//					Toast.makeText(getBaseContext(), "Null PDU",
//							Toast.LENGTH_SHORT).show();
                        break;
                    case SmsManager.RESULT_ERROR_RADIO_OFF:
//					Toast.makeText(getBaseContext(), "Radio off",
//							Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }, new IntentFilter(SENT));

        // ---when the SMS has been delivered---
        mContext.registerReceiver(new BroadcastReceiver() {
            @Override
            public void onReceive(Context arg0, Intent arg1) {
                switch (getResultCode()) {
                    case Activity.RESULT_OK:
//					Toast.makeText(getBaseContext(), "SMS delivered",
//							Toast.LENGTH_SHORT).show();
                        break;
                    case Activity.RESULT_CANCELED:
//					Toast.makeText(getBaseContext(), "SMS not delivered",
//							Toast.LENGTH_SHORT).show();
                        break;
                }
            }
        }, new IntentFilter(DELIVERED));

        SmsManager sms = SmsManager.getDefault();
        sms.sendTextMessage(phoneNumber, null, message, sentPI, deliveredPI);
        Log.d("sms", "sent");
        return smssentbool;
    }


    private volatile ProgressDialog pDialog;

    public void showProgressDialog(Context ctx) {
        pDialog = new ProgressDialog(ctx);
        pDialog.setMessage("Please wait...");
        pDialog.setCancelable(false);
        pDialog.show();
    }

    public void hideProgressDialog(Context ctx) {
        if (pDialog.isShowing()) {
            pDialog.hide();
            pDialog.dismiss();
        }


    }


    /**
     * this will copy all ssa file from assets to SD card;
     *
     * @return
     */

    public void copyAllSSAFileToDevice(Context mContext) {

        AssetManager manager = mContext.getResources().getAssets();

        InputStream in = null;
        OutputStream out = null;

        try {

            String folderPath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/" + ParserConstant.DIRECTORY_PATH;

            File file = new File(folderPath);
            if (!file.exists()) {
                file.mkdirs();

                wireFiles(in, out, manager, folderPath);


            } else {

                wireFiles(in, out, manager, folderPath);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void wireFiles(InputStream in, OutputStream out, AssetManager manager, String folderPath) {

        try {

            for (int i = 0; i < ParserConstant.ALL_SSA_FILES_LIST.length; i++) {

                in = manager.open(ParserConstant.ALL_SSA_FILES_LIST[i]);

                String outPutFilePath = folderPath + ParserConstant.ALL_SSA_FILES_LIST[i];

                out = new FileOutputStream(outPutFilePath);
                copyFile(in, out);

            }
            in.close();
            out.flush();
            out.close();
            in = null;
            out = null;

        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while ((read = in.read(buffer)) != -1) {
            out.write(buffer, 0, read);
        }

    }


    public String generateId(Context mContext) {
        String gid = null;
        try {
            TelephonyManager tManager = (TelephonyManager) mContext.getSystemService(Context.TELEPHONY_SERVICE);
            String id = tManager.getDeviceId();
            String uid = id.substring(id.length() - 5);
            Calendar cal = Calendar.getInstance();
            gid = String.valueOf(uid/*+cal.get(Calendar.MILLISECOND)*/ + cal.getTimeInMillis());
            if (gid == null) {
                gid = String.valueOf(uid +/*cal.get(Calendar.MILLISECOND)+*/cal.getTimeInMillis());
            }
        } catch (Exception e) {
            // TODO: handle exception
        }

        return gid;
    }

    public String getStringFromObjectArrayList(ArrayList list, String requestCode) {

        String returnString = "";

        switch (requestCode) {

            case JsonParserConstant.PARSER_KEY_SYMPTOMS_DETAILS:

                for (int i = 0; i < list.size(); i++) {
//
//					Symptoms symptoms = (Symptoms) list.get(i);
//
//					returnString = returnString + symptoms.toString() + ", ";

                }
                break;

            case JsonParserConstant.PARSER_KEY_DIAGNOSTIC_DETAILS:

                for (int i = 0; i < list.size(); i++) {

//					Diagnostics diagnostics = (Diagnostics) list.get(i);
//
//					returnString = returnString + diagnostics.toString() + ", ";

                }

                break;

            case JsonParserConstant.PARSER_KEY_MEDICATON_DETAILS:


                for (int i = 0; i < list.size(); i++) {

//					Medication medication = (Medication) list.get(i);
//
//					returnString = returnString + medication.toString() + ", ";

                }

                break;
        }

        return returnString;

    }


    /**
     * show exit app dialog
     *
     * @param mContext context
     */
    public void exitAlert(final Context mContext) {

        try {
            AlertDialog.Builder exitAlert = new AlertDialog.Builder(mContext);

            exitAlert.setTitle("Are you sure you want to exit the application?"/*mContext.getResources().getString(R.string.are_u_sure_to_exit)*/);

            exitAlert.setPositiveButton("Yes",
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {

                            try {

                                Vector<Activity> activityVector = ActivityManagerForFinish.getInstance().getActivityVector();
                                if (activityVector != null) {
                                    for (int i = 0; i < activityVector.size(); i++) {
                                        activityVector.get(i).finish();
                                    }
                                }
                                ActivityManagerForFinish.instance = null;
                                android.os.Process.killProcess(android.os.Process.myPid());

                            } catch (Throwable e) {
                                e.printStackTrace();
                            }
                        }
                    });

            exitAlert.setNegativeButton("No",
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

            exitAlert.show();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * this method will display image help dialog
     *
     * @param mContext
     * @param engDrawableId
     * @param
     */
    public synchronized void helpDialog(final Context mContext, int engDrawableId) {

        try {
            Dialog dia = new Dialog(mContext);
            dia.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dia.setContentView(R.layout.helpdialogs);
            dia.setCancelable(true);

            LinearLayout layout = (LinearLayout) dia.findViewById(R.id.llhelp);


            layout.setBackgroundResource(engDrawableId);
            dia.show();

        } catch (Throwable e) {
            Log.e(TAG, "helpDialog, Exception :" + e);
            e.printStackTrace();
        }
    }

    /**
     * load patient picture from sd card
     *
     * @param mContext
     * @param picView
     * @param picName
     */
    public void loadPatientPic(Context mContext, ImageView picView, String picName) {
        try {
            if (null != picName && !picName.equalsIgnoreCase("") && !picName.equalsIgnoreCase("null")) {

                File imageFile = new File(Environment.getExternalStorageDirectory() + "/telemedhc/pictures/" + picName);

                Log.v(TAG, "imageFile abs path" + imageFile.getAbsolutePath());

                if (imageFile.exists()) {

                    Bitmap bitmap = BitmapFactory.decodeFile(imageFile.getAbsolutePath());
                    Log.v(TAG, "Bitmap: " + bitmap);
                    picView.setImageBitmap(bitmap);
                }
            }/*else{
				picView.setBackgroundResource(R.drawable.frame);
			}*/


        } catch (Exception e) {
            e.printStackTrace();
            Log.v(TAG, "loadPatientPic, Exception : " + e);
        }
    }

    public static String loadJSONFromAsset(Context context) {
        String json = null;
        try {
            InputStream is = context.getAssets().open("country_code.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    /**
     * validate if phone number is valid or not
     *
     * @param phoneNo
     * @return
     */
    public boolean validatePhone(String phoneNo) {
        boolean isValid = false;

        String PHONE_REGEX = "^[0-9]*$";
        isValid = phoneNo.matches(PHONE_REGEX);


        return isValid;
    }
}
