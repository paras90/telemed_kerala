
package com.phtt.telemedhc.utils;

/**
 * ValueConvertor class is responsible for manipulation on data.It takes the
 * value, processes it and returns the value in desired data type.
 * 
 * @author kewal
 * 
 */
public class ValueConvertor
{
	/**
	 * getLongValue
	 * 
	 * @param String
	 *            val
	 * @return long
	 */
	public static long getLongValue(String val)
	{
		long ret = -1;
		
		try
		{
			ret = Long.parseLong(val);
		}
		catch (Exception e)
		{
			ret = -1;
		}
		return ret;
	}
	
	/**
	 * getIntValue method
	 * 
	 * @param String
	 *            val
	 * @return int
	 */
	public static int getIntValue(String val)
	{
		int ret = -1;
		try
		{
			ret = Integer.parseInt(val);
		}
		catch (Exception e)
		{
			ret = -1;
		}
		return ret;
	}
	
	/**
	 * getBooleanValue method
	 * 
	 * @param String
	 *            val
	 * @return boolean
	 */
	public static boolean getBooleanValue(String val)
	{
		return val.equalsIgnoreCase("true") ? true : false;
	}
	
	/**
	 * getFloatValue method
	 * 
	 * @param String
	 *            val
	 * @return float
	 */
	public static float getFloatValue(String val)
	{
		float ret = -1;
		try
		{
			ret = Float.parseFloat(val);
		}
		catch (Exception e)
		{
			ret = -1;
		}
		return ret;
	}
	
	/**
	 * getDoubleValue method
	 * 
	 * @param String
	 *            val
	 * @return double
	 */
	public static double getDoubleValue(String val)
	{
		double ret = -1;
		try
		{
			ret = Double.parseDouble(val);
		}
		catch (Exception e)
		{
			ret = 0;
		}
		return ret;
	}
	
	public static String getStringValue(int val){
		String ret=null;
		try {
			ret = String.valueOf(val);
		} catch (Exception e) {
			ret = null;
			e.printStackTrace();
		}
		return ret;
	}
}
