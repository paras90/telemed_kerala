package com.phtt.telemedhc.datahandlers;


public class FollowupDetailHandler {

	private volatile String furtherTest;
	private volatile String followupVisit;
	private volatile String signToWatch;
	private volatile String comments;
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public String getSignToWatch() {
		return signToWatch;
	}
	public void setSignToWatch(String signToWatch) {
		this.signToWatch = signToWatch;
	}
	public String getFollowupVisit() {
		return followupVisit;
	}
	public void setFollowupVisit(String followupVisit) {
		this.followupVisit = followupVisit;
	}
	public String getFurtherTest() {
		return furtherTest;
	}
	public void setFurtherTest(String furtherTest) {
		this.furtherTest = furtherTest;
	}
	

	
}
