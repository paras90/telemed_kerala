/**
 * 
 */
package com.phtt.telemedhc.datahandlers;

/**
 * @author varun
 *
 */
public class MedSaltDatahandler {


	private String id;
	private String saltname;
	private String dosaage;
	private String precaution;
	private String subCategory;
	private String mainCategory;
	private String subCatid;
	
	

	/**
	 * @return the subCatid
	 */
	public String getSubCatid() {
		return subCatid;
	}

	/**
	 * @param subCatid the subCatid to set
	 */
	public void setSubCatid(String subCatid) {
		this.subCatid = subCatid;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {

		return saltname;
				
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	
	/**
	 * @return the subCategory
	 */
	public String getSubCategory() {
		return subCategory;
	}
	/**
	 * @param subCategory the subCategory to set
	 */
	public void setSubCategory(String subCategory) {
		this.subCategory = subCategory;
	}
	/**
	 * @return the mainCategory
	 */
	public String getCategory() {
		return mainCategory;
	}
	/**
	 * @param mainCategory the mainCategory to set
	 */
	public void setCategory(String category) {
		this.mainCategory = category;
	}
	
	/**
	 * @return the saltname
	 */
	public String getSaltname() {
		return saltname;
	}
	/**
	 * @param saltname the saltname to set
	 */
	public void setSaltname(String saltname) {
		this.saltname = saltname;
	}
	/**
	 * @return the dosaage
	 */
	public String getDosaage() {
		return dosaage;
	}
	/**
	 * @param dosaage the dosaage to set
	 */
	public void setDosaage(String dosaage) {
		this.dosaage = dosaage;
	}
	/**
	 * @return the precaution
	 */
	public String getPrecaution() {
		return precaution;
	}
	/**
	 * @param precaution the precaution to set
	 */
	public void setPrecaution(String precaution) {
		this.precaution = precaution;
	}
}
