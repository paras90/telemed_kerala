package com.phtt.telemedhc.datahandlers;

/**
 * class to handle ssa tag
 * @author ankit
 *
 */

public class SSAHandler {
	
	private static final String TAG = "SSAHandler";
	
	
	private volatile String tile="";
	private volatile String tileId="";
	private volatile String tileImage="";
	private volatile String tileTableName="";
	private volatile String questionType="";
	private volatile String questionValue="";
	private volatile String questionId="";
	private volatile String questionGoTo="";
	private volatile String questionImageHelp="";
	private volatile String questionAudioHelp="";
	private volatile String questionVideoHelp="";
	private volatile String questionHint="";
	private volatile String questionCompulsoryFiled="";
	private volatile String questionResult="";
	private volatile String questionCondition="";
	private volatile String questionFollowUp="";
	private volatile String questionReferTo="";
	private volatile String questionRefer="";
	private volatile String questionAncVisit="";
	private volatile String questionFollowTiming="";
	private volatile String questionCategory="";
	private volatile String optionsValue="";
	private volatile String optionsState="";
	private volatile String optionsCount="";
	private volatile String questionInputType="";
	private volatile String check="";
	
	
	public String getTile() {
		return tile;
	}
	public void setTile(String tile) {
		this.tile = tile;
	}
	public String getTileId() {
		return tileId;
	}
	public void setTileId(String tileId) {
		this.tileId = tileId;
	}
	public String getTileImage() {
		return tileImage;
	}
	public void setTileImage(String tileImage) {
		this.tileImage = tileImage;
	}
		/**
		 * get table name
		 */
	public String getTileTableName() {
		return tileTableName;
	}
	
	/**
	 * set table name
	 */
	
	public void setTileTableName(String tileTableName) {
		this.tileTableName = tileTableName;
	}
	
	/**
	 * get question type
	 */
	
	public String getQuestionType() {
		return questionType;
	}
	
	/**
	 * set question type
	 */
	
	public void setQuestionType(String questionType) {
		this.questionType = questionType;
	}
	

	/**
	 * get question value
	 */
	
	public String getQuestionValue() {
		return questionValue;
	}
	
	/**
	 * set question value
	 */
	
	public void setQuestionValue(String questionValue) {
		this.questionValue = questionValue;
	}
	
	/**
	 * get question id
	 */
	
	
	public String getQuestionId() {
		return questionId;
	}
	
	/**
	 * set question id
	 */
	
	public void setQuestionId(String questionId) {
		this.questionId = questionId;
	}
	public String getQuestionGoTo() {
		return questionGoTo;
	}
	public void setQuestionGoTo(String questionGoTo) {
		this.questionGoTo = questionGoTo;
	}

	/**
	 * get question Image Help
	 */
	
	public String getQuestionImageHelp() {
		return questionImageHelp;
	}
	

	/**
	 * set question Image Help
	 */
	
	public void setQuestionImageHelp(String questionImageHelp) {
		this.questionImageHelp = questionImageHelp;
	}
	
	/**
	 * get question Audio Help
	 */
	
	public String getQuestionAudioHelp() {
		return questionAudioHelp;
	}
	
	/**
	 * set question Audio Help
	 */
	
	public void setQuestionAudioHelp(String questionAudioHelp) {
		this.questionAudioHelp = questionAudioHelp;
	}

	/**
	 * get question video help
	 */
	
	public String getQuestionVideoHelp() {
		return questionVideoHelp;
	}
	
	/**
	 * set question video help
	 */
	
	
	public void setQuestionVideoHelp(String questionVideoHelp) {
		this.questionVideoHelp = questionVideoHelp;
	}
	
	
	/**
	 * get question hint
	 */
	
	
	public String getQuestionHint() {
		return questionHint;
	}
	/**
	 * set question hint
	 */
	
	public void setQuestionHint(String questionHint) {
		this.questionHint = questionHint;
	}
	
	/**
	 * get question compulsory filed
	 */
	
	public String getQuestionCompulsoryFiled() {
		return questionCompulsoryFiled;
	}
	
	/**
	 * set question compulsory filed
	 */
	
	public void setQuestionCompulsoryFiled(String questionCompulsoryFiled) {
		this.questionCompulsoryFiled = questionCompulsoryFiled;
	}
	public String getQuestionResult() {
		return questionResult;
	}
	public void setQuestionResult(String questionResult) {
		this.questionResult = questionResult;
	}
	public String getQuestionCondition() {
		return questionCondition;
	}
	public void setQuestionCondition(String questionCondition) {
		this.questionCondition = questionCondition;
	}
	public String getQuestionFollowUp() {
		return questionFollowUp;
	}
	public void setQuestionFollowUp(String questionFollowUp) {
		this.questionFollowUp = questionFollowUp;
	}
	public String getQuestionReferTo() {
		return questionReferTo;
	}
	public void setQuestionReferTo(String questionReferTo) {
		this.questionReferTo = questionReferTo;
	}
	public String getQuestionRefer() {
		return questionRefer;
	}
	public void setQuestionRefer(String questionRefer) {
		this.questionRefer = questionRefer;
	}
	public String getQuestionAncVisit() {
		return questionAncVisit;
	}
	public void setQuestionAncVisit(String questionAncVisit) {
		this.questionAncVisit = questionAncVisit;
	}
	public String getQuestionFollowTiming() {
		return questionFollowTiming;
	}
	public void setQuestionFollowTiming(String questionFollowTiming) {
		this.questionFollowTiming = questionFollowTiming;
	}
	public String getQuestionCategory() {
		return questionCategory;
	}
	public void setQuestionCategory(String questionCategory) {
		this.questionCategory = questionCategory;
	}
	public String getOptionsValue() {
		return optionsValue;
	}
	public void setOptionsValue(String optionsValue) {
		this.optionsValue = optionsValue;
	}
	public String getOptionsState() {
		return optionsState;
	}
	public void setOptionsState(String optionsState) {
		this.optionsState = optionsState;
	}
	public String getOptionsCount() {
		return optionsCount;
	}
	public void setOptionsCount(String optionsCount) {
		this.optionsCount = optionsCount;
	}
	

	/**
	 * get question input type 
	 */
	
	public String getQuestionInputType() {
		return questionInputType;
	}
	
	/**
	 * set question input type 
	 */
	
	public void setQuestionInputType(String questionInputType) {
		this.questionInputType = questionInputType;
	}
	public String getCheck() {
		return check;
	}
	public void setCheck(String check) {
		this.check = check;
	}
	public static String getTag() {
		return TAG;
	}

	
	
}
