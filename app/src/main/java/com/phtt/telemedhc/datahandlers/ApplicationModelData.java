package com.phtt.telemedhc.datahandlers;


import com.phtt.telemedhc.models.PatientInfoDataHandler;

import java.util.ArrayList;

/**
 * Created by varun on 6/11/2015.
 */
public class ApplicationModelData {


	private String doctorId="";
  
	

	ArrayList<PatientInfoDataHandler> patientList;
	
	/*ArrayList<MedicineDetailHandler> medicineList;*/
	
	ArrayList<MedSubCategoryHandler> medSubCatList;
	
	ArrayList<MedCategoryHandler> medCatList;
	
	ArrayList<MedSaltDatahandler> medSaltList;
	
	ArrayList<MedicineDataHandler> medDataList;
	
	
	public String getDoctorId() {
		return doctorId;
	}

	public void setDoctorId(String doctorId) {
		this.doctorId = doctorId;
	}
    
	private PatientInfoDataHandler mPatientInfoDataHandler;


	/**
	 * @return the medDataList
	 */
	public ArrayList<MedicineDataHandler> getMedDataList() {
		return medDataList;
	}

	/**
	 * @param medDataList the medDataList to set
	 */
	public void setMedDataList(ArrayList<MedicineDataHandler> medDataList) {
		this.medDataList = medDataList;
	}


	/**
	 * @return the medSubCatList
	 */
	public ArrayList<MedSubCategoryHandler> getMedSubCatList() {
		return medSubCatList;
	}

	/**
	 * @param medSubCatList the medSubCatList to set
	 */
	public void setMedSubCatList(ArrayList<MedSubCategoryHandler> medSubCatList) {
		this.medSubCatList = medSubCatList;
	}

	/**
	 * @return the medCatList
	 */
	public ArrayList<MedCategoryHandler> getMedCatList() {
		return medCatList;
	}

	/**
	 * @param medCatList the medCatList to set
	 */
	public void setMedCatList(ArrayList<MedCategoryHandler> medCatList) {
		this.medCatList = medCatList;
	}

	/**
	 * @return the medSaltList
	 */
	public ArrayList<MedSaltDatahandler> getMedSaltList() {
		return medSaltList;
	}

	/**
	 * @param medSaltList the medSaltList to set
	 */
	public void setMedSaltList(ArrayList<MedSaltDatahandler> medSaltList) {
		this.medSaltList = medSaltList;
	}

	
	
    private static class SingletonHolder{
        private static final ApplicationModelData mInstance = new ApplicationModelData();
    }

    public static ApplicationModelData getInstance(){
        return SingletonHolder.mInstance;
    }

    public ArrayList<PatientInfoDataHandler> getPatientList() {
        return patientList;
    }

    public void setPatientList(ArrayList<PatientInfoDataHandler> patientList) {
        this.patientList = patientList;
    }

	public PatientInfoDataHandler getmPatientInfoDataHandler() {
		return mPatientInfoDataHandler;
	}

	public void setmPatientInfoDataHandler(PatientInfoDataHandler mPatientInfoDataHandler) {
		this.mPatientInfoDataHandler = mPatientInfoDataHandler;
	}

	/**
	 * @return the medicineList
	 */
	/*public ArrayList<MedicineDetailHandler> getMedicineList() {
		return medicineList;
	}

	*//**
	 * @param medicineList the medicineList to set
	 *//*
	public void setMedicineList(ArrayList<MedicineDetailHandler> medicineList) {
		this.medicineList = medicineList;
	}*/
    

}
