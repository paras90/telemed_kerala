package com.phtt.telemedhc.datahandlers;

import java.util.ArrayList;


public class OrderSupplyHolder {

	ArrayList<OrderSupplyHandler> itemList;
	private OrderSupplyHandler mOrderSupplyHandler; 
	
	private static class SingletonHolder {
		private static final OrderSupplyHolder mInstance = new OrderSupplyHolder();
	}

	public static OrderSupplyHolder getInstance() {
		return SingletonHolder.mInstance;
	}

	public ArrayList<OrderSupplyHandler> getItemList() {
		return itemList;
	}

	public void setItemList(ArrayList<OrderSupplyHandler> itemList) {
		this.itemList = itemList;
	}

	public OrderSupplyHandler getmOrderSupplyHandler() {
		return mOrderSupplyHandler;
	}

	public void setmOrderSupplyHandler(OrderSupplyHandler mOrderSupplyHandler) {
		this.mOrderSupplyHandler = mOrderSupplyHandler;
	}
	
	
}
