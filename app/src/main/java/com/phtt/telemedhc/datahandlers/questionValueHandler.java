/**
 * 
 */
package com.phtt.telemedhc.datahandlers;

/**
 * @author varun
 *
 */
public class questionValueHandler {

	private String col1;
	private String col2;
	private String col3;
	private String col4;
	private String col5;
	private String col6;
	private String col7;
	private String col8;
	private String col9;
	private String col10;
	private String col11;
	private String col12;
	private String col13;
	private String col14;
	private String col15;
	private String col16;
	private String col17;
	private String col18;
	private String col19;
	private String col20;
	private String col21;
	private String col22;
	private String col23;
	private String col24;
	private String col25;
	private String col26;
	private String col27;
	private String col28;
	private String col29;
	private String col30;
	private String col31;
	private String col32;
	private String col33;
	private String col34;
	private String col35;
	private String col36;
	private String col37;
	private String col38;
	private String col39;
	private String col40;
	private String col41;
	private String col42;
	private String col43;
	private String col44;
	private String col45;
	private String col46;
	private String col47;
	private String col48;
	private String col49;
	private String col50;
	private String col51;
	private String col52;
	private String col53;
	private String col54;
	private String col55;
	private String col56;
	private String col57;
	private String col58;
	private String col59;
	private String col60;
	private String col61;
	private String col62;
	private String col63;
	private String col64;
	private String col65;
	private String col66;
	private String col67;
	private String col68;
	private String col69;
	private String col70;
	private String col71;
	private String col72;
	private String col73;
	private String col74;
	private String col75;

	private String patientId;
	private String doctorId;
	private String subUserId;
	private String clinicId;
	private String datetime;
	private String checkUpDate;
	private String checkUpTime;
	private String editFlag;
	private String uploadFlag;
	private String tokenId;
	
	
	/**
	 * @return the col1
	 */
	public String getCol1() {
		return col1;
	}
	/**
	 * @param col1 the col1 to set
	 */
	public void setCol1(String col1) {
		this.col1 = col1;
	}
	/**
	 * @return the col2
	 */
	public String getCol2() {
		return col2;
	}
	/**
	 * @param col2 the col2 to set
	 */
	public void setCol2(String col2) {
		this.col2 = col2;
	}
	/**
	 * @return the col3
	 */
	public String getCol3() {
		return col3;
	}
	/**
	 * @param col3 the col3 to set
	 */
	public void setCol3(String col3) {
		this.col3 = col3;
	}
	/**
	 * @return the col4
	 */
	public String getCol4() {
		return col4;
	}
	/**
	 * @param col4 the col4 to set
	 */
	public void setCol4(String col4) {
		this.col4 = col4;
	}
	/**
	 * @return the col5
	 */
	public String getCol5() {
		return col5;
	}
	/**
	 * @param col5 the col5 to set
	 */
	public void setCol5(String col5) {
		this.col5 = col5;
	}
	/**
	 * @return the col6
	 */
	public String getCol6() {
		return col6;
	}
	/**
	 * @param col6 the col6 to set
	 */
	public void setCol6(String col6) {
		this.col6 = col6;
	}
	/**
	 * @return the col7
	 */
	public String getCol7() {
		return col7;
	}
	/**
	 * @param col7 the col7 to set
	 */
	public void setCol7(String col7) {
		this.col7 = col7;
	}
	/**
	 * @return the col8
	 */
	public String getCol8() {
		return col8;
	}
	/**
	 * @param col8 the col8 to set
	 */
	public void setCol8(String col8) {
		this.col8 = col8;
	}
	/**
	 * @return the col9
	 */
	public String getCol9() {
		return col9;
	}
	/**
	 * @param col9 the col9 to set
	 */
	public void setCol9(String col9) {
		this.col9 = col9;
	}
	/**
	 * @return the col10
	 */
	public String getCol10() {
		return col10;
	}
	/**
	 * @param col10 the col10 to set
	 */
	public void setCol10(String col10) {
		this.col10 = col10;
	}
	/**
	 * @return the col11
	 */
	public String getCol11() {
		return col11;
	}
	/**
	 * @param col11 the col11 to set
	 */
	public void setCol11(String col11) {
		this.col11 = col11;
	}
	/**
	 * @return the col12
	 */
	public String getCol12() {
		return col12;
	}
	/**
	 * @param col12 the col12 to set
	 */
	public void setCol12(String col12) {
		this.col12 = col12;
	}
	/**
	 * @return the col13
	 */
	public String getCol13() {
		return col13;
	}
	/**
	 * @param col13 the col13 to set
	 */
	public void setCol13(String col13) {
		this.col13 = col13;
	}
	/**
	 * @return the col14
	 */
	public String getCol14() {
		return col14;
	}
	/**
	 * @param col14 the col14 to set
	 */
	public void setCol14(String col14) {
		this.col14 = col14;
	}
	/**
	 * @return the col15
	 */
	public String getCol15() {
		return col15;
	}
	/**
	 * @param col15 the col15 to set
	 */
	public void setCol15(String col15) {
		this.col15 = col15;
	}
	/**
	 * @return the col16
	 */
	public String getCol16() {
		return col16;
	}
	/**
	 * @param col16 the col16 to set
	 */
	public void setCol16(String col16) {
		this.col16 = col16;
	}
	/**
	 * @return the col17
	 */
	public String getCol17() {
		return col17;
	}
	/**
	 * @param col17 the col17 to set
	 */
	public void setCol17(String col17) {
		this.col17 = col17;
	}
	/**
	 * @return the col18
	 */
	public String getCol18() {
		return col18;
	}
	/**
	 * @param col18 the col18 to set
	 */
	public void setCol18(String col18) {
		this.col18 = col18;
	}
	/**
	 * @return the col19
	 */
	public String getCol19() {
		return col19;
	}
	/**
	 * @param col19 the col19 to set
	 */
	public void setCol19(String col19) {
		this.col19 = col19;
	}
	/**
	 * @return the col20
	 */
	public String getCol20() {
		return col20;
	}
	/**
	 * @param col20 the col20 to set
	 */
	public void setCol20(String col20) {
		this.col20 = col20;
	}
	/**
	 * @return the col21
	 */
	public String getCol21() {
		return col21;
	}
	/**
	 * @param col21 the col21 to set
	 */
	public void setCol21(String col21) {
		this.col21 = col21;
	}
	/**
	 * @return the col22
	 */
	public String getCol22() {
		return col22;
	}
	/**
	 * @param col22 the col22 to set
	 */
	public void setCol22(String col22) {
		this.col22 = col22;
	}
	/**
	 * @return the col23
	 */
	public String getCol23() {
		return col23;
	}
	/**
	 * @param col23 the col23 to set
	 */
	public void setCol23(String col23) {
		this.col23 = col23;
	}
	/**
	 * @return the col24
	 */
	public String getCol24() {
		return col24;
	}
	/**
	 * @param col24 the col24 to set
	 */
	public void setCol24(String col24) {
		this.col24 = col24;
	}
	/**
	 * @return the col25
	 */
	public String getCol25() {
		return col25;
	}
	/**
	 * @param col25 the col25 to set
	 */
	public void setCol25(String col25) {
		this.col25 = col25;
	}
	/**
	 * @return the col26
	 */
	public String getCol26() {
		return col26;
	}
	/**
	 * @param col26 the col26 to set
	 */
	public void setCol26(String col26) {
		this.col26 = col26;
	}
	/**
	 * @return the col27
	 */
	public String getCol27() {
		return col27;
	}
	/**
	 * @param col27 the col27 to set
	 */
	public void setCol27(String col27) {
		this.col27 = col27;
	}
	/**
	 * @return the col28
	 */
	public String getCol28() {
		return col28;
	}
	/**
	 * @param col28 the col28 to set
	 */
	public void setCol28(String col28) {
		this.col28 = col28;
	}
	/**
	 * @return the col29
	 */
	public String getCol29() {
		return col29;
	}
	/**
	 * @param col29 the col29 to set
	 */
	public void setCol29(String col29) {
		this.col29 = col29;
	}
	/**
	 * @return the col30
	 */
	public String getCol30() {
		return col30;
	}
	/**
	 * @param col30 the col30 to set
	 */
	public void setCol30(String col30) {
		this.col30 = col30;
	}
	/**
	 * @return the col31
	 */
	public String getCol31() {
		return col31;
	}
	/**
	 * @param col31 the col31 to set
	 */
	public void setCol31(String col31) {
		this.col31 = col31;
	}
	/**
	 * @return the col32
	 */
	public String getCol32() {
		return col32;
	}
	/**
	 * @param col32 the col32 to set
	 */
	public void setCol32(String col32) {
		this.col32 = col32;
	}
	/**
	 * @return the col33
	 */
	public String getCol33() {
		return col33;
	}
	/**
	 * @param col33 the col33 to set
	 */
	public void setCol33(String col33) {
		this.col33 = col33;
	}
	/**
	 * @return the col34
	 */
	public String getCol34() {
		return col34;
	}
	/**
	 * @param col34 the col34 to set
	 */
	public void setCol34(String col34) {
		this.col34 = col34;
	}
	/**
	 * @return the col35
	 */
	public String getCol35() {
		return col35;
	}
	/**
	 * @param col35 the col35 to set
	 */
	public void setCol35(String col35) {
		this.col35 = col35;
	}
	/**
	 * @return the col36
	 */
	public String getCol36() {
		return col36;
	}
	/**
	 * @param col36 the col36 to set
	 */
	public void setCol36(String col36) {
		this.col36 = col36;
	}
	/**
	 * @return the col37
	 */
	public String getCol37() {
		return col37;
	}
	/**
	 * @param col37 the col37 to set
	 */
	public void setCol37(String col37) {
		this.col37 = col37;
	}
	/**
	 * @return the col38
	 */
	public String getCol38() {
		return col38;
	}
	/**
	 * @param col38 the col38 to set
	 */
	public void setCol38(String col38) {
		this.col38 = col38;
	}
	/**
	 * @return the col39
	 */
	public String getCol39() {
		return col39;
	}
	/**
	 * @param col39 the col39 to set
	 */
	public void setCol39(String col39) {
		this.col39 = col39;
	}
	/**
	 * @return the col40
	 */
	public String getCol40() {
		return col40;
	}
	/**
	 * @param col40 the col40 to set
	 */
	public void setCol40(String col40) {
		this.col40 = col40;
	}
	/**
	 * @return the col41
	 */
	public String getCol41() {
		return col41;
	}
	/**
	 * @param col41 the col41 to set
	 */
	public void setCol41(String col41) {
		this.col41 = col41;
	}
	/**
	 * @return the col42
	 */
	public String getCol42() {
		return col42;
	}
	/**
	 * @param col42 the col42 to set
	 */
	public void setCol42(String col42) {
		this.col42 = col42;
	}
	/**
	 * @return the col43
	 */
	public String getCol43() {
		return col43;
	}
	/**
	 * @param col43 the col43 to set
	 */
	public void setCol43(String col43) {
		this.col43 = col43;
	}
	/**
	 * @return the col44
	 */
	public String getCol44() {
		return col44;
	}
	/**
	 * @param col44 the col44 to set
	 */
	public void setCol44(String col44) {
		this.col44 = col44;
	}
	/**
	 * @return the col45
	 */
	public String getCol45() {
		return col45;
	}
	/**
	 * @param col45 the col45 to set
	 */
	public void setCol45(String col45) {
		this.col45 = col45;
	}
	/**
	 * @return the col46
	 */
	public String getCol46() {
		return col46;
	}
	/**
	 * @param col46 the col46 to set
	 */
	public void setCol46(String col46) {
		this.col46 = col46;
	}
	/**
	 * @return the col47
	 */
	public String getCol47() {
		return col47;
	}
	/**
	 * @param col47 the col47 to set
	 */
	public void setCol47(String col47) {
		this.col47 = col47;
	}
	/**
	 * @return the col48
	 */
	public String getCol48() {
		return col48;
	}
	/**
	 * @param col48 the col48 to set
	 */
	public void setCol48(String col48) {
		this.col48 = col48;
	}
	/**
	 * @return the col49
	 */
	public String getCol49() {
		return col49;
	}
	/**
	 * @param col49 the col49 to set
	 */
	public void setCol49(String col49) {
		this.col49 = col49;
	}
	/**
	 * @return the col50
	 */
	public String getCol50() {
		return col50;
	}
	/**
	 * @param col50 the col50 to set
	 */
	public void setCol50(String col50) {
		this.col50 = col50;
	}
	/**
	 * @return the col51
	 */
	public String getCol51() {
		return col51;
	}
	/**
	 * @param col51 the col51 to set
	 */
	public void setCol51(String col51) {
		this.col51 = col51;
	}
	/**
	 * @return the col52
	 */
	public String getCol52() {
		return col52;
	}
	/**
	 * @param col52 the col52 to set
	 */
	public void setCol52(String col52) {
		this.col52 = col52;
	}
	/**
	 * @return the col53
	 */
	public String getCol53() {
		return col53;
	}
	/**
	 * @param col53 the col53 to set
	 */
	public void setCol53(String col53) {
		this.col53 = col53;
	}
	/**
	 * @return the col54
	 */
	public String getCol54() {
		return col54;
	}
	/**
	 * @param col54 the col54 to set
	 */
	public void setCol54(String col54) {
		this.col54 = col54;
	}
	/**
	 * @return the col55
	 */
	public String getCol55() {
		return col55;
	}
	/**
	 * @param col55 the col55 to set
	 */
	public void setCol55(String col55) {
		this.col55 = col55;
	}
	/**
	 * @return the col56
	 */
	public String getCol56() {
		return col56;
	}
	/**
	 * @param col56 the col56 to set
	 */
	public void setCol56(String col56) {
		this.col56 = col56;
	}
	/**
	 * @return the col57
	 */
	public String getCol57() {
		return col57;
	}
	/**
	 * @param col57 the col57 to set
	 */
	public void setCol57(String col57) {
		this.col57 = col57;
	}
	/**
	 * @return the col58
	 */
	public String getCol58() {
		return col58;
	}
	/**
	 * @param col58 the col58 to set
	 */
	public void setCol58(String col58) {
		this.col58 = col58;
	}
	/**
	 * @return the col59
	 */
	public String getCol59() {
		return col59;
	}
	/**
	 * @param col59 the col59 to set
	 */
	public void setCol59(String col59) {
		this.col59 = col59;
	}
	/**
	 * @return the col60
	 */
	public String getCol60() {
		return col60;
	}
	/**
	 * @param col60 the col60 to set
	 */
	public void setCol60(String col60) {
		this.col60 = col60;
	}
	/**
	 * @return the col61
	 */
	public String getCol61() {
		return col61;
	}
	/**
	 * @param col61 the col61 to set
	 */
	public void setCol61(String col61) {
		this.col61 = col61;
	}
	/**
	 * @return the col62
	 */
	public String getCol62() {
		return col62;
	}
	/**
	 * @param col62 the col62 to set
	 */
	public void setCol62(String col62) {
		this.col62 = col62;
	}
	/**
	 * @return the col63
	 */
	public String getCol63() {
		return col63;
	}
	/**
	 * @param col63 the col63 to set
	 */
	public void setCol63(String col63) {
		this.col63 = col63;
	}
	/**
	 * @return the col64
	 */
	public String getCol64() {
		return col64;
	}
	/**
	 * @param col64 the col64 to set
	 */
	public void setCol64(String col64) {
		this.col64 = col64;
	}
	/**
	 * @return the col65
	 */
	public String getCol65() {
		return col65;
	}
	/**
	 * @param col65 the col65 to set
	 */
	public void setCol65(String col65) {
		this.col65 = col65;
	}
	/**
	 * @return the col66
	 */
	public String getCol66() {
		return col66;
	}
	/**
	 * @param col66 the col66 to set
	 */
	public void setCol66(String col66) {
		this.col66 = col66;
	}
	/**
	 * @return the col67
	 */
	public String getCol67() {
		return col67;
	}
	/**
	 * @param col67 the col67 to set
	 */
	public void setCol67(String col67) {
		this.col67 = col67;
	}
	/**
	 * @return the col68
	 */
	public String getCol68() {
		return col68;
	}
	/**
	 * @param col68 the col68 to set
	 */
	public void setCol68(String col68) {
		this.col68 = col68;
	}
	/**
	 * @return the col69
	 */
	public String getCol69() {
		return col69;
	}
	/**
	 * @param col69 the col69 to set
	 */
	public void setCol69(String col69) {
		this.col69 = col69;
	}
	/**
	 * @return the col70
	 */
	public String getCol70() {
		return col70;
	}
	/**
	 * @param col70 the col70 to set
	 */
	public void setCol70(String col70) {
		this.col70 = col70;
	}
	/**
	 * @return the col71
	 */
	public String getCol71() {
		return col71;
	}
	/**
	 * @param col71 the col71 to set
	 */
	public void setCol71(String col71) {
		this.col71 = col71;
	}
	/**
	 * @return the col72
	 */
	public String getCol72() {
		return col72;
	}
	/**
	 * @param col72 the col72 to set
	 */
	public void setCol72(String col72) {
		this.col72 = col72;
	}
	/**
	 * @return the col73
	 */
	public String getCol73() {
		return col73;
	}
	/**
	 * @param col73 the col73 to set
	 */
	public void setCol73(String col73) {
		this.col73 = col73;
	}
	/**
	 * @return the col74
	 */
	public String getCol74() {
		return col74;
	}
	/**
	 * @param col74 the col74 to set
	 */
	public void setCol74(String col74) {
		this.col74 = col74;
	}
	/**
	 * @return the col75
	 */
	public String getCol75() {
		return col75;
	}
	/**
	 * @param col75 the col75 to set
	 */
	public void setCol75(String col75) {
		this.col75 = col75;
	}
	/**
	 * @return the patientId
	 */
	public String getPatientId() {
		return patientId;
	}
	/**
	 * @param patientId the patientId to set
	 */
	public void setPatientId(String patientId) {
		this.patientId = patientId;
	}
	/**
	 * @return the doctorId
	 */
	public String getDoctorId() {
		return doctorId;
	}
	/**
	 * @param doctorId the doctorId to set
	 */
	public void setDoctorId(String doctorId) {
		this.doctorId = doctorId;
	}
	/**
	 * @return the subUserId
	 */
	public String getSubUserId() {
		return subUserId;
	}
	/**
	 * @param subUserId the subUserId to set
	 */
	public void setSubUserId(String subUserId) {
		this.subUserId = subUserId;
	}
	/**
	 * @return the clinicId
	 */
	public String getClinicId() {
		return clinicId;
	}
	/**
	 * @param clinicId the clinicId to set
	 */
	public void setClinicId(String clinicId) {
		this.clinicId = clinicId;
	}
	/**
	 * @return the datetime
	 */
	public String getDatetime() {
		return datetime;
	}
	/**
	 * @param datetime the datetime to set
	 */
	public void setDatetime(String datetime) {
		this.datetime = datetime;
	}
	/**
	 * @return the checkUpDate
	 */
	public String getCheckUpDate() {
		return checkUpDate;
	}
	/**
	 * @param checkUpDate the checkUpDate to set
	 */
	public void setCheckUpDate(String checkUpDate) {
		this.checkUpDate = checkUpDate;
	}
	/**
	 * @return the checkUpTime
	 */
	public String getCheckUpTime() {
		return checkUpTime;
	}
	/**
	 * @param checkUpTime the checkUpTime to set
	 */
	public void setCheckUpTime(String checkUpTime) {
		this.checkUpTime = checkUpTime;
	}
	/**
	 * @return the editFlag
	 */
	public String getEditFlag() {
		return editFlag;
	}
	/**
	 * @param editFlag the editFlag to set
	 */
	public void setEditFlag(String editFlag) {
		this.editFlag = editFlag;
	}
	/**
	 * @return the uploadFlag
	 */
	public String getUploadFlag() {
		return uploadFlag;
	}
	/**
	 * @param uploadFlag the uploadFlag to set
	 */
	public void setUploadFlag(String uploadFlag) {
		this.uploadFlag = uploadFlag;
	}
	/**
	 * @return the tokenId
	 */
	public String getTokenId() {
		return tokenId;
	}
	/**
	 * @param tokenId the tokenId to set
	 */
	public void setTokenId(String tokenId) {
		this.tokenId = tokenId;
	}

}
