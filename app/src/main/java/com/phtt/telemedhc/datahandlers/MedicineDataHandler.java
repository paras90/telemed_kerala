/**
 * 
 */
package com.phtt.telemedhc.datahandlers;

/**
 * @author varun
 *
 */
public class MedicineDataHandler {


	/**
	 * @return the companyName
	 */
	public String getCompanyName() {
		return CompanyName;
	}

	/**
	 * @param companyName the companyName to set
	 */
	public void setCompanyName(String companyName) {
		CompanyName = companyName;
	}

	private String id;
	private String medName;
	private String saltId;
	private String mainCategory;
	private String subCategory;
	private String Dosage;
	private String precaution;
	private String saltName;
	private String subCatId;
	private String catId;
	private String med_type;
	private String strength;
	private String MRP;
	private String packing_qty;
	private String CompanyName;
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		
		return medName+" "+med_type;
	}
	
	/**
	 * @return the mainCategory
	 */
	public String getMainCategory() {
		return mainCategory;
	}
	/**
	 * @param mainCategory the mainCategory to set
	 */
	public void setMainCategory(String mainCategory) {
		this.mainCategory = mainCategory;
	}
	/**
	 * @return the subCategory
	 */
	public String getSubCategory() {
		return subCategory;
	}
	/**
	 * @param subCategory the subCategory to set
	 */
	public void setSubCategory(String subCategory) {
		this.subCategory = subCategory;
	}
	/**
	 * @return the dosage
	 */
	public String getDosage() {
		return Dosage;
	}
	/**
	 * @param dosage the dosage to set
	 */
	public void setDosage(String dosage) {
		Dosage = dosage;
	}
	/**
	 * @return the precaution
	 */
	public String getPrecaution() {
		return precaution;
	}
	/**
	 * @param precaution the precaution to set
	 */
	public void setPrecaution(String precaution) {
		this.precaution = precaution;
	}
	/**
	 * @return the saltName
	 */
	public String getSaltName() {
		return saltName;
	}
	/**
	 * @param saltName the saltName to set
	 */
	public void setSaltName(String saltName) {
		this.saltName = saltName;
	}
	/**
	 * @return the subCatId
	 */
	public String getSubCatId() {
		return subCatId;
	}
	/**
	 * @param subCatId the subCatId to set
	 */
	public void setSubCatId(String subCatId) {
		this.subCatId = subCatId;
	}
	/**
	 * @return the catId
	 */
	public String getCatId() {
		return catId;
	}
	/**
	 * @param catId the catId to set
	 */
	public void setCatId(String catId) {
		this.catId = catId;
	}
	
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the medName
	 */
	public String getMedName() {
		return medName;
	}
	/**
	 * @param medName the medName to set
	 */
	public void setMedName(String medName) {
		this.medName = medName;
	}
	/**
	 * @return the saltId
	 */
	public String getSaltId() {
		return saltId;
	}
	/**
	 * @param saltId the saltId to set
	 */
	public void setSaltId(String saltId) {
		this.saltId = saltId;
	}

	public String getMed_type() {
		return med_type;
	}

	public void setMed_type(String med_type) {
		this.med_type = med_type;
	}

	public String getStrength() {
		return strength;
	}

	public void setStrength(String strength) {
		this.strength = strength;
	}

	public String getMRP() {
		return MRP;
	}

	public void setMRP(String mRP) {
		MRP = mRP;
	}

	public String getPacking_qty() {
		return packing_qty;
	}

	public void setPacking_qty(String packing_qty) {
		this.packing_qty = packing_qty;
	}
}
