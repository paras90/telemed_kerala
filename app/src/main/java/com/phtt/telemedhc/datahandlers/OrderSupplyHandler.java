package com.phtt.telemedhc.datahandlers;

import java.io.Serializable;

public class OrderSupplyHandler implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String itemId="";

	private String itemName="";

	private String itemPrice="";

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getItemPrice() {
		return itemPrice;
	}

	public void setItemPrice(String itemPrice) {
		this.itemPrice = itemPrice;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	

}
