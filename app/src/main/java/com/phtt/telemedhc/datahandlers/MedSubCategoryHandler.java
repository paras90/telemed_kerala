/**
 * 
 */
package com.phtt.telemedhc.datahandlers;

/**
 * @author varun
 *
 */
public class MedSubCategoryHandler {



	private String id;
	private String subCatName;
	private String mainCatId;
	private String mainCatName;
	
	

	/**
	 * @return the mainCatId
	 */
	public String getMainCatId() {
		return mainCatId;
	}
	/**
	 * @param mainCatId the mainCatId to set
	 */
	public void setMainCatId(String mainCatId) {
		this.mainCatId = mainCatId;
	}
	
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the subCatName
	 */
	public String getSubCatName() {
		return subCatName;
	}
	/**
	 * @param subCatName the subCatName to set
	 */
	public void setSubCatName(String subCatName) {
		this.subCatName = subCatName;
	}
	/**
	 * @return the mainCatName
	 */
	public String getMainCatName() {
		return mainCatName;
	}
	/**
	 * @param mainCatName the mainCatName to set
	 */
	public void setMainCatName(String mainCatName) {
		this.mainCatName = mainCatName;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return subCatName;
	}
}
