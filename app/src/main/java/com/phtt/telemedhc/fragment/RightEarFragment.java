package com.phtt.telemedhc.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Gallery;

import com.phtt.telemedhc.R;
import com.phtt.telemedhc.adapter.GalleryImageAdapter;

import java.util.ArrayList;

/**
 * Created by Arvind on 14-01-2016.
 */
public class RightEarFragment extends BaseFragment {
    View view;
    ArrayList<String> right_ear_image;
    Gallery gallery;
    GalleryImageAdapter right_image_adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_right_ear, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initViews(view);
        initData();
        setListeners();
    }

    @Override
    public void initViews(View rootView) {
        gallery= (Gallery) rootView.findViewById(R.id.gallery);

    }

    @Override
    public void initData() {
        Bundle lbundle=getArguments();
        if(lbundle!=null){
            right_ear_image=lbundle.getStringArrayList("right_ear_list");
            right_image_adapter=new GalleryImageAdapter(getActivity(),right_ear_image);
            gallery.setSpacing(5);
            gallery.setAdapter(right_image_adapter);
            right_image_adapter.notifyDataSetChanged();
        }

    }

    @Override
    public void setListeners() {

    }
}
