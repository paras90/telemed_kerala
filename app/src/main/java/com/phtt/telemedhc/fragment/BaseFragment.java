package com.phtt.telemedhc.fragment;

import android.support.v4.app.Fragment;
import android.view.View;

/**
 * Created by Arvind on 16-09-2015.
 */
public abstract class BaseFragment extends Fragment {

    public abstract void initViews(View rootView);
    public abstract void initData();
    public abstract void setListeners();

    public boolean onBackPressed(){
        return false;
    }
}
