package com.phtt.telemedhc.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Gallery;

import com.phtt.telemedhc.R;
import com.phtt.telemedhc.adapter.GalleryImageAdapter;

import java.util.ArrayList;

/**
 * Created by Arvind on 14-01-2016.
 */
public class LeftNoseFragment extends BaseFragment {
    View view;
    ArrayList<String> left_nose_image;
    Gallery gallery;
    GalleryImageAdapter left_nose_adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_left_nose, container, false);
        return view;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initViews(view);
        initData();
        setListeners();
    }

    @Override
    public void initViews(View rootView) {
        gallery= (Gallery) rootView.findViewById(R.id.gallery);

    }

    @Override
    public void initData() {
        Bundle lbundle=getArguments();
        if(lbundle!=null){
            left_nose_image=lbundle.getStringArrayList("left_nose_list");
            left_nose_adapter=new GalleryImageAdapter(getActivity(),left_nose_image);
            gallery.setSpacing(5);
            gallery.setAdapter(left_nose_adapter);
        }

    }

    @Override
    public void setListeners() {

    }
}
