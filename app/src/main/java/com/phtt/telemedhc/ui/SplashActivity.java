package com.phtt.telemedhc.ui;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.widget.TextView;

import com.phtt.telemedhc.R;
import com.phtt.telemedhc.database.DbConstant;
import com.phtt.telemedhc.models.ApplicationDataModel;
import com.phtt.telemedhc.parser.ParserActivity;
import com.phtt.telemedhc.parser.ParserRequiredData;
import com.phtt.telemedhc.parser.UriHandler;
import com.phtt.telemedhc.service.CopyService;
import com.phtt.telemedhc.utils.Constants;

public class SplashActivity extends Activity {

    private static long SPLASH_TIME = 4000;

    //    SharedPreferences.Editor editor;
//    VideoView video_view;
//    private Handler handler = new Handler();
    Context mContext;


    SharedPreferences mPref;
//    SharedPreferences.Editor editor;
//    VideoView video_view;
//    private Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Remove title bar

        setContentView(R.layout.activity_splash);

        mContext = this;

      /*  Utils.getInstance().copyAllSSAFileToDevice(mContext);*/
        PackageInfo pInfo = null;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        TextView version = (TextView) findViewById(R.id.version);
        version.setText(pInfo.versionName);
        Intent service = new Intent(mContext, CopyService.class);
        startService(service);


//        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_FORCE_NOT_FULLSCREEN);
//        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    Thread.sleep(3000);

                    mPref = getApplicationContext().getSharedPreferences(
                            Constants.MyPREFERENCES, Context.MODE_WORLD_WRITEABLE);
                    if (!mPref.contains(Constants.name) && !mPref.contains(Constants.passw)) {
                        Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                        startActivity(intent);
                        finish();
                    } else {
//                        openSsa();
                        ApplicationDataModel.getInstance().setHwid(mPref.getString(Constants.name, "tel1"));
                        Intent intent = new Intent(SplashActivity.this, BeneficiarySelection.class);
                        startActivity(intent);
                        finish();

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        }).start();

    }

    public void openSsa() {
        Intent intent;
        ParserRequiredData requiredData;
        requiredData = new ParserRequiredData();
        requiredData.setSsaName(Constants.SSA_NAME_REGISTRATION);
        requiredData.setHeaderTitle(getResources().getString(
                R.string.PARSER_TITLE_REGISTRATION));
        requiredData.setTable("");
        UriHandler.getInstance().setContentUri(DbConstant.URI_TABLE_REGISTRATION);

        intent = new Intent(getApplicationContext(), ParserActivity.class);
        intent.putExtra(Constants.PARSER_DATA, requiredData);
        startActivity(intent);
        finish();

    }
}