package com.phtt.telemedhc.ui;

import android.app.Activity;

import java.util.Vector;

public class ActivityManagerForFinish {
	
	public static ActivityManagerForFinish instance;
	Vector<Activity> activityVector;
	
	
	public Vector<Activity> getActivityVector() {
		return activityVector;
	}

	public void setActivityVector(Vector<Activity> activityVector) {
		this.activityVector = activityVector;
	}

	public static  ActivityManagerForFinish getInstance()
	{
		if(instance== null)
			instance = new ActivityManagerForFinish();
		return instance;
	}
	
	public ActivityManagerForFinish() {
		activityVector =new Vector<Activity>();
	}
	
	
	
	
	public void addActivity(Activity activity)
	{
		activityVector.add(activity);
		
	}
	
	public void removeActivity(Activity activity)
	{
		if(activityVector.contains(activity))
			activityVector.remove(activity);
	}
	
	
	
}
