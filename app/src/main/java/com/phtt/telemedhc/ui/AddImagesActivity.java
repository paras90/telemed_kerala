package com.phtt.telemedhc.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Gallery;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.phtt.telemedhc.R;
import com.phtt.telemedhc.adapter.GalleryImageAdapter;
import com.phtt.telemedhc.database.DatabaseManager;
import com.phtt.telemedhc.database.DbConstant;
import com.phtt.telemedhc.fragment.LeftEarFragment;
import com.phtt.telemedhc.fragment.LeftNoseFragment;
import com.phtt.telemedhc.fragment.RightEarFragment;
import com.phtt.telemedhc.fragment.RightNoseFragment;
import com.phtt.telemedhc.models.ApplicationDataModel;
import com.phtt.telemedhc.models.PatientInfoDataHandler;
import com.phtt.telemedhc.network.NetworkConstants;
import com.phtt.telemedhc.network.NetworkRequestManager;
import com.phtt.telemedhc.network.ServiceHandler;
import com.phtt.telemedhc.sliding.SlidingTabLayout;
import com.phtt.telemedhc.utils.Constants;
import com.phtt.telemedhc.utils.INetResult;
import com.phtt.telemedhc.utils.NotificationsDisplay;
import com.phtt.telemedhc.utils.Utils;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Calendar;

public class AddImagesActivity extends AppCompatActivity implements INetResult {

    String userPicName = null;
    String picName = null;
    SlidingTabLayout eartabLayout, nosetabLayout;
    ViewPager ear_pager, nose_pager;
    private Context mContext;
    Button ear_camera, nose_camera, mouth_camera, other_camera;
    Gallery mouth_galley, other_gallery;
    ImageButton back_btn, help_btn, exit_btn, upload;
    ImageView patientPicView;
    TextView name, id;
    RelativeLayout next;
    PatientInfoDataHandler patientInfo;
    ArrayList<String> mouth_images, other_images;
    ArrayList<String> left_ear_images, right_ear_images, left_nose_images, right_nose_images;
    GalleryImageAdapter mouth_adapter, other_adapter;
    EarAdapter ear_pager_adapter;
    NoseAdapter nose_pager_adapter;
    String side = "";
    NotificationsDisplay notify;
    private INetResult listener;
    private Uri fileUri = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_images);
        ActivityManagerForFinish.getInstance().addActivity(this);

        mContext = this;
        listener = this;
        notify = new NotificationsDisplay(mContext);

        initViews();
        initData();
        setData();
        setListeners();
    }

    private void initViews() {
        eartabLayout = (SlidingTabLayout) findViewById(R.id.ear_tabs);
        ear_pager = (ViewPager) findViewById(R.id.ear_pager);

        nosetabLayout = (SlidingTabLayout) findViewById(R.id.nose_tabs);
        nose_pager = (ViewPager) findViewById(R.id.nose_pager);

        back_btn = (ImageButton) findViewById(R.id.back);
        help_btn = (ImageButton) findViewById(R.id.help);
        exit_btn = (ImageButton) findViewById(R.id.exit);
        upload = (ImageButton) findViewById(R.id.upload);
        upload.setVisibility(View.VISIBLE);
        help_btn.setVisibility(View.GONE);
        patientPicView = (ImageView) findViewById(R.id.userPic);
        name = (TextView) findViewById(R.id.name_ben);
        id = (TextView) findViewById(R.id.id_ben);
        ear_camera = (Button) findViewById(R.id.ear_camera);
        nose_camera = (Button) findViewById(R.id.nose_camera);
        mouth_camera = (Button) findViewById(R.id.mouth_camera);
        other_camera = (Button) findViewById(R.id.other_camera);
        mouth_galley = (Gallery) findViewById(R.id.mouth_gallery);
        other_gallery = (Gallery) findViewById(R.id.other_gallery);
        next = (RelativeLayout) findViewById(R.id.next);


    }


    private void initData() {
        Intent intent = getIntent();
        if (intent != null)
            this.patientInfo = (PatientInfoDataHandler) intent.getSerializableExtra(Constants.PATIENT_DATA_INTENT);

        // load patient picture
        Utils.getInstance().loadPatientPic(mContext, patientPicView, ApplicationDataModel.getInstance().getPatientPicName());

        ear_pager_adapter = new EarAdapter(getSupportFragmentManager());
        eartabLayout.setDistributeEvenly(true);
        ear_pager.setAdapter(ear_pager_adapter);
        eartabLayout.setViewPager(ear_pager);

        nose_pager_adapter = new NoseAdapter(getSupportFragmentManager());
        nosetabLayout.setDistributeEvenly(true);
        nose_pager.setAdapter(nose_pager_adapter);
        nosetabLayout.setViewPager(nose_pager);

    }

    private void setData() {
        try {
            name.setText(ApplicationDataModel.getInstance().getPatientName());
            String idtest = "";
            if (ApplicationDataModel.getInstance().getPatientGid() != null && !ApplicationDataModel.getInstance().getPatientGid().isEmpty()) {
                idtest = ApplicationDataModel.getInstance().getPatientGid();
            } else {
                idtest = ApplicationDataModel.getInstance().getPatientLid();
            }
            id.setText(idtest);
        } catch (NullPointerException e) {
            e.printStackTrace();
            Toast.makeText(this, R.string.no_data, Toast.LENGTH_SHORT).show();
        }
    }

    private void setListeners() {
        ear_camera.setOnClickListener(m_click);
        nose_camera.setOnClickListener(m_click);
        mouth_camera.setOnClickListener(m_click);
        other_camera.setOnClickListener(m_click);
        back_btn.setOnClickListener(m_click);
        help_btn.setOnClickListener(m_click);
        exit_btn.setOnClickListener(m_click);
        upload.setOnClickListener(m_click);
        next.setOnClickListener(m_click);
    }

    View.OnClickListener m_click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v == ear_camera) {
                selectEarSideDialog();
            } else if (v == nose_camera) {
                selectNoseSideDialog();
            } else if (v == mouth_camera) {
                Intent mouth_cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                fileUri = getOutputMediaFileUri();
                mouth_cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
                startActivityForResult(mouth_cameraIntent, Constants.CAMERA_MOUTH_PIC_REQUEST_SIDE);
            } else if (v == other_camera) {
                Intent other_cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                fileUri = getOutputMediaFileUri();
                other_cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
                startActivityForResult(other_cameraIntent, Constants.CAMERA_OTHER_PIC_REQUEST_SIDE);
            } else if (v == back_btn) {
                finish();
            } else if (v == help_btn) {
                try {
                    int engDrawableId = R.drawable.home_help;
                    Utils.getInstance().helpDialog(mContext, engDrawableId);
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            } else if (v == exit_btn) {
                Utils.getInstance().exitAlert(mContext);
            } else if (v == next) {
                // call the web service to upload and go to back screen
                AddImagesActivity.this.finish();
            } else if (v == upload) {
                if (Utils.getInstance().isInternetAvailable(mContext)) {
                    NetworkRequestManager nrm = new NetworkRequestManager(mContext, listener);
                    nrm.generateServiceRequestParams(NetworkConstants.REQUEST_ID_ALLIMAGE_UPLOAD, null);
                } else {
                    notify.customToast(mContext, getResources().getString(R.string.info), getResources().getString(R.string.check_internet));
                }
            }

        }
    };

    private void selectEarSideDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View view = getLayoutInflater().inflate(
                R.layout.dialog_select_side, null);

        final AlertDialog mDialog = builder.create();
        mDialog.setView(view, 0, 0, 0, 0);
        mDialog.show();

        TextView btn_ok = (TextView) view.findViewById(R.id.ok);
        Spinner side_spinner = (Spinner) view.findViewById(R.id.spinnerside);
        ArrayAdapter<String> adapter_state = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.side));
        adapter_state.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        side_spinner.setAdapter(adapter_state);
        side_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                side = parent.getItemAtPosition(position).toString();
                Log.d("selected_side", side);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mDialog.cancel();

                Intent ear_cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                fileUri = getOutputMediaFileUri();
                ear_cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
                startActivityForResult(ear_cameraIntent, Constants.CAMERA_EAR_PIC_REQUEST_SIDE);
            }
        });

    }

    private void selectNoseSideDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        View view = getLayoutInflater().inflate(
                R.layout.dialog_select_side, null);

        final AlertDialog mDialog = builder.create();
        mDialog.setView(view, 0, 0, 0, 0);
        mDialog.show();

        TextView btn_ok = (TextView) view.findViewById(R.id.ok);
        Spinner side_spinner = (Spinner) view.findViewById(R.id.spinnerside);
        ArrayAdapter<String> adapter_state = new ArrayAdapter<String>(this,
                android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.side));
        adapter_state.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        side_spinner.setAdapter(adapter_state);
        side_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                side = parent.getItemAtPosition(position).toString();
                Log.d("selected_side", side);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                mDialog.cancel();
                Intent nose_cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                fileUri = getOutputMediaFileUri();
                nose_cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);
                startActivityForResult(nose_cameraIntent, Constants.CAMERA_NOSE_PIC_REQUEST_SIDE);
            }
        });

    }

    @Override
    protected void onResume() {

        left_ear_images = DatabaseManager.getInstance().getAllLImagesPath(mContext, DbConstant.URI_IMAGES, "Ear", "Left", ApplicationDataModel.getInstance().getPatientLid());
        right_ear_images = DatabaseManager.getInstance().getAllLImagesPath(mContext, DbConstant.URI_IMAGES, "Ear", "Right", ApplicationDataModel.getInstance().getPatientLid());

        left_nose_images = DatabaseManager.getInstance().getAllLImagesPath(mContext, DbConstant.URI_IMAGES, "Nose", "Left", ApplicationDataModel.getInstance().getPatientLid());
        right_nose_images = DatabaseManager.getInstance().getAllLImagesPath(mContext, DbConstant.URI_IMAGES, "Nose", "Right", ApplicationDataModel.getInstance().getPatientLid());

        ear_pager_adapter.notifyDataSetChanged();
        nose_pager_adapter.notifyDataSetChanged();

        mouth_images = DatabaseManager.getInstance().getAllLImagesPath(mContext, DbConstant.URI_IMAGES, "Mouth", null, ApplicationDataModel.getInstance().getPatientLid());
        if (mouth_images != null && mouth_images.size() > 0) {
            mouth_adapter = new GalleryImageAdapter(mContext, mouth_images);
            mouth_galley.setSpacing(5);
            mouth_galley.setAdapter(mouth_adapter);
        }

        other_images = DatabaseManager.getInstance().getAllLImagesPath(mContext, DbConstant.URI_IMAGES, "Others", null, ApplicationDataModel.getInstance().getPatientLid());
        if (other_images != null && other_images.size() > 0) {
            other_adapter = new GalleryImageAdapter(mContext, other_images);
            other_gallery.setSpacing(5);
            other_gallery.setAdapter(other_adapter);
            registerForContextMenu(other_gallery);
        }
        super.onResume();
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        menu.setHeaderTitle("Select The Action");
        menu.add(0, v.getId(), 0, "Delete");
        menu.add(0, v.getId(), 0, "Cancel");
    }


    @Override
    public boolean onContextItemSelected(MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        try {
            if (item.getTitle() == "Delete") {
                if (other_images != null && other_images.size() > 0) {
                    String user_pic = other_images.get(info.position);
                    DatabaseManager.getInstance().deleteOthersImages(mContext, DbConstant.URI_IMAGES, "Others", null, user_pic, ApplicationDataModel.getInstance().getPatientLid());
                    other_adapter.notifyDataSetChanged();
                }

            } else if (item.getTitle() == "Cancel") {

            } else {
                return false;
            }
            return true;
        } catch (Exception e) {
            return true;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == Constants.CAMERA_EAR_PIC_REQUEST_SIDE || requestCode == Constants.CAMERA_NOSE_PIC_REQUEST_SIDE
                || requestCode == Constants.CAMERA_MOUTH_PIC_REQUEST_SIDE || requestCode == Constants.CAMERA_OTHER_PIC_REQUEST_SIDE) {
            if (resultCode == Activity.RESULT_OK) {
                // Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
                //            BitmapDrawable bitmap = new BitmapDrawable(thumbnail);

                String type = "";
                if (requestCode == Constants.CAMERA_EAR_PIC_REQUEST_SIDE)
                    type = "Ear";
                if (requestCode == Constants.CAMERA_NOSE_PIC_REQUEST_SIDE)
                    type = "Nose";
                if (requestCode == Constants.CAMERA_MOUTH_PIC_REQUEST_SIDE)
                    type = "Mouth";
                if (requestCode == Constants.CAMERA_OTHER_PIC_REQUEST_SIDE)
                    type = "Others";

                DatabaseManager.getInstance().insertImages(mContext, DbConstant.URI_IMAGES, type, side, userPicName);

            }
        }
    }

    @Override
    public void onResult(boolean resultFlag, String message) {
        ArrayList<String> all_images_path;
        if (resultFlag) {
            all_images_path = DatabaseManager.getInstance().getAllLImagesPathToUpload(mContext, DbConstant.URI_IMAGES, ApplicationDataModel.getInstance().getPatientLid());
            if (all_images_path != null && all_images_path.size() > 0) {
                new UploadImageTask(all_images_path).execute();
            }

        }

    }

    @Override
    public void onResult(boolean resultFlag, String message, ArrayList<?> dataList) {
    }

    @Override
    public void onResult(boolean resultFlag, String message, Object data) {

    }

    class UploadImageTask extends AsyncTask<String, Void, String> {
        private ProgressDialog pDialog;
        ArrayList<String> all_image_path;

        public UploadImageTask(ArrayList<String> all_image_path) {
            this.all_image_path = all_image_path;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(mContext);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String response = null;
            String url = Constants.UPLOAD_IMAGE_API;
            for (int i = 0; i < all_image_path.size(); i++) {
                ServiceHandler servicehandler = new ServiceHandler();
                try {
                    BitmapFactory.Options options = new BitmapFactory.Options();
                    // downsizing image size for uploading.
                    options.inSampleSize = 5;
                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    Bitmap bitmap = BitmapFactory.decodeFile(all_image_path.get(i), options);
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos);
                    byte[] data = bos.toByteArray();
                    JSONObject json_obj = new JSONObject();
                    json_obj.put(DbConstant.COLUMN_LID, ApplicationDataModel.getInstance().getPatientLid());
                    response = servicehandler.executeHttpPostImage(url, data, json_obj, all_image_path.get(i));
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d("error", e.toString());
                }
            }

            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            if (pDialog != null && pDialog.isShowing()) {
                pDialog.cancel();
                pDialog.dismiss();
            }

            if (result != null) {
                Log.d("upload_image_response", result);
                if (result.contains("Success")) {
                    DatabaseManager.getInstance().updateAllImages(mContext, DbConstant.URI_IMAGES, ApplicationDataModel.getInstance().getPatientLid());
                    notify.customToast(mContext, getResources().getString(R.string.info), getResources().getString(R.string.data_upload_successful));
                } else {
                    notify.customToast(mContext, getResources().getString(R.string.info), getResources().getString(R.string.data_not_upload_successful));
                }

            }
        }

    }


    class EarAdapter extends FragmentStatePagerAdapter {

        private Fragment f = null;

        public EarAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    f = new LeftEarFragment();
                    Bundle lbundle = new Bundle();
                    lbundle.putStringArrayList("left_ear_list", left_ear_images);
                    f.setArguments(lbundle);
                    break;

                case 1:
                    f = new RightEarFragment();
                    Bundle rbundle = new Bundle();
                    rbundle.putStringArrayList("right_ear_list", right_ear_images);
                    f.setArguments(rbundle);
                    break;
            }
            return f;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            String title = null;
            switch (position) {
                case 0:
                    title = "Left";
                    break;
                case 1:
                    title = "Right";
                    break;
            }
            return title;

        }

        @Override
        public int getItemPosition(Object object) {
            return PagerAdapter.POSITION_NONE;
        }

        @Override
        public int getCount() {
            return 2;
        }
    }


    class NoseAdapter extends FragmentStatePagerAdapter {

        private Fragment f = null;

        public NoseAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    f = new LeftNoseFragment();
                    Bundle lbundle = new Bundle();
                    lbundle.putStringArrayList("left_nose_list", left_nose_images);
                    f.setArguments(lbundle);
                    break;

                case 1:
                    f = new RightNoseFragment();
                    Bundle rbundle = new Bundle();
                    rbundle.putStringArrayList("right_nose_list", right_nose_images);
                    f.setArguments(rbundle);
                    break;
            }
            return f;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            String title = null;
            switch (position) {
                case 0:
                    title = "Left";
                    break;
                case 1:
                    title = "Right";
                    break;
            }
            return title;

        }

        @Override
        public int getItemPosition(Object object) {
            return PagerAdapter.POSITION_NONE;
        }

        @Override
        public int getCount() {
            return 2;
        }
    }


    /*
     * Creating file uri to store image/video
	 */
    public Uri getOutputMediaFileUri() {
        return Uri.fromFile(getOutputMediaFile());
    }

    private File getOutputMediaFile() {
        Calendar c = Calendar.getInstance();
        int day = c.get(Calendar.DATE);
        int milliseconds = c.get(Calendar.MILLISECOND);
        int seconds = c.get(Calendar.SECOND);
        // Log.d("type", type);
        picName = "telemedhc_Picture" + seconds + milliseconds + day + ".jpeg";

        File picStorePath = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/telemedhc/pictures/");
        picStorePath.mkdirs();

        FileOutputStream fOut = null;

        try {

            File save = new File(picStorePath, picName);
            userPicName = Environment.getExternalStorageDirectory().getAbsolutePath() + "/telemedhc/pictures/" + picName;
          /*  fOut = new FileOutputStream(userPicName);
            //thumbnail.compress(Bitmap.CompressFormat.JPEG,100, fOut);
            fOut.flush();
            fOut.close();*/

        } catch (Exception e) {
            e.printStackTrace();
        }
        return new File(userPicName);
    }

}
