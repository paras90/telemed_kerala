package com.phtt.telemedhc.ui;


import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.phtt.telemedhc.R;
import com.phtt.telemedhc.database.DatabaseManager;
import com.phtt.telemedhc.database.DbConstant;
import com.phtt.telemedhc.models.ApplicationDataModel;
import com.phtt.telemedhc.network.NetworkConstants;
import com.phtt.telemedhc.network.NetworkRequestManager;
import com.phtt.telemedhc.parser.ParserRequiredData;
import com.phtt.telemedhc.utils.Constants;
import com.phtt.telemedhc.utils.INetResult;
import com.phtt.telemedhc.utils.NotificationsDisplay;
import com.phtt.telemedhc.utils.Utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

@SuppressWarnings("ALL")
public class LoginActivity extends Activity implements INetResult{


	private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 0;
	private static final String TELEMEDAPP_REG_ID = "telemedapp_reg_id";
	private static final String TELEMED_APP_VERSION = "telemed_app_version";

	private EditText user, pass;
	private Button mSubmit;
	Button forgot;

	Context mContext;
	ImageButton exit, help,back;
	static int setpage;
	Dialog contDialog;
	NotificationsDisplay notify;
	Button contDialogbtn1,contDialogbtn2,contDialogBtnClose;
	EditText nameForgot, passwordForgot;
	private static final String TAG_SUCCESS = "success";
	private static final String TAG_CLINIC_ID = "clinic_id";
	private static final String TAG_MESSAGE = "message";
	public static final String MyPREFERENCES = "RegPreferences";
	public static String BASE_PKG = "com.phfi.undp_app";
	String smsstring;
	TextView textviewStatus;
	private static final int DIALOG_REALLY_EXIT_ID = 0;
	public static final String name = "nameKey";
	public static final String passw = "passwordKey";
	public static String clinic = "clinicId";
	private ProgressDialog pDialog;
	ParserRequiredData requiredData;
	private GoogleCloudMessaging gcm;
	public SharedPreferences token_prefs;
	INetResult listener;
	String username=null,password=null;



	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//Remove title bar

		setContentView(R.layout.activity_login);
		ActivityManagerForFinish.getInstance().addActivity(this);

		initViews();
		initData();
		setListeners();







//		saveLogin();

	}
	/*public void openSsa() {
		Intent intent;
		requiredData = new ParserRequiredData();
		requiredData.setSsaName(Constants.SSA_NAME_REGISTRATION);
		requiredData.setHeaderTitle(getResources().getString(
				R.string.PARSER_TITLE_REGISTRATION));
		requiredData.setTable("");
		UriHandler.getInstance().setContentUri(DbConstant.URI_TABLE_REGISTRATION);

		intent = new Intent(getApplicationContext(), ParserActivity.class);
		intent.putExtra(Constants.PARSER_DATA, requiredData);
		startActivity(intent);

		finish();

	}*/
	@Override
	public void onBackPressed()
	{
		Utils.getInstance().exitAlert(mContext);
		// code here to show dialog
	}

	private void setListeners() {
		mSubmit.setOnClickListener(m_click);
		exit.setOnClickListener(m_click);


	}
	View.OnClickListener m_click =new View.OnClickListener() {
		@Override
		public void onClick(View v) {
			if(v==exit){
				Utils.getInstance().exitAlert(mContext);
			}
			if(v==mSubmit){
				startLoginProcess();
			}
		}
	};

	private void initData() {
		mContext=this;
		listener = this;
		notify=new NotificationsDisplay(mContext);


	}

	private String registerToGCM() {
		String regid = "";
		if (checkPlayServices()) {
			gcm = GoogleCloudMessaging.getInstance(this);
			regid = getRegistrationId(LoginActivity.this);
			if (regid.isEmpty()) {
				regid = registerInBackground();
			}

		} else {
			Log.i("message", "No valid Google Play Services APK found.");
		}

		return regid;
	}

	private void initViews() {
		user = (EditText) findViewById(R.id.username);
		pass = (EditText) findViewById(R.id.password);
		mSubmit = (Button) findViewById(R.id.login);
		exit = (ImageButton) findViewById(R.id.exit);
		help = (ImageButton) findViewById(R.id.help);
		help.setVisibility(View.GONE);
		back = (ImageButton) findViewById(R.id.back);
		back.setVisibility(View.GONE);
	}

	private void saveLogin(String username,String password) {
		Context myContext;

//		if(!checkDatabaseforLogin(username,password)) {
		try {

			myContext = createPackageContext(Constants.PACKAGE_NAME,
					Context.MODE_WORLD_WRITEABLE);

			SharedPreferences sharedpreferences = myContext.getSharedPreferences(
					Constants.MyPREFERENCES, Context.MODE_WORLD_WRITEABLE);
			Editor editor = sharedpreferences.edit();
			editor.putString(Constants.name, username);
			editor.putString(Constants.passw, password);
			editor.commit();

			HashMap<String, String> values = new HashMap<String, String>();
			values.put(DbConstant.COLUMN_2, username);
			values.put(DbConstant.COLUMN_3, password);
//			 values.put(DbConstants.CLINIC_ID,LoginModel.getInstance().getClinicId());

			DatabaseManager.getInstance().insertLogin(mContext, DbConstant.URI_TABLE_LOGIN, values);

		} catch (NameNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		/*}else{
			Intent intent = new Intent(LoginActivity.this,BeneficiarySelection.class);
			startActivity(intent);
			finish();
		}*/

	}


	private void startLoginProcess() {
//		String username,password;
		username = user.getText().toString();
		password = pass.getText().toString();
		/*userName="hon1";
		password="1234";*/
		if (username != null && username.length() >= 3 && password != null && password.length() >= 3) {
			if(!checkDatabaseforLogin(username,password)) {
				if (Utils.getInstance().isInternetAvailable(mContext)) {
					NetworkRequestManager nrm = new NetworkRequestManager(mContext, this);
					Bundle bundle = new Bundle();
					bundle.putString(NetworkConstants.REQUEST_KEY_USERNAME, username);
					bundle.putString(NetworkConstants.REQUEST_KEY_PASSWORD, password);

					nrm.generateServiceRequestParams(NetworkConstants.REQUEST_ID_LOGIN, bundle);

//				openSsa();

				}else {
					notify.customToast(mContext, getResources().getString(R.string.info), getResources().getString(R.string.check_internet));
				}

			}
			else {
				Context myContext;

				ApplicationDataModel.getInstance().setHwid(username);
//				.myContext = createPackageContext(Constants.PACKAGE_NAME,

				try {

					myContext = createPackageContext(Constants.PACKAGE_NAME,
							Context.MODE_WORLD_WRITEABLE);

					SharedPreferences sharedpreferences = myContext.getSharedPreferences(
							Constants.MyPREFERENCES, Context.MODE_WORLD_WRITEABLE);
					Editor editor = sharedpreferences.edit();
					editor.putString(Constants.name, username);
					editor.putString(Constants.passw, password);
					editor.commit();

					Intent intent = new Intent(LoginActivity.this,BeneficiarySelection.class);
					startActivity(intent);
					finish();

				} catch (NameNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
//				Intent intent = new Intent(LoginActivity.this,BeneficiarySelection.class);
//				startActivity(intent);
//				finish();
//                openSsa();
			}
		}else {
			notify.customToast(mContext, getResources().getString(R.string.info), getResources().getString(R.string.enter_valid_credentials));
		}
	}


	private boolean checkDatabaseforLogin(String userName, String passWord){
		boolean bool = false;

		String[] arr = DatabaseManager.getInstance().getLoginValue(mContext, DbConstant.URI_TABLE_LOGIN, userName);

		if (arr != null) {
			if ((arr[9].equalsIgnoreCase(userName))	&& (arr[10].equalsIgnoreCase(passWord))) {

				// login successfull
				Context myContext = null;

				try {
					myContext = createPackageContext(Constants.PACKAGE_NAME, Context.MODE_WORLD_WRITEABLE);

					SharedPreferences sharedpreferences = myContext.getSharedPreferences(
							Constants.MyPREFERENCES,
							Context.MODE_WORLD_READABLE);
					Editor editor = sharedpreferences.edit();
					editor.putString(Constants.name, userName);
					editor.putString(Constants.passw, passWord);
					editor.commit();

				} catch (NameNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				bool = true;

				// start new activity
				// applicationController.handleEvent(ApplicationEvents.EVENT_ID_SELECT_OPTION_SCREEN);
				// applicationController.finishCurrentActivity(this);

			} else {

				bool = false;
				// notify.customToast(getBaseContext(), "notify",
				// getString(R.string.LS_INVALID_PASS));
			}

		}
		return bool;
	}


	@Override
	public void onResult(boolean resultFlag, String message) {

	}

	@Override
	public void onResult(boolean resultFlag, String message, ArrayList<?> dataList) {

	}

	@Override
	public void onResult(boolean resultFlag, String message, Object data) {
		if(resultFlag==true){

			if(null!=user.getText().toString())
				ApplicationDataModel.getInstance().setHwid(user.getText().toString());

			registerToGCM();
			Intent intent = new Intent(LoginActivity.this,BeneficiarySelection.class);
			startActivity(intent);
			notify.customToast(mContext, getResources().getString(R.string.info), message);
			saveLogin(username, password);
//			LoginActivity.this.finish();
//            openSsa();

		}else{
			notify.customToast(mContext, getResources().getString(R.string.info), getString(R.string.login_unsucess));
		}

	}


	private boolean checkPlayServices() {
		int resultCode = GooglePlayServicesUtil
				.isGooglePlayServicesAvailable(this);
		if (resultCode != ConnectionResult.SUCCESS) {
			if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
				GooglePlayServicesUtil.getErrorDialog(resultCode, this,
						PLAY_SERVICES_RESOLUTION_REQUEST).show();
			} else {
				Log.i("message", "This device is not supported.");
				finish();
			}
			return false;
		}
		return true;
	}


	private String getRegistrationId(Context context) {
		final SharedPreferences prefs = getGCMPreferences(context);
		String registrationId = prefs.getString(TELEMEDAPP_REG_ID, "");
		if (registrationId.isEmpty()) {
			Log.i("message", "Registration not found.");
			return "";
		} else {
			Log.i("REGISTRATION ID", registrationId);

		}
		// Check if app was updated; if so, it must clear the registration ID
		// since the existing regID is not guaranteed to work with the new
		// app version.
		int registeredVersion = prefs.getInt(TELEMED_APP_VERSION, Integer.MIN_VALUE);
		int currentVersion = getAppVersion(context);
		if (registeredVersion != currentVersion) {
			Log.i("message", "App version changed.");
			return "";
		}
		return registrationId;
	}

	private SharedPreferences getGCMPreferences(Context context) {
		// This sample app persists the registration ID in shared preferences,
		// but
		// how you store the regID in your app is up to you.
		return PreferenceManager.getDefaultSharedPreferences(this);
	}

	private static int getAppVersion(Context context) {
		try {
			PackageInfo packageInfo = context.getPackageManager()
					.getPackageInfo(context.getPackageName(), 0);
			return packageInfo.versionCode;
		} catch (PackageManager.NameNotFoundException e) {
			// should never happen
			throw new RuntimeException("Could not get package name: " + e);
		}
	}

	/**
	 * Registers the application with GCM servers asynchronously.
	 * <p>
	 * Stores the registration ID and app versionCode in the application's
	 * shared preferences.
	 */

	private String registerInBackground() {
		new AsyncTask<String, Void, String>() {

			String msg = "";
			String regId = "";
			@Override
			protected String doInBackground(String... params) {

				try {
					if (gcm == null) {
						gcm = GoogleCloudMessaging.getInstance(LoginActivity.this);
					}
					regId = gcm.register(Constants.SENDER_ID);
					msg = "Device registered, registration ID=" + regId;
					ApplicationDataModel.getInstance().setDeviceId(regId);
					storeRegistrationId(LoginActivity.this, regId);
				} catch (IOException ex) {
					msg = "Error :" + ex.getMessage();

				}

				Log.i("REGISTRATION ID", msg);
				return regId;
			}

			protected void onPostExecute(String result) {
				if(result!=null){
//					String id = getGCMPreferences(mContext);

					final SharedPreferences prefs = getGCMPreferences(mContext);
					String registrationId = prefs.getString(TELEMEDAPP_REG_ID, "");
					Bundle bundle = new Bundle();
					NetworkRequestManager nrm = new NetworkRequestManager(mContext, listener);

					if(registrationId.equalsIgnoreCase(regId)) {
						bundle.putString(NetworkConstants.APP_GCM_UPDATE_FLAG, "0");
						bundle.putString(NetworkConstants.APP_REG_UPDATE_ID, "");

					}else {
						bundle.putString(NetworkConstants.APP_GCM_UPDATE_FLAG, "1");
						bundle.putString(NetworkConstants.APP_REG_UPDATE_ID, registrationId);
					}

					bundle.putString(NetworkConstants.APP_NAME, "TeleMed_App");
//					bundle.putString(NetworkConstants.APP_REG_UPDATE_ID, "");
					bundle.putString(NetworkConstants.GCM_API_KEY, Constants.GCM_API_KEY);
					bundle.putString(NetworkConstants.USER_ID, ApplicationDataModel.getInstance().getHwid());
					bundle.putString(NetworkConstants.APP_REG_ID, regId);
					nrm.generateServiceRequestParams(NetworkConstants.REQUEST_ID_GCM_ID_UPLOAD, bundle);

				}
			};

		}.execute(null, null, null);
		return null;

	}

	private void storeRegistrationId(Context context, String regId) {

		final SharedPreferences prefs = getGCMPreferences(context);
		int appVersion = getAppVersion(context);
		Log.i("GCM Message", "Saving regId on app version " + appVersion);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString(TELEMEDAPP_REG_ID, regId);
		editor.putInt(TELEMED_APP_VERSION, appVersion);
		editor.commit();
	}
}
