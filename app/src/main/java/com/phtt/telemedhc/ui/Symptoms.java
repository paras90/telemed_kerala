package com.phtt.telemedhc.ui;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.phtt.telemedhc.R;
import com.phtt.telemedhc.database.DatabaseManager;
import com.phtt.telemedhc.database.DbConstant;
import com.phtt.telemedhc.database.symptoms.SymptomDataModel;
import com.phtt.telemedhc.database.symptoms.SymptomDetailHandler;
import com.phtt.telemedhc.dialogs.CustomDialog;
import com.phtt.telemedhc.dialogs.DialogConstant;
import com.phtt.telemedhc.models.ApplicationDataModel;
import com.phtt.telemedhc.network.NetworkConstants;
import com.phtt.telemedhc.network.NetworkRequestManager;
import com.phtt.telemedhc.utils.INetResult;
import com.phtt.telemedhc.utils.NotificationsDisplay;
import com.phtt.telemedhc.utils.Utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by vr3v3n on 15/01/16.
 */
public class Symptoms extends Activity implements View.OnClickListener,
        CustomDialog.CreateSymptomDialog, INetResult {


    private ImageView userPic;
    private TextView userName;
    private TextView userId;

    private ListView mSymtomList;

    private Button btnAddSymptom;
    private Context mContext;

    SimpleAdapter adapter;

    //adding Result listener for upload
    private INetResult listener;

    //adding notification object
    NotificationsDisplay notify;

    ArrayList<SymptomDetailHandler> symptomPrefilllist;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_symptoms);

        ActivityManagerForFinish.getInstance().addActivity(this);

        //getting activity context\
        mContext = this;

        //initialising Inet listener
        listener = this;

        //initialising notify object
        notify = new NotificationsDisplay(mContext);

        initView();
        setUserInfo();

        symptomPrefilllist = SymptomDataModel.getInstance().getSymptomList();
    }

    private void initView() {

        btnAddSymptom = (Button) findViewById(R.id.btnAdd);

        userPic = (ImageView) findViewById(R.id.userPic);
        userName = (TextView) findViewById(R.id.name_ben);
        userId = (TextView) findViewById(R.id.id_ben);
        ImageButton upload = (ImageButton) findViewById(R.id.upload);
        upload.setVisibility(View.VISIBLE);
        ImageButton help = (ImageButton) findViewById(R.id.help);
        help.setVisibility(View.GONE);
        mSymtomList = (ListView) findViewById(R.id.symptomList);
        showListView();
    }


    private void setUserInfo() {

        Intent intent = getIntent();

        if (intent == null)
            return;


        Utils.getInstance().loadPatientPic(mContext, userPic, ApplicationDataModel.getInstance().getPatientPicName());

        if (ApplicationDataModel.getInstance().getPatientName() != null) {
            userName.setText(ApplicationDataModel.getInstance().getPatientName());
        }

        if (ApplicationDataModel.getInstance().getPatientLid() != null) {

            if (ApplicationDataModel.getInstance().getPatientGid() != null &&
                    !ApplicationDataModel.getInstance().getPatientGid().equalsIgnoreCase("")) {

                userId.setText(ApplicationDataModel.getInstance().getPatientGid());
            } else {
                userId.setText(ApplicationDataModel.getInstance().getPatientLid());
            }
        }
    }


    @Override
    public void createSympsDialog() {

        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        Fragment fragment = getFragmentManager().findFragmentByTag(DialogConstant.TAG_SYMPTOM_DIALOG);
        if (fragment != null) {
            transaction.remove(fragment);
        }

        Bundle bundle = new Bundle();
        bundle.putSerializable(DialogConstant.KEY_DRUG_DIALOG, symptomPrefilllist);

        CustomDialog dialog = new CustomDialog();
        dialog.setArguments(bundle);
        dialog.show(getFragmentManager(), DialogConstant.TAG_SYMPTOM_DIALOG);
    }

    @Override
    public void onAddSymtomCallback(int resultCode) {

        if (resultCode == DialogConstant.RESULT_EMPTY) {
            notify.customToast(mContext, getResources().getString(R.string.info), getString(R.string.symptom_compulsroy));
//            Toast.makeText(this, , Toast.LENGTH_LONG).show();
        } else if (resultCode == DialogConstant.RESULT_OK) {
            showListView();
        }

    }

    private void showListView() {

        Set<Map<String, String>> uSet = DatabaseManager.getInstance().getUserSymptoms(mContext);

        List<Map<String, String>> uList = new ArrayList<>();
        uList.addAll(uSet);

        adapter = new SimpleAdapter(mContext, uList, R.layout.symptom_list_view,
                new String[]{DbConstant.COLUMN_1, DbConstant.COLUMN_2, DbConstant.COLUMN_3,
                        DbConstant.COLUMN_4, DbConstant.COLUMN_4}, new int[]{R.id.symptomName, R.id.complaints,
                R.id.onset, R.id.illness, R.id.associateSym});
        mSymtomList.setAdapter(adapter);
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.btnAdd:
                createSympsDialog();
                break;

            case R.id.back:
                Symptoms.this.finish();
                break;

            case R.id.exit:

                Utils.getInstance().exitAlert(mContext);
                break;

            case R.id.upload:

                if (Utils.getInstance().isInternetAvailable(mContext)) {
                    NetworkRequestManager nrm = new NetworkRequestManager(mContext, listener);
                    nrm.generateServiceRequestParams(NetworkConstants.REQUEST_ID_SYMPTOM, null);
                } else {
                    notify.customToast(mContext, getResources().getString(R.string.info), getResources().getString(R.string.check_internet));
                }
                break;

        }
    }

    @Override
    public void onResult(boolean resultFlag, String message) {
        if (message.equalsIgnoreCase("Success")) {
            Toast.makeText(mContext, getResources().getString(R.string.data_upload_successful), Toast.LENGTH_LONG).show();

        } else {
            Toast.makeText(mContext, getResources().getString(R.string.data_not_upload_successful), Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onResult(boolean resultFlag, String message, ArrayList<?> dataList) {

    }

    @Override
    public void onResult(boolean resultFlag, String message, Object data) {


    }
}
