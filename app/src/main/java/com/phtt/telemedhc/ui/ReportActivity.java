package com.phtt.telemedhc.ui;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import com.phtt.telemedhc.R;
import com.phtt.telemedhc.models.ReportData;
import com.phtt.telemedhc.network.NetworkConstants;
import com.phtt.telemedhc.network.NetworkRequestManager;
import com.phtt.telemedhc.utils.INetResult;
import com.phtt.telemedhc.utils.NotificationsDisplay;
import com.phtt.telemedhc.utils.Utils;

import java.io.File;
import java.util.ArrayList;

public class ReportActivity extends Activity implements INetResult {

    private INetResult listener;
    NotificationsDisplay notify;
    private Context mContext;
    ReportData report_data;
    Button back_btn,help_btn,exit_btn;
    String pdf_path="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);

        mContext = this;
        listener = this;
        notify = new NotificationsDisplay(mContext);

        initViews();
        initData();
        setListeners();

    }

    private void initViews() {
        back_btn= (Button) findViewById(R.id.backButton);
        help_btn= (Button) findViewById(R.id.helpButton);
        exit_btn= (Button) findViewById(R.id.exitButton);

    }

    private void initData() {
        if (Utils.getInstance().isInternetAvailable(mContext)) {
            NetworkRequestManager nrm = new NetworkRequestManager(mContext, listener);
            Bundle bundle = new Bundle();
          /*  bundle.putString(NetworkConstants.REQUEST_KEY_PATIENT_LID,data.getString("patient_id"));
            bundle.putString(NetworkConstants.REQUEST_KEY_DOCTOR_ID,data.getString("doctor_id"));
            bundle.putString(NetworkConstants.REQUEST_KEY_APPOINTMENT_ID,data.getString("apptId"));*/
            nrm.generateServiceRequestParams(NetworkConstants.REQUEST_ID_REPORT, bundle);
        } else {
            notify.customToast(mContext, getResources().getString(R.string.info), getResources().getString(R.string.check_internet));
        }

        Intent pdf_intent=getIntent();
        if(pdf_intent!=null){
            pdf_path=pdf_intent.getStringExtra("pdf_path");
            File targetFile = new File(pdf_path);
            Uri targetUri = Uri.fromFile(targetFile);
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(targetUri, "application/pdf");
            startActivity(intent);
        }

    }

    private void setListeners() {
        back_btn.setOnClickListener(m_click);
        help_btn.setOnClickListener(m_click);
        exit_btn.setOnClickListener(m_click);
    }

    View.OnClickListener m_click=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
              if(v==back_btn){
                  finish();
              }else if(v==help_btn){
                  try {
                      int engDrawableId= R.drawable.home_help;
                      Utils.getInstance().helpDialog(mContext, engDrawableId);
                  } catch (Exception e) {
                      // TODO Auto-generated catch block
                      e.printStackTrace();
                  }
              }
              else if(v==exit_btn){
                  Utils.getInstance().exitAlert(mContext);
              }
        }
    };

    @Override
    public void onResult(boolean resultFlag, String message) {

    }

    @Override
    public void onResult(boolean resultFlag, String message, ArrayList<?> dataList) {

    }

    @Override
    public void onResult(boolean resultFlag, String message, Object data) {
        if (resultFlag) {
            report_data = (ReportData) data;
            File targetFile = new File(report_data.getReport_path());
            Uri targetUri = Uri.fromFile(targetFile);

            Intent intent;
            intent = new Intent(Intent.ACTION_VIEW);
            intent.setDataAndType(targetUri, "application/pdf");

            startActivity(intent);
        }

    }
}
