package com.phtt.telemedhc.ui;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.phfi.model.ApplicationRequiredInfo;
import com.phtt.telemedhc.R;
import com.phtt.telemedhc.database.DbConstant;
import com.phtt.telemedhc.doctor.DoctorActivity;
import com.phtt.telemedhc.models.ApplicationDataModel;
import com.phtt.telemedhc.models.DiagnosticData;
import com.phtt.telemedhc.models.PatientInfoDataHandler;
import com.phtt.telemedhc.models.ReportData;
import com.phtt.telemedhc.parser.ParserActivity;
import com.phtt.telemedhc.parser.ParserRequiredData;
import com.phtt.telemedhc.parser.UriHandler;
import com.phtt.telemedhc.utils.Constants;
import com.phtt.telemedhc.utils.INetResult;
import com.phtt.telemedhc.utils.NotificationsDisplay;
import com.phtt.telemedhc.utils.Utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

public class HomeScreen extends Activity implements INetResult {


    Button swasthyaslate_icon, report_icon, doctor;
    TextView editPatient;
    Button add_symptom, add_images;
    ImageButton back_btn, help_btn, exit_btn;
    private ParserRequiredData requiredData;
    private Context mContext;
    PatientInfoDataHandler patientInfo;
    ImageView patientPicView;
    TextView name, id;
    private Intent intent;
    private INetResult listener;
    NotificationsDisplay notify;
    DiagnosticData diagnostic_name;
    ReportData report_data;
    Bundle diagnostics_bundle;
    String lid;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);

        ActivityManagerForFinish.getInstance().addActivity(this);
        mContext = this;
        listener = this;
        notify = new NotificationsDisplay(mContext);

        initViews();
        initData();
        setListeners();
    }

    @Override
    public void onBackPressed() {
        Utils.getInstance().exitAlert(mContext);
        // TODO Auto-generated method stub
        //super.onBackPressed();
//        dialogBackConfirmation();
       /* if (diagnostics_bundle != null) {
            Intent benificary_intent = new Intent(mContext, BeneficiarySelection.class);
            startActivity(benificary_intent);
            finish();
        }*/
    }

    private void initViews() {
        swasthyaslate_icon = (Button) findViewById(R.id.childClosureTile);
        report_icon = (Button) findViewById(R.id.report_icon);
        add_symptom = (Button) findViewById(R.id.vaccinationTile);
        add_images = (Button) findViewById(R.id.takePicButton);
        editPatient = (TextView) findViewById(R.id.editPatient);
        doctor = (Button) findViewById(R.id.breastFeedingTile);
        back_btn = (ImageButton) findViewById(R.id.back);
        help_btn = (ImageButton) findViewById(R.id.help);
        exit_btn = (ImageButton) findViewById(R.id.exit);
        patientPicView = (ImageView) findViewById(R.id.userPic);
        name = (TextView) findViewById(R.id.name_ben);
        id = (TextView) findViewById(R.id.id_ben);

    }

    private void initData() {
        Intent intent = getIntent();
        if (intent != null) {
            this.patientInfo = (PatientInfoDataHandler) intent.getSerializableExtra(Constants.PATIENT_DATA_INTENT);
            // load patient picture
            Utils.getInstance().loadPatientPic(mContext, patientPicView, patientInfo.getImagePath());
        }

//        diagnostics_bundle = intent.getExtras();

        /*if (diagnostics_bundle != null) {
            BeneficiaryDataHandler patient_data = DatabaseManager.getInstance().getSinglePatientData(mContext, diagnostics_bundle.getString("patient_id"));

            if (patient_data != null) {

                ApplicationDataModel.getInstance().setPatientLid(patient_data.getBeneficiarytId());
                ApplicationDataModel.getInstance().setPatientName(patient_data.getBeneficiaryName());
                ApplicationDataModel.getInstance().setPatientPicName(patient_data.getImagePath());
                ApplicationDataModel.getInstance().setPatientGid(patient_data.getBeneficiaryGId());
                ApplicationDataModel.getInstance().setPatientSex(patient_data.getBeneficiarySex());
                name.setText(patient_data.getBeneficiaryName());
                String idtest = "";
                if (patient_data.getBeneficiarytId() != null && !patient_data.getBeneficiarytId().isEmpty()) {
                    idtest = patient_data.getBeneficiaryGId();
                } else {
                    idtest = patient_data.getBeneficiarytId();
                }
                id.setText(idtest);
                Utils.getInstance().loadPatientPic(mContext, patientPicView, patient_data.getImagePath());

                String type = diagnostics_bundle.getString("type");
                if (type != null) {
                    // call the api to get diagnostic name
                    if (diagnostics_bundle.getString("type").equalsIgnoreCase("Diagnostics")) {
                        if (Utils.getInstance().isInternetAvailable(mContext)) {
                            NetworkRequestManager nrm = new NetworkRequestManager(mContext, listener);
                            Bundle diagnostic_bundle = new Bundle();
                            diagnostic_bundle.putString(NetworkConstants.REQUEST_KEY_PATIENT_LID, diagnostics_bundle.getString("patient_id"));
                            diagnostic_bundle.putString(NetworkConstants.REQUEST_KEY_DOCTOR_ID, diagnostics_bundle.getString("doctor_id"));
                            diagnostic_bundle.putString(NetworkConstants.REQUEST_KEY_APPOINTMENT_ID, diagnostics_bundle.getString("apptId"));
                            nrm.generateServiceRequestParams(NetworkConstants.REQUEST_ID_DIAGNOSTIC_LIST, diagnostic_bundle);
                        } else {
                            notify.customToast(mContext, getResources().getString(R.string.info), getResources().getString(R.string.check_internet));
                        }
                    } *//*else if (diagnostics_bundle.getString("type").equalsIgnoreCase("Report")) {
                        if (Utils.getInstance().isInternetAvailable(mContext)) {
                            NetworkRequestManager nrm = new NetworkRequestManager(mContext, listener);
                            Bundle report_bundle = new Bundle();
                            report_bundle.putString(NetworkConstants.REQUEST_KEY_PATIENT_LID, diagnostics_bundle.getString("patient_id"));
                            report_bundle.putString(NetworkConstants.REQUEST_KEY_DOCTOR_ID, diagnostics_bundle.getString("doctor_id"));
                            report_bundle.putString(NetworkConstants.REQUEST_KEY_APPOINTMENT_ID, diagnostics_bundle.getString("apptId"));
                            nrm.generateServiceRequestParams(NetworkConstants.REQUEST_ID_REPORT, report_bundle);
                        } else {
                            notify.customToast(mContext, getResources().getString(R.string.info), getResources().getString(R.string.check_internet));
                        }*//*

                }
            }
        }*/


    }

    private void setData() {
        try {
            name.setText(patientInfo.getFirstName());
            String idtest = "";
            if (ApplicationDataModel.getInstance().getPatientGid() != null && !ApplicationDataModel.getInstance().getPatientGid().isEmpty()) {
                idtest = ApplicationDataModel.getInstance().getPatientGid();
            } else {
                idtest = ApplicationDataModel.getInstance().getPatientLid();
            }
            id.setText(idtest);
        } catch (NullPointerException e) {
            e.printStackTrace();
            Toast.makeText(this, R.string.no_data, Toast.LENGTH_SHORT).show();
        }
    }
//        patientPicView.setImageURI();


    private void setListeners() {
        swasthyaslate_icon.setOnClickListener(m_click);
        report_icon.setOnClickListener(m_click);
        add_symptom.setOnClickListener(m_click);
        add_images.setOnClickListener(m_click);
        doctor.setOnClickListener(m_click);
        back_btn.setOnClickListener(m_click);
        help_btn.setOnClickListener(m_click);
        exit_btn.setOnClickListener(m_click);
        editPatient.setOnClickListener(m_click);
    }

    @Override
    protected void onResume() {
        super.onResume();
        initData();
        setData();
    }

    View.OnClickListener m_click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v == swasthyaslate_icon) {
                // launch swasthyaslate app
                startSwasthyaslate();
            } else if (v == add_symptom) {
                startActivity(new Intent(HomeScreen.this, Symptoms.class));
            } else if (v == add_images) {
                Intent add_images = new Intent(mContext, AddImagesActivity.class);
                startActivity(add_images);
            } else if (v == doctor) {
                Intent doctor_intent = new Intent(mContext, DoctorActivity.class);
                startActivity(doctor_intent);
            } else if (v == report_icon) {
                downloadAndOpenReport();
            } else if (v == back_btn) {
                finish();
            } else if (v == help_btn) {
                try {
                    // help button on click event
//                    int hinDrawableId = R.drawable.;
                    int engDrawableId = R.drawable.home_help;
                    Utils.getInstance().helpDialog(mContext, engDrawableId);
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            } else if (v == exit_btn) {
                Utils.getInstance().exitAlert(mContext);
            }else if (v == editPatient) {
                onEditButtonPressed();
                finish();
            }

        }
    };

    public void onEditButtonPressed() {
        Intent intent;
        requiredData = new ParserRequiredData();
        requiredData.setSsaName(Constants.SSA_NAME_REGISTRATION);
        requiredData.setHeaderTitle(getResources().getString(
                R.string.PARSER_TITLE_REGISTRATION));
        requiredData.setTable("");
        UriHandler.getInstance().setContentUri(DbConstant.URI_TABLE_REGISTRATION);

        intent = new Intent(getApplicationContext(), ParserActivity.class);
        intent.putExtra(Constants.PARSER_DATA, requiredData);
        intent.putExtra("lid", patientInfo.getLocalId());
        startActivityForResult(intent, 447);

    }

    private void startSymptom() {
        openSymptomSsa();

    }

    private void downloadAndOpenReport() {
        if (Utils.getInstance().isInternetAvailable(mContext)) {
            String url;
            url = "https://healthcubed.com/telemedicine/pdf_genrate.php?patient_lid=";
            lid = ApplicationDataModel.getInstance().getPatientLid();
            url += lid;
            new DownloadReportTask().execute(url);
        } else {
            notify.customToast(mContext, getResources().getString(R.string.info), getResources().getString(R.string.check_internet));
        }

    }

    private void openSymptomSsa() {
        requiredData = new ParserRequiredData();
        requiredData.setSsaName(Constants.SSA_NAME_ADD_SYMPTOMS);
        requiredData.setHeaderTitle(getResources().getString(
                R.string.PARSER_TITLE_Symptoms));
        requiredData.setTable("");
        UriHandler.getInstance().setContentUri(DbConstant.C_U_T_COMPLAINTS_SYMPTOM);

        intent = new Intent(mContext, ParserActivity.class);
        intent.putExtra(Constants.PARSER_DATA, requiredData);
        startActivityForResult(intent, 885);

    }


    private void startSwasthyaslate() {
        if (ApplicationDataModel.getInstance().getPatientLid() != null) {
            ApplicationRequiredInfo appInfo = new ApplicationRequiredInfo();
            appInfo.setApplicationName("Telemedhc");
            appInfo.setSelectedLanguage("EN");
            appInfo.setVisitNo("1");

            com.phfi.model.PatientInfoDataHandler mm = new com.phfi.model.PatientInfoDataHandler();
            String imagePath = Environment.getExternalStorageDirectory().getAbsolutePath() + "/telemedhc/pictures/" + ApplicationDataModel.getInstance().getPatientPicName();
            mm.setImagePath(imagePath);
            mm.setFirstName(ApplicationDataModel.getInstance().getPatientName());
            mm.setGloablId(ApplicationDataModel.getInstance().getPatientGid());
            mm.setHWId(ApplicationDataModel.getInstance().getHwid());
            mm.setLocalId(ApplicationDataModel.getInstance().getPatientLid());
            mm.setPatientSex(ApplicationDataModel.getInstance().getPatientSex());
            appInfo.setPatientInfoModel(mm);
            appInfo.setVisitNo("1");


            try {
                Intent intent = new Intent(getPackageManager().getLaunchIntentForPackage("com.healthcubed.diagnostics"));
                if (intent != null) {
                    intent.putExtra(ApplicationRequiredInfo.APPLICATION_DATA, appInfo);
                    startActivity(intent);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void openReportSsa() {
        Intent intent;
        requiredData = new ParserRequiredData();
        requiredData.setSsaName(Constants.SSA_NAME_BENEFICIARY_REPORT);
        requiredData.setHeaderTitle(getResources().getString(
                R.string.PARSER_TITLE_REPORT));
        requiredData.setTable("");
        UriHandler.getInstance().setContentUri(DbConstant.URI_HEALTH_REPORT);

        intent = new Intent(getApplicationContext(), ParserActivity.class);
        intent.putExtra(Constants.PARSER_DATA, requiredData);
        startActivity(intent);

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {


        if (resultCode == Constants.EXIT_CODE_SYMPTOM_SSA) {
            requiredData = new ParserRequiredData();
            requiredData.setSsaName(Constants.SSA_NAME_ADD_SYMPTOMS);
            requiredData.setHeaderTitle("Symptoms");
            requiredData.setTable(DbConstant.TABLE_COMPLAINTS_SYMPTOM);
            UriHandler.getInstance().setContentUri(DbConstant.C_U_T_COMPLAINTS_SYMPTOM);

            intent = new Intent(mContext, ParserActivity.class);
            intent.putExtra(Constants.PARSER_DATA, requiredData);
            startActivityForResult(intent, 885);
        }
    }


    @Override
    public void onResult(boolean resultFlag, String message) {

    }

    @Override
    public void onResult(boolean resultFlag, String message, ArrayList<?> dataList) {

    }

    @Override
    public void onResult(boolean resultFlag, String message, Object data) {
        if (resultFlag) {
            if (data instanceof DiagnosticData) {
                diagnostic_name = (DiagnosticData) data;
                Log.d("diagnostic_name", diagnostic_name.toString());
            }
        }

    }


    class DownloadReportTask extends AsyncTask<String, Void, File> {

        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(HomeScreen.this);
            progressDialog.setTitle("Please Wait..");
            progressDialog.setMessage("Loading...");
            progressDialog.setCancelable(false);
            progressDialog.show();
        }

        @Override
        protected File doInBackground(String... params) {
            File file = null;
            try {

                URL url = new URL(params[0]);
                HttpURLConnection urlConnection = (HttpURLConnection) url
                        .openConnection();

                urlConnection.setRequestMethod("GET");
                urlConnection.setDoOutput(true);
                // connect
                urlConnection.connect();

                // set the path where we want to save the file
                File SDCardRoot = Environment.getExternalStorageDirectory();
                // create a new file, to save the <span id="IL_AD3" class="IL_AD">downloaded</span> file
                file = new File(SDCardRoot, "/telemedhc/" + ApplicationDataModel.getInstance().getPatientLid() + ".pdf");

                FileOutputStream fileOutput = new FileOutputStream(file);

                // Stream used for reading the data from the internet
                InputStream inputStream = urlConnection.getInputStream();

                // this is the total size of the file which we are
                // <span id="IL_AD6" class="IL_AD">downloading</span>
                int totalsize = urlConnection.getContentLength();
//            setText("Starting <span id="IL_AD1" class="IL_AD">PDF download</span>...");

                // create a buffer...
                byte[] buffer = new byte[1024 * 1024];
                int bufferLength = 0;

                while ((bufferLength = inputStream.read(buffer)) > 0) {
                    fileOutput.write(buffer, 0, bufferLength);


                }
                // close the output stream when complete //
                fileOutput.close();
//            setText("Download Complete. Open PDF Application installed in the device.");

            } catch (final MalformedURLException e) {

            } catch (final IOException e) {

            } catch (final Exception e) {

            }
            return file;
        }

        @Override
        protected void onPostExecute(File s) {
            super.onPostExecute(s);
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
            try {
                Uri path = Uri.fromFile(s);
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setDataAndType(path, "application/pdf");
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);

            } catch (ActivityNotFoundException e) {
                e.printStackTrace();
            }
        }
    }
}
