package com.phtt.telemedhc.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.telephony.gsm.GsmCellLocation;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.phtt.telemedhc.R;
import com.phtt.telemedhc.database.DatabaseManager;
import com.phtt.telemedhc.database.DbConstant;
import com.phtt.telemedhc.database.symptoms.SymptomDataModel;
import com.phtt.telemedhc.database.symptoms.SymptomDetailHandler;
import com.phtt.telemedhc.models.ApplicationDataModel;
import com.phtt.telemedhc.models.BeneficiaryDataHandler;
import com.phtt.telemedhc.models.BeneficiaryListAdapter;
import com.phtt.telemedhc.models.ImageData;
import com.phtt.telemedhc.models.LocationHandler;
import com.phtt.telemedhc.models.PatientInfoDataHandler;
import com.phtt.telemedhc.models.RegistrationData;
import com.phtt.telemedhc.models.UploadResponseData;
import com.phtt.telemedhc.network.NetworkConstants;
import com.phtt.telemedhc.network.NetworkRequestManager;
import com.phtt.telemedhc.network.ServiceHandler;
import com.phtt.telemedhc.parser.NotificationsDisplay;
import com.phtt.telemedhc.parser.ParserActivity;
import com.phtt.telemedhc.parser.ParserRequiredData;
import com.phtt.telemedhc.parser.UriHandler;
import com.phtt.telemedhc.utils.Constants;
import com.phtt.telemedhc.utils.Utils;
import com.phtt.telemedhc.utils.INetResult;
import com.phtt.telemedhc.utils.ValueConvertor;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Vector;

/**
 * Created by Sharma on 09-12-2015.
 */
public class BeneficiarySelection extends Activity implements View.OnClickListener, INetResult, LocationListener {


    public static final boolean previewData = false;
    Context mContext;
    Location location;
    ListView listView;
    TextView totalRegisterUserTextView;
    ArrayList<BeneficiaryDataHandler> patientDataArrayList;
    BeneficiaryListAdapter listAdapter;
    NotificationsDisplay notify;
    BeneficiaryDataHandler patientInfoModel;
    ParserRequiredData requiredData;
    private INetResult listener;
    EditText inputSearch;
    ArrayList<ImageData> failed_image_path;
    ImageButton back, help, upload, exit;


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_beneficiary_selction);
        mContext = BeneficiarySelection.this;
        listener = this;
        notify = new NotificationsDisplay(mContext);
        ActivityManagerForFinish.getInstance().addActivity(this);


        totalRegisterUserTextView = (TextView) findViewById(R.id.valuesrecord);
        listView = (ListView) findViewById(R.id.patientListView);
        inputSearch = (EditText) findViewById(R.id.search_name);
        back = (ImageButton) findViewById(R.id.back);
        back.setBackgroundResource(R.drawable.logout);
        upload = (ImageButton) findViewById(R.id.upload);
        exit = (ImageButton) findViewById(R.id.exit);
        help = (ImageButton) findViewById(R.id.help);
        help.setVisibility(View.GONE);
        upload.setVisibility(View.VISIBLE);
        initGps();
        getGSMCellLocation();
        initData();
//        inputSearch.setShowSoftInputOnFocus(true);
        inputSearch.setEnabled(false);
        inputSearch.setVisibility(View.INVISIBLE);

        ArrayList<SymptomDetailHandler> symptomPrefilllist = DatabaseManager.getInstance().getSymptomsListData();
        if (symptomPrefilllist != null) {
            SymptomDataModel.getInstance().setSymptomList(symptomPrefilllist);
        }
        initList();
        getHwId();
    }

    private void getHwId() {
        /*Context myContext;
        try {
            myContext = createPackageContext(Constants.PACKAGE_NAME, Context.MODE_WORLD_WRITEABLE);
            SharedPreferences sharedpreferences = myContext.getSharedPreferences(
                    Constants.MyPREFERENCES, Context.MODE_WORLD_WRITEABLE);
            String hwid = sharedpreferences.getString(Constants.name, " ");
            ApplicationDataModel.getInstance().setHwid(hwid);
        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }

    private SharedPreferences getGCMPreferences(Context context) {
        // This sample app persists the registration ID in shared preferences,
        // but
        // how you store the regID in your app is up to you.
        return PreferenceManager.getDefaultSharedPreferences(this);
    }

    private void initData() {
        try {
            SharedPreferences sharedpreferences = getGCMPreferences(mContext);
            String device = sharedpreferences.getString("telemedapp_reg_id", " ");
            ApplicationDataModel.getInstance().setDeviceId(device);

        } catch (Exception e) {
            // TODO: handle exception
        }
    }

    private void initGps() {

        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getBaseContext());

        // Showing status
        if (status != ConnectionResult.SUCCESS) { // Google Play Services are
            // not available

            int requestCode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, this,
                    requestCode);
            dialog.show();


        } else {
            LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

            if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                // Creating a criteria object to retrieve provider
                Criteria criteria = new Criteria();
                criteria.setAccuracy(Criteria.ACCURACY_FINE);

                // Getting the name of the best provider
                String provider = locationManager.getBestProvider(criteria, true);

                // Getting Current Location
                location = locationManager.getLastKnownLocation(provider);

                if (location != null) {
                    onLocationChanged(location);
                }

                locationManager.requestLocationUpdates(provider, 5000, 0, this);
            } else {

                AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);
                // Setting Dialog Title
                alertDialog.setTitle(R.string.WGT_GPS_SETTING);
                // Setting Dialog Message
                alertDialog.setMessage(R.string.WGT_GPS_NOT_ENABLE);
                // On pressing Settings button
                alertDialog.setPositiveButton(R.string.WGT_SETTINGS, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        mContext.startActivity(intent);
                    }
                });
                // on pressing cancel button
                alertDialog.setNegativeButton(R.string.WGT_CANCEL, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                // Showing Alert Message
                alertDialog.show();
            }

        }
    }

    private void getGSMCellLocation() {

        try {
            TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);

            final int simState = tm.getSimState();
            if (simState != TelephonyManager.SIM_STATE_ABSENT) {
                GsmCellLocation cellLoc = (GsmCellLocation) tm.getCellLocation();

                int cellId = cellLoc.getCid();
                int lac = cellLoc.getLac();
//					Log.v(TAG, "Cell Id: " + cellId + "lac :" + lac);
                LocationHandler.getInstance().setGsmCellId(ValueConvertor.getStringValue(cellId));
                LocationHandler.getInstance().setGsmLac(ValueConvertor.getStringValue(lac));

            } else {
//					Log.v(TAG, "SIM_STATE_ABSENT");
//					notify.customToast(mContext, "info", "SIM card not found! Please insert SIM card.");
//					MapActivity.this.finish();
            }

        } catch (Exception e) {
//				Log.v(TAG, "Exception: " + e);
            e.printStackTrace();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
        // TODO Auto-generated method stub
        LocationHandler.getInstance().setCurrentLatitude(location.getLongitude());
        LocationHandler.getInstance().setCurrentLongitude(location.getLatitude());
        DatabaseManager.getInstance().insertGPS(mContext, "" + LocationHandler.getInstance().getCurrentLatitude(), "" + LocationHandler.getInstance().getCurrentLongitude(), LocationHandler.getInstance().getGsmLac(), LocationHandler.getInstance().getGsmCellId());

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.adduser:
                onAddButtonPressed();
                break;
            case R.id.exit:
                Utils.getInstance().exitAlert(mContext);
                break;
            case R.id.upload:
            /*    if (Utils.getInstance().isInternetAvailable(mContext)) {

                    NetworkRequestManager nrm = new NetworkRequestManager(mContext, listener);
                    Bundle dataBundle;
                    String[] lidToUpload = DatabaseManager.getInstance().getPatientListToUpload(mContext);
                    if (lidToUpload != null && lidToUpload.length > 0) {
                        for (int i = 0; i < lidToUpload.length; i++) {
                            dataBundle = DatabaseManager.getInstance().getPatientToUpload(mContext, DbConstant.URI_TABLE_REGISTRATION, lidToUpload[i]);
                            dataBundle.putString("device_id", ApplicationDataModel.getInstance().getDeviceId());
                            dataBundle.putString("lat", String.valueOf(LocationHandler.getInstance().getCurrentLatitude()));
                            dataBundle.putString("long", String.valueOf(LocationHandler.getInstance().getCurrentLongitude()));
                            nrm.generateServiceRequestParams(NetworkConstants.REQUEST_ID_REGISTRATION, dataBundle);
                        }
                        if (checkHWid()) {
                            NetworkRequestManager nrm1 = new NetworkRequestManager(mContext, listener);
                            nrm1.generateServiceRequestParams(NetworkConstants.REQUEST_ID_SYNC_UPLOAD, null);
                        } else {
                            notify.customToast(mContext, getResources().getString(R.string.info), getString(R.string.login_agaon));
                        }

                    } else {*/
                if (checkHWid()) {
                    NetworkRequestManager nrm1 = new NetworkRequestManager(mContext, listener);
                    nrm1.generateServiceRequestParams(NetworkConstants.REQUEST_ID_SYNC_UPLOAD, null);
                } else {
                    notify.customToast(mContext, getResources().getString(R.string.info), getString(R.string.login_agaon));
                }

/*
                    }
                } else {
                    //TODO save data and upload later

                    notify.customToast(mContext, getResources().getString(R.string.info), getResources().getString(R.string.check_internet));

                }*/

                break;
            case R.id.searchButton:
                searchName();
                break;
            case R.id.back:
                exitDialogue(BeneficiarySelection.this);
                break;

        }
    }

    private boolean checkHWid() {

        String hwid = ApplicationDataModel.getInstance().getHwid();
        if (hwid != null || hwid != "") {
            return true;
        } else {
            return false;
        }
    }

    public void exitDialogue(final Context mContext) {

        try {
            AlertDialog.Builder exitAlert = new AlertDialog.Builder(mContext);

            exitAlert.setTitle(R.string.exit_app/*mContext.getResources().getString(R.string.are_u_sure_to_exit)*/);

            exitAlert.setPositiveButton(R.string.yes,
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface arg0, int arg1) {

                            try {

                                SharedPreferences mPref = getApplicationContext().getSharedPreferences(
                                        Constants.MyPREFERENCES, Context.MODE_WORLD_WRITEABLE);
                                SharedPreferences.Editor editor = mPref.edit();
                                editor.clear();
                                editor.remove(Constants.name);
                                editor.remove(Constants.passw);
                                editor.commit();

                                Vector<Activity> activityVector = ActivityManagerForFinish.getInstance().getActivityVector();
                                if (activityVector != null) {
                                    for (int i = 0; i < activityVector.size(); i++) {
                                        activityVector.get(i).finish();
                                    }
                                }
                                ActivityManagerForFinish.instance = null;
                                android.os.Process.killProcess(android.os.Process.myPid());

                            } catch (Throwable e) {
                                e.printStackTrace();
                            }
                        }
                    });

            exitAlert.setNegativeButton(R.string.no,
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

            exitAlert.show();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    private void searchName() {
        inputSearch.setEnabled(true);
        inputSearch.setVisibility(View.VISIBLE);
        inputSearch.requestFocus();
        inputSearch.setCursorVisible(true);

        inputSearch.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence cs, int arg1, int arg2, int arg3) {
                // When user changed the Text
                if(listAdapter!=null)
                BeneficiarySelection.this.listAdapter.getFilter().filter(cs);
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
                // TODO Auto-generated method stub
            }

            @Override
            public void afterTextChanged(Editable arg0) {
                // TODO Auto-generated method stub
            }
        });
    }


    @Override
    protected void onStart() {
        // TODO Auto-generated method stub
        super.onStart();
    }


    @Override
    protected void onRestart() {
        // TODO Auto-generated method stub
        super.onRestart();
    }


    @Override
    protected void onResume() {
        // TODO Auto-generated method stub
        super.onResume();
        getHwId();
        initList();

    }


    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
    }


    @Override
    public void onBackPressed() {
        Utils.getInstance().exitAlert(mContext);
        // TODO Auto-generated method stub
        //super.onBackPressed();
//        dialogBackConfirmation();
    }


    private void initList() {

        patientDataArrayList = DatabaseManager.getInstance().getPatientData(mContext, ApplicationDataModel.getInstance().getHwid());

        if (patientDataArrayList != null && patientDataArrayList.size() > 0) {
            if (listAdapter == null) {
                listAdapter = new BeneficiaryListAdapter(this, patientDataArrayList);
                listView.setAdapter(listAdapter);

            } else {
                listAdapter = new BeneficiaryListAdapter(this, patientDataArrayList);
                listView.setAdapter(listAdapter);
                listAdapter.notifyDataSetChanged();
            }

            totalRegisterUserTextView.setText(patientDataArrayList.size() + "");
            totalRegisterUserTextView.invalidate();
        }


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {


            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1,
                                    int arg2, long arg3) {
                try {
                 //   String value = listView.getItemAtPosition(arg2).toString();
                  //  BeneficiaryDataHandler model = patientDataArrayList.get(arg2);

                    BeneficiaryDataHandler model= (BeneficiaryDataHandler) arg0.getAdapter().getItem(arg2);
                    ApplicationDataModel.getInstance().setPatientLid(model.getBeneficiarytId());
                    ApplicationDataModel.getInstance().setPatientGid(model.getBeneficiaryGId());
                    ApplicationDataModel.getInstance().setPatientName(model.getBeneficiaryName());
                    ApplicationDataModel.getInstance().setPatientPicName(model.getImagePath());
                    ApplicationDataModel.getInstance().setPatientSex(model.getBeneficiarySex());
                    PatientInfoDataHandler info = new PatientInfoDataHandler();
                    info.setFirstName(model.getBeneficiaryName());
                    info.setPatientId(model.getBeneficiaryGId());
                    info.setImagePath(model.getImagePath());
                    info.setGender(model.getBeneficiarySex());
                    info.setLocalId(model.getBeneficiarytId());
                     /*  ApplicationDataModel.getInstance().setPatientLid(patientDataArrayList.get(arg2).getBeneficiarytId());
                    ApplicationDataModel.getInstance().setPatientGid(patientDataArrayList.get(arg2).getBeneficiaryGId());
                    ApplicationDataModel.getInstance().setPatientName(patientDataArrayList.get(arg2).getBeneficiaryName());
                    ApplicationDataModel.getInstance().setPatientPicName(patientDataArrayList.get(arg2).getImagePath());
                    ApplicationDataModel.getInstance().setPatientSex(patientDataArrayList.get(arg2).getBeneficiarySex());
                    PatientInfoDataHandler info = new PatientInfoDataHandler();
                    info.setFirstName(patientDataArrayList.get(arg2).getBeneficiaryName());
                    info.setPatientId(patientDataArrayList.get(arg2).getBeneficiaryGId());
                    info.setImagePath(patientDataArrayList.get(arg2).getImagePath());
                    info.setGender(patientDataArrayList.get(arg2).getBeneficiarySex());
                    info.setLocalId(patientDataArrayList.get(arg2).getBeneficiarytId());
*/
                    Intent intent = new Intent(getApplicationContext(), HomeScreen.class);
                    intent.putExtra(Constants.PATIENT_DATA_INTENT, info);

                    startActivity(intent);

                } catch (Exception e) {
                    e.printStackTrace();
                }


            }
        });

       /* } catch (Exception e)
        {
            Log.d("listException", e.getLocalizedMessage());
//            DebugUtils.showException(" Problem to load mother data "+e);
        }*/


    }


    public void onAddButtonPressed() {
        Intent intent;
        requiredData = new ParserRequiredData();
        requiredData.setSsaName(Constants.SSA_NAME_REGISTRATION);
        requiredData.setHeaderTitle(getResources().getString(
                R.string.PARSER_TITLE_REGISTRATION));
        requiredData.setTable("");
        UriHandler.getInstance().setContentUri(DbConstant.URI_TABLE_REGISTRATION);

        intent = new Intent(getApplicationContext(), ParserActivity.class);
        intent.putExtra(Constants.PARSER_DATA, requiredData);
        startActivityForResult(intent, 447);

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 447) {
            if (resultCode == RESULT_OK) {
                initList();
            }
        }


    }

    public void dialogBackConfirmation() {

        AlertDialog.Builder backAlert = new AlertDialog.Builder(BeneficiarySelection.this);
        backAlert.setTitle(R.string.exit_dialog_title);
        backAlert.setPositiveButton(R.string.YES, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface arg0, int arg1) {
                // TODO Auto-generated method stub

                BeneficiarySelection.this.finish();
            }
        });

        backAlert.setNegativeButton(R.string.NO, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // TODO Auto-generated method stub
                dialog.dismiss();
            }
        });

        backAlert.show();
    }


    @Override
    public void onResult(boolean resultFlag, String message) {

    }

    @Override
    public void onResult(boolean resultFlag, String message, ArrayList<?> dataList) {

    }

    @Override
    public void onResult(boolean resultFlag, String message, Object data) {
        ArrayList<String> all_image_path = new ArrayList<>();

        if (resultFlag) {
            UploadResponseData upload_response = (UploadResponseData) data;
            HashMap all_lid = upload_response.getLid_gid();
            Iterator it = all_lid.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry) it.next();
                System.out.println(pair.getKey() + " = " + pair.getValue());
                // Get the GId and update the adapter
                DatabaseManager.getInstance().updatePatientData(mContext, DbConstant.URI_TABLE_REGISTRATION, "" + pair.getKey(), "" + pair.getValue());
                for (int i = 0; i < DbConstant.ALL_UPLOAD_URI.length; i++) {
                    DatabaseManager.getInstance().updateReportValue(mContext, DbConstant.ALL_UPLOAD_URI[i], (String) pair.getKey());
                }

                String img_path = Environment.getExternalStorageDirectory().getAbsolutePath() + "/telemedhc/pictures/" +
                        DatabaseManager.getInstance().getPicPath(mContext, DbConstant.URI_TABLE_REGISTRATION, (String) pair.getKey());
                if (Uri.parse(img_path).getLastPathSegment() != null && !Uri.parse(img_path).getLastPathSegment().isEmpty())
                    all_image_path.add(img_path);
            }

            if (listAdapter != null) {
                initList();
            }


            if ((all_image_path != null && all_image_path.size() > 0)) {
                new UploadImageTask(all_image_path).execute();
            } else if (failed_image_path != null && failed_image_path.size() > 0) {

                Type type = new TypeToken<List<ImageData>>() {
                }.getType();
                SharedPreferences appSharedPrefs = PreferenceManager
                        .getDefaultSharedPreferences(mContext.getApplicationContext());
                String json = appSharedPrefs.getString("Failed_image", "");
                Gson gson = new Gson();
                List<ImageData> img_data = gson.fromJson(json, type);
                new ReUploadImageTask(img_data).execute();
            }

        } else {
            notify.customToast(mContext, getResources().getString(R.string.info), getString(R.string.something_wrong));
        }

    }

    class UploadImageTask extends AsyncTask<String, Void, String> {
        private ProgressDialog pDialog;
        ArrayList<String> all_image_path;

        public UploadImageTask(ArrayList<String> all_image_path) {
            this.all_image_path = all_image_path;
            failed_image_path = new ArrayList<>();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(mContext);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String response = null;
            String url = Constants.UPLOAD_IMAGE_API;
            for (int i = 0; i < all_image_path.size(); i++) {
                ServiceHandler servicehandler = new ServiceHandler();
                try {
                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    Bitmap bitmap = BitmapFactory.decodeFile(all_image_path.get(i));
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 75, bos);
                    byte[] data = bos.toByteArray();
                    JSONObject json_obj = new JSONObject();
                    json_obj.put(DbConstant.COLUMN_LID, ApplicationDataModel.getInstance().getPatientLid());
                    response = servicehandler.executeHttpPostImage(url, data, json_obj, all_image_path.get(i));
                    if (response.contains("Failed")) {
                        ImageData img_data = new ImageData();
                        img_data.setLid(ApplicationDataModel.getInstance().getPatientLid());
                        img_data.setImage_path(all_image_path.get(i));
                        failed_image_path.add(img_data);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d("error", e.toString());
                }
            }
            if (failed_image_path != null && failed_image_path.size() > 0) {
                SharedPreferences sharedPreference = PreferenceManager.getDefaultSharedPreferences(mContext.getApplicationContext());
                SharedPreferences.Editor prefsEditor = sharedPreference.edit();
                Gson gson = new Gson();
                String json = gson.toJson(failed_image_path);
                prefsEditor.putString("Failed_image", json);
                prefsEditor.commit();
            }
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            if (pDialog != null && pDialog.isShowing()) {
                pDialog.cancel();
                pDialog.dismiss();
            }

            if (result != null) {
                Log.d("upload_image_response", result);
                if (result.contains("Success"))
                    notify.customToast(mContext, getResources().getString(R.string.info), getResources().getString(R.string.data_upload_successful));
                else {
                    notify.customToast(mContext, getResources().getString(R.string.info), getResources().getString(R.string.data_not_upload_successful));
                }

            }
        }

    }


    class ReUploadImageTask extends AsyncTask<String, Void, String> {
        private ProgressDialog pDialog;
        List<ImageData> failed_image_path;

        public ReUploadImageTask(List<ImageData> failed_image_path) {
            this.failed_image_path = failed_image_path;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(mContext);
            pDialog.setMessage(getString(R.string.pleasewait));
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String response = null;
            String url = Constants.UPLOAD_IMAGE_API;
            for (int i = 0; i < failed_image_path.size(); i++) {
                ServiceHandler servicehandler = new ServiceHandler();
                try {
                    ByteArrayOutputStream bos = new ByteArrayOutputStream();
                    Bitmap bitmap = BitmapFactory.decodeFile(failed_image_path.get(i).getImage_path());
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 75, bos);
                    byte[] data = bos.toByteArray();
                    JSONObject json_obj = new JSONObject();
                    json_obj.put(DbConstant.COLUMN_LID, failed_image_path.get(i).getImage_path());
                    response = servicehandler.executeHttpPostImage(url, data, json_obj, failed_image_path.get(i).getImage_path());
                    if (response.contains("Success")) {
                        Type type = new TypeToken<List<ImageData>>() {
                        }.getType();
                        SharedPreferences appSharedPrefs = PreferenceManager
                                .getDefaultSharedPreferences(mContext.getApplicationContext());
                        SharedPreferences.Editor prefsEditor = appSharedPrefs.edit();
                        String json = appSharedPrefs.getString("Failed_image", "");
                        Gson gson = new Gson();
                        List<ImageData> img_data = gson.fromJson(json, type);
                        img_data.remove(failed_image_path.get(i));
                        prefsEditor.commit();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.d("error", e.toString());
                }
            }
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            if (pDialog != null && pDialog.isShowing()) {
                pDialog.cancel();
                pDialog.dismiss();
            }

            if (result != null) {
                Log.d("upload_image_response", result);
                if (result.contains("Success"))
                    notify.customToast(mContext, getResources().getString(R.string.info), getResources().getString(R.string.data_upload_successful));
                else {
                    notify.customToast(mContext, getResources().getString(R.string.info), getResources().getString(R.string.data_not_upload_successful));
                }

            }
        }

    }

}
