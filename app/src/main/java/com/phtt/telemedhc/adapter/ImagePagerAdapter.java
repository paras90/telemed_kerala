package com.phtt.telemedhc.adapter;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.phtt.telemedhc.R;


/**
 * Created by Arvind on 23-09-2015.
 */
public class ImagePagerAdapter extends PagerAdapter {

    Context context;
    String[] image_url;
    LayoutInflater mLayoutInflater;
    private boolean isInfiniteLoop;
    ImageLoader imageLoader;
    private int default_clinic;

    public ImagePagerAdapter(Context doctorDetailActivity, String[] image_url, ImageLoader imageLoader) {
        context=doctorDetailActivity;
        this.image_url=image_url;
        this.imageLoader=imageLoader;
        mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        isInfiniteLoop = false;
    }



    public int getCount() {
        return image_url.length;
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = mLayoutInflater.inflate(R.layout.image_pager_item, container,
                false);

        final ImageView imageView = (ImageView) itemView
                .findViewById(R.id.imageView1);


        imageLoader.get(image_url[position], new ImageLoader.ImageListener() {
            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                if(response.getBitmap()!=null){
                    imageView.setImageBitmap(response.getBitmap());
                }else {
                    imageView.setImageResource(R.drawable.default_clinic);
                }
            }

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("image_load_error", "volley_image_loader");
                imageView.setImageResource(R.drawable.default_clinic);
            }
        });

        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }

    public boolean isInfiniteLoop() {
        return isInfiniteLoop;
    }


    public ImagePagerAdapter setInfiniteLoop(boolean isInfiniteLoop) {
        this.isInfiniteLoop = isInfiniteLoop;
        return this;
    }

}
