package com.phtt.telemedhc.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import com.phtt.telemedhc.R;
import java.util.ArrayList;

/**
 * Created by Arvind on 13-01-2016.
 */
public class GalleryImageAdapter extends BaseAdapter {
    private LayoutInflater inflater;
    private Context mContext;
    ArrayList<String> images_path;

    public GalleryImageAdapter(Context context,ArrayList<String> images_path) {
        mContext = context;
        this.images_path=images_path;
        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }


    @Override
    public int getCount() {
        return images_path.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    // create Holder
    private class ViewHolder {
        private ImageView imageview;

        public ViewHolder(View v) {
            imageview = (ImageView) v.findViewById(R.id.rowimageView);
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        ViewHolder holder = null;
        if (convertView == null) {
            vi = inflater.inflate(R.layout.gallery_layout, null, false);
            holder = new ViewHolder(vi);
            vi.setTag(holder);
        } else {
            holder = (ViewHolder) vi.getTag();
        }


        BitmapFactory.Options options = new BitmapFactory.Options();

        // downsizing image as it throws OutOfMemory Exception for larger images
        options.inSampleSize = 8;
        final Bitmap bitmap = BitmapFactory.decodeFile(images_path.get(position),options);
        holder.imageview.setImageBitmap(bitmap);

        return  vi;
    }
}
