package com.phtt.telemedhc.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.phtt.telemedhc.R;
import com.phtt.telemedhc.doctor.BookActivity;
import com.phtt.telemedhc.doctor.SlotBookActivity;
import com.phtt.telemedhc.models.ApplicationDataModel;
import com.phtt.telemedhc.models.ClinicData;
import com.phtt.telemedhc.models.TimeSlot;
import com.phtt.telemedhc.utils.AppointmentConstants;

import java.util.ArrayList;



/**
 * Created by Arvind on 08-10-2015.
 */
public class SlotAdapter extends BaseAdapter {

    Context context;
    ArrayList<TimeSlot> slots_data;
    LayoutInflater inflater;

    public ClinicData getClinic_data() {
        return clinic_data;
    }

    public void setClinic_data(ClinicData clinic_data) {
        this.clinic_data = clinic_data;
    }

    volatile ClinicData clinic_data;

    public SlotAdapter(Context context, ArrayList<TimeSlot> slots_data) {
        setClinic_data(ApplicationDataModel.getInstance().getClinicData());
        this.context=context;
        this.slots_data=slots_data;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }



    @Override
    public int getCount() {
        return slots_data.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    // create Holder
    private class ViewHolder {
        private TextView slot_time;
        public ViewHolder(View v) {
         slot_time= (TextView) v.findViewById(R.id.slot_time);
        }
    }

    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        View vi = convertView;
        ViewHolder holder = null;
        if (convertView == null) {
            vi = inflater.inflate(R.layout.grid_row_layout, null, false);
            holder = new ViewHolder(vi);
            vi.setTag(holder);
        } else {
            holder = (ViewHolder) vi.getTag();
        }

        holder.slot_time.setText(slots_data.get(position).getStartTime());

        if(slots_data.get(position).getSlotStatus().equalsIgnoreCase(AppointmentConstants.BOOKING_STATUS_OPEN)){
            holder.slot_time.setTextColor(Color.CYAN);

            holder.slot_time.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(context,SlotBookActivity.class);
                    Bundle bundle=new Bundle();
                    bundle.putString("time_slot",slots_data.get(position).getStartTime());
                    bundle.putString("appt_date", slots_data.get(position).getDate() );
//                    bundle.putSerializable("clinic_data", getClinic_data());
                    intent.putExtras(bundle);
                    context.startActivity(intent);
                    ((BookActivity)context).finish();
                }
            });

        }else{
            holder.slot_time.setTextColor(Color.GRAY);
        }
        return vi;
    }


}
