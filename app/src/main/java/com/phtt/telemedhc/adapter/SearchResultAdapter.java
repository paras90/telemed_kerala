package com.phtt.telemedhc.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.phtt.telemedhc.R;
import com.phtt.telemedhc.doctor.DoctorDetailActivity;
import com.phtt.telemedhc.models.ApplicationDataModel;
import com.phtt.telemedhc.models.ClinicData;
import com.phtt.telemedhc.models.DoctorData;
import com.pkmmte.view.CircularImageView;

import java.util.Collections;
import java.util.List;


/**
 * Created by Arvind on 09-10-2015.
 */
public class SearchResultAdapter extends RecyclerView.Adapter<SearchResultAdapter.MyViewHolder>
{
    private final LayoutInflater layoutInflator;
    List<ClinicData> data= Collections.EMPTY_LIST;
    Context context;
    ImageLoader imageLoader;
    ClinicData clinicData;

    public SearchResultAdapter(Activity context, List<ClinicData> data, ImageLoader imageLoader){
        this.context=context;
        this.imageLoader=imageLoader;
        this.data=data;
        layoutInflator= LayoutInflater.from(context);
    }


    @Override
    public SearchResultAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view=layoutInflator.inflate(R.layout.clinic_search_result_row, parent, false);
        MyViewHolder holder=new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final SearchResultAdapter.MyViewHolder holder, int position) {

        clinicData=data.get(position);
        holder.clinic_name.setText(clinicData.getName());
        holder.clinicAddress.setText(clinicData.getAddress());
        holder.phoneNumber.setText(clinicData.getPhone());

        DoctorData doctorData=clinicData.getDoctorDataList().get(0);
        if(doctorData!=null) {
            holder.doctor_name.setText(""+clinicData.getDoctorDataList().get(0).getName());
            if(clinicData.getDoctorDataList().get(0).getQualification()!=null &&
                    !(clinicData.getDoctorDataList().get(0).getQualification()).isEmpty())
            holder.docDgree.setText("(" +clinicData.getDoctorDataList().get(0).getQualification() +")");

            if(doctorData.getProfilePic()!=null) {
                imageLoader.get(doctorData.getProfilePic(), new ImageLoader.ImageListener() {
                    @Override
                    public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                        if (response.getBitmap() != null)
                            holder.docPic.setImageBitmap(response.getBitmap());
                        else holder.docPic.setImageResource(R.drawable.default_doctor_profile);
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("image_load_error", "volley_image_loader");
                        holder.docPic.setImageResource(R.drawable.default_doctor_profile);
                    }
                });
            }else{
                holder.docPic.setImageResource(R.drawable.default_doctor_profile);
            }
        }




    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView doctor_name;
        TextView clinic_name;
        TextView phoneNumber;
        TextView clinicAddress;
        TextView docDgree;
        CircularImageView docPic;

        public MyViewHolder(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            doctor_name = (TextView) itemView.findViewById(R.id.doc_name);
            docDgree = (TextView) itemView.findViewById(R.id.doc_degree);
            phoneNumber = (TextView) itemView.findViewById(R.id.phone_number);
            clinic_name = (TextView) itemView.findViewById(R.id.clinic_name);
            clinicAddress = (TextView) itemView.findViewById(R.id.clinic_address);
            docPic = (CircularImageView) itemView.findViewById(R.id.image_area);
        }

        @Override
        public void onClick(View v) {
            ApplicationDataModel.getInstance().setClinicData(data.get(getPosition()));
            Intent intent = new Intent(context, DoctorDetailActivity.class);
            Bundle search_bundle = new Bundle();
            search_bundle.putSerializable("clinic_data",data.get(getPosition()));
            intent.putExtras(search_bundle);
            context.startActivity(intent);
        }
    }
}
