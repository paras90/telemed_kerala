package com.phtt.telemedhc.gcm;

import android.content.Context;
import android.content.Intent;

public class CommonUtilities {

	static final String SERVER_URL = "http://10.0.2.2/gcm_server_php/register.php";

	// Google project id
	static final String SENDER_I = "396748752008";// "63371467641";//";

	/**
	 * Tag used on log messages.
	 */
	static final String TAG = "Bayyinah GCM";

	static final String DISPLAY_MESSAGE_ACTION = "com.app.cogentsea.DISPLAY_MESSAGE";

	static final String EXTRA_MESSAGE = "message";

	/**
	 * Notifies UI to display a message.
	 * <p>
	 * This method is defined in the common helper because it's used both by the
	 * UI and the background service.
	 * 
	 * @param context
	 *            application's context.
	 * @param message
	 *            message to be displayed.
	 */
	static void displayMessage(Context context, String message) {
		Intent intent = new Intent(DISPLAY_MESSAGE_ACTION);
		intent.putExtra(EXTRA_MESSAGE, message);
		context.sendBroadcast(intent);
	}
}
