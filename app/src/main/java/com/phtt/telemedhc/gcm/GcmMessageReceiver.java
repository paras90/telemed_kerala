package com.phtt.telemedhc.gcm;

import android.os.Bundle;

public interface GcmMessageReceiver {
	
	void onGcmMessage(Bundle data);

}
