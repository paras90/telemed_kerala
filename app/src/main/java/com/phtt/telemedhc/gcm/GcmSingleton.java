package com.phtt.telemedhc.gcm;


public class GcmSingleton {

	private static class SingletonHolder {
		private static final GcmSingleton INSTANCE = new GcmSingleton();
	}

	public static GcmSingleton getInstance() {
		return SingletonHolder.INSTANCE;
	}
	
	public GcmMessageReceiver getListener() {
		return listener;
	}

	public void setListener(GcmMessageReceiver listener) {
		this.listener = listener;
	}

	private GcmMessageReceiver listener;
	
}
