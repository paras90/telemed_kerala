package com.phtt.telemedhc.gcm;

import android.annotation.SuppressLint;
import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.phtt.telemedhc.R;
import com.phtt.telemedhc.ui.HomeScreen;
import com.phtt.telemedhc.zoom.JoinMeetingActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;


public class GcmIntentService extends IntentService {

    static final String TAG = "GCMDemo";

    public static final int NOTIFICATION_ID = 1;
    private NotificationManager mNotificationManager;
    NotificationCompat.Builder builder;

    GcmMessageReceiver gcmListener;
    String patient_id = "";
    String doctor_id = "";
    String type = "";
    String apptId = "";
    String received_msg = "";
    String meeting_id = "";


    public String getMeeting_id() {
        return meeting_id;
    }

    public void setMeeting_id(String meeting_id) {
        this.meeting_id = meeting_id;
    }


    public String getPatient_id() {
        return patient_id;
    }

    public void setPatient_id(String patient_id) {
        this.patient_id = patient_id;
    }

    public void setDoctor_id(String doctor_id) {
        this.doctor_id = doctor_id;
    }

    public String getDoctor_id() {
        return doctor_id;
    }

    public void setTestType(String type) {
        this.type = type;
    }

    public String getTestType() {
        return type;
    }

    public void setAppt_id(String apptId) {
        this.apptId = apptId;
    }

    public String getAppt_id() {
        return apptId;
    }


    public String getReceived_msg() {
        return received_msg;
    }

    public void setReceived_msg(String received_msg) {
        this.received_msg = received_msg;
    }

    public GcmIntentService() {
        super("GcmIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(this);
        // The getMessageType() intent parameter must be the intent you received
        // in your BroadcastReceiver.
        String messageType = gcm.getMessageType(intent);

        if (!extras.isEmpty()) {  // has effect of unparcelling Bundle
        /*
         * Filter messages based on message type. Since it is likely that GCM
		 * will be extended in the future with new message types, just ignore
		 * any message types you're not interested in, or that you don't
		 * recognize.
		 */
            if (GoogleCloudMessaging.
                    MESSAGE_TYPE_SEND_ERROR.equals(messageType)) {
                sendNotification("Send error: " + extras.toString());
            } else if (GoogleCloudMessaging.
                    MESSAGE_TYPE_DELETED.equals(messageType)) {
                sendNotification("Deleted messages on server: " +
                        extras.toString());
                // If it's a regular GCM message, do some work.
            } else if (GoogleCloudMessaging.
                    MESSAGE_TYPE_MESSAGE.equals(messageType)) {
                // This loop represents the service doing some work.

                Log.i(TAG, "Completed work @ " + SystemClock.elapsedRealtime());
                // Post notification of received message.
                sendNotification(extras);
                Log.i(TAG, "Received: " + extras.toString());
            }
        }
        // Release the wake lock provided by the WakefulBroadcastReceiver.
        GcmBroadcastReceiver.completeWakefulIntent(intent);
    }

    // Put the message into a notification and post it.
    // This is just one simple example of what you might choose to do with
    // a GCM message.

    @SuppressLint("NewApi")
    private void sendNotification(Bundle extras) {
        JSONArray mArray = null;
        JSONObject data = null;
        Intent intent = null;
        String message = null;

        String dataString = extras.getString("message");
        if (dataString == null) {
            return;
        }

        try {
            mArray = new JSONArray(dataString);
            data = mArray.getJSONObject(0);
            message = data.getString("message");
            setPatient_id(data.getString("patient_id"));
            setDoctor_id(data.getString("doctor_id"));
            setTestType(data.getString("type"));
            setAppt_id(data.getString("apptId"));
            setReceived_msg(data.getString("message"));
            if (data.getString("meeting_id") != null && !data.getString("meeting_id").isEmpty())
                setMeeting_id(data.getString("meeting_id"));
        } catch (JSONException e) {
            e.printStackTrace();
        }


        Bundle bundle = new Bundle();
        bundle.putString("patient_id", getPatient_id());
        bundle.putString("doctor_id", getDoctor_id());
        bundle.putString("type", getTestType());
        bundle.putString("apptId", getAppt_id());
        bundle.putString("message", getReceived_msg());

        if (getMeeting_id() != null && !getMeeting_id().isEmpty())
            bundle.putString("meeting_id", getMeeting_id());

        Log.d("message", bundle.toString());

        // GcmSingleton.getInstance().getListener().onGcmMessage(bundle);


        Log.d("message", extras.toString());

        if (isApplicationSentToBackground(this)) {
            TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
            if (getTestType().equalsIgnoreCase("Zoom-Meeting")) {
                intent = new Intent(getApplicationContext(), JoinMeetingActivity.class);
                intent.putExtras(bundle);
                stackBuilder.addNextIntentWithParentStack(intent);
            } else {
                intent = new Intent(getApplicationContext(), HomeScreen.class);
                intent.putExtras(bundle);
                stackBuilder.addNextIntentWithParentStack(intent);
                //stackBuilder.addParentStack(HomeScreen.class);
               // stackBuilder.addNextIntent(intent);
            }

            // Gets a PendingIntent containing the entire back stack
            PendingIntent resultPendingIntent =
                    stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
            /*PendingIntent contentIntent = PendingIntent.getActivity(getApplicationContext(), 0,
                    intent, PendingIntent.FLAG_UPDATE_CURRENT);*/
            Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

            Notification mNotification = new Notification.Builder(getApplicationContext())

                    .setContentTitle("TeleMed App")
                    .setContentText(message)
                    .setSmallIcon(R.drawable.notification)
                    .setContentIntent(resultPendingIntent)
                    .setSound(soundUri)
                    .build();

            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            mNotification.defaults |= Notification.DEFAULT_VIBRATE;
            mNotification.flags |= Notification.FLAG_AUTO_CANCEL;
            notificationManager.notify(0, mNotification);
        } else {
            if (getTestType().equalsIgnoreCase("Zoom-Meeting")) {
                intent = new Intent(getApplicationContext(), JoinMeetingActivity.class);
                intent.putExtras(bundle);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            } else {
                intent = new Intent(getApplicationContext(), HomeScreen.class);
                intent.putExtras(bundle);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK| Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            }
            Uri soundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Notification mNotification = new Notification.Builder(getApplicationContext())

                    .setContentTitle("TeleMed App")
                    .setContentText(message)
                    .setSmallIcon(R.drawable.notification)
                    .setSound(soundUri)
                    .build();

            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            mNotification.defaults |= Notification.DEFAULT_VIBRATE;
            mNotification.flags |= Notification.FLAG_AUTO_CANCEL;
            notificationManager.notify(0, mNotification);
        }
    }


    private void sendNotification(String msg) {
        mNotificationManager = (NotificationManager)
                this.getSystemService(Context.NOTIFICATION_SERVICE);

		/*PendingIntent contentIntent = PendingIntent.getActivity(this, 0,
                new Intent(this, HomeActivity.class), 0);*/

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.notification)
                        .setContentTitle("GCM Notification")
                        .setStyle(new NotificationCompat.BigTextStyle()
                                .bigText(msg))
                        .setContentText(msg);

        //mBuilder.setContentIntent(contentIntent);
        mNotificationManager.notify(NOTIFICATION_ID, mBuilder.build());
    }

    public static boolean isApplicationSentToBackground(final Context context) {
        ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        List<RunningTaskInfo> tasks = am.getRunningTasks(1);
        if (!tasks.isEmpty()) {
            ComponentName topActivity = tasks.get(0).topActivity;
            if (!topActivity.getPackageName().equals(context.getPackageName())) {
                return true;
            }
        }

        return false;
    }


    public void showError(String msg) {
        Toast.makeText(getApplicationContext(), msg, Toast.LENGTH_LONG).show();
    }

    //register / initialize the result listner
    public synchronized void registerGcmReceiver(GcmMessageReceiver gcmListener) {
        this.gcmListener = gcmListener;
    }


}
