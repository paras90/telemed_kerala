package com.phtt.telemedhc.dialogs;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.phtt.telemedhc.R;
import com.phtt.telemedhc.database.DatabaseManager;
import com.phtt.telemedhc.database.DbConstant;
import com.phtt.telemedhc.database.symptoms.SymptomDetailHandler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;


/**
 * Created by vr3v3n on 17/09/15.
 */
public class CustomDialog extends DialogFragment {

    CreateSymptomDialog listener;
    Vector<Integer> selectedIndex;
    int curImageIndex=0;

    public interface CreateSymptomDialog{
        void createSympsDialog();
        void onAddSymtomCallback(int resultCode);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        listener = (CreateSymptomDialog) activity;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {


//        final int[] drawableIdList = getArguments().getIntArray(DialogConstant.KEY_DRUG_DIALOG);

//        final int length = drawableIdList.length;

        final Dialog symDialog= new Dialog(getActivity());
        symDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        symDialog.setContentView(R.layout.custom_dialog);
        symDialog.setCancelable(false);

        final AutoCompleteTextView mSymptoms=(AutoCompleteTextView) symDialog.findViewById(R.id.symptoms);
        final Spinner mMode = (Spinner) symDialog.findViewById(R.id.onsetMode);
        final Spinner mComplaints = (Spinner) symDialog.findViewById(R.id.complaints);
        final Spinner mIllness = (Spinner) symDialog.findViewById(R.id.illness);
        final EditText mAssociate = (EditText) symDialog.findViewById(R.id.associateSym);

        final Button exitButton = (Button) symDialog.findViewById(R.id.btnClose);
        final Button addButton = (Button) symDialog.findViewById(R.id.btnAdd);

        //setting up symptom list

        Bundle arg = getArguments();
        ArrayList<SymptomDetailHandler> list = (ArrayList<SymptomDetailHandler>) arg.getSerializable(DialogConstant.KEY_DRUG_DIALOG);

        if (list != null) {
            ArrayAdapter<SymptomDetailHandler> symptomAdaptor = new ArrayAdapter<SymptomDetailHandler>(
                    getActivity(),android.R.layout.simple_dropdown_item_1line, list);

            mSymptoms.setAdapter(symptomAdaptor);
        }


        exitButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                symDialog.dismiss();

            }
        });

        addButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                if(mSymptoms.getText().toString() != null &&
                        mSymptoms.getText().toString().equalsIgnoreCase("")){

                    listener.onAddSymtomCallback(DialogConstant.RESULT_EMPTY);
                }else {
                    HashMap<String,String> values = new HashMap<String, String>();
                    values.put(DbConstant.COLUMN_1, mSymptoms.getText().toString());
                    values.put(DbConstant.COLUMN_2, mMode.getSelectedItem().toString());
                    values.put(DbConstant.COLUMN_3, mComplaints.getSelectedItem().toString());
                    values.put(DbConstant.COLUMN_4, mIllness.getSelectedItem().toString());
                    values.put(DbConstant.COLUMN_5, mAssociate.getText().toString());

                    DatabaseManager.getInstance().insertQuestionValue(getActivity(), DbConstant.C_U_T_SYMPTONS, values);
                    listener.onAddSymtomCallback(DialogConstant.RESULT_OK);
                    symDialog.dismiss();
                }
            }
        });


        return symDialog;
    }


}
