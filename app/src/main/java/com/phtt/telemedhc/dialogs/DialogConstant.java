package com.phtt.telemedhc.dialogs;

/**
 * Created by vr3v3n on 15/01/16.
 */
public class DialogConstant {

    public static final String KEY_DRUG_DIALOG = "drug_dialog";
    public static final String TAG_SYMPTOM_DIALOG = "symptom_dialog";

    public static final int RESULT_OK = 001;
    public static final int RESULT_EMPTY = 002;
}
