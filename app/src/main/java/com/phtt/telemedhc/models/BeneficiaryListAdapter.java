package com.phtt.telemedhc.models;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.phtt.telemedhc.R;

import java.util.ArrayList;


public class BeneficiaryListAdapter extends BaseAdapter implements Filterable {

    Context context;
    ArrayList<BeneficiaryDataHandler> list;
    ArrayList<BeneficiaryDataHandler> fullList;
    LayoutInflater inflater;
    SearchFilter filter = null;

    public BeneficiaryListAdapter(Context context, ArrayList<BeneficiaryDataHandler> list) {
        this.context = context;
        this.list = list;
        this.fullList = list;
        inflater = LayoutInflater.from(this.context);
        this.fullList = new ArrayList<BeneficiaryDataHandler>();
        this.fullList.addAll(list);

    }

    @Override
    public int getCount() {
        return this.list.size();
    }

    @Override
    public Object getItem(int position) {
        return this.list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return list.indexOf(getItem(getCount() - position - 1));
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_view_component, null);
            holder = new ViewHolder();
            holder.name = (TextView) convertView.findViewById(R.id.name);
            holder.id = (TextView) convertView.findViewById(R.id.id);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        BeneficiaryDataHandler data = this.list.get(position);
        holder.name.setText(data.getBeneficiaryName());
        if (data.getBeneficiaryGId() != null && !data.getBeneficiaryGId().isEmpty()) {
            holder.id.setText(data.getBeneficiaryGId());
        } else {
            holder.id.setText(data.getBeneficiarytId());
        }

        return convertView;
    }

    @Override
    public Filter getFilter() {
        if (filter == null) {
            filter = new SearchFilter();
        }

        return filter;
    }


    /*private view holder class*/
    private class ViewHolder {
        TextView name, id;

    }

    private class SearchFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {

            FilterResults results = new FilterResults();

// We implement here the filter logic
            if (constraint == null || constraint.length() == 0)

            { // No filter implemented we return all the list

                results.values = fullList;
                results.count = fullList.size();
            } else { // We perform filtering operation

                ArrayList<BeneficiaryDataHandler> nList = new ArrayList<BeneficiaryDataHandler>();

                for (BeneficiaryDataHandler p : list) {
//Add id check if required
                    if (p.getBeneficiaryName().toUpperCase().startsWith(constraint.toString().toUpperCase()))
                        nList.add(p);
                }

                results.values = nList;
                results.count = nList.size();
            }

            return results;

        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

// Now we have to inform the adapter about the new list filtered

            if (results.count == 0) {
                list = (ArrayList<BeneficiaryDataHandler>) results.values;

                notifyDataSetInvalidated();
            } else {

                list = (ArrayList<BeneficiaryDataHandler>) results.values;

                notifyDataSetChanged();
            }

        }

    }


}

