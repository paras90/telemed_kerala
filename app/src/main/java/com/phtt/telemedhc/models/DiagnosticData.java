package com.phtt.telemedhc.models;

import java.util.ArrayList;

/**
 * Created by Arvind on 16-01-2016.
 */
public class DiagnosticData {
    private String status="";
    private String message="";

    public ArrayList<String> getDiagnostic_data() {
        return diagnostic_data;
    }

    public void setDiagnostic_data(ArrayList<String> diagnostic_data) {
        this.diagnostic_data = diagnostic_data;
    }

    private ArrayList<String> diagnostic_data=null;



    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
