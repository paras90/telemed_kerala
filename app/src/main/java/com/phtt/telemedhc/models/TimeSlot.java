package com.phtt.telemedhc.models;

import java.util.ArrayList;

/**
 * Created by Kewal on 03-10-2015.
 */
public class TimeSlot {

    private String slotId="";
    private String startTime="";
    private String endTime="";
    private String slotStatus="";
    private String shift="";
    private String timeFormat="";
    private String date="";

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    private ArrayList<TimeSlot> timeList=null;

    public ArrayList<TimeSlot> getTimeList() {
        return timeList;
    }

    public void setTimeList(ArrayList<TimeSlot> timeList) {
        this.timeList = timeList;
    }

    public String getSlotId() {
        return slotId;
    }

    public void setSlotId(String slotId) {
        this.slotId = slotId;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getSlotStatus() {
        return slotStatus;
    }

    public void setSlotStatus(String slotStatus) {
        this.slotStatus = slotStatus;
    }

    public String getShift() {
        return shift;
    }

    public void setShift(String shift) {
        this.shift = shift;
    }

    public String getTimeFormat() {
        return timeFormat;
    }

    public void setTimeFormat(String timeFormat) {
        this.timeFormat = timeFormat;
    }


}
