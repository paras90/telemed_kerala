package com.phtt.telemedhc.models;

/**
 * location handler class
 * @author kewal
 * 
 */
public class LocationHandler {

	private final static LocationHandler INSTANCE = new LocationHandler();

	
	/**
	 * method to get singleton object
	 * 
	 * @return instance
	 */
	public synchronized static LocationHandler getInstance() {
		return INSTANCE;
	}

	private volatile boolean isLocatinSavaed;
	
	public boolean isLocatinSavaed() {
		return isLocatinSavaed;
	}

	public void setLocatinSavaed(boolean isLocatinSavaed) {
		this.isLocatinSavaed = isLocatinSavaed;
	}

	private volatile double currentLatitude  =0d;
	private volatile double currentLongitude =0d;

	private volatile String gsmLac="";
	private volatile String gsmCellId="";
	
	
	
	
	public String getGsmLac() {
		return gsmLac;
	}

	public void setGsmLac(String gsmLac) {
		this.gsmLac = gsmLac;
	}

	public String getGsmCellId() {
		return gsmCellId;
	}

	public void setGsmCellId(String gsmCellId) {
		this.gsmCellId = gsmCellId;
	}

	
	
	/**
	 * get current latitude
	 * @return current latitude
	 */
	public double getCurrentLatitude() {
		return currentLatitude;
	}

	/**
	 * set current latitude
	 * @param currentLatitude
	 */
	public void setCurrentLatitude(double currentLatitude) {
		this.currentLatitude = currentLatitude;
	}

	/**
	 * get current longitude
	 * @return current longitude
	 */
	public double getCurrentLongitude() {
		return currentLongitude;
	}

	/**
	 * set current longitude
	 * @param currentLongitude
	 */
	public void setCurrentLongitude(double currentLongitude) {
		this.currentLongitude = currentLongitude;
	}

	

}
