package com.phtt.telemedhc.models;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Kewal on 03-10-2015.
 */
public class ClinicData implements Serializable{

    private String name="";
    private String id="";
    private String[] imagePath=null;
    private String[] videoPath=null;
    private String address="";
    private String phone="";
    private String lat="";
    private String lon="";
    private String status="";
    private String timing="";
    private ArrayList<DoctorData> doctorDataList=null;
    private ArrayList<ServicesData> serviceList=null;

    public ArrayList<ServicesData> getServiceList() {
        return serviceList;
    }

    public void setServiceList(ArrayList<ServicesData> serviceList) {
        this.serviceList = serviceList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String[] getImagePath() {
        return imagePath;
    }

    public void setImagePath(String[] imagePath) {
        this.imagePath = imagePath;
    }

    public String[] getVideoPath() {
        return videoPath;
    }

    public void setVideoPath(String[] videoPath) {
        this.videoPath = videoPath;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLon() {
        return lon;
    }

    public void setLon(String lon) {
        this.lon = lon;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTiming() {
        return timing;
    }

    public void setTiming(String timing) {
        this.timing = timing;
    }

    public ArrayList<DoctorData> getDoctorDataList() {
        return doctorDataList;
    }

    public void setDoctorDataList(ArrayList<DoctorData> doctorDataList) {
        this.doctorDataList = doctorDataList;
    }
}
