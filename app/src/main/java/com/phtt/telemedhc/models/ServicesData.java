package com.phtt.telemedhc.models;

import java.io.Serializable;

/**
 * Created by Kewal on 06-10-2015.
 */
public class ServicesData implements Serializable{

    private String serviceID="";
    private String serviceName="";
    private String serviceDescription="";

    public String getServiceID() {
        return serviceID;
    }

    public void setServiceID(String serviceID) {
        this.serviceID = serviceID;
    }

    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    public String getServiceDescription() {
        return serviceDescription;
    }

    public void setServiceDescription(String serviceDescription) {
        this.serviceDescription = serviceDescription;
    }
}
