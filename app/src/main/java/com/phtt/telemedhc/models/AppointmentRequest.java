package com.phtt.telemedhc.models;

/**
 * Created by Kewal on 03-10-2015.
 * This class will hold the appt creation response
 * data returned from the server at runtime
 */
public class AppointmentRequest {

    private String status="";
    private String message="";
    private String patientLid="";
    private String apptId="";

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPatientLid() {
        return patientLid;
    }

    public void setPatientLid(String patientLid) {
        this.patientLid = patientLid;
    }

    public String getApptId() {
        return apptId;
    }

    public void setApptId(String apptId) {
        this.apptId = apptId;
    }
}
