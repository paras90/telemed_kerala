package com.phtt.telemedhc.models;

import java.io.Serializable;

/**
 * 
 * Created by vr3v3n on 10/06/15.
 */

public class PatientInfoDataHandler implements Serializable {

	/**

       *

        */

	private static final long serialVersionUID = 1L;

	public String getGloablId() {
		return gloablId;
	}

	public void setGloablId(String gloablId) {
		this.gloablId = gloablId;
	}

	public String getLocalId() {
		return localId;
	}

	public void setLocalId(String localId) {
		this.localId = localId;
	}

	private String gloablId = "";
	private String localId = "";
	private String PatientGlobalId="";
	private String PatientId="";

	private String DoctorId="";

	private String ClinicId="";

	private String SubUserId="";

	private String FirstName="";

	private String LastName="";

	private String Gender="";

	private String PhoneNumber="";

	private String Age="";

	private String MatiralStatus="";

	private String Email="";

	private String Address="";

	private String EmergencyContact="";

	private String EmergencyContactPerson="";

	private String TokenId;
	
	private String TokenIdData;
	
	private String imagePath;
	// TODO: need to add something here



	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	/**
	 * @return the tokenId
	 */
	public String getTokenId() {
		return TokenId;
	}

	/**
	 * @param tokenId the tokenId to set
	 */
	public void setTokenId(String tokenId) {
		TokenId = tokenId;
	}

	/**
	 * @return the tokenIdData
	 */
	public String getTokenIdData() {
		return TokenIdData;
	}

	/**
	 * @param tokenIdData the tokenIdData to set
	 */
	public void setTokenIdData(String tokenIdData) {
		TokenIdData = tokenIdData;
	}

	private String UserImagePath = "";

	public String getDoctorId() {

		return DoctorId;

	}

	public void setDoctorId(String doctorId) {

		DoctorId = doctorId;

	}

	public String getPatientId() {

		return PatientId;

	}

	public void setPatientId(String patientId) {

		PatientId = patientId;

	}

	public String getClinicId() {

		return ClinicId;

	}

	public void setClinicId(String clinicId) {

		ClinicId = clinicId;

	}

	public String getSubUserId() {

		return SubUserId;

	}

	public void setSubUserId(String subUserId) {

		SubUserId = subUserId;

	}

	public String getFirstName() {

		return FirstName;

	}

	public void setFirstName(String firstName) {

		FirstName = firstName;

	}

	public String getLastName() {

		return LastName;

	}

	public void setLastName(String lastName) {

		LastName = lastName;

	}

	public String getGender() {

		return Gender;

	}

	public void setGender(String gender) {

		Gender = gender;

	}

	public String getPhoneNumber() {

		return PhoneNumber;

	}

	public void setPhoneNumber(String phoneNumber) {

		PhoneNumber = phoneNumber;

	}

	public String getAge() {

		return Age;

	}

	public void setAge(String age) {

		Age = age;

	}

	public String getMatiralStatus() {

		return MatiralStatus;

	}

	public void setMatiralStatus(String matiralStatus) {

		MatiralStatus = matiralStatus;

	}

	public String getEmail() {

		return Email;

	}

	public void setEmail(String email) {

		Email = email;

	}

	public String getAddress() {

		return Address;

	}

	public void setAddress(String address) {

		Address = address;

	}

	public String getEmergencyContact() {

		return EmergencyContact;

	}

	public void setEmergencyContact(String emergencyContact) {

		EmergencyContact = emergencyContact;

	}

	public String getEmergencyContactPerson() {

		return EmergencyContactPerson;

	}

	public void setEmergencyContactPerson(String emergencyContactPerson) {

		EmergencyContactPerson = emergencyContactPerson;

	}

	public String getUserImagePath() {

		return UserImagePath;

	}

	public void setUserImagePath(String userImagePath) {

		UserImagePath = userImagePath;

	}

	public String getPatientGlobalId() {

		return PatientGlobalId;

	}

	public void setPatientGlobalId(String patientGlobalId) {

		PatientGlobalId = patientGlobalId;

	}

}
