package com.phtt.telemedhc.models;

/**
 * Created by Arvind on 24-12-2015.
 */
public class ImageData {
    private String lid="";
    private String image_path="";

    public String getLid() {
        return lid;
    }

    public void setLid(String lid) {
        this.lid = lid;
    }

    public String getImage_path() {
        return image_path;
    }

    public void setImage_path(String image_path) {
        this.image_path = image_path;
    }
}
