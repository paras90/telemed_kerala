package com.phtt.telemedhc.models;

import java.io.Serializable;
import java.util.ArrayList;

public class ApplicationRequiredInfo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static String APPLICATION_DATA = "application_data";
	String applicationName;
	String appPackageName;
	String visitNo;
	int id;
	MotherInformationModel motherInfomationModel;

	public ArrayList<String> getDiagnostic_name() {
		return diagnostic_name;
	}

	public void setDiagnostic_name(ArrayList<String> diagnostic_name) {
		this.diagnostic_name = diagnostic_name;
	}

	ArrayList<String> diagnostic_name=null;

	
	String selectedLanguage;            

     /**
     * @return the selectedLanguage
     */
     public String getSelectedLanguage() {
            return selectedLanguage;
     }

     /**
     * @param selectedLanguage the selectedLanguage to set
     */
     public void setSelectedLanguage(String selectedLanguage) {
            this.selectedLanguage = selectedLanguage;
     }
	
	public MotherInformationModel getMotherInfomationModel() {
		return motherInfomationModel;
	}
	public void setMotherInfomationModel(
			MotherInformationModel motherInfomationModel) {
		this.motherInfomationModel = motherInfomationModel;
	}
	public String getAppPackageName() {
		return appPackageName;
	}
	public void setAppPackageName(String appPackageName) {
		this.appPackageName = appPackageName;
	}
	public String getApplicationName() {
		return applicationName;
	}
	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}
	public String getVisitNo() {
		return visitNo;
	}
	public void setVisitNo(String visitNo) {
		this.visitNo = visitNo;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
}
