package com.phtt.telemedhc.models;

/**
 * Created by Kewal on 03-10-2015.
 */
public class RegistrationData {

    private String patientLid="";
    private String patientGid="";
    private String status="";
    private String message="";
    private String profilePic="";

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getPatientLid() {
        return patientLid;
    }

    public void setPatientLid(String patientLid) {
        this.patientLid = patientLid;
    }

    public String getPatientGid() {
        return patientGid;
    }

    public void setPatientGid(String patientGid) {
        this.patientGid = patientGid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


}
