package com.phtt.telemedhc.models;

/**
 * Created by Kajal on 05-01-2017.
 */

public class HomeScreenModel {

    private static class SingletonHolder {
        private static final HomeScreenModel mInstance = new HomeScreenModel();
    }

    public static HomeScreenModel getInstance() {
        return SingletonHolder.mInstance;
    }

    public String getPatLid() {
        return patLid;
    }

    public void setPatLid(String patLid) {
        this.patLid = patLid;
    }

    public String getPatGid() {
        return patGid;
    }

    public void setPatGid(String patGid) {
        this.patGid = patGid;
    }

    public String getPatName() {
        return patName;
    }

    public void setPatName(String patName) {
        this.patName = patName;
    }

    public String getPatImage() {
        return patImage;
    }

    public void setPatImage(String patImage) {
        this.patImage = patImage;
    }

    private volatile String patLid="";
    private volatile String patGid="";
    private volatile String patName="";
    private volatile String patImage="";

}
