package com.phtt.telemedhc.models;

import java.io.Serializable;
import java.util.List;

/**
 * Created by kajal on 05-12-2016.
 */

public class StateDistrictData implements Serializable {

    private String stateName = "";
    private String stateCode = "";
    private String districtName = "";

    public List<StateDistrictData> getDistrictDataList() {
        return districtDataList;
    }

    public void setDistrictDataList(List<StateDistrictData> districtDataList) {
        this.districtDataList = districtDataList;
    }

    private volatile List<StateDistrictData> districtDataList;

    public String getStateName() {
        return stateName;
    }

    public void setStateName(String stateName) {
        this.stateName = stateName;
    }

    public String getStateCode() {
        return stateCode;
    }

    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public String getDistrictCode() {
        return districtCode;
    }

    public void setDistrictCode(String districtCode) {
        this.districtCode = districtCode;
    }

    private String districtCode = "";

}
