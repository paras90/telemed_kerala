package com.phtt.telemedhc.models;

import java.io.Serializable;

public class MotherInformationModel implements Serializable
{
	 
		public static final String MOTHER_PARSE_DATA ="mother_parse_data";
		private String firstName ="";
		private String lastName="";
		private String gloablId="";
		private String localId="";
		private String imagePath="";
		private String age="";
		private String mctsNo="";
		private String phoneNo="";
		 
		private String childNo;
		private String mother_lid;
		private String  mother_name;
		private String child_lid;
		private String child_mcts_id;
		private String child_reg_date;
		private String child_name;
		private String child_dob;
		private String motherGender="";

		public String getFirstName() {
			return firstName;
		}
		public void setFirstName(String firstName) {
			this.firstName = firstName;
		}
		public String getLastName() {
			return lastName;
		}
		public void setLastName(String lastName) {
			this.lastName = lastName;
		}
		public String getGloablId() {
			return gloablId;
		}
		public void setGloablId(String gloablId) {
			this.gloablId = gloablId;
		}
		public String getLocalId() {
			return localId;
		}
		public void setLocalId(String localId) {
			this.localId = localId;
		}
		public String getImagePath() {
			return imagePath;
		}
		public void setImagePath(String imagePath) {
			this.imagePath = imagePath;
		}
		public String getAge() {
			return age;
		}
		public void setAge(String age) {
			this.age = age;
		}
		public String getMctsNo() {
			return mctsNo;
		}
		public void setMctsNo(String mctsNo) {
			this.mctsNo = mctsNo;
		}
		public String getPhoneNo() {
			return phoneNo;
		}
		public void setPhoneNo(String phoneNo) {
			this.phoneNo = phoneNo;
		}
		public String getChildNo() {
			return childNo;
		}
		public void setChildNo(String childNo) {
			this.childNo = childNo;
		}
		public String getMother_lid() {
			return mother_lid;
		}
		public void setMother_lid(String mother_lid) {
			this.mother_lid = mother_lid;
		}
		public String getMother_name() {
			return mother_name;
		}
		public void setMother_name(String mother_name) {
			this.mother_name = mother_name;
		}
		public String getChild_lid() {
			return child_lid;
		}
		public void setChild_lid(String child_lid) {
			this.child_lid = child_lid;
		}
		public String getChild_mcts_id() {
			return child_mcts_id;
		}
		public void setChild_mcts_id(String child_mcts_id) {
			this.child_mcts_id = child_mcts_id;
		}
		public String getChild_reg_date() {
			return child_reg_date;
		}
		public void setChild_reg_date(String child_reg_date) {
			this.child_reg_date = child_reg_date;
		}
		public String getChild_name() {
			return child_name;
		}
		public void setChild_name(String child_name) {
			this.child_name = child_name;
		}
		public String getChild_dob() {
			return child_dob;
		}
		public void setChild_dob(String child_dob) {
			this.child_dob = child_dob;
		}
		public String getMotherGender() {
			return motherGender;
		}
		public void setMotherGender(String motherGender) {
			this.motherGender = motherGender;
		}
		
	
	
}
