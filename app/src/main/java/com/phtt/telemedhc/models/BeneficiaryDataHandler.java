package com.phtt.telemedhc.models;

import java.io.Serializable;

public class BeneficiaryDataHandler implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	private String beneficiarytId="";

	private String beneficiaryGId="";

	private String beneficiarytAge="";

	private String beneficiaryName="";

	private String beneficiarySex="";

	private String beneficiaryPhone="";

	private String uploadStatus="";

	private String imagePath="";


	public String getBeneficiarytId() {
		return beneficiarytId;
	}

	public void setBeneficiarytId(String beneficiarytId) {
		this.beneficiarytId = beneficiarytId;
	}

	public String getBeneficiaryGId() {
		return beneficiaryGId;
	}

	public void setBeneficiaryGId(String beneficiaryGId) {
		this.beneficiaryGId = beneficiaryGId;
	}

	public String getBeneficiarytAge() {
		return beneficiarytAge;
	}

	public void setBeneficiarytAge(String beneficiarytAge) {
		this.beneficiarytAge = beneficiarytAge;
	}

	public String getBeneficiaryName() {
		return beneficiaryName;
	}

	public void setBeneficiaryName(String beneficiaryName) {
		this.beneficiaryName = beneficiaryName;
	}

	public String getBeneficiarySex() {
		return beneficiarySex;
	}

	public void setBeneficiarySex(String beneficiarySex) {
		this.beneficiarySex = beneficiarySex;
	}

	public String getBeneficiaryPhone() {
		return beneficiaryPhone;
	}

	public void setBeneficiaryPhone(String beneficiaryPhone) {
		this.beneficiaryPhone = beneficiaryPhone;
	}

	public String getUploadStatus() {
		return uploadStatus;
	}

	public void setUploadStatus(String uploadStatus) {
		this.uploadStatus = uploadStatus;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}


	



	





















	
	
	
}
