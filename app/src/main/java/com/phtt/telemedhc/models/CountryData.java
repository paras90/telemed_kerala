package com.phtt.telemedhc.models;

/**
 * Created by Kajal on 12-01-2017.
 */
public class CountryData {
    private String code="";
    private String phoneCode="";
    private String name="";

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getPhoneCode() {
        return phoneCode;
    }

    public void setPhoneCode(String phoneCode) {
        this.phoneCode = phoneCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
