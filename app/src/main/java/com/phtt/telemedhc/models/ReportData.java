package com.phtt.telemedhc.models;

/**
 * Created by Arvind on 16-01-2016.
 */
public class ReportData {
    private String status="";
    private String message="";
    private String report_path="";

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public String getReport_path() {
        return report_path;
    }

    public void setReport_path(String report_path) {
        this.report_path = report_path;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


}
