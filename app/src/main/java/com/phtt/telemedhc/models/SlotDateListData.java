package com.phtt.telemedhc.models;

import java.util.ArrayList;

/**
 * Created by Kewal on 05-10-2015.
 */
public class SlotDateListData {


    private String status="";
    private String message="";
    private ArrayList<TimeSlot> slotList=null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ArrayList<TimeSlot> getSlotList() {
        return slotList;
    }

    public void setSlotList(ArrayList<TimeSlot> slotList) {
        this.slotList = slotList;
    }
}
