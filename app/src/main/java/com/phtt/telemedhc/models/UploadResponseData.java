package com.phtt.telemedhc.models;

import java.util.HashMap;

/**
 * Created by Arvind on 15-12-2015.
 */
public class UploadResponseData {
    private String status="";
    private String message="";
    private HashMap<String,String> lid_gid=null;

    public HashMap<String, String> getLid_gid() {
        return lid_gid;
    }

    public void setLid_gid(HashMap<String, String> lid_gid) {
        this.lid_gid = lid_gid;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
