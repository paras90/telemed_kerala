/**
 * 
 */
package com.phtt.telemedhc.parser;

/**
 * @author varun
 *
 */
public class Heading {

	private String value;
	private String Id;
	
	public String getId() {
		return Id;
	}
	public void setId(String id) {
		Id = id;
	}
	public String getValue() {
		return value;
	}
	
	public void setValue(String value) {
		this.value = value;
	}
}
