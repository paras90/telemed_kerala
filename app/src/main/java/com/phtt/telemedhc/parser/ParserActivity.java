package com.phtt.telemedhc.parser;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.phtt.telemedhc.R;
import com.phtt.telemedhc.database.DatabaseManager;
import com.phtt.telemedhc.database.DbConstant;

import com.phtt.telemedhc.models.LocationHandler;
import com.phtt.telemedhc.models.RegistrationData;
import com.phtt.telemedhc.models.ApplicationDataModel;
import com.phtt.telemedhc.network.NetworkConstants;
import com.phtt.telemedhc.network.NetworkRequestManager;
import com.phtt.telemedhc.parser.customcomponent.CustomSpinner;
import com.phtt.telemedhc.utils.INetResult;
import com.phtt.telemedhc.network.ServiceHandler;
import com.phtt.telemedhc.parser.customcomponent.ComponentType;
import com.phtt.telemedhc.parser.customcomponent.CustomView;
import com.phtt.telemedhc.parser.customcomponent.IEvent;
import com.phtt.telemedhc.parser.customcomponent.MyView;
import com.phtt.telemedhc.utils.Constants;
import com.phtt.telemedhc.utils.Utils;

import org.json.JSONObject;
import org.xml.sax.SAXException;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Vector;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;


@SuppressWarnings("ResourceType")
public class ParserActivity extends Activity implements IEvent, INetResult {


    private IEvent listener;
    INetResult listenerINT;

    /**
     * @return the displayWidth
     */
    public int getDisplayWidth() {
        return displayWidth;
    }


    /**
     * @param displayWidth the displayWidth to set
     */
    public void setDisplayWidth(int displayWidth) {
        this.displayWidth = displayWidth;
    }


    /**
     * @return the displayHeight
     */
    public int getDisplayHeight() {
        return displayHeight;
    }


    /**
     * @param displayHeight the displayHeight to set
     */
    public void setDisplayHeight(int displayHeight) {
        this.displayHeight = displayHeight;
    }

    HashMap<String, String> values = null;
    private static final String TAG = ParserActivity.class.getName();
    NotificationsDisplay notify;
    ParserRequiredData parserRequiredData;
    String[][] prefillDataArray = null;
    SAXParser saxParser;
    Context mContext;
    // int index ;
    int pageId;
    Dialog diaReset = null;
    MyView view = null;
    Uri contentURI = null;

    String userPicName = null;
    String picName = null;

    // ArrayList<SsaTableHandler> ANMList;
    HashMap<String, CustomView> viewSubArrayList;

    HashMap<String, View> viewArrayList;

    volatile HashMap<String, String> values1 = null;

    volatile String interviewerID = null;
    volatile String userGid = null;
    // volatile String childGid=null;


    LinearLayout parserDataLayout;
    volatile List<Options> optionsList;
    volatile int hideCount = -1;
    HashMap<Integer, Integer> hideCountMap = null;

    private int displayWidth;
    private int displayHeight;
    String patientLid;
    volatile boolean reset;
    CustomView v = null;
    int result = -1;


    volatile boolean familyUpdateFlag = false;

    public boolean isFamilyUpdateFlag() {
        return familyUpdateFlag;
    }


    public void setFamilyUpdateFlag(boolean familyUpdateFlag) {
        this.familyUpdateFlag = familyUpdateFlag;
    }


    public boolean isReset() {
        return reset;
    }


    public void setReset(boolean reset) {
        this.reset = reset;
    }

    volatile boolean isCompulsoryField;

    public boolean isCompulsoryField() {
        return isCompulsoryField;
    }


    public void setCompulsoryField(boolean isCompulsoryField) {
        this.isCompulsoryField = isCompulsoryField;
    }

    volatile boolean addMoreSymptoms;


    /**
     * @return the addMoreSymptoms
     */
    public boolean isAddMoreSymptoms() {
        return addMoreSymptoms;
    }


    /**
     * @param addMoreSymptoms the addMoreSymptoms to set
     */
    public void setAddMoreSymptoms(boolean addMoreSymptoms) {
        this.addMoreSymptoms = addMoreSymptoms;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//    ActivityManagerForFinish.getInstance().addActivity(this);


//    applicationController.addActivity(this);
        //    ActivityManagerForFinish.getInstance().addActivity(this);

        listenerINT = this;
        this.getWindow().setBackgroundDrawableResource(R.drawable.background);

        mContext = ParserActivity.this;

        SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
        try {
            saxParser = saxParserFactory.newSAXParser();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }

        contentURI = UriHandler.getInstance().getContentUri();

        notify = new NotificationsDisplay(getApplicationContext());

        listener = this;
        Intent i = getIntent();
        parserRequiredData = (ParserRequiredData) i.getSerializableExtra(Constants.PARSER_DATA);
        patientLid = (String) i.getSerializableExtra("lid");
        if (contentURI.equals(DbConstant.URI_TABLE_REGISTRATION)) {
            if (patientLid != null && !patientLid.equalsIgnoreCase("")) {
                initPrefill(patientLid);
            }

        }

//    String where = DbConstant.COLUMN_LID + "=?";
//    String selection[] = {com.phtt.telemedhc.models.ApplicationDataModel.getInstance().getPatientLid()};

//       if (contentURI.equals(DbConstant.URI_HEALTH_REPORT)) {
//       String where = DbConstant.COLUMN_LID + "=?";
//       String selection[] = {ApplicationDataModel.getInstance().getPatientLid()};
//
//       result = DatabaseManager.getInstance().getQuestionValue(getApplicationContext(), contentURI, where, selection);
//       if(result>0) {
//          initPrefill();
//       }
//
//
//    }


        createLayout();

    }

    private void initPrefill(String lid) {
        try {
            // by default set id null
            UriHandler.getInstance().setId(null);

            HashMap<String, String> value = new HashMap<String, String>();
            if (lid != null && !lid.equalsIgnoreCase("")) {
                value.put(DbConstant.COLUMN_LID, lid);
            }
            value.put(DbConstant.COLUMN_LID, lid);

            prefillDataArray = DatabaseManager.getInstance().getSsaQuestionValue(getApplicationContext(), contentURI, value);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    ScrollView scrollView;
    Button btnback;
    Button btnhelp, btnreset;
    TextView newHeader;
    List<QuestionEntity> queList = null;
    String[][] answerArray;


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    public void createLayout() {

        boolean setHeading = false;
        scrollView = new ScrollView(this);
        btnback = new Button(this);
        btnhelp = new Button(this);
        btnreset = new Button(this);
        newHeader = new TextView(this);

        Display display = getWindowManager().getDefaultDisplay();
        int screenWidth = display.getWidth();


        ScrollView.LayoutParams s1 = new ScrollView.LayoutParams(
                ScrollView.LayoutParams.MATCH_PARENT,
                ScrollView.LayoutParams.MATCH_PARENT);
        scrollView.setLayoutParams(s1);
//		scrollView.setBackgroundColor(Color.parseColor("#ffffff"));
        scrollView.setBackgroundResource(R.drawable.background);
        scrollView.setScrollbarFadingEnabled(false);


        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.MATCH_PARENT);
        LinearLayout l1 = new LinearLayout(this);
        l1.setOrientation(LinearLayout.VERTICAL);
        l1.setLayoutParams(params);

        RelativeLayout linearheader = new RelativeLayout(this);
        RelativeLayout.LayoutParams lay = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
        lay.addRule(RelativeLayout.CENTER_VERTICAL);
        linearheader.setLayoutParams(lay);
//    linearheader.setBackgroundColor(Color.parseColor("#5E4C39"));
//		linearheader.setBackgroundResource(R.drawable.titlebar);


        // reset button
        btnreset.setBackgroundResource(R.drawable.reset);
        btnreset.setId(001);
        RelativeLayout.LayoutParams resetBtnParams = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        resetBtnParams.addRule(RelativeLayout.CENTER_VERTICAL);
        resetBtnParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        resetBtnParams.setMarginEnd(15);
        btnreset.setLayoutParams(resetBtnParams);
        btnreset.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {

                diaReset = new AlertDialog.Builder(mContext).setMessage(mContext.getResources().getString(R.string.RESET_MSG))
                        .setCancelable(false)
                        .setPositiveButton(mContext.getResources().getString(R.string.YES), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                setReset(true);
                                view.updateResetEvent();
                                diaReset.dismiss();
                            }
                        }).setNegativeButton(mContext.getResources().getString(R.string.NO), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                diaReset.dismiss();

                            }
                        }).create();
                diaReset.show();

            }
        });

        // help button
        btnhelp.setBackgroundResource(R.drawable.help);
        btnhelp.setId(002);
        RelativeLayout.LayoutParams helpBtnParams = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        helpBtnParams.addRule(RelativeLayout.CENTER_VERTICAL);
        helpBtnParams.addRule(RelativeLayout.LEFT_OF, btnreset.getId());
//    helpBtnParams.setMarginEnd(12);
//    helpBtnParams.setMarginStart(12);
        btnhelp.setLayoutParams(helpBtnParams);
        btnhelp.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {

                //           handle help assets for all forms
                int engId = R.drawable.form_screen;

                Utils.getInstance().helpDialog(mContext, engId);
            }
        });


        // back button
        btnback.setBackgroundResource(R.drawable.back);
        RelativeLayout.LayoutParams backBtnParams = new RelativeLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        backBtnParams.addRule(RelativeLayout.CENTER_VERTICAL);
        backBtnParams.addRule(RelativeLayout.LEFT_OF, btnhelp.getId());
        backBtnParams.setMarginStart(10);
        btnback.setLayoutParams(backBtnParams);

        btnback.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {

                ParserActivity.this.finish();

            }


        });


        // title view
        RelativeLayout.LayoutParams maintextlay = new RelativeLayout.LayoutParams(screenWidth / 2 - 60, LayoutParams.WRAP_CONTENT);
        maintextlay.addRule(RelativeLayout.CENTER_VERTICAL);
        maintextlay.addRule(RelativeLayout.LEFT_OF, btnback.getId());
        maintextlay.setMargins(screenWidth / 2 - 250, 0, 0, 0);
        newHeader.setId(003);
        newHeader.setLayoutParams(maintextlay);
        newHeader.setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
        newHeader.setTextColor(Color.parseColor("#FFFFFF"));
        newHeader.setSingleLine(false);
        newHeader.setText(parserRequiredData.getHeaderTitle());
        newHeader.setTypeface(null, Typeface.BOLD);
        newHeader.setTextSize(22);

        linearheader.addView(newHeader);
        linearheader.addView(btnback);
        linearheader.addView(btnhelp);
        linearheader.addView(btnreset);


        l1.addView(linearheader);
        LinearLayout vl = new LinearLayout(this);
        vl.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, 1));
        vl.setBackgroundResource(R.drawable.background);
        vl.setOrientation(LinearLayout.VERTICAL);
        vl.setVisibility(View.VISIBLE);
        l1.addView(vl);


        parserDataLayout = new LinearLayout(this);
        parserDataLayout.setOrientation(LinearLayout.VERTICAL);

        MyHandler handler = new MyHandler();
        String fileName = null;
        fileName = Environment.getExternalStorageDirectory() + "/" + ParserConstant.DIRECTORY_PATH + parserRequiredData.getSsaName();


        try {

            saxParser.parse(new File(fileName), handler);
            queList = handler.getQuestionEntityList();
            if (queList != null) {
                ComponentType.setmQlist(queList);
            }
            // newHeader.setText(queList.get(0).getTableName());
            // set prefill data to question
            Display mDisplay = this.getWindowManager().getDefaultDisplay();
            int temp1 = (mDisplay.getWidth() / 2) - 10;
            setDisplayWidth(temp1);
            view = new MyView(this, getDisplayWidth(), ParserActivity.this);
            viewArrayList = new HashMap<String, View>();
            viewSubArrayList = new HashMap<String, CustomView>();

            hideCountMap = new HashMap<Integer, Integer>();


            if (queList != null && prefillDataArray != null) {
                setFamilyUpdateFlag(true);
                for (int temp = 0; temp < queList.size(); temp++) {

                    for (int j = 0; j < prefillDataArray.length; j++) {

                        if (queList.get(temp).getColumnname().equalsIgnoreCase(prefillDataArray[j][0])) {
                            queList.get(temp).setPrefillData(prefillDataArray[j][1]);
                            break;

                        }
                    }

                }

                if (parserRequiredData.getHeaderTitle().equalsIgnoreCase("Registration")) {
                    if (prefillDataArray[8][1] != null && !prefillDataArray[8][1].equalsIgnoreCase("")) {

                        ApplicationDataModel.getInstance().setSavedDist(prefillDataArray[8][1]);

                    }

                }

            }

            if (queList != null) {
                answerArray = new String[queList.size()][2];
                int visiblityState = 0;

                for (int i = 0; i < queList.size(); i++) {

                    try {
                        if (queList.get(i).getInitial_visibility() == null || queList.get(i).getInitial_visibility().equalsIgnoreCase("") || queList.get(i).getInitial_visibility().toLowerCase().contains("visible")) {

                            visiblityState = View.VISIBLE;
                        } else {
                            visiblityState = View.GONE;
                        }
                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }

               /* if(VisitHandler.getInstance().isPreviewMode()){

                          v =view.generatePreviewView(queList.get(i),listener ,i);
                     }else{*/

               /*if(queList.get(i).getType().equalsIgnoreCase(ComponentType.HEADER_TYPE_TEXT)){
//                      i--;
                        v =view.generateView(queList.get(i),listener ,60);
                        flagCheck = true;
                     }
                     else{
                        if(pageId == Constants.ASHA_HANDLER_ID && flagCheck){
                           v =view.generateView(queList.get(i),listener ,i-1);
                        }
                        else{
                           v =view.generateView(queList.get(i),listener ,i);
                        }
                     }
                     System.out.println("i "+i);*/


                    v = view.generateView(queList.get(i), listener, queList.get(i).getId(), i, queList.get(i).getColumnname());


                    if (queList.get(i).getResult() != null && queList.get(i).getResult().length() > 2) {
                        setHeading = true;
                    }

                    if (v != null) {
                        LinearLayout questionLayout = new LinearLayout(this);
                        questionLayout.setOrientation(LinearLayout.VERTICAL);

                        questionLayout.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
                        questionLayout.setVisibility(visiblityState);
                        v.setGravity(Gravity.CENTER_VERTICAL);
                        questionLayout.addView(v);

                        LinearLayout verticalLine = new LinearLayout(this);
                        verticalLine.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, 1));
                        verticalLine.setBackgroundColor(Color.BLACK);
                        verticalLine.setOrientation(LinearLayout.VERTICAL);
                        verticalLine.setVisibility(View.VISIBLE);
                        questionLayout.addView(verticalLine);

                        if (setHeading) {
                            View head = view.generateHeading(queList.get(i));
                            questionLayout.addView(head);

                            LinearLayout verticalLine1 = new LinearLayout(this);
                            verticalLine1.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, 1));
                            verticalLine1.setBackgroundColor(Color.BLACK);
                            verticalLine1.setOrientation(LinearLayout.VERTICAL);
                            verticalLine1.setVisibility(visiblityState);
                            questionLayout.addView(verticalLine1);
                            setHeading = false;
                        }


                        viewArrayList.put(queList.get(i).getId(), questionLayout);
                        viewSubArrayList.put(queList.get(i).getId(), v);

                        parserDataLayout.addView(questionLayout);

                        questionLayout.setVisibility(visiblityState);


                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        scrollView.addView(parserDataLayout);

        // add the save  button
        LinearLayout.LayoutParams pl = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT, Gravity.BOTTOM
                | Gravity.BOTTOM);
        LinearLayout l2 = new LinearLayout(this);
        l2.setOrientation(LinearLayout.HORIZONTAL);
        l2.setLayoutParams(pl);
        l2.setGravity(Gravity.BOTTOM);

        // Layout only for symptom
        if (parserRequiredData != null && parserRequiredData.getSsaName().equalsIgnoreCase(Constants.SSA_NAME_ADD_SYMPTOMS)) {

            LinearLayout.LayoutParams p23 = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT);
            LinearLayout l23 = new LinearLayout(this);
            l23.setOrientation(LinearLayout.HORIZONTAL);
            l23.setGravity(Gravity.CENTER);
            l23.setBackgroundColor(Color.TRANSPARENT);
            p23.setMargins(10, 10, 10, 10);
            l23.setLayoutParams(p23);
            p23.weight = 1f;
            l2.addView(l23);
            addMoreButton = new Button(this);
            addMoreButton.setLayoutParams(new LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT));
            addMoreButton.setText("+");
            addMoreButton.setTextColor(Color.parseColor("#FFFFFF"));
            addMoreButton.setBackgroundResource(R.drawable.additem);
//                addMoreButton.setText(mContext.getResources().getString(R.string.SAVE_BUTTON_CAPTION));
            addMoreButton.setTextSize(20);
            addMoreButton.setTextColor(Color.WHITE);
            this.setResult(Constants.EXIT_CODE_SYMPTOM_SSA);
            l23.addView(addMoreButton);
         /* if(VisitHandler.getInstance().isPreviewMode()){
               saveButton.setVisibility(View.GONE);
            }*/
            addMoreButton.setOnClickListener(new Button.OnClickListener() {

                @Override
                public void onClick(View v) {

                    setAddMoreSymptoms(true);
                    handleSaveButtonEvent();

                }
            });

        }

        // Layout two part two
        LinearLayout.LayoutParams p22 = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT);
        LinearLayout l22 = new LinearLayout(this);
        l22.setOrientation(LinearLayout.HORIZONTAL);
        l22.setGravity(Gravity.BOTTOM);
        l22.setBackgroundColor(Color.WHITE);
        p22.setMargins(10, 10, 10, 10);
        l22.setLayoutParams(p22);
        p22.weight = 1f;
        l2.addView(l22);
        saveButton = new Button(this);
        saveButton.setLayoutParams(new LinearLayout.LayoutParams(
                LayoutParams.MATCH_PARENT,
                LayoutParams.MATCH_PARENT));
        saveButton.setBackgroundResource(R.drawable.button1);
        saveButton.setText(mContext.getResources().getString(R.string.SAVE_BUTTON_CAPTION));
//    saveButton.setTextSize(20);
        saveButton.setTextColor(Color.WHITE);

        l22.addView(saveButton);
      /* if(VisitHandler.getInstance().isPreviewMode()){
         saveButton.setVisibility(View.GONE);
      }*/
        saveButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                setAddMoreSymptoms(false);
                handleSaveButtonEvent();
            }
        });


        parserDataLayout.addView(l2);
        l1.addView(scrollView);
        setContentView(l1);

    }

    Button saveButton;
    Button addMoreButton;

    /**
     * the we save all data into database or check for is any field is compulsory or not
     * if field is compulsory then then we have to check is field is fill or not
     */

    public void handleSaveButtonEvent() {
        try {
            // using this we can check is all compulsory field are filled or not
            if (queList != null) {
                // here we checking all compulsory field is filed or not

                for (int i = 0; i < queList.size(); i++) {

                    if (queList.get(i).getCompulsoryfiled() != null && queList.get(i).getCompulsoryfiled().equalsIgnoreCase("true")) {
                        if (answerArray != null && ((answerArray[i][1] == null || answerArray[i][1].equalsIgnoreCase("") || (answerArray[i][1].startsWith(" "))))) {
                            //
                            setCompulsoryField(true);
                            break;
                        }
                    }
                }

            }
            if (!isCompulsoryField()) {


                values1 = new HashMap<String, String>();

                for (int i = 0; i < answerArray.length; i++) {
                    if (null != answerArray[i][1]) {
                        String value = "";
                        if (answerArray[i][1].contains("NA") || (answerArray[i][1].startsWith(" ") && answerArray[i][1].length() == 1)) {
                            value = "";
                        } else if (answerArray[i][1].contains("heading")) {

                            continue;
                        } else {
                            value = answerArray[i][1];
                        }

                        values1.put(answerArray[i][0], value);


                    }

                }

                saveData(values1);

            } else {
                setCompulsoryField(false);

                notify.customToast(getApplicationContext(), mContext.getResources().getString(R.string.info), mContext.getResources().getString(R.string.please_fill_compulsory_field));
//          Utils.getInstance().alertDialog(this, mContext.getResources().getString(R.string.info), mContext.getResources().getString(R.string.please_fill_compulsory_field));
            }
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }


    private void saveData(HashMap<String, String> values) {


        if (contentURI != null) {

            if (contentURI.equals(DbConstant.URI_TABLE_REGISTRATION)) {


                if (patientLid != null && !patientLid.equalsIgnoreCase("")) {
                    ApplicationDataModel.getInstance().setPatientLid(patientLid);
                    String where = DbConstant.COLUMN_LID + "=?";
                    String[] selection = {patientLid};
                    DatabaseManager.getInstance().updateQuestionValue(mContext, contentURI, values, where, selection);
                    //                    ParserActivity.this.finish();
                } else {
                    String lid = Utils.getInstance().generateId(mContext);
                    values1.put(DbConstant.COLUMN_LID, lid);
                    ApplicationDataModel.getInstance().setPatientLid(lid);
                   /* ApplicationDataModel.getInstance().setPatientLid(Utils.getInstance().generateId(mContext));
                    values1.put(DbConstant.COLUMN_LID, ApplicationDataModel.getInstance().getPatientLid());*/
                    DatabaseManager.getInstance().insertQuestionValue(mContext, DbConstant.URI_TABLE_REGISTRATION, values1);
                    queList = null;
//                    ParserActivity.this.finish();
                    setCompulsoryField(false);
                    notify.customToast(getApplicationContext(), mContext.getResources().getString(R.string.info), mContext.getResources().getString(R.string.data_save_successful));
                }
                if (Utils.getInstance().isInternetAvailable(mContext)) {

                    NetworkRequestManager nrm = new NetworkRequestManager(mContext, listenerINT);
                    Bundle bundle = new Bundle();

                   /* Iterator iterator = values.keySet().iterator();
                    while (iterator.hasNext()) {
                        String key = (String) iterator.next();
                        bundle.putString(key, values.get(key));
                    }*/
               /*     bundle = DatabaseManager.getInstance().getPatientToUpload(mContext,DbConstant.URI_TABLE_REGISTRATION);
                    bundle.putString("device_id", ApplicationDataModel.getInstance().getDeviceId());
                    bundle.putString("lat", String.valueOf(LocationHandler.getInstance().getCurrentLatitude()));
                    bundle.putString("long", String.valueOf(LocationHandler.getInstance().getCurrentLongitude()));*/
                    //bundle=(Bundle)values.clone();
                    nrm.generateServiceRequestParams(NetworkConstants.REQUEST_ID_REGISTRATION, bundle);

                } else {

                    //TODO save data and upload later
//                    DatabaseManager.getInstance().insertQuestionValue(mContext, DbConstant.URI_TABLE_REGISTRATION, values);
                    notify.customToast(mContext, getResources().getString(R.string.info), getResources().getString(R.string.check_internet));
                    queList = null;
                    ParserActivity.this.finish();
                    setCompulsoryField(false);
                    notify.customToast(getApplicationContext(), mContext.getResources().getString(R.string.info), mContext.getResources().getString(R.string.data_save_successful));

                }

            }


			/*answerArray=null;
            queList=null;


			setCompulsoryField(false);
			notify.customToast(getApplicationContext(), mContext.getResources().getString(R.string.info), mContext.getResources().getString(R.string.data_save_successful));
*/


        } else {
            Log.v(TAG, "No content Uri found!");
        }
    }


    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
    }

    @Override
    public void onClick(String data, int index, String columnName) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onClick(String data, String Id, int index, String columnName) {
        // TODO Auto-generated method stub

        if (data.equalsIgnoreCase("picture")) {
            setIndex(index);
            setColumnName(columnName);
            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(cameraIntent, Constants.CAMERA_PIC_REQUEST_SIDE);

        }

//    if(Id !=null && viewArrayList.get(Id)!=null){


        if (data != null) {
            answerArray[index][0] = columnName;
            answerArray[index][1] = data;

        } else if (data.equalsIgnoreCase("heading")) {

            answerArray[index][0] = columnName;
            answerArray[index][1] = data;
        } else {
            answerArray[index][0] = columnName;
            answerArray[index][1] = "";
        }
//    }


    }

    @Override
    public void onClick(String data, String Id, int componentType, int index, String columnName) {
        // TODO Auto-generated method stub

//    if(Id !=null && viewArrayList.get(Id)!=null){

        switch (componentType) {

            case ComponentType.DATE_PICKER_TYPE: {
                answerArray[index][0] = columnName;
                answerArray[index][1] = data;
                break;
            }
            case ComponentType.EDIT_TEXT_TYPE: {

                answerArray[index][0] = columnName;
                answerArray[index][1] = data;
                break;
            }

        }
//    }


    }

    // @Override
    // public boolean onKeyDown(int keyCode, KeyEvent event) {
    //    super.exitButtonPressed();
    //    return true;
    // }


    @Override
    public void onClickViewUpdate(String data, String Id, int visibiltyState, int count, int index, String columnName) {

        if (viewArrayList != null) {

//          for (int i=0;i<viewArrayList.size();i++){

            LinearLayout view = (LinearLayout) viewArrayList.get("104");
            CustomView c = (CustomView) view.getChildAt(0);
            LinearLayout l1 = (LinearLayout) c.getChildAt(0);

            try {
                EditText ed = (EditText) l1.getChildAt(1);
                String bmi = ApplicationDataModel.getInstance().getBmi();
                if (null != bmi)
                    ed.setText(bmi);
            } catch (Exception e) {
                e.printStackTrace();
            }


            if (parserDataLayout != null) {

                parserDataLayout.invalidate();

                parserDataLayout.requestLayout();
            }


//          }

        }
    }


    @Override
    public void onClick(Vector<String> questionIdVec, String data, String columnName) {
        // TODO Auto-generated method stub

    }


    @Override
    public void onClick(Vector<String> questionId, String data, String Id, String columnName) {
        // TODO Auto-generated method stub

    }

    public int getIndexNative(String[] arr, String nativeDist) {
        int index = 0;
        for (int i = 0; i < arr.length; i++) {
            if (nativeDist.equalsIgnoreCase(arr[i])) {
                index = Arrays.asList(arr).indexOf(nativeDist);
            }
        }
        return index;
    }

    @Override
    public void onClickViewUpdate(Vector<String> questionId, String data, String Id, int visibiltyState, int hideCount, int index, String columnName, String[] depArr) {
        {
            int nativeIndex = 0;
//            int visibleState = visibiltyState;
            if (viewArrayList != null) {

                int vecSize = questionId.size();

                for (int i = 0; i < vecSize; i++) {

                    String key = questionId.get(i).toLowerCase();
                    if (null != key) {

                        LinearLayout view = (LinearLayout) viewArrayList.get(key);
                        if (view != null) {

                            CustomView c = (CustomView) view.getChildAt(0);
                            LinearLayout l1 = (LinearLayout) c.getChildAt(0);

                            CustomSpinner spinner = (CustomSpinner) l1.getChildAt(1);
                            nativeIndex = getIndexNative(depArr, ApplicationDataModel.getInstance().getSavedDist());
                            spinner.setSpinnerAdapter(depArr);
                            spinner.setSelection(nativeIndex);
                        }


                        if (parserDataLayout != null) {

                            parserDataLayout.invalidate();

                            parserDataLayout.requestLayout();
                        }

                    }

                }


            }


        }
    }

    @Override
    public void onClickViewUpdate(Vector<String> questionIdVec, String data,
                                  String Id, int visibiltyState, int hideCount, int index, String columnName) {

        int visibleState = visibiltyState;
        if (viewArrayList != null) {

            int vecSize = questionIdVec.size();

            for (int i = 0; i < vecSize; i++) {

                String key = questionIdVec.get(i).toLowerCase();
                if (null != key) {

                    View view = viewArrayList.get(key);

                    if (view != null) {

                        if (hideCount == -1) {

                            if (view.getVisibility() == View.VISIBLE && view.getVisibility() == visibiltyState) {
                                visibleState = View.GONE;

                            } else if (view.getVisibility() == View.GONE && view.getVisibility() == visibiltyState) {
                                visibleState = View.VISIBLE;
                            } else {
                                visibleState = View.GONE;
                            }


                        }

                        view.setVisibility(visibleState);

                    }


                    if (parserDataLayout != null) {

                        parserDataLayout.invalidate();

                        parserDataLayout.requestLayout();
                    }

                }

            }


        }


    }


    @Override
    public <E> void onClick(ArrayList<E> data, int index, String Id, String columnName) {
        // TODO Auto-generated method stub

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == Constants.CAMERA_PIC_REQUEST_SIDE && resultCode == Activity.RESULT_OK) {
            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
            //            BitmapDrawable bitmap = new BitmapDrawable(thumbnail);

            Calendar c = Calendar.getInstance();
            int day = c.get(Calendar.DATE);
            int milliseconds = c.get(Calendar.MILLISECOND);
            int seconds = c.get(Calendar.SECOND);

            picName = "telemedhc_Picture" + seconds + milliseconds + day + ".jpeg";

            //            userPicName = picName;

            answerArray[getIndex()][0] = getColumnName();
            answerArray[getIndex()][1] = picName;

            File picStorePath = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + "/telemedhc/pictures/");
            picStorePath.mkdirs();

            FileOutputStream fOut = null;

            try {
                File save = new File(picStorePath, picName);

                userPicName = Environment.getExternalStorageDirectory().getAbsolutePath() + "/telemedhc/pictures/" + picName;
//				ApplicationDataModel.getInstance().setPatientPicName(userPicName);
                fOut = new FileOutputStream(userPicName);

                thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, fOut);


                fOut.flush();
                fOut.close();

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }

    @Override
    public void onBackPressed() {
        // TODO Auto-generated method stub
        // super.onBackPressed();
//    super.exitButtonPressed();
        finish();

    }

    @Override
    public void onResult(boolean resultFlag, String message) {

    }

    @Override
    public void onResult(boolean resultFlag, String message, ArrayList<?> dataList) {

    }

    @Override
    public void onResult(boolean resultFlag, String message, Object data) {

        if (resultFlag) {
            RegistrationData model = (RegistrationData) data;
            if (model.getStatus().equalsIgnoreCase(NetworkConstants.MESSAGE_SUCCESS)) {
                if (userPicName != null && !userPicName.isEmpty()) {
                    new UploadImageTask().execute();
                }
            }
//            ApplicationDataModel.getInstance().setPatientGid(model.getPatientGid());
//            ApplicationDataModel.getInstance().setPatientLid(model.getPatientLid());
//			values1.put(DbConstant.COLUMN_LID, ApplicationDataModel.getInstance().getPatientLid());

            if (userPicName == null) {
                setResult(447);
                ParserActivity.this.finish();
            }
           /* if (userPicName != null && !userPicName.isEmpty()) {
                new UploadImageTask().execute();
            }*/

            queList = null;

            setCompulsoryField(false);
            notify.customToast(getApplicationContext(), mContext.getResources().getString(R.string.info), mContext.getResources().getString(R.string.data_save_successful));

        } else {
            notify.customToast(getApplicationContext(), mContext.getResources().getString(R.string.info), message);

        }

    }


    class UploadImageTask extends AsyncTask<String, Void, String> {
        private ProgressDialog pDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(mContext);
            pDialog.setMessage("Please wait...");
            pDialog.setCancelable(false);
            pDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            String response = null;
            String url = Constants.UPLOAD_IMAGE_API;
            ServiceHandler servicehandler = new ServiceHandler();
            try {
                ByteArrayOutputStream bos = new ByteArrayOutputStream();
                Bitmap bitmap = BitmapFactory.decodeFile(userPicName);
                bitmap.compress(Bitmap.CompressFormat.JPEG, 75, bos);
                byte[] data = bos.toByteArray();
                JSONObject json_obj = new JSONObject();
                json_obj.put(DbConstant.COLUMN_LID, ApplicationDataModel.getInstance().getPatientLid());
                response = servicehandler.executeHttpPostImage(url, data, json_obj, picName);
            } catch (Exception e) {
                e.printStackTrace();
                e.printStackTrace();
                Log.d("error", e.toString());
            }
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            if (pDialog != null && pDialog.isShowing()) {
                pDialog.cancel();
                pDialog.dismiss();
            }

            if (result != null) {
                Log.d("upload_image_response", result);
                setResult(447);
                ParserActivity.this.finish();
            }
        }

    }


    volatile int index;
    volatile String columnName;

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
