package com.phtt.telemedhc.parser;

import java.util.ArrayList;
import java.util.List;

public class QuestionEntity {

	private String type;
	private String value;
	private String id;
	private String tpgo;
	private String imagehelp;
	private String audiohelp;
	private String videohelp;
	private String hint;
	private String inputtype;
	private String compulsoryfiled;
	private String result;
	private String condition;
	private String followup;
	private String advice;
	private String referto;
	private String refer;
	private String visit;
	private String followtiming;
	private String Category;
	private Options options;
	private String prefillData;
	private String hintTextMessage;
	private String setOptionValue;
	private String tableName;
	
	private String visiblity_condition="";
	private String dependent_qid="";
	private String initial_visibility="";
	private String columnname="";
	
	private String dependent_table="";
	private String dependent_column_name="";
	private String dependent_db_name="";

	private String min_size="";
	private String max_size="";
	private String validation_msg="";
	private String min_value="";
	private String max_value="";

	public String getVisit() {
		return visit;
	}

	public String getMin_size() {
		return min_size;
	}

	public void setMin_size(String min_size) {
		this.min_size = min_size;
	}

	public String getMax_size() {
		return max_size;
	}

	public void setMax_size(String max_size) {
		this.max_size = max_size;
	}

	public String getValidation_msg() {
		return validation_msg;
	}

	public void setValidation_msg(String validation_msg) {
		this.validation_msg = validation_msg;
	}

	public String getMin_value() {
		return min_value;
	}

	public void setMin_value(String min_value) {
		this.min_value = min_value;
	}

	public String getMax_value() {
		return max_value;
	}

	public void setMax_value(String max_value) {
		this.max_value = max_value;
	}

	private String [] autoCompleteDataList;
	

	public String getTableName() {
		
		return tableName;
	}


	public void setTableName(String tableName) {
		this.tableName = tableName;
	}


	List<Options> optionList;
	
	public QuestionEntity() {
		// TODO Auto-generated constructor stub
		optionList = new ArrayList<Options>();
	}
	

	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTpgo() {
		return tpgo;
	}
	public void setTpgo(String tpgo) {
		this.tpgo = tpgo;
	}
	public String getImagehelp() {
		return imagehelp;
	}
	public void setImagehelp(String imagehelp) {
		this.imagehelp = imagehelp;
	}
	public String getAudiohelp() {
		return audiohelp;
	}
	public void setAudiohelp(String audiohelp) {
		this.audiohelp = audiohelp;
	}
	public String getVideohelp() {
		return videohelp;
	}
	public void setVideohelp(String videohelp) {
		this.videohelp = videohelp;
	}
	public String getHint() {
		return hint;
	}
	public void setHint(String hint) {
		this.hint = hint;
	}
	public String getInputtype() {
		return inputtype;
	}
	public void setInputtype(String inputtype) {
		this.inputtype = inputtype;
	}
	public String getCompulsoryfiled() {
		return compulsoryfiled;
	}
	public void setCompulsoryfiled(String compulsoryfiled) {
		this.compulsoryfiled = compulsoryfiled;
	}
	public String getResult() {
		return result;
	}
	public void setResult(String result) {
		this.result = result;
	}
	public String getCondition() {
		return condition;
	}
	public void setCondition(String condition) {
		this.condition = condition;
	}
	public String getFollowup() {
		return followup;
	}
	public void setFollowup(String followup) {
		this.followup = followup;
	}
	public String getAdvice() {
		return advice;
	}
	public void setAdvice(String advice) {
		this.advice = advice;
	}
	public String getReferto() {
		return referto;
	}
	public void setReferto(String referto) {
		this.referto = referto;
	}
	public String getRefer() {
		return refer;
	}
	public void setRefer(String refer) {
		this.refer = refer;
	}
	public String getvisit() {
		return visit;
	}
	public void setVisit(String visit) {
		this.visit = visit;
	}
	public String getFollowtiming() {
		return followtiming;
	}
	public void setFollowtiming(String followtiming) {
		this.followtiming = followtiming;
	}
	public String getCategory() {
		return Category;
	}
	public void setCategory(String category) {
		Category = category;
	}
	public Options getOptions() {
		return options;
	}
	public void setOptions(Options options) {
		this.options = options;
	}
	public void addOptions(Options opps)
	{
		optionList.add(opps);
	}
	public List<Options> getOptionList() {
		return optionList;
	}

	public void setOptionList(List<Options> optionList) {
		this.optionList = optionList;
	}


	public String getPrefillData() {
		return prefillData;
	}


	public void setPrefillData(String prefillData) {
		this.prefillData = prefillData;
	}


	public String getHintTextMessage() {
		return hintTextMessage;
	}


	public void setHintTextMessage(String hintTextMessage) {
		this.hintTextMessage = hintTextMessage;
	}


	public String getSetOptionValue() {
		return setOptionValue;
	}


	public void setSetOptionValue(String setOptionValue) {
		this.setOptionValue = setOptionValue;
	}


	public String getDependent_qid() {
		return dependent_qid;
	}


	public void setDependent_qid(String dependent_qid) {
		this.dependent_qid = dependent_qid;
	}


	public String getVisiblity_condition() {
		return visiblity_condition;
	}


	public void setVisiblity_condition(String visiblity_condition) {
		this.visiblity_condition = visiblity_condition;
	}


	public String getInitial_visibility() {
		return initial_visibility;
	}


	public void setInitial_visibility(String initial_visibility) {
		this.initial_visibility = initial_visibility;
	}


	public String[] getAutoCompleteDataList() {
		return autoCompleteDataList;
	}


	public void setAutoCompleteDataList(String autoCompleteDataList[]) {
		this.autoCompleteDataList = autoCompleteDataList;
	}


	public String getColumnname() {
		return columnname;
	}


	public void setColumnname(String columnname) {
		this.columnname = columnname;
	}


	public String getDependent_table() {
		return dependent_table;
	}


	public void setDependent_table(String dependent_table) {
		this.dependent_table = dependent_table;
	}


	public String getDependent_column_name() {
		return dependent_column_name;
	}


	public void setDependent_column_name(String dependent_column_name) {
		this.dependent_column_name = dependent_column_name;
	}


	public String getDependent_db_name() {
		return dependent_db_name;
	}


	public void setDependent_db_name(String dependent_db_name) {
		this.dependent_db_name = dependent_db_name;
	}
}
