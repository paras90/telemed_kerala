package com.phtt.telemedhc.parser.customcomponent;

import java.util.ArrayList;
import java.util.Vector;


public interface IEvent {

	
	void onClick(String data, int index, String columnName);
	void onClick(String data, String Id, int index, String columnName);
	void onClick(String data, String Id, int componentType, int index, String columnName);
	<E> void onClick(ArrayList<E> data, int index, String Id, String columnName);
	void onClickViewUpdate(String data, String Id, int visibiltyState, int hideCount, int index, String columnName);
	
	void onClick(Vector<String> questionId, String data, String columnName);
	void onClick(Vector<String> questionId, String data, String Id, String columnName);
	void onClickViewUpdate(Vector<String> questionId, String data, String Id, int visibiltyState, int hideCount, int index, String columnName);
	void onClickViewUpdate(Vector<String> questionId, String data, String Id, int visibiltyState, int hideCount, int index, String columnName, String[] depArr);

}
