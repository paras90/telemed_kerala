package com.phtt.telemedhc.parser;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

//import com.phfi.childcare.SSAHandler;


public class MyHandler extends DefaultHandler {




    //List to hold Employees object
    private List<QuestionEntity> questionList = null;
    private QuestionEntity question = null;
    private List<Options> optionList;
    private Options option;
//	    SSAHandler ssaHandler = null;

 // Debug trying new thing
    private List<Heading> headingList =null;
    private Heading heading;
    //getter method for employee ANMList
    public List<QuestionEntity> getQuestionEntityList() {
        return questionList;
    }


    boolean optionStatus = false;

    public MyHandler() {
        if(questionList == null)
            questionList = new ArrayList<QuestionEntity>();

    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes)
            throws SAXException {


        if(qName.equalsIgnoreCase("Question"))
        {
            question = new QuestionEntity();

            question.setType(attributes.getValue("type"));
            question.setValue(attributes.getValue("value"));
            question.setId(attributes.getValue("id"));
            question.setTpgo(attributes.getValue("goto"));
            question.setImagehelp(attributes.getValue("imagehelp"));
            question.setAudiohelp(attributes.getValue("audiohelp"));
            question.setVideohelp(attributes.getValue("videohelp"));
            question.setHint(attributes.getValue("hint"));
            question.setInputtype(attributes.getValue("inputtype"));
            question.setCompulsoryfiled(attributes.getValue("compulsoryfield"));
            question.setResult(attributes.getValue("result"));
            question.setCondition(attributes.getValue("condition"));
            question.setFollowup(attributes.getValue("followup"));
            question.setAdvice(attributes.getValue("advice"));
            question.setReferto(attributes.getValue("referto"));
            question.setRefer(attributes.getValue("refer"));
            question.setVisit(attributes.getValue("visit"));
            question.setFollowtiming(attributes.getValue("followtiming"));
            question.setCategory(attributes.getValue("category"));
            question.setColumnname(attributes.getValue("columnname"));

            question.setVisiblity_condition(attributes.getValue("visiblity_condition"));
            question.setDependent_qid(attributes.getValue("dependent_qid"));
            question.setInitial_visibility(attributes.getValue("initial_visibility"));

            question.setDependent_db_name(attributes.getValue("dependent_db_name"));
            question.setDependent_table(attributes.getValue("dependent_table"));
            question.setDependent_column_name(attributes.getValue("dependent_column_name"));

            question.setMin_size(attributes.getValue("min_size"));
            question.setMax_size(attributes.getValue("max_size"));
            question.setMin_value(attributes.getValue("min_value"));
            question.setMax_value(attributes.getValue("max_value"));
            question.setValidation_msg(attributes.getValue("validation_msg"));

        }
        else if(qName.equalsIgnoreCase("options"))
            {

                if(optionList == null)
                    optionList = new ArrayList<Options>();

                    option = new Options();
                    option.setCount(attributes.getValue("count"));
                    option.setState(attributes.getValue("state"));
                    option.setValue(attributes.getValue("value"));
                    option.setVisiblityCondition(attributes.getValue("visiblity_condition"));
                    option.setDependentQid(attributes.getValue("dependent_qid"));
                    option.setDependentDbName(attributes.getValue("dependent_db_name"));
                    option.setDependentTable(attributes.getValue("dependent_table"));
                    option.setDependentColumnName(attributes.getValue("dependent_column_name"));

                    optionList.add(option);
                    question.addOptions(option);
            }
        else if(qName.equalsIgnoreCase("questionHeading")){
//
            if(headingList == null)
                headingList = new ArrayList<Heading>();

            heading = new Heading();
            heading.setValue(attributes.getValue("value"));
            heading.setId(attributes.getValue("id"));

            headingList.add(heading);


        }
    }


    /**
     * @return the headingList
     */
    public List<Heading> getHeadingList() {
        return headingList;
    }




    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equalsIgnoreCase("Question")) {

            questionList.add(question);
        }
        else if(qName.equalsIgnoreCase("options"))
        {
        }

    }



    @Override
    public void characters(char ch[], int start, int length) throws SAXException {

        if (optionStatus)
        {
        }
    }
}
