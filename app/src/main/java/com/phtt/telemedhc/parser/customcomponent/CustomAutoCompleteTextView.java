package com.phtt.telemedhc.parser.customcomponent;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.TableLayout;

import com.phtt.telemedhc.database.DatabaseManager;
import com.phtt.telemedhc.database.DbConstant;
import com.phtt.telemedhc.parser.QuestionEntity;

import java.util.ArrayList;
import java.util.Arrays;


public class CustomAutoCompleteTextView extends AutoCompleteTextView implements IResetListener{

	
	 
	Context context;
	String optionArray[];
	TableLayout.LayoutParams param;
	public CustomAutoCompleteTextView(Context con, IEvent listener, String id, int index, String columnName) {
		super(con);
		this.context = con;
		this.listenr = listener;
		setListenr(listener);
		param = new TableLayout.LayoutParams(0,TableLayout.LayoutParams.WRAP_CONTENT,0.5f);
		setLayoutParams(param);
		setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL);
		 setIndex(index);
			
//		 setTextSize(20);
		setQuestionID(id);
		setColumnName(columnName);
		// TODO Auto-generated constructor stub
	}

	public CustomAutoCompleteTextView(Context con, IEvent listener, String id) {
		super(con);
		this.context = con;
		this.listenr = listener;
		setListenr(listener);
		param = new TableLayout.LayoutParams(0,TableLayout.LayoutParams.WRAP_CONTENT,0.5f);
		setLayoutParams(param);
		setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL);
		 
		setQuestionID(id);
		// TODO Auto-generated constructor stub
	}
	
	public void initialize()
	{
		 
//		 ViewUtils.setTypeFace(context, this, ViewUtils.MEDIUM_NORMAL_SIZE, Typeface.NORMAL);
			
			initAdapter(context, getRequiredData());
		
			if(getRequiredData()!= null && getRequiredData().getPrefillData()!=null)
			{
				this.setText(getRequiredData().getPrefillData());
			}
			else
			{
				setHint(getRequiredData().getHintTextMessage());
			}
			String value = getRequiredData().getSetOptionValue() ;
			if(value !=null)
			{
				for(int i = 0; i<optionArray.length;i++)
				{
					if(optionArray[i].equalsIgnoreCase(value))
					{
						setText(value);
						listenr.onClick(value, getQuestionID(), getIndex(),getColumnName());
					}
				}
			}
			
	}

	
	/**
	 * this method init the autotextview adapter
	 * @param context
	 * @param data
	 */
	private void initAdapter(Context context, QuestionEntity data){
		
		if(null!=data){
			switch(data.getDependent_db_name())
			{

				case DbConstant.DATABASE_NAME:
				{
					// TODO need to store data list globally for re-usability
					
					try {
						String columnList[] = {data.getDependent_column_name()};
						
						ArrayList<String> list = DatabaseManager.getInstance().getAutofillDataList(context, getTableUri(data.getDependent_table()), columnList);
//					if(list!=null)
//						MedicineDataModel.getInstance().setList(list);
						
						if(null!=list && !list.isEmpty() ) {
							
							setAdapter( list.toArray(new String[list.size()]));
						}
						
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					break;
				}
				default:
					break;
					
				 
			}
		}
	}
	
	private Uri getTableUri(String tableName){
		Uri uri = null;
		
		switch(tableName)
		{
			/*case DbConstants.TABLE_PATIENT_INFO:
			{
				uri = DbConstants.C_U_T_PATIENT_INFO;
				break;
			}
			case DbConstants.TABLE_PATIENT_GEOLOCATION: 				
			{
				uri = DbConstants.C_U_T_PATIENT_GEOLOCATION;
				break;
			}
			case DbConstants.TABLE_PATIENT_CHILD_ILLNESS: 				
			{
				uri = DbConstants.C_U_T_PATIENT_CHILD_ILLNESS;
				break;
			}
			case DbConstants.TABLE_PATIENT_HISTORY_DIETARY: 			
			{
				uri = DbConstants.C_U_T_PATIENT_HISTORY_DIETARY;
				break;
			}
			case DbConstants.TABLE_PATIENT_HISTORY_DISEASES:			
			{
				uri = DbConstants.C_U_T_PATIENT_HISTORY_DISEASES;
				break;
			}
//			case DbConstants.TABLE_PATIENT_HISTORY_FAMILY: 			
//			{
//				uri = DbConstants.C_U_T_PATIENT_HISTORY_FAMILY;
//				break;
//			}
			case DbConstants.TABLE_HISTORY_MOTHER: 			
			{
				uri = DbConstants.C_U_T_HISTORY_MOTHER;
				break;
			}
			case DbConstants.TABLE_HISTORY_FATHER: 			
			{
				uri = DbConstants.C_U_T_HISTORY_FATHER;
				break;
			}
			case DbConstants.TABLE_HISTORY_MOTHERS_FAMILY:
			{
				uri = DbConstants.C_U_T_HISTORY_MOTHERS_FALILY;
				break;
			}
			case DbConstants.TABLE_HISTORY_FATHERS_FAMILY: 			
			{
				uri = DbConstants.C_U_T_HISTORY_FATHERS_FAMILY;
				break;
			}
			case DbConstants.TABLE_HISTORY_SPOUSE: 			
			{
				uri = DbConstants.C_U_T_HISTORY_SPOUSE;
				break;
			}
			case DbConstants.TABLE_HISTORY_SIBLING: 			
			{
				uri = DbConstants.C_U_T_HISTORY_SIBLING;
				break;
			}
			case DbConstants.TABLE_HISTORY_CHILDREN: 			
			{
				uri = DbConstants.C_U_T_HISTORY_CHILDREN;
				break;
			}
			case DbConstants.TABLE_PATIENT_HISTORY_LIFESTYLE:			
			{
				uri = DbConstants.C_U_T_PATIENT_HISTORY_LIFESTYLE;
				break;
			}
			case DbConstants.TABLE_PATIENT_HISTORY_OBSTETRIC:			
			{
				break;
			}
			case DbConstants.TABLE_PATIENT_HISTORY_OCCUPATION: 		
			{
				uri = DbConstants.C_U_T_PATIENT_HISTORY_OCCUPATION;
				break;
			}
			case DbConstants.TABLE_PATIENT_HISTORY_PSYCHIATRIC:		
			{
				uri = DbConstants.C_U_T_PATIENT_HISTORY_PSYCHIATRIC;
				break;
			}
			case DbConstants.TABLE_PATIENT_HISTORY_TRAVEL:				
			{
				uri = DbConstants.C_U_T_PATIENT_HISTORY_TRAVEL;
				break;
			}
			case DbConstants.TABLE_PATIENT_HISTORY_TREATMENT:			
			{
				uri = DbConstants.C_U_T_PATIENT_HISTORY_TREATMENT;
				break;
			}
			case DbConstants.TABLE_PATIENT_TREATMENT_FOLLOWUP:			
			{
				uri = DbConstants.C_U_T_PATIENT_TREATMENT_FOLLOWUP;
				break;
			}
			case DbConstants.TABLE_PATIENT_TREATMENT_GIVEN:			
			{
				uri = DbConstants.C_U_T_PATIENT_TREATMENT_GIVEN;
				break;
			}*/
			case DbConstant.TABLE_SYMPTONS:
			{
				uri = DbConstant.C_U_T_SYMPTONS;
				break;
			}
			/*case DbConstant.TABLE_COMPLAINTS_SYMPTOM:
			{
				uri = DbConstant.C_U_T_COMPLAINTS_SYMPTOM;
				break;
			}*/
			/*case DbConstants.TABLE_SYMPTONS_INVESTIGATIONS:
			{
				uri = DbConstants.C_U_T_SYMPTONS_INVESTIGATIONS;
				break;
			}
			case DbConstants.TABLE_CHECKUP:							
			{
				uri = DbConstants.C_U_T_CHECKUP;
				break;
			}
			case DbConstants.TABLE_COMPLAINTS_AILMENT:					
			{
				uri = DbConstants.C_U_T_COMPLAINTS_AILMENT;
				break;
			}
			case DbConstants.TABLE_COMPLAINTS_FEVER:					
			{
				uri = DbConstants.C_U_T_COMPLAINTS_FEVER;
				break;
			}
			case DbConstants.TABLE_COMPLAINTS_GENERAL_QUESTIONS:		
			{
				uri = DbConstants.C_U_T_COMPLAINTS_GENERAL_QUESTIONS;
				break;
			}
			case DbConstants.TABLE_COMPLAINTS_PAIN:					
			{
				uri = DbConstants.C_U_T_COMPLAINTS_PAIN;
				break;
			}

			case DbConstants.TABLE_DOC_HEALTHCUBE_RATING:				
			{
				uri = DbConstants.C_U_T_DOC_HEALTHCUBE_RATING;
				break;
			}
			case DbConstants.TABLE_DOC_INFO:
			{
				uri = DbConstants.C_U_T_DOC_INFO;
				break;
			}
			case DbConstants.TABLE_DRUGS_CATEGORY_DETAILS:				
			{
				uri = DbConstants.C_U_T_DRUGS_CATEGORY_DETAILS;
				break;
			}
			case DbConstants.TABLE_DRUGS_CONTRA_INDICATIONS_DETAILS: 	
			{
				uri = DbConstants.C_U_T_DRUGS_CONTRA_INDICATIONS_DETAILS;
				break;
			}
			case DbConstants.TABLE_DRUGS_MEDICINE_DETAILS: 			
			{
				uri = DbConstants.C_U_T_DRUGS_MEDICINE_DETAILS;
				break;
			}
			case DbConstants.TABLE_DRUGS_SALT_DETAILS: 				
			{
				uri = DbConstants.C_U_T_DRUGS_SALT_DETAILS;
				break;
			}
			case DbConstants.TABLE_DRUGS_SUBCATEGORY_DETAILS:			
			{
				uri = DbConstants.C_U_T_DRUGS_SUBCATEGORY_DETAILS;
				break;
			}
			 		
			case DbConstants.TABLE_PATIENT_TREATMENT_NUTRITION:
			{
				uri = DbConstants.C_U_T_PATIENT_TREATMENT_NUTRITION;
				break;
			} 		*/
			default:
				break;
		}
		return uri;
}
	
	String columnName;
	 
	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	
		int index;
		 
		 public int getIndex() {
			return index;
		}
		public void setIndex(int index) {
			this.index = index;
		}
		
	 String questionID;
		public String getQuestionID() {
			return questionID;
		}

		public void setQuestionID(String questionID) {
			this.questionID = questionID;
		}
		
	 
	
	QuestionEntity questionEntity;
	public void setRequiredData(QuestionEntity data)
	{
		this.questionEntity = data;
	}
	
	public QuestionEntity getRequiredData()
	{
		return questionEntity;
	}
	
	
	
	IEvent listenr;
	public IEvent getListenr() {
		return listenr;
	}

	public void setListenr(IEvent listenr) {
		this.listenr = listenr;
	}

	
	@SuppressLint("ClickableViewAccessibility")
	public void setAdapter(String[] arr)
	{
		optionArray = arr;
		 //Creating the instance of ArrayAdapter containing ANMList of language names  
        ArrayAdapter<String> adapter = new ArrayAdapter<String>  
         (context,android.R.layout.select_dialog_item,arr);  
     //Getting the instance of AutoCompleteTextView  
        this.setThreshold(0);//will start working from first character  
        this.setAdapter(adapter);//setting the adapter data into the AutoCompleteTextView  
       // this.setTextColor(Color.RED);  
        this.setValidator(new Validator());
        
        this.setOnTouchListener(new OnTouchListener(){
     	  
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				CustomAutoCompleteTextView.this.showDropDown();
				return false;
			}
     	});
        
        this.setOnItemClickListener(new OnItemClickListener() {

            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
            	String value = arg0.getItemAtPosition(arg2).toString();
            	listenr.onClick(value, getQuestionID(), getIndex(),getColumnName());
            }
        });
        
	}
	

	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		// TODO Auto-generated method stub
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
		
		CustomAutoCompleteTextView.this.listenr.onClick(CustomAutoCompleteTextView.this.getText().toString(), getQuestionID(),getIndex(), getColumnName());
    	
	}
	
	@SuppressLint("ClickableViewAccessibility")
	public <E> void setAdapter(final ArrayList<E> arr, final String id, int pageId)
	{
		
		ArrayAdapter<E> adapter = new ArrayAdapter<E>(context, android.R.layout.select_dialog_item, arr);
		this.setThreshold(0);
		this.setAdapter(adapter);
		this.setValidator(new Validator());
	
		this.setOnTouchListener(new OnTouchListener() {
			
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				
				CustomAutoCompleteTextView.this.showDropDown();
				return false;
			}
		});
		
		  this.setOnItemClickListener(new OnItemClickListener() { 

	            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

	            	listenr.onClick(arr, arg2, id,getColumnName());
				}
	        });
	
	}
	

	 class Validator implements AutoCompleteTextView.Validator {

	        @Override
	        public boolean isValid(CharSequence text) {
	           
	        	if(optionArray !=null){
	        		Arrays.sort(optionArray);
	        		if (Arrays.binarySearch(optionArray, text.toString()) > -1) {
	        			return true;
	        		}
	        	}

	            return false;
	        }

	        @Override
	        public CharSequence fixText(CharSequence invalidText) {
	        	
	        	CustomAutoCompleteTextView.this.listenr.onClick(CustomAutoCompleteTextView.this.getText().toString(), getQuestionID(),getIndex(), getColumnName());
	        	//CustomAutoCompleteTextView.this.getText().toString();
	            return CustomAutoCompleteTextView.this.getText().toString();
	        }
	    }
	 
	
	    class FocusListener implements OnFocusChangeListener {

	        @Override
	        public void onFocusChange(View v, boolean hasFocus) {
	           
	            if (/*v.getId() == R.id.autoCompleteTextView1 &&*/ !hasFocus) {
	                ((AutoCompleteTextView)v).performValidation();
	                CustomAutoCompleteTextView.this.listenr.onClick(CustomAutoCompleteTextView.this.getText().toString(), getQuestionID(),getIndex(), getColumnName());
		        	
	            }
	        }
	    }


		@Override
		public void resetField() {
			// TODO Auto-generated method stub
			this.setText("");
			this.invalidate();
		}
	  
	    
	
}
