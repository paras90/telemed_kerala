package com.phtt.telemedhc.parser.jsonparser;

/**
 * Created by Kewal on 05-10-2015.
 */
public class JsonParserConstant {

    public static final String PARSER_KEY_HEADER                                = "response_data";
    public static final String PARSER_KEY_STATUS                                = "status";
    public static final String PARSER_KEY_MESSAGE                               = "message";
    public static final String PARSER_KEY_PATIENT_LID                           = "patient_lid";
    public static final String PARSER_KEY_PATIENT_GID                           = "patient_gid";
    public static final String PARSER_KEY_UPLOAD_STATUS                         = "up_status";

    public static final String PARSER_KEY_CLINIC_DATA                           = "clinic_data";
    public static final String PARSER_KEY_CLINIC_ID                             = "id";
    public static final String PARSER_KEY_CLINIC_NAME                           = "name";
    public static final String PARSER_KEY_CLINIC_IMAGES                         = "images_path";
    public static final String PARSER_KEY_CLINIC_VIDEOS                         = "video_path";
    public static final String PARSER_KEY_CLINIC_ADDRESS                        = "address";
    public static final String PARSER_KEY_CLINIC_PHONE                          = "phone";
    public static final String PARSER_KEY_CLINIC_LATITUDE                       = "lat";
    public static final String PARSER_KEY_CLINIC_LONGITUDE                      = "lon";
    public static final String PARSER_KEY_CLINIC_STATUS                         = "status";
    public static final String PARSER_KEY_CLINIC_TIMING                         = "timing";

    public static final String PARSER_KEY_DOCTOR_DATA                           = "doctor_data";
    public static final String PARSER_KEY_DOCTOR_NAME                           = "name";
    public static final String PARSER_KEY_DOCTOR_ID                             = "id";
    public static final String PARSER_KEY_DOCTOR_DEGREE                         = "qualification";
    public static final String PARSER_KEY_DOCTOR_EXPERIENCE                     = "experience";
    public static final String PARSER_KEY_DOCTOR_ASSOCIATION                    = "association";
    public static final String PARSER_KEY_DOCTOR_FEES                           = "fees";
    public static final String PARSER_KEY_DOCTOR_SPECIALITY                     = "speciality";
    public static final String PARSER_KEY_DOCTOR_AVAILABILITY                   = "availability";
    public static final String PARSER_KEY_DOCTOR_TIMING                         = "timing";
    public static final String PARSER_KEY_DOCTOR_MOBILE                         = "mobile";
    public static final String PARSER_KEY_DOCTOR_PROFILE_PIC                    = "profile_pic";

    public static final String PARSER_KEY_APPOINTMENT_ID                        = "appt_id";

    public static final String PARSER_KEY_TIME_SLOT_DATA                        = "slot_data";
    public static final String PARSER_KEY_TIME_SLOT_ID                          = "slot_id";
    public static final String PARSER_KEY_START_TIME                            = "start_time";
    public static final String PARSER_KEY_END_TIME                              = "end_time";
    public static final String PARSER_KEY_SLOT_STATUS                           = "slot_status";
    public static final String PARSER_KEY_SHIFT                                 = "shift";
    public static final String PARSER_KEY_TIME_FORMAT                           = "time_format";

    public static final String PARSER_KEY_SERVICES_DATA                         = "services";
    public static final String PARSER_KEY_SERVICE_NAME                          = "service_name";
    public static final String PARSER_KEY_SERVICE_ID                            = "service_id";
    public static final String PARSER_KEY_SERVICE_DESCRIPTION                   = "description";
    public static final String PARSER_KEY_DIAGNOSTIC_LIST                       = "diagnostic_list";
    public static final String PARSER_KEY_DIAGNOSTIC_KEY                        = "diagnostic_name";
    public static final String PARSER_KEY_REPORT_PATH                           = "report_path";



    public static final String PARSER_KEY_FEED_ID                               = "id";
    public static final String PARSER_KEY_FEED_PROFILE_PIC_URL                  = "profile_pic_url";
    public static final String PARSER_KEY_FEED_VIDEO_URL                        = "videos_url";
    public static final String PARSER_KEY_FEED_HEADING                          = "heading";
    public static final String PARSER_KEY_FEED_SUB_HEADING                      = "sub_heading";
    public static final String PARSER_KEY_FEED_DESCRIPTION                      = "description";
    public static final String PARSER_KEY_FEED_PUBLISH_DATE                     = "publish_date";
    public static final String PARSER_KEY_FEED_AUTHOR_NAME                      = "author_name";
    public static final String PARSER_KEY_FEED_AUTHOR_PIC_URL                   = "author_pic_url";
    public static final String PARSER_KEY_FEED_AUTHOR_DETAIL                    = "author_detail";


    /**
     *
     *   User Treatment Model Section
     */

    public static final String PARSER_KEY_TREATMENT_HEADER = "treatment_data";

    public static final String PARSER_KEY_DOC_NAME = "doctor_name";
    public static final String PARSER_KEY_DOC_ID = "doctor_id";
    public static final String PARSER_KEY_VISIT_DATE = "visit_date";

    public static final String PARSER_KEY_SYMPTOMS_DETAILS = "symptoms_details";
    public static final String PARSER_KEY_DIAGNOSTIC_DETAILS = "diagnostics_details";
    public static final String PARSER_KEY_MEDICATON_DETAILS = "medication_details";

    public static final String PARSER_KEY_SYM_NAME = "symptom_name";

    public static final String PARSER_KEY_DIAG_NAME = "diagnostics_name";
    public static final String PARSER_KEY_DIAG_RESULT = "diagnostics_result";

    public static final String PARSER_KEY_MED_NAME = "med_name";
    public static final String PARSER_KEY_MED_SALT = "med_salt";
    public static final String PARSER_KEY_MED_DOSAGE = "med_dosage";
    public static final String PARSER_KEY_MED_MANUFACTURE = "med_manufacture";
    public static final String PARSER_KEY_MED_PRICE = "med_price";
    public static final String PARSER_KEY_MED_PRSCRIPTION_DAYS = "med_prescription_days";

    /***
     *   END
     */

}
