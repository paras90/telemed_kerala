package com.phtt.telemedhc.parser;

import android.net.Uri;


public class UriHandler {

	private UriHandler(){}

	private static class SingletonHolder {
		private static final UriHandler mInstance = new UriHandler();
	}

	public static UriHandler getInstance() {
		return SingletonHolder.mInstance;
	}
	
	private volatile Uri contentUri;

	public Uri getContentUri() {
		return contentUri;
	}

	public void setContentUri(Uri contentUri) {
		this.contentUri = contentUri;
	}
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	private volatile String id;
	
}
