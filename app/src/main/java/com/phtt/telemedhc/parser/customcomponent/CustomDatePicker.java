package com.phtt.telemedhc.parser.customcomponent;

import android.app.DatePickerDialog.OnDateSetListener;
import android.content.Context;
import android.text.format.Time;
import android.widget.DatePicker;
import android.widget.TableLayout;

import com.phtt.telemedhc.parser.QuestionEntity;

import java.util.Calendar;
import java.util.Date;


public class CustomDatePicker extends DatePicker implements OnDateSetListener,IResetListener {
	private static final String TAG = "CustomDatePicker";
	TableLayout.LayoutParams param;
	long maxDate;

	public CustomDatePicker(Context context, IEvent listener, String id, int index, String columnName) {
		super(context);
		
		 maxDate =  new Date().getTime();
		 
		param = new TableLayout.LayoutParams(0,TableLayout.LayoutParams.WRAP_CONTENT,0.2f);

		setLayoutParams(param);
//		setLay
		setListenr(listener);
		setIndex(index);
		setQuestionID(id);
		setColumnName(columnName);
		
		Calendar c = Calendar.getInstance();
		try {
			
			this.setMaxDate(maxDate);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		 init(c.get(Calendar.YEAR), (c.get(Calendar.MONTH)), c.get(Calendar.DATE), dateSetListener);
		 String data = getYear()+"-"+(getMonth()+1)+"-"+(getDayOfMonth());

	     listenr.onClick(data, getQuestionID(),getIndex(), getColumnName());
		 
//	     ViewUtils.setTypeFace(context, this, ViewUtils.MEDIUM_NORMAL_SIZE, Typeface.NORMAL);
			
	}
	
	 public void initialize(){
		 
//		 listenr.onClick("", getQuestionID(),getIndex(), getColumnName());
		 
		 if(getRequiredData() != null && getRequiredData().getPrefillData()!=null )
		 {
			 try {
				String tempDate = getRequiredData().getPrefillData();
//				 Log.v(TAG, "initialize() tempDate : "+tempDate);
				 String[] arrDate =tempDate.split("-");
				 if(tempDate.equalsIgnoreCase("")){
					 Calendar c = Calendar.getInstance();
						
//					 init(c.get(Calendar.YEAR), (c.get(Calendar.MONTH)), c.get(Calendar.DATE), dateSetListener);
					 this.updateDate((c.get(Calendar.YEAR)), (c.get(Calendar.MONTH)), (c.get(Calendar.DATE)));
				 }else{
					 this.updateDate(Integer.parseInt(arrDate[0]), (Integer.parseInt(arrDate[1])-1), Integer.parseInt(arrDate[2]));
				 }
				 
				 listenr.onClick(tempDate, getQuestionID(),getIndex(), getColumnName());
				
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		 }
			
	}

		
	 String columnName;
	 
	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	
		int index;
		 
		 public int getIndex() {
			return index;
		}
		public void setIndex(int index) {
			this.index = index;
		}
		
	 String questionID;
		public String getQuestionID() {
			return questionID;
		}

		public void setQuestionID(String questionID) {
			this.questionID = questionID;
		}
		
	
	QuestionEntity questionEntity;
	public void setRequiredData(QuestionEntity data)
	{
		this.questionEntity = data;
	}
	
	public QuestionEntity getRequiredData()
	{
		return questionEntity;
	}
	
	
	
	IEvent listenr;
	public IEvent getListenr() {
		return listenr;
	}

	public void setListenr(IEvent listenr) {
		this.listenr = listenr;
	}
	
	private OnDateChangedListener dateSetListener = new OnDateChangedListener() {

	    public void onDateChanged(DatePicker view, int year, int monthOfYear,
	            int dayOfMonth) {
//	         Calendar c = Calendar.getInstance();
//	         c.set(year, monthOfYear, dayOfMonth);
//	         String data = year+"-"+(monthOfYear+1)+"-"+(dayOfMonth);
//	         String data = year+"-"+(monthOfYear)+"-"+(dayOfMonth);
	         
//	         Calendar c = Calendar.getInstance();
				String date="";
//				if(year<= c.get(Calendar.YEAR) && (monthOfYear+1)<=  (c.get(Calendar.MONTH)+1) && dayOfMonth<= c.get(Calendar.DATE)){
					date = year+"-"+(monthOfYear+1)+"-"+(dayOfMonth);
					
		/*		}else{
					
					date = c.get(Calendar.YEAR)+"-"+(c.get(Calendar.MONTH)+1)+"-"+c.get(Calendar.DATE);
					
				}*/
				
		        listenr.onClick(date, getQuestionID(),getIndex(), getColumnName());
	         System.out.println(" on date set "+date);
	    }
	};
	

	
	


	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
//		Calendar c = Calendar.getInstance();
//		 init(c.get(Calendar.YEAR), (c.get(Calendar.MONTH)), c.get(Calendar.DATE), dateSetListener);
		 
//		 String data = getYear()+"-"+(getMonth()+1)+"-"+(getMonth());
		 
		 Calendar c = Calendar.getInstance();
		 try {
				this.setMaxDate(maxDate);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
			String date="";
			if(getYear()<= c.get(Calendar.YEAR) && (getMonth()+1)<=  (c.get(Calendar.MONTH)+1) && getMonth()<= c.get(Calendar.DATE)){
				date = this.getYear()+"-"+(this.getMonth()+1)+"-"+this.getDayOfMonth();
			}else{

				try {
						this.setMaxDate(maxDate);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				date = c.get(Calendar.YEAR)+"-"+(c.get(Calendar.MONTH)+1)+"-"+c.get(Calendar.DATE);
				
			}
			
	        listenr.onClick(date, getQuestionID(),getIndex(), getColumnName());
	  //System.out.println(" getQ"+getQuestionID()+"   on meseaure ");      
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	}

	@Override
	public void onDateSet(DatePicker view, int year, int monthOfYear,
			int dayOfMonth) {
//		 String data = year+"-"+(monthOfYear+1)+"-"+(dayOfMonth);
		 
		 Calendar c = Calendar.getInstance();
		
			String date="";
			if(year<= c.get(Calendar.YEAR) && (monthOfYear+1)<=  (c.get(Calendar.MONTH)+1) && dayOfMonth<= c.get(Calendar.DATE)){
				date = year+"-"+(monthOfYear+1)+"-"+(dayOfMonth);
			}else{
				
				this.setMaxDate(maxDate);
				date = c.get(Calendar.YEAR)+"-"+(c.get(Calendar.MONTH)+1)+"-"+c.get(Calendar.DATE);
				
			}
//			else{
//				notify.customToast(getApplicationContext(), mContext.getResources().getString(R.string.info), mContext.getResources().getString(R.string.no_child_dob_greater_dn_current_date));
//				return;
//			}
			
	        listenr.onClick(date, getQuestionID(),getIndex(), getColumnName());
		
	}
	
	@Override
	public void resetField() {
		
		Time now = new Time();
		now.setToNow();
		this.updateDate(now.year, now.month, now.monthDay);
	}


}
