package com.phtt.telemedhc.parser.customcomponent;

import android.content.Context;
import android.text.Editable;
import android.text.Html;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.widget.EditText;
import android.widget.TableLayout;

import com.phtt.telemedhc.database.DatabaseManager;
import com.phtt.telemedhc.models.ApplicationDataModel;
import com.phtt.telemedhc.parser.QuestionEntity;
import com.phtt.telemedhc.utils.Utils;


public class CustomEditText extends EditText implements OnFocusChangeListener,IResetListener {

	private static final String TAG = "CustomEditText";


	TableLayout.LayoutParams param;


	public CustomEditText(Context context, IEvent listener, String id, int index, String columnName) {
		super(context);
		setListenr(listener);
		// TODO Auto-generated constructor stub
		param = new TableLayout.LayoutParams(0,TableLayout.LayoutParams.WRAP_CONTENT,0.5f);
		setLayoutParams(param);
		setGravity(Gravity.CENTER_VERTICAL);
		setFocusable(true);


		setOnFocusChangeListener(this);
		setQuestionID(id);
		addTextChangedListener(watch);
		setIndex(index);
		setColumnName(columnName);
		
		}
	
	
	 public void initialize()
		{
			try {

				Log.v(TAG, "getRequiredData().getPrefillData(): "+ getRequiredData().getPrefillData());
				if(getRequiredData()!= null && getRequiredData().getPrefillData()!=null)
				{

					 this.setText(getRequiredData().getPrefillData());
					
					 listenr.onClick(getRequiredData().getPrefillData(), getQuestionID(), getIndex(), getColumnName());
				}else {
					String result="";
					if(getQuestionID().equalsIgnoreCase("105")){
						result = DatabaseManager.getInstance().getBPData(ApplicationDataModel.getInstance().getPatientLid());

					}else if(getQuestionID().equalsIgnoreCase("106")){
						//TODO pulse
						result = DatabaseManager.getInstance().getPulseData(ApplicationDataModel.getInstance().getPatientLid());


					}else if(getQuestionID().equalsIgnoreCase("108")){
						//TODO blood group
						result = DatabaseManager.getInstance().getBloodGroupData(ApplicationDataModel.getInstance().getPatientLid());

					}else if(getQuestionID().equalsIgnoreCase("109")){
						//TODO bg
						result = DatabaseManager.getInstance().getBloodGlucoseData(ApplicationDataModel.getInstance().getPatientLid());

					}else if(getQuestionID().equalsIgnoreCase("110")){
						//TODO hg
						result = DatabaseManager.getInstance().getHaemoglobinData(ApplicationDataModel.getInstance().getPatientLid());

					}else if(getQuestionID().equalsIgnoreCase("111")){
						//TODO urine
						result = DatabaseManager.getInstance().getUrineProteinData(ApplicationDataModel.getInstance().getPatientLid());

					}else if(getQuestionID().equalsIgnoreCase("113")){
						//TODO malaria
						result = DatabaseManager.getInstance().getMaleriaData(ApplicationDataModel.getInstance().getPatientLid());

					}else if(getQuestionID().equalsIgnoreCase("114")){
						//TODO Typhoid
						result = DatabaseManager.getInstance().getTyphoidData(ApplicationDataModel.getInstance().getPatientLid());
					}else if(getQuestionID().equalsIgnoreCase("115")){
						//TODO hepa b
						result = DatabaseManager.getInstance().getHepatitisBData(ApplicationDataModel.getInstance().getPatientLid());
					}else if(getQuestionID().equalsIgnoreCase("116")){
						//TODO Pregnancy
						result = DatabaseManager.getInstance().getPregnancyData(ApplicationDataModel.getInstance().getPatientLid());
					}else if(getQuestionID().equalsIgnoreCase("117")){
						//TODO Syphilis
						result = DatabaseManager.getInstance().getSyphilisData(ApplicationDataModel.getInstance().getPatientLid());
					}else if(getQuestionID().equalsIgnoreCase("118")){
						//TODO Temperature
						result = DatabaseManager.getInstance().getTemperatureData(ApplicationDataModel.getInstance().getPatientLid());
					}

					this.setText(result);

					listenr.onClick(result, getQuestionID(), getIndex(), getColumnName());

				}
				
				
				
				if(!getRequiredData().getInputtype().equalsIgnoreCase(null) && getRequiredData().getInputtype().equalsIgnoreCase("number")){
					this.setInputType(InputType.TYPE_CLASS_NUMBER);

				}else if(!getRequiredData().getInputtype().equalsIgnoreCase(null) && getRequiredData().getInputtype().equalsIgnoreCase("decimal")){
					this.setInputType(InputType.TYPE_CLASS_NUMBER|InputType.TYPE_NUMBER_FLAG_DECIMAL);

				}else{
					this.setInputType(InputType.TYPE_CLASS_TEXT);

				}
				
			} catch (Exception e) {
				Log.e(TAG, "initialize, Exception: "+e);
				e.printStackTrace();
			}
				
		}

	 String columnName;
	 
	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	
		int index;
		 
		 public int getIndex() {
			return index;
		}
		public void setIndex(int index) {
			this.index = index;
		}
		
		String questionID;
		public String getQuestionID() {
			return questionID;
		}

		public void setQuestionID(String questionID) {
			this.questionID = questionID;
		}
		
	 
	
	QuestionEntity questionEntity;
	public void setRequiredData(QuestionEntity data)
	{
		this.questionEntity = data;
	}
	
	public QuestionEntity getRequiredData()
	{
		return questionEntity;
	}
	TextWatcher watch = new TextWatcher(){
		  @Override
		  public void afterTextChanged(Editable arg0) {
			  
//			  listenr.onClick(CustomEditText.this.getText().toString(), getQuestionID(),ComponentType.EDIT_TEXT_TYPE);
//		    listenr.onClick(  CustomEditText.this.getText().toString(), getQuestionID(), getIndex(), getColumnName());
			  String data = CustomEditText.this.getText().toString();
			  if(!getRequiredData().getInputtype().equalsIgnoreCase(null) && !getRequiredData().getInputtype().equalsIgnoreCase("") && !getRequiredData().getInputtype().equalsIgnoreCase("text")){

				  if(null!=data && !data.equalsIgnoreCase("") && !data.equalsIgnoreCase("NA")){


					  if(checkValidation(data)){
						  if(Utils.getInstance().validatePhone(data)){
							  listenr.onClick(data, getQuestionID(), getIndex(), getColumnName());
						  }else{
							  // TODO highlight error message in red color

							  setError(Html.fromHtml("<font color='red'>" + "" + getRequiredData().getValidation_msg() + "</font>"));
							  listenr.onClick("error", getQuestionID(), getIndex(), getColumnName());
							  data="";
						  }


					  }else{
						  // TODO highlight error message in red color

						  setError(Html.fromHtml("<font color='red'>" + "" + getRequiredData().getValidation_msg() + "</font>"));
						  listenr.onClick("error", getQuestionID(), getIndex(), getColumnName());
						  data="";
					  }

//					  if(checkValidation(data)){
					   if(getQuestionID().equalsIgnoreCase("102")){
						   ApplicationDataModel.getInstance().setHeight(data);
					   }else if(getQuestionID().equalsIgnoreCase("103")){
						   ApplicationDataModel.getInstance().setWeight(data);
						   ApplicationDataModel.getInstance().setBmi(calculateBMI());
						   listenr.onClickViewUpdate(data,getQuestionID(),getVisibility(),0,getIndex(),getColumnName());
					   }

					  listenr.onClick(data, getQuestionID(), getIndex(), getColumnName());

//					  }else{
//						  // TODO highlight error message in red color
//
//						  setError(Html.fromHtml("<font color='red'>" + "" + getRequiredData().getValidation_msg() + "</font>"));
//						  listenr.onClick("error", getQuestionID(), getIndex(), getColumnName());
//
//					  }
				  }else{
					  listenr.onClick(data, getQuestionID(), getIndex(), getColumnName());
				  }

			  }else {
				  listenr.onClick(data, getQuestionID(), getIndex(), getColumnName());
			  }




		  }
		  @Override
		  public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
		      int arg3) {
		  }
		  @Override
		  public void onTextChanged(CharSequence s, int a, int b, int c) {
		  }};
	
	
	IEvent listenr;
	public IEvent getListenr() {
		return listenr;
	}

	public void setListenr(IEvent listenr) {
		this.listenr = listenr;
	}

	@Override
	public void onFocusChange(View v, boolean hasFocus) {
			
			if(!hasFocus)
			{
				String data = getText().toString();

				if(!getRequiredData().getInputtype().equalsIgnoreCase(null) && !getRequiredData().getInputtype().equalsIgnoreCase("") && !getRequiredData().getInputtype().equalsIgnoreCase("text")) {

					if(null!=data && !data.equalsIgnoreCase("") && !data.equalsIgnoreCase("NA")){

//						if(checkValidation(data)){
							listenr.onClick(data, getQuestionID(), getIndex(), getColumnName());

//						}else{
//							// TODO highlight error message in red color
//
//							setError(Html.fromHtml("<font color='red'>" + "" + getRequiredData().getValidation_msg() + "</font>"));
//							listenr.onClick("error", getQuestionID(), getIndex(), getColumnName());
//						}
					}else{
						listenr.onClick(data, getQuestionID(), getIndex(), getColumnName());
					}
				}else {
					listenr.onClick(data, getQuestionID(), getIndex(), getColumnName());
				}



				//System.out.println(" focus gone "+getText().toString()+ " hase focus "+hasFocus+ " ID "+getQuestionID());
			}
				
		
	}



	@Override
	protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
		// TODO Auto-generated method stub
		String data = getText().toString();
//		if(checkValidation(data)){
//			listenr.onClick(data, getQuestionID(), getIndex(), getColumnName());
//
//		}else{
//			// TODO highlight error message in red color
//			setText(""+getRequiredData().getValidation_msg());
//		}


		
		super.onMeasure(widthMeasureSpec, heightMeasureSpec);
	}
	
	@Override
	public void resetField() 
	{
		//System.out.println("answerArray[Id]  reset done ");
			this.setText("");
			this.invalidate();
		
	}


	/**
	 * this method will check for edit test data validation
	 * @param data
	 * @return
	 */
	/*private boolean checkValidation(String data){

		boolean flag=false;

		try{

			if(data!=null && data.length()>0){


				long minValue = Long.parseLong((getRequiredData().getMin_value() != null ? getRequiredData().getMin_value() : "-1"));
				long maxValue = Long.parseLong((getRequiredData().getMax_value() != null ? getRequiredData().getMax_value() : "-1"));
				long minSize = Long.parseLong((getRequiredData().getMin_size() != null ? getRequiredData().getMin_size() : "-1"));
				long maxSize = Long.parseLong((getRequiredData().getMax_size() != null ? getRequiredData().getMax_size() : "-1"));


				long currentValue = Long.parseLong(data);
				long currentSize = data.length();

				flag = currentValue >= 0 && currentValue >= minValue && currentValue <= maxValue && currentSize >= minSize && currentSize >= maxSize;
			}

		}catch (Exception e){e.printStackTrace();}


		return flag;
	}
*/

	private boolean checkValidation(String data){

		boolean flagV=false;
		boolean flagS=false;
		try{

			if(data!=null && data.length()>0){


				long minValue = Long.parseLong(((getRequiredData().getMin_value() != null && !getRequiredData().getMin_value().equalsIgnoreCase(""))? getRequiredData().getMin_value() : "-1"));
				long maxValue = Long.parseLong(((getRequiredData().getMax_value() != null && !getRequiredData().getMax_value().equalsIgnoreCase(""))? getRequiredData().getMax_value() : "-1"));
				long minSize = Long.parseLong(((getRequiredData().getMin_size() != null && !getRequiredData().getMin_size().equalsIgnoreCase(""))? getRequiredData().getMin_size() : "-1"));
				long maxSize = Long.parseLong(((getRequiredData().getMax_size() != null && !getRequiredData().getMax_size().equalsIgnoreCase("") )? getRequiredData().getMax_size() : "-1"));


				long currentValue = Long.parseLong(data);
				long currentSize = data.length();

				if(currentValue >= 0){

					if(minValue>=0 ){
						if(currentValue >= minValue){
							flagV = true;
						}else{
							flagV=false;
						}
					}else{
						flagV = true;
					}

					if(maxValue>=0 ){
						if(currentValue <= maxValue){
							flagV = true;
						}else{
							flagV=false;
						}
					}else{
						flagV = true;
					}


				}

				if(currentSize >= 0){

					if(minSize>=0 ){
						if(currentSize >= minSize){
							flagS = true;
						}else{
							flagS=false;
						}
					}else{
						flagS = true;
					}

					if(maxSize>=0 ){
						if(currentSize <= maxSize){
							flagS = true;
						}else{
							flagS=false;
						}
					}else{
						flagS = true;
					}
				}

//          flag = currentValue >= 0 && currentValue >= minValue && currentValue <= maxValue && currentSize >= minSize && currentSize <= maxSize;
			}

		}catch (Exception e){e.printStackTrace();}


		return (flagV&&flagS)==true? true:false;
	}

	private String calculateBMI(){
		String bmi="";
		try {

//			if(ApplicationDataModel.getInstance().getPatientSex().equalsIgnoreCase("Male")){
//
//			}else{
//
//			}
//			final double KILOGRAMS_PER_POUND = 0.453;
//			final double METERS_PER_INCH = 0.0254;
			final double METERS_PER_CM = 0.01;

			double weight = Double.parseDouble(ApplicationDataModel.getInstance().getWeight());

			double height = Double.parseDouble(ApplicationDataModel.getInstance().getHeight());

			double weightInKilogram = weight;
			double heightInMeters = height * METERS_PER_CM;
			double bmiV = weightInKilogram /
					(heightInMeters * heightInMeters);
			bmi=""+round(bmiV,1);
		}catch (Exception e){
			e.printStackTrace();
		}
		return bmi;
	}

	public double round(double value, int places) {
		if (places < 0) throw new IllegalArgumentException();

		long factor = (long) Math.pow(10, places);
		value = value * factor;
		long tmp = Math.round(value);
		return (double) tmp / factor;
	}

}
