package com.phtt.telemedhc.parser.customcomponent;

import android.content.Context;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TableLayout;

import com.phtt.telemedhc.database.DatabaseManager;
import com.phtt.telemedhc.models.ApplicationDataModel;
import com.phtt.telemedhc.models.CountryData;
import com.phtt.telemedhc.models.StateDistrictData;
import com.phtt.telemedhc.parser.ParserConstant;
import com.phtt.telemedhc.parser.QuestionEntity;

import java.util.ArrayList;
import java.util.Vector;


public class CustomSpinner extends Spinner implements AdapterView.OnItemSelectedListener, IResetListener {
    TableLayout.LayoutParams param;
    Context context;
    ArrayAdapter<String> spinnerAdapter;
    String spinnerData[];
    ArrayList<StateDistrictData> stateDistrictList;
    Vector<String> questinIdVec = null;
    ArrayList<CountryData> countryList;

    public CustomSpinner(Context con, IEvent listener, String id, int index, String columnName) {
        super(con);
        context = con;
        param = new TableLayout.LayoutParams(0, TableLayout.LayoutParams.WRAP_CONTENT, 0.5f);
        setLayoutParams(param);
        setListenr(listener);
        setQuestionID(id);
        setOnItemSelectedListener(this);
        setIndex(index);
        setColumnName(columnName);
        int count = this.getChildCount();
    }

    public CustomSpinner(Context con, IEvent listener, String id, int index, String columnName, ArrayList<CountryData> countryList) {
        super(con);
        context = con;
        param = new TableLayout.LayoutParams(0, TableLayout.LayoutParams.WRAP_CONTENT, 0.5f);
        setLayoutParams(param);
        getChildCount();
        setListenr(listener);
        setQuestionID(id);
        setOnItemSelectedListener(this);
        setIndex(index);
        setColumnName(columnName);
        int count = getChildCount();
        this.countryList = countryList;


    }

    public void initialize() {

			/*String dependentid = questionEntity.getDependent_qid();

			if(!dependentid.equalsIgnoreCase("") && dependentid!=null && dependentid.contains(",")){

				questinIdVec = com.healthcubed.patientapp.parser.ParserConstant.formatString(dependentid, ",");

			}
			else{
				questinIdVec = new Vector<String>();
				questinIdVec.add(dependentid);
			}
			*/
        stateDistrictList = DatabaseManager.getInstance().getStateListData(context);
        ApplicationDataModel.getInstance().setStateDistrictDataArrayList(stateDistrictList);
        for (int i = 0; i < questionEntity.getOptionList().size(); i++) {
            String dependentid = questionEntity.getOptionList().get(i).getDependentQid();

            if (dependentid != null && !dependentid.equalsIgnoreCase("") && dependentid.contains(",") && !dependentid.equalsIgnoreCase("null")) {

                questinIdVec = ParserConstant.formatString(dependentid, ",");

            } else if (dependentid == null) {

            } else {
                questinIdVec = new Vector<String>();
                questinIdVec.add(dependentid);
            }
        }


        if (getRequiredData() != null && getRequiredData().getPrefillData() != null && spinnerData != null) {

            String condtion = "";

            listenr.onClick(getRequiredData().getPrefillData(), getQuestionID(), getIndex(), getColumnName());

            for (int i = 0; i < spinnerData.length; i++) {
                if (spinnerData[i].equalsIgnoreCase(getRequiredData().getPrefillData())) {

                    this.setSelection(i);
                    condtion = questionEntity.getOptionList().get(i).getVisiblityCondition().toLowerCase();

                    break;
                }
            }

            String value = getRequiredData().getPrefillData();
//				int visiblityState = View.VISIBLE;
            int visiblityState = 0;


            try {
//					for(int i = 0;i<questionEntity.getOptionList().size();i++)
//					{


                if (!value.equalsIgnoreCase("")/* && value.equalsIgnoreCase(questionEntity.getOptionList().get(i).getValue())*/) {

//							String condtion = questionEntity.getVisiblity_condition().toLowerCase();

                    if (condtion.contains("visible") /*&& condtion.contains(value.toLowerCase())*/) {

                        visiblityState = View.VISIBLE;

                    } else if (condtion.contains("hide") /*&& condtion.contains(value.toLowerCase())*/) {

                        visiblityState = View.GONE;

                    }/*else if( condtion.contains("visible") && !condtion.contains(value.toLowerCase())){

								visiblityState = View.GONE;
								
							}else if( condtion.contains("hide") && !condtion.contains(value.toLowerCase())){
								
								visiblityState = View.VISIBLE;
								
							}*/

                    try {

                        if (questinIdVec != null)
                            listenr.onClickViewUpdate(questinIdVec, value, getQuestionID(), visiblityState, 0, getIndex(), getColumnName());

                    } catch (Exception e) {
                        // TODO: handle exception
                        e.printStackTrace();
                    }


                } else {

                    try {

//								String condtion = questionEntity.getVisiblity_condition().toLowerCase();
//								String condtion = questionEntity.getOptionList().get(i).getVisiblityCondition().toLowerCase();

                        if (condtion.contains("visible")) {

                            visiblityState = View.VISIBLE;

                        } else if (condtion.contains("hide")) {

                            visiblityState = View.GONE;

                        }
                        if (questinIdVec != null)
                            listenr.onClickViewUpdate(questinIdVec, value, getQuestionID(), visiblityState, -1, getIndex(), getColumnName());

                    } catch (Exception e) {
                        // TODO: handle exception
                        e.printStackTrace();
                    }
                }

                //}
            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }


        }
    }


    String columnName;

    public String getColumnName() {
        return columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }


    int index;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    String questionID;

    public String getQuestionID() {
        return questionID;
    }

    public void setQuestionID(String questionID) {
        this.questionID = questionID;
    }


    /**
     * this will set spinner adapter
     *
     * @param arr
     */
    public void setSpinnerAdapter(String[] arr) {

        this.spinnerData = arr;
        spinnerAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item, android.R.id.text1, arr);
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        //spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        this.setAdapter(spinnerAdapter);
    }


    QuestionEntity questionEntity;

    public void setRequiredData(QuestionEntity data) {
        this.questionEntity = data;
    }

    public QuestionEntity getRequiredData() {
        return questionEntity;
    }

    IEvent listenr;

    public IEvent getListenr() {
        return listenr;
    }

    public void setListenr(IEvent listenr) {
        this.listenr = listenr;
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        // TODO Auto-generated method stub

        if (getRequiredData() != null && getRequiredData().getPrefillData() != null && spinnerData != null) {

        }

        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View arg1, int position,
                               long arg3) {
        // TODO Auto-generated method stub

        String value = parent.getItemAtPosition(position).toString();

        listenr.onClick(value, getQuestionID(), getIndex(), getColumnName());

        getRequiredData().setPrefillData(value);

        int visiblityState = 0;

        String condtion = "";

        try {
            //for(int i = 0;i<questionEntity.getOptionList().size();i++)
            //{
            if (questionEntity.getOptionList() != null) {
                condtion = questionEntity.getOptionList().get(position).getVisiblityCondition()/*.toLowerCase()*/;
            }

            if (!value.equalsIgnoreCase("")/* && value.equalsIgnoreCase(questionEntity.getOptionList().get(i).getValue())*/) {

//					String condtion = questionEntity.getVisiblity_condition().toLowerCase();

                if (condtion.contains("visible")) {

                    visiblityState = View.VISIBLE;

                } else if (condtion.contains("hide")) {

                    visiblityState = View.GONE;

                }/*else if( condtion.contains("visible") && !condtion.contains(value.toLowerCase())){

						visiblityState = View.GONE;
						
					}else if( condtion.contains("hide") && !condtion.contains(value.toLowerCase())){
						
						visiblityState = View.VISIBLE;
						
					}*/

                try {

                    if (questinIdVec != null)
                        listenr.onClickViewUpdate(questinIdVec, value, getQuestionID(), visiblityState, 0, getIndex(), getColumnName());

                } catch (Exception e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }


            } else {

                try {

//						String condtion = questionEntity.getVisiblity_condition().toLowerCase();
//						String condtion = questionEntity.getOptionList().get(i).getVisiblityCondition().toLowerCase();

                    if (condtion.contains("visible")) {

                        visiblityState = View.VISIBLE;

                    } else if (condtion.contains("hide")) {

                        visiblityState = View.GONE;

                    }
                    if (questinIdVec != null)
                        listenr.onClickViewUpdate(questinIdVec, value, getQuestionID(), visiblityState, -1, getIndex(), getColumnName());

                } catch (Exception e) {
                    // TODO: handle exception
                    e.printStackTrace();
                }
            }

            //}
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        if (questionID.equalsIgnoreCase("7")) {

            String stateid = "";
            if (ApplicationDataModel.getInstance().getStateDistrictDataArrayList() != null) {
                for (StateDistrictData model : DatabaseManager.getInstance().getStateListData(context)
                        ) {
                    if (value != null && !value.isEmpty()) {
                        if (value.equalsIgnoreCase(model.getStateName())) {
                            stateid = model.getStateCode();
                            ApplicationDataModel.getInstance().setStateCode(stateid);
                        }
                        String[] spinData = getDistrictList(context);
                        if (null != spinData && spinData.length > 0) {
                            questinIdVec = new Vector<String>();
                            questinIdVec.add("8");
                            listenr.onClickViewUpdate(questinIdVec, value, getQuestionID(), View.VISIBLE, -1, getIndex(), getColumnName(), spinData);
                        }
                    } else {
                        questinIdVec.add("8");
                        listenr.onClickViewUpdate(questinIdVec, value, getQuestionID(), visiblityState, -1, getIndex(), getColumnName());

                    }
                }
            }
        }

    }

    private String[] getDistrictList(Context mContext) {

        String arr[] = null;
        ArrayList<StateDistrictData> districtList = DatabaseManager.getInstance().getDistrictListData(mContext, ApplicationDataModel.getInstance().getStateCode());
        arr = new String[districtList.size()];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = districtList.get(i).getDistrictName();
        }

        return arr;

    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub
//		System.out.println(" got data  nothing selected ");

    }

    @Override
    public void resetField() {
        // TODO Auto-generated method stub
        this.setSelection(0);
        invalidate();
    }


}


