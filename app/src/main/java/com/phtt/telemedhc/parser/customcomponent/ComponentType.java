package com.phtt.telemedhc.parser.customcomponent;

import com.phtt.telemedhc.parser.QuestionEntity;

import java.util.List;


public class ComponentType {


	private static List<QuestionEntity> mQlist;


	public static final String componentType [] = {
		"edittext",
		"TextView",
		"Button",
		"Spinner",
		"checkbox",
		"datepicker",
		"searchSpinner",
		"autoCompleteTextView",
		"heading",
		"MultiSelectionSpinner",
		"NumberPicker",
		"TimePicker",
		
	};

	public static final int EDIT_TEXT_TYPE = 0;
	public static final int TEXT_VIEW_TYPE = 1;
	public static final int BUTTON_TYPE = 2;
	public static final int SPINNER_TYPE = 3;
	public static final int CHECK_BOX_TYPE= 4;
	public static final int DATE_PICKER_TYPE = 5;
	public static final int SEARCH_SPINNER_TYPE = 6;
	public static final int AUTO_COMPLETE_TEXTVIEW_TYPE = 7;
	public static final int HEADING_TYPE = 8;
	public static final int MULTI_SELECTION_SPINNER = 9;
	public static final int NUMBER_PICKER = 10;
	public static final int TIME_PICKER = 11;
	
	
	/**
	 * this will return the component id using component type
	 * @param type
	 * @return
	 */
	public static int getComponentId(String type)
	{
		for(int i = 0;i<componentType.length;i++)
		{
			if(componentType[i].equalsIgnoreCase(type))
				return i;
		}
		return -1;
	}



	public static List<QuestionEntity> getmQlist() {
		return mQlist;
	}



	public static void setmQlist(List<QuestionEntity> mQlist) {
		ComponentType.mQlist = mQlist;
	}



	public static final String HEADER_TYPE_TEXT = "heading";
}
