package com.phtt.telemedhc.parser.customcomponent;

import android.content.Context;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TableLayout;

import com.phtt.telemedhc.parser.ParserActivity;
import com.phtt.telemedhc.parser.QuestionEntity;


public class CustomButton extends Button implements OnClickListener {

    ParserActivity mActivity =null;
	Context mContext;
	
	TableLayout.LayoutParams param;
	public CustomButton(Context context, IEvent listener, String id, ParserActivity myActivity, int index, String columnName) {
		super(context);
		setListenr(listener);
		mContext = context;
		// TODO Auto-generated constructor stub
        param = new TableLayout.LayoutParams(0,TableLayout.LayoutParams.WRAP_CONTENT,0.5f);
		setLayoutParams(param);
		setGravity(Gravity.CENTER_HORIZONTAL | Gravity.CENTER_VERTICAL);
		setQuestionID(id);
        mActivity = myActivity;
        setOnClickListener(this);
        setIndex(index);
        setColumnName(columnName);
//        ViewUtils.setTypeFace(mContext, this, ViewUtils.HEADING_SMALL_NORMAL_SIZE, Typeface.BOLD);
		
        
	}

	public void initialize()
	{
			
	}
	 
	 String columnName;
	 
	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}
	
	int index;
	 
	 public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	
	String questionID;
		public String getQuestionID() {
			return questionID;
		}

		public void setQuestionID(String questionID) {
			this.questionID = questionID;
		}
		

			
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		listenr.onClick("picture", getQuestionID(), getIndex(), getColumnName());

	}
	

	QuestionEntity questionEntity;
	public void setRequiredData(QuestionEntity data)
	{
		this.questionEntity = data;
	}
	
	public QuestionEntity getRequiredData()
	{
		return questionEntity;
	}
	
	
	
	IEvent listenr;
	public IEvent getListenr() {
		return listenr;
	}

	public void setListenr(IEvent listenr) {
		this.listenr = listenr;
	}


}
