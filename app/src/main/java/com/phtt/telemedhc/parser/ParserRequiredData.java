package com.phtt.telemedhc.parser;

import java.io.Serializable;

/**
 * this class only contain data which is required for parser
 * @author Varun
 *
 */
@SuppressWarnings("serial")
public class ParserRequiredData  implements Serializable {

	
	// table name
	private String table;
	// previous data
	private boolean previewData = false;
	// date
	private String textLMpDate;
	// date
	private String textEDDdate;
	// ssa file name
	private String ssaName;
	// count tile
	private int countTile; 
	// ssque
	private String ssQue;
	// header title
	private String headerTitle;
	// data base updated or not
	private boolean dbUpdate =false;
	
	private String ssTitle;
	
	private String lid;
	
	

	public ParserRequiredData() {
		// TODO Auto-generated constructor stub
	}
	
	
	public String getLid() {
		return lid;
	}

	public void setLid(String lid) {
		this.lid = lid;
	}


	public String getTable() {
		return table;
	}
	public void setTable(String table) {
		this.table = table;
	}
	public boolean isPreviewData() {
		return previewData;
	}
	public void setPreviewData(boolean previewData) {
		this.previewData = previewData;
	}
	public String getTextLMpDate() {
		return textLMpDate;
	}
	public void setTextLMpDate(String textLMpDate) {
		this.textLMpDate = textLMpDate;
	}
	public String getTextEDDdate() {
		return textEDDdate;
	}
	public void setTextEDDdate(String textEDDdate) {
		this.textEDDdate = textEDDdate;
	}
	public String getSsaName() {
		return ssaName;
	}
	public void setSsaName(String ssaName) {
		this.ssaName = ssaName;
	}
	public int getCountTile() {
		return countTile;
	}
	public void setCountTile(int countTile) {
		this.countTile = countTile;
	}
	public String getSsQue() {
		return ssQue;
	}
	public void setSsQue(String ssQue) {
		this.ssQue = ssQue;
	}
	public String getHeaderTitle() {
		return headerTitle;
	}
	public void setHeaderTitle(String headerTitle) {
		this.headerTitle = headerTitle;
	}
	public boolean isDbUpdate() {
		return dbUpdate;
	}
	public void setDbUpdate(boolean dbUpdate) {
		this.dbUpdate = dbUpdate;
	}



	public String getSsTitle() {
		return ssTitle;
	}



	public void setSsTitle(String ssTitle) {
		this.ssTitle = ssTitle;
	}



 
	
	
}
