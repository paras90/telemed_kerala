package com.phtt.telemedhc.parser.customcomponent;

import android.content.Context;
import android.view.Gravity;
import android.widget.NumberPicker;
import android.widget.NumberPicker.OnValueChangeListener;
import android.widget.TableLayout;

import com.phtt.telemedhc.parser.QuestionEntity;


public class CustomNumberPicker extends NumberPicker implements
		OnValueChangeListener, IResetListener {

	TableLayout.LayoutParams param;

	public CustomNumberPicker(Context context, IEvent listener, String id,
			int index, String columnName) {
		super(context);

//		ViewUtils.setTypeFace(context, this, ViewUtils.MEDIUM_NORMAL_SIZE, Typeface.NORMAL);
		
		setListenr(listener);
		param = new TableLayout.LayoutParams(0,
				TableLayout.LayoutParams.WRAP_CONTENT, 0.5f);
		setLayoutParams(param);
		setGravity(Gravity.CENTER_VERTICAL);
		setQuestionID(id);
		setIndex(index);
		setOnValueChangedListener(this);
		setColumnName(columnName);
		setMaxValue(0);
		setMaxValue(100);
		
	}

	public void initialize() {

		try {
				if(getRequiredData()!= null && getRequiredData().getPrefillData()!=null)
				{
					String tempVal = getRequiredData().getPrefillData();
					if(null!=tempVal && !tempVal.equalsIgnoreCase("")){
						
						int value = Integer.parseInt(tempVal);
						this.setValue(value);
					}
					
					 listenr.onClick(getRequiredData().getPrefillData(), getQuestionID(), getIndex(), getColumnName());
				}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void resetField() {
		// TODO Auto-generated method stub
		this.setValue(0);
	}

	@Override
	public void onValueChange(NumberPicker picker, int oldVal, int newVal) {

		 listenr.onClick(""+newVal, getQuestionID(), getIndex(), getColumnName());
		 
	}

	String columnName;

	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	int index;

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	String questionID;

	public String getQuestionID() {
		return questionID;
	}

	public void setQuestionID(String questionID) {
		this.questionID = questionID;
	}

	QuestionEntity questionEntity;

	public void setRequiredData(QuestionEntity data) {
		this.questionEntity = data;
	}

	public QuestionEntity getRequiredData() {
		return questionEntity;
	}

	IEvent listenr;
	public IEvent getListenr() {
		return listenr;
	}

	public void setListenr(IEvent listenr) {
		this.listenr = listenr;
	}
	
}