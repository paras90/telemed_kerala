package com.phtt.telemedhc.parser.customcomponent;

import android.content.Context;
import android.widget.TableLayout;
import android.widget.TimePicker;
import android.widget.TimePicker.OnTimeChangedListener;

import com.phtt.telemedhc.parser.QuestionEntity;


public class CustomTimePicker extends TimePicker implements OnTimeChangedListener, IResetListener{

	TableLayout.LayoutParams param;

	public CustomTimePicker(Context context, IEvent listener, String id,
			int index, String columnName) {
		super(context);
		setListenr(listener);
		param = new TableLayout.LayoutParams(0,
				TableLayout.LayoutParams.WRAP_CONTENT, 0.3f);
		setLayoutParams(param);
		setQuestionID(id);
		setIndex(index);
		setOnTimeChangedListener(this);
		setColumnName(columnName);
//		ViewUtils.setTypeFace(context, this, ViewUtils.MEDIUM_NORMAL_SIZE, Typeface.NORMAL);
		

	}
	
	
	public void initialize() {

		try {
				if(getRequiredData()!= null && getRequiredData().getPrefillData()!=null)
				{
					String tempVal = getRequiredData().getPrefillData();
					
					if(null!=tempVal && !tempVal.equalsIgnoreCase("") && tempVal.contains("-")){
						
						 String[] arrTime =tempVal.split("-");
						 
						int hrsValue = Integer.parseInt(arrTime[0]);
						int minValue = Integer.parseInt(arrTime[1]);
						this.setCurrentHour(hrsValue);
						this.setCurrentMinute(minValue);
						
					}
					
					 listenr.onClick(getRequiredData().getPrefillData(), getQuestionID(), getIndex(), getColumnName());
				}
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onTimeChanged(TimePicker view, int hourOfDay, int minute) {
		// TODO Auto-generated method stub
		
		 String data = hourOfDay+"-"+minute;
		 
		 listenr.onClick(data, getQuestionID(), getIndex(), getColumnName());
			
	}

	@Override
	public void resetField() {
		// TODO Auto-generated method stub
		
		this.setCurrentHour(0);
		this.setCurrentMinute(0);
	}
	
	
	String columnName;

	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	int index;

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	String questionID;

	public String getQuestionID() {
		return questionID;
	}

	public void setQuestionID(String questionID) {
		this.questionID = questionID;
	}

	QuestionEntity questionEntity;

	public void setRequiredData(QuestionEntity data) {
		this.questionEntity = data;
	}

	public QuestionEntity getRequiredData() {
		return questionEntity;
	}

	IEvent listenr;
	public IEvent getListenr() {
		return listenr;
	}

	public void setListenr(IEvent listenr) {
		this.listenr = listenr;
	}
	

}
