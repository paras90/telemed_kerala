/**
 * 
 */
package com.phtt.telemedhc.parser.customcomponent;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnMultiChoiceClickListener;
import android.util.AttributeSet;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;

import com.phtt.telemedhc.parser.QuestionEntity;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;


/**
 * @author varun
 *
 */
public class CustomMultiSelectionSpinner extends Spinner implements OnMultiChoiceClickListener,IResetListener {



	String[] _items =null;
	boolean[] mSelection  = null;
	Context context;
	ArrayAdapter<String> simpleAdaptor;
	/**
	 * @param context
	 */
	public CustomMultiSelectionSpinner(Context context, IEvent mlistener, String id, int index, String columnName) {
		super(context);
		this.context=context;
		simpleAdaptor =  new ArrayAdapter<String>(context, android.R.layout.simple_spinner_item);
		super.setAdapter(simpleAdaptor);
		
		setListenr(mlistener);
		setQuestionID(id);
		setIndex(index);
		setColumnName(columnName);
//		setTextSize(20);
	}

	public CustomMultiSelectionSpinner(Context mContext, AttributeSet attars){
		super(mContext, attars);
		this.context=mContext;
		simpleAdaptor = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item);
		super.setAdapter(simpleAdaptor);
	}


	/* (non-Javadoc)
	 * @see android.content.DialogInterface.OnMultiChoiceClickListener#onClick(android.content.DialogInterface, int, boolean)
	 */
	@Override
	public void onClick(DialogInterface dialog, int which, boolean isChecked) {
		// TODO Auto-generated method stub

		if(mSelection != null && which < mSelection.length){
			mSelection[which]  = isChecked;

			simpleAdaptor.clear();
			simpleAdaptor.add(buildSelectedItemString());
		}
		else{
			throw new IllegalArgumentException("Argument 'which' is out of bounds");
		}

	}
	
	public void initialize(){
		
//		 ViewUtils.setTypeFace(context, this, ViewUtils.MEDIUM_NORMAL_SIZE, Typeface.NORMAL);
			
		if(getRequiredData() != null && getRequiredData().getPrefillData() != null){

			List<String> prefillItem = Arrays.asList(getRequiredData().getPrefillData().split("\\s*~\\s*"));
			for (int i = 0; i < _items.length; i++) {
				
				for (String listItm : prefillItem) {
					if(_items[i].equalsIgnoreCase(listItm))
						mSelection[i] = true;
				}
			}
			
			simpleAdaptor.clear();
			simpleAdaptor.add(buildSelectedItemString());
			
		}
	}

	 String columnName;
	 
	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

	int index;
	 
	 public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}
	
	QuestionEntity questionEntity;
	public void setRequiredData(QuestionEntity data)
	{
		this.questionEntity = data;
	}
	
	public QuestionEntity getRequiredData()
	{
		return questionEntity;
	}
	
	IEvent listenr;
	public IEvent getListenr() {
		return listenr;
	}

	public void setListenr(IEvent listenr) {
		this.listenr = listenr;
	}
	
	
	String questionID;
	public String getQuestionID() {
		return questionID;
	}

	public void setQuestionID(String questionID) {
		this.questionID = questionID;
	}

	/* (non-Javadoc)
	 * @see android.widget.Spinner#setAdapter(android.widget.SpinnerAdapter)
	 */
	@Override
	public void setAdapter(SpinnerAdapter adapter) {
		throw new RuntimeException(  
				"setAdapter is not supported by MultiSelectSpinner.");
	}

	/* (non-Javadoc)
	 * @see android.widget.Spinner#performClick()
	 */
	@SuppressLint("ClickableViewAccessibility")
	@Override
	public boolean performClick() {

		AlertDialog.Builder mBuilder = new AlertDialog.Builder(getContext());
		mBuilder.setMultiChoiceItems(_items	, mSelection, this);
		mBuilder.show();
		return true;
	}

	public void setItem(String[] items){

		_items = items;
		mSelection = new boolean[_items.length];
		simpleAdaptor.clear();
		simpleAdaptor.add("");
		Arrays.fill(mSelection, false);
	}

	public void setItem(List<String> items){
		_items = items.toArray(new String[items.size()]);
		mSelection = new boolean[_items.length];
		simpleAdaptor.clear();
		simpleAdaptor.add(_items[0]);
		Arrays.fill(mSelection, false);

	}

	public void setSelection(String[] Selection){
		for (String cell : Selection) {
			for (int i = 0; i < _items.length; i++) {
				if (_items[i].equals(cell)) {
					mSelection[i] =  true;
				}
			}
		}
	}

	public void setSelection(List<String> Selection){
		for (int i = 0; i < mSelection.length; i++) {
			mSelection[i] = false;
		}

		for (String cell : Selection) {
			for (int i = 0; i < _items.length; i++) {
				if(_items[i].equals(cell))
					mSelection[i] = true;
			}
		}

		simpleAdaptor.clear();
		simpleAdaptor.add(buildSelectedItemString());
	}

	public void setSelection(int index){
		for (int i = 0; i < mSelection.length; i++) {  
			mSelection[i] = false;  
		}  
		if (index >= 0 && index < mSelection.length) {  
			mSelection[index] = true;  
		} else {  
			throw new IllegalArgumentException("Index " + index  
					+ " is out of bounds.");  
		}  
		simpleAdaptor.clear();  
		simpleAdaptor.add(buildSelectedItemString()); 
	}


	public void setSelection(int[] selectedIndicies){
		for (int i = 0; i < mSelection.length; i++) {
			mSelection[i] = false;
		}
		for(int index : selectedIndicies){
			if(index > 0 && index < mSelection.length){
				mSelection[index] =  true;
			}
			else{
				throw new IllegalArgumentException("Index " + index  
						+ " is out of bounds."); 
			}
		}

		simpleAdaptor.clear();
		simpleAdaptor.add(buildSelectedItemString());

	}


	public List<String> getSeletedString(){
		List<String> selection = new LinkedList<String>();
		for (int i = 0; i < _items.length; i++) {
			if(mSelection[i])
				selection.add(_items[i]);
		}

		return selection;
	}

	public List<Integer> getSelectedIndicies(){
		List<Integer> selection = new LinkedList<Integer>();
		for (int i = 0; i < _items.length; i++) {
			if(mSelection[i])
				selection.add(i);
		}

		return selection;
	}

	/**
	 * @return mBuilder : object of String Builder
	 */
	private String buildSelectedItemString() {
		// TODO Auto-generated method stub
		StringBuilder mBuilder =  new StringBuilder();
		boolean foundOne = false;

		for (int i = 0; i < _items.length; i++) {
			if(mSelection[i]){
				if(foundOne){
					mBuilder.append("~ ");
				}
				foundOne = true;

				mBuilder.append(_items[i]);
			}
		}
		listenr.onClick(mBuilder.toString(), getQuestionID(),getIndex(),getColumnName());
		return mBuilder.toString();
	}

	public String getSelectedItemAsString() {
		StringBuilder mBuilder = new StringBuilder();
		boolean foundOne =false;

		for (int i = 0; i < _items.length; i++) {
			if(mSelection[i]){
				if(foundOne){
					mBuilder.append("~ ");
				}
				foundOne = true;
				mBuilder.append(_items[i]);
			}
		}

		return mBuilder.toString();
	}

	/* (non-Javadoc)
	 * @see com.phfi.customparsercomponent.IResetListener#resetField()
	 */
	@Override
	public void resetField() {
		// TODO Auto-generated method stub
//		this.setSelection(0);
//		invalidate();
	}


}
