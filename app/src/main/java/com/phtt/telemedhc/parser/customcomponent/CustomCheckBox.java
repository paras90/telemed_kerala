package com.phtt.telemedhc.parser.customcomponent;

import android.content.Context;
import android.view.Gravity;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TableLayout;

import com.phtt.telemedhc.parser.QuestionEntity;


public class CustomCheckBox extends CheckBox implements CompoundButton.OnCheckedChangeListener{

	TableLayout.LayoutParams param;


	public CustomCheckBox(Context context, IEvent listener, String id, int index, String columnName) {
		super(context);
		// TODO Auto-generated constructor stub
		param = new TableLayout.LayoutParams(0,TableLayout.LayoutParams.WRAP_CONTENT,0.5f);
		setLayoutParams(param);
		setGravity(Gravity.CENTER_HORIZONTAL|Gravity.CENTER_VERTICAL);
		setListenr(listener);
		setQuestionID(id);
		setOnCheckedChangeListener(this);
		 setIndex(index);
		 setColumnName(columnName);
//		 ViewUtils.setTypeFace(context, this, ViewUtils.MEDIUM_NORMAL_SIZE, Typeface.NORMAL);
			
	}
	 public void initialize()
		{
			
		}

	 String columnName;
	 
	public String getColumnName() {
		return columnName;
	}

	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}

		int index;
		 
		 public int getIndex() {
			return index;
		}
		public void setIndex(int index) {
			this.index = index;
		}
		
	 String questionID;
	public String getQuestionID() {
		return questionID;
	}

	public void setQuestionID(String questionID) {
		this.questionID = questionID;
	}

	QuestionEntity questionEntity;
	public void setRequiredData(QuestionEntity data)
	{
		this.questionEntity = data;
	}
	
	public QuestionEntity getRequiredData()
	{
		return questionEntity;
	}
	
	
	
	IEvent listenr;
	public IEvent getListenr() {
		return listenr;
	}

	public void setListenr(IEvent listenr) {
		this.listenr = listenr;
	}

 

	@Override
	public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
		// TODO Auto-generated method stub
		if(isChecked)
		{
			listenr.onClick("Yes", questionID, getIndex(), getColumnName());
		}
		else
		{
			listenr.onClick("No", questionID, getIndex(), getColumnName());
		}
		
		
		
	}

	
}
