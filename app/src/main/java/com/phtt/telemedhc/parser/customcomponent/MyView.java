package com.phtt.telemedhc.parser.customcomponent;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.phtt.telemedhc.R;
import com.phtt.telemedhc.database.DatabaseManager;
import com.phtt.telemedhc.models.ApplicationDataModel;
import com.phtt.telemedhc.models.CountryData;
import com.phtt.telemedhc.models.StateDistrictData;
import com.phtt.telemedhc.parser.ParserActivity;
import com.phtt.telemedhc.parser.QuestionEntity;
import com.phtt.telemedhc.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Vector;

//import com.phtt.mohealth.parser.customcomponent.searchspinner.SpinnerParentComponent;


public class MyView {


    //	private static final String TAG = "MyView";
    Context context;
    View view = null;
    LinearLayout linearLayout;
    IResetListener resetListener;
    int displayWidth;
    int count = 0;
    ParserActivity myActivity = null;

    public MyView(Context con, int dWidth) {
        this.context = con;
        iListenerVector = new Vector<IResetListener>();

        displayWidth = dWidth;
        count = 1;
//		Display mDisplay = ParserActivity.this.getWindowManager().getDefaultDisplay();
    }

    public MyView(Context con, int dWidth, Activity mActivity) {
        this.context = con;
        iListenerVector = new Vector<IResetListener>();

        displayWidth = dWidth;
        count = 1;
        if (mActivity != null) {
            myActivity = (ParserActivity) mActivity;
        } else {
            System.out.println("nulll");
        }
//		Display mDisplay = ParserActivity.this.getWindowManager().getDefaultDisplay();
    }


    private int layoutMargin = 10;
    CustomView tempView = null;

    public CustomView generateView(QuestionEntity data, IEvent listener, String id, int index, String columnName) {
        linearLayout = null;

        tempView = null;
        switch (ComponentType.getComponentId(data.getType())) {


            case ComponentType.BUTTON_TYPE: {
                tempView = new CustomView(context);

                linearLayout = new LinearLayout(context);
                linearLayout.setGravity(Gravity.CENTER_VERTICAL);
                LayoutParams ly = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
                ly.setMargins(layoutMargin, layoutMargin, layoutMargin, layoutMargin);
                linearLayout.setLayoutParams(ly);
                CustomTextView textView = new CustomTextView(context, listener, id, index);
                textView.setText(count + ") " + data.getValue());
                textView.setTextColor(Color.WHITE);
                count++;
                textView.setRequiredData(data);
                textView.initialize();


                CustomButton button = new CustomButton(context, listener, id, myActivity, index, columnName);
                //					  button.setGravity(Gravity.CENTER_HORIZONTAL);
                button.setBackgroundResource(R.drawable.button1);
                button.setRequiredData(data);
                button.initialize();
                button.setTextColor(Color.WHITE);
                button.setText("Take Picture");
                linearLayout.setOrientation(LinearLayout.HORIZONTAL);
                linearLayout.addView(textView);
                linearLayout.addView(button);

                tempView.addSubView(textView);
                tempView.addSubView(button);
                tempView.addView(linearLayout);

                return tempView;

            }
            case ComponentType.TEXT_VIEW_TYPE: {

            }
            break;
            case ComponentType.EDIT_TEXT_TYPE: {
                tempView = new CustomView(context);

                linearLayout = new LinearLayout(context);
                linearLayout.setGravity(Gravity.CENTER_VERTICAL);
                LayoutParams ly = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
                ly.setMargins(layoutMargin, layoutMargin, layoutMargin, layoutMargin);
                linearLayout.setLayoutParams(ly);

                CustomTextView textView = new CustomTextView(context, listener, id, index);

                textView.setText(count + ") " + data.getValue());
                count++;
                textView.setRequiredData(data);
                textView.initialize();


                CustomEditText edit = new CustomEditText(context, listener, id, index, columnName);
                //					  edit.setGravity(Gravity.CENTER_HORIZONTAL);

                edit.setRequiredData(data);

                edit.initialize();
                iListenerVector.add(edit);
                linearLayout.setOrientation(LinearLayout.HORIZONTAL);
                linearLayout.addView(textView);
                linearLayout.addView(edit);

                tempView.addSubView(textView);
                tempView.addSubView(edit);
                tempView.addView(linearLayout);

                return tempView;

                //					  return linearLayout;

            }

            case ComponentType.HEADING_TYPE: {
                tempView = new CustomView(context);
                linearLayout = new LinearLayout(context);
                linearLayout.setGravity(Gravity.CENTER_VERTICAL);

                LinearLayout linearLayoutTemp = new LinearLayout(context);
//			linearLayoutTemp.setGravity(Gravity.CENTER_VERTICAL);
                LayoutParams ly = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
                ly.setMargins(layoutMargin, layoutMargin, layoutMargin, layoutMargin);
                linearLayoutTemp.setLayoutParams(ly);
                CustomTextView quesTextView = new CustomTextView(context, listener, id, index);
                quesTextView.setText(data.getValue());
                quesTextView.setRequiredData(data);
                quesTextView.initialize();
                quesTextView.setTextSize(30);
                quesTextView.setGravity(Gravity.CENTER);
                quesTextView.setTextColor(Color.WHITE);
                quesTextView.setLayoutParams(ly);
                linearLayoutTemp.setOrientation(LinearLayout.HORIZONTAL);
                linearLayoutTemp.addView(quesTextView);
                linearLayout.addView(linearLayoutTemp);
                linearLayout.setBackgroundColor(Color.GRAY);

                //					 tempView.addSubView(quesTextView);
                //					  tempView.addSubView(quesTextView);
                tempView.addView(linearLayout);
                return tempView;
            }


            case ComponentType.SPINNER_TYPE: {

                tempView = new CustomView(context);


                linearLayout = new LinearLayout(context);
                linearLayout.setGravity(Gravity.CENTER_VERTICAL);
                LayoutParams ly = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
                ly.setMargins(layoutMargin, layoutMargin, layoutMargin, layoutMargin);
                linearLayout.setLayoutParams(ly);

                CustomTextView textView = new CustomTextView(context, listener, id, index);

                textView.setText(count + ") " + data.getValue());
                count++;
                textView.setRequiredData(data);
                textView.initialize();

               /* CustomSpinner spinner = new CustomSpinner(context,listener ,id, index, columnName);
                //					  spinner.setGravity(Gravity.CENTER_HORIZONTAL);

                spinner.setRequiredData(data);

                String arr []  = new String[data.getOptionList().size()];
                for(int i = 0;i <arr.length;i++)
                {

                    arr[i] = data.getOptionList().get(i).getValue();
                }*/
                CustomSpinner spinner = null;
                String arr[] = null;
                if (id != null && id.equalsIgnoreCase("country9")) {
                    //TODO get the panchyat list from the database and set spinner adapter
                    ArrayList<CountryData> country_data = new ArrayList<>();
                    try {
                        JSONObject obj = new JSONObject(Utils.loadJSONFromAsset(context));
                        JSONObject json_obj = obj.optJSONObject("countries");
                        if (json_obj != null && json_obj.length() > 0) {

                            JSONArray json_array = json_obj.optJSONArray("country");
                            if (json_array != null && json_array.length() > 0) {
                                for (int i = 0; i < json_array.length(); i++) {
                                    CountryData cData = new CountryData();
                                    JSONObject temp_json = json_array.optJSONObject(i);
                                    cData.setCode(temp_json.optString("code"));
                                    cData.setPhoneCode(temp_json.optString("phoneCode"));
                                    cData.setName(temp_json.optString("name"));

                                    country_data.add(cData);
                                }
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    spinner = new CustomSpinner(context, listener, id, index, columnName, country_data);
                    spinner.setRequiredData(data);
                    arr = new String[country_data.size()];

                    for (int i = 0; i < arr.length; i++) {
                        arr[i] = country_data.get(i).getName();
                    }

                } else {
                    spinner = new CustomSpinner(context, listener, id, index, columnName);
                    spinner.setRequiredData(data);
                    arr = new String[data.getOptionList().size()];
                    for (int i = 0; i < arr.length; i++) {
                        arr[i] = data.getOptionList().get(i).getValue();
                    }
                }

                spinner.setSpinnerAdapter(arr);
                spinner.initialize();
                iListenerVector.add(spinner);
                linearLayout.setOrientation(LinearLayout.HORIZONTAL);
                linearLayout.addView(textView);
                linearLayout.addView(spinner);

                tempView.addSubView(textView);
                tempView.addSubView(spinner);
                tempView.addView(linearLayout);

                return tempView;
                //					  return linearLayout;
            }
            case ComponentType.CHECK_BOX_TYPE: {
                tempView = new CustomView(context);


                linearLayout = new LinearLayout(context);
                linearLayout.setGravity(Gravity.CENTER_VERTICAL);
                LayoutParams ly = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
                ly.setMargins(layoutMargin, layoutMargin, layoutMargin, layoutMargin);
                linearLayout.setLayoutParams(ly);

                CustomTextView textView = new CustomTextView(context, listener, id, index);

                textView.setText(count + ") " + data.getValue());
                count++;
                textView.setRequiredData(data);
                textView.initialize();

                CustomCheckBox checkBox = new CustomCheckBox(context, listener, id, index, columnName);
                //					 checkBox.setGravity(Gravity.CENTER_HORIZONTAL);
                checkBox.setRequiredData(data);
                checkBox.initialize();
                //iListenerVector.add(checkBox);
                linearLayout.setOrientation(LinearLayout.HORIZONTAL);
                linearLayout.addView(textView);
                linearLayout.addView(checkBox);

                tempView.addSubView(textView);
                tempView.addSubView(checkBox);
                tempView.addView(linearLayout);

                return tempView;
                // return linearLayout;
            }
            case ComponentType.DATE_PICKER_TYPE: {

                tempView = new CustomView(context);

                linearLayout = new LinearLayout(context);
                linearLayout.setGravity(Gravity.CENTER_VERTICAL);
                LayoutParams ly = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
                ly.setMargins(layoutMargin, layoutMargin, layoutMargin, layoutMargin);
                linearLayout.setLayoutParams(ly);


                CustomTextView textView = new CustomTextView(context, listener, id, index);
                textView.setText(count + ") " + data.getValue());
                count++;
                textView.setRequiredData(data);
                textView.initialize();

                CustomDatePicker datePicker = new CustomDatePicker(context, listener, id, index, columnName);
                //					datePicker.setGravity(Gravity.CENTER_HORIZONTAL);
                datePicker.setRequiredData(data);
                datePicker.initialize();
                iListenerVector.add(datePicker);
                linearLayout.setOrientation(LinearLayout.HORIZONTAL);
                linearLayout.addView(textView);
                linearLayout.addView(datePicker);

                tempView.addSubView(textView);
                tempView.addSubView(datePicker);
                tempView.addView(linearLayout);
                return tempView;
                //					  return linearLayout;
            }
            /*case ComponentType.SEARCH_SPINNER_TYPE:
            {

                tempView = new CustomView(context);

                linearLayout = new LinearLayout(context);
                linearLayout.setGravity(Gravity.CENTER_VERTICAL);
                LayoutParams ly = new LayoutParams(LayoutParams.MATCH_PARENT,LayoutParams.WRAP_CONTENT);
                ly.setMargins(layoutMargin, layoutMargin, layoutMargin, layoutMargin);
                linearLayout.setLayoutParams(ly);


                CustomTextView textView = new CustomTextView(context,listener ,id,index);
                textView.setText(count+") "+data.getValue());
                count++;
                textView.setRequiredData(data);
                textView.initialize();

                SpinnerParentComponent spinnerComponent = new SpinnerParentComponent(context, index, columnName);
                //					spinnerComponent.setGravity(Gravity.CENTER_HORIZONTAL);
                spinnerComponent.setRequiredData(data);
                spinnerComponent.initialize();
                // iListenerVector.add(spinnerComponent);
                spinnerComponent.setGravity(Gravity.CENTER);
                spinnerComponent.setLayoutParams(new LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT));
                //spinnerComponent.setComponentWidth(500);
                //	spinnerComponent.setConponentHeight(200);
                spinnerComponent.setMessage(data.getOptionList().get(1).getValue());
                spinnerComponent.setTextSize(20);
                ArrayList<String> temp = new ArrayList<String>();
                for(int i = 0;i<data.getOptionList().size();i++)
                {
                    temp.add(data.getOptionList().get(i).getValue());
                }
                spinnerComponent.initComponent(temp);



                linearLayout.setOrientation(LinearLayout.HORIZONTAL);
                linearLayout.addView(textView);
                linearLayout.addView(spinnerComponent);

                tempView.addSubView(textView);
                tempView.addSubView(spinnerComponent);
                tempView.addView(linearLayout);
                return tempView;

                //					  return linearLayout;
            }*/
            case ComponentType.AUTO_COMPLETE_TEXTVIEW_TYPE: {
                tempView = new CustomView(context);

                linearLayout = new LinearLayout(context);
                linearLayout.setGravity(Gravity.CENTER_VERTICAL);
                LayoutParams ly = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
                ly.setMargins(layoutMargin, layoutMargin, layoutMargin, layoutMargin);
                linearLayout.setLayoutParams(ly);

                CustomTextView textView = new CustomTextView(context, listener, id, index);
                textView.setText(count + ") " + data.getValue());
                count++;
                textView.setRequiredData(data);
                textView.initialize();


                CustomAutoCompleteTextView autoCompleteTextView = new CustomAutoCompleteTextView(context, listener, id, index, columnName);
                //autoCompleteTextView.setGravity(Gravity.CENTER_HORIZONTAL);
                autoCompleteTextView.setRequiredData(data);
//			String arr []  = new String[data.getCimsMedicineList().size()];
//			for(int i = 0;i <arr.length;i++)
//			{
//				arr[i] = data.getCimsMedicineList().get(i).getMedName();
//			}

//                autoCompleteTextView.setAdapter(data.getAutoCompleteDataList());
                autoCompleteTextView.initialize();

                //iListenerVector.add(autoCompleteTextView);
                linearLayout.setOrientation(LinearLayout.HORIZONTAL);
                linearLayout.addView(textView);
                linearLayout.addView(autoCompleteTextView);

                tempView.addSubView(textView);
                tempView.addSubView(autoCompleteTextView);
                tempView.addView(linearLayout);
                return tempView;

                //					  return linearLayout;

            }
            case ComponentType.MULTI_SELECTION_SPINNER:

                tempView = new CustomView(context);

                linearLayout = new LinearLayout(context);
                linearLayout.setGravity(Gravity.CENTER_VERTICAL);
                LayoutParams ly = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
                ly.setMargins(layoutMargin, layoutMargin, layoutMargin, layoutMargin);
                linearLayout.setLayoutParams(ly);

                CustomTextView textView = new CustomTextView(context, listener, id, index);
                textView.setText(count + ") " + data.getValue());
                count++;
                textView.setRequiredData(data);
                textView.initialize();

                CustomMultiSelectionSpinner mSpinner = new CustomMultiSelectionSpinner(context, listener, id, index, columnName);
                mSpinner.setGravity(Gravity.CENTER);
                iListenerVector.add(mSpinner);
                mSpinner.setLayoutParams(new LayoutParams(displayWidth, LayoutParams.WRAP_CONTENT));
                mSpinner.setRequiredData(data);
                String arr[] = new String[data.getOptionList().size()];
                for (int i = 0; i < arr.length; i++) {
                    //  arr[i] = data.getList().get(i).getValue();
                    arr[i] = data.getOptionList().get(i).getValue();
                }

                mSpinner.setItem(arr);
                mSpinner.initialize();

                linearLayout.setOrientation(LinearLayout.HORIZONTAL);
                linearLayout.addView(textView);
                linearLayout.addView(mSpinner);

                tempView.addSubView(textView);
                tempView.addSubView(mSpinner);


                tempView.addView(linearLayout);
                return tempView;

        }

        return tempView;
    }

    Vector<IResetListener> iListenerVector;

    public void updateResetEvent() {
        for (int i = 0; i < iListenerVector.size(); i++) {
            iListenerVector.get(i).resetField();
        }

    }

    public View generateHeading(QuestionEntity data) {


        TextView tv = new TextView(context);
        tv.setLayoutParams(new LayoutParams(android.view.ViewGroup.LayoutParams.MATCH_PARENT, android.view.ViewGroup.LayoutParams.WRAP_CONTENT));
        tv.setText(data.getResult());
        tv.setGravity(Gravity.CENTER);
        tv.setTextSize(30);
        tv.setTextColor(Color.WHITE);
        tv.setBackgroundColor(Color.GRAY);

        return tv;

    }


    public CustomView generatePreviewView(QuestionEntity data, IEvent listener, String id, int index) {
        tempView = new CustomView(context);

        try {
            linearLayout = new LinearLayout(context);
            linearLayout.setGravity(Gravity.CENTER_VERTICAL);
            LayoutParams ly = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
            ly.setMargins(layoutMargin, layoutMargin, layoutMargin, layoutMargin);
            linearLayout.setLayoutParams(ly);
            CustomTextView quesTextView = new CustomTextView(context, listener, id, index);
            quesTextView.setText(data.getValue());
            quesTextView.setRequiredData(data);
            quesTextView.initialize();

            CustomTextView ansTextView = new CustomTextView(context, listener, id, index);
            ansTextView.setGravity(Gravity.CENTER_HORIZONTAL);
            String retdata = data.getPrefillData();
            ansTextView.setText(retdata);
            ansTextView.setRequiredData(data);
            ansTextView.initialize();

            linearLayout.setOrientation(LinearLayout.HORIZONTAL);
            linearLayout.addView(quesTextView);
            linearLayout.addView(ansTextView);

            tempView.addSubView(quesTextView);
            tempView.addSubView(ansTextView);
            tempView.addView(linearLayout);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return tempView;
        //		return linearLayout;

    }

}