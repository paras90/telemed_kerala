package com.phtt.telemedhc.parser;

import java.util.Vector;

public class ParserConstant {


    public static  final String DIRECTORY_PATH = "/telemedhc/English/";
    public static final String PARSER_DATA = "PARSER_REQUIRED_DATA";
    public static final String SSA_FOLDER_NAME = "/PatientApp/";

    public static final int PREFILL_INDEX=6;

    //SSA NAME Decleration
    public static final String SSA_NAME_SOCIAL_DIETARY = "socialpersonalhistory_dietary.ssa";
    public static final String SSA_NAME_SOCIAL_LIFESTYLE ="socialpersonalhistory_lifestyle.ssa";
    public static final String SSA_NAME_PAST_HISTORY = "patientpasthistory.ssa";
    public static final String SSA_NAME_PSYCHIATRIC = "psychiatrichistory.ssa";
    public static final String SSA_NAME_TREATMENT = "treatmenthistory.ssa";
    public static final String SSA_NAME_CHILD_INVENTORY = "childinventory.ssa";
    public static final String SSA_NAME_CONTACT_TRAVEL = "contactsandtravel.ssa";
    public static final String SSA_NAME_OBSTETRIC_HISTORY = "obstetrichistory.ssa";
    public static final String SSA_NAME_OCCUPATION = "occupation.ssa";
    public static final String SSA_NAME_FAMILY_INFO = "familyinfo.ssa";
    public static final String SSA_NAME_FAMILY_RELATION = "familyrelation.ssa";
    public static final String PATIENT_REGISTRATION_SSA_TITLE	= "registration.ssa";
    public static final String SSA_NAME_ADD_SYMPTOMS = "addasymptom.ssa";

    public static final String SSA_NAME_COMMON_AILMENTS = "CurrentillnessCommonAilment.ssa";
    public static final String SSA_NAME_FEVER = "CurrentillnessFever.ssa";
    public static final String SSA_NAME_GENERAL_QUESTIONS = "CurrentillnessGeneralQuest.ssa";
    public static final String SSA_NAME_PAIN = "CurrentillnessPain.ssa";

    public final static String MIMS_DATABASE_NAME = "cims_drugs";
    public final static String SYMPTOM_DATABASE_NAME = "symptom_ailment";
    public final static String STATE_DISTRICT_DB = "statedistrictsdetails";


    public static final String SSA_NAME_REGISTRATION     = "registration.ssa";
    public static final String SSA_NAME_BENEFICIARY_REPORT     = "healthcube_data.ssa";


    public static final String[]  ALL_SSA_FILES_LIST = new String[]{
            SSA_NAME_REGISTRATION,
            SSA_NAME_ADD_SYMPTOMS,
            SYMPTOM_DATABASE_NAME,
            STATE_DISTRICT_DB
    };

	public static Vector<String> formatString(String data, String delimiter)
	{
		Vector<String> vec = new Vector<String>();
		try{

			String[] strArr = data.split(delimiter);
			for (String str : strArr) {
				char[] stringArray = str.trim().toCharArray();
				stringArray[0] = Character.toUpperCase(stringArray[0]);
				str = new String(stringArray);

				vec.add(str);
			}
		}
		catch(Exception e){
			e.printStackTrace();
		}
		
		return vec;
	}
}
