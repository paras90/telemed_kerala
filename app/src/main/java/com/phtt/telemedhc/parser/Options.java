package com.phtt.telemedhc.parser;

public class Options {

	private String value;
	private String count;
	private String state;
	private String visiblityCondition="";
	private String dependentQid="";
	private String dependentTable="";
	private String dependentColumnName="";
	private String dependentDbName="";

	public String getVisiblityCondition() {
		return visiblityCondition;
	}

	public void setVisiblityCondition(String visiblityCondition) {
		this.visiblityCondition = visiblityCondition;
	}

	public String getDependentQid() {
		return dependentQid;
	}

	public void setDependentQid(String dependentQid) {
		this.dependentQid = dependentQid;
	}

	public String getDependentTable() {
		return dependentTable;
	}

	public void setDependentTable(String dependentTable) {
		this.dependentTable = dependentTable;
	}

	public String getDependentColumnName() {
		return dependentColumnName;
	}

	public void setDependentColumnName(String dependentColumnName) {
		this.dependentColumnName = dependentColumnName;
	}

	public String getDependentDbName() {
		return dependentDbName;
	}

	public void setDependentDbName(String dependentDbName) {
		this.dependentDbName = dependentDbName;
	}

	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getCount() {
		return count;
	}
	public void setCount(String count) {
		this.count = count;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
}
