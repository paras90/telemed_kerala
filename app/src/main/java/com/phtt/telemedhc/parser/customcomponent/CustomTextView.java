package com.phtt.telemedhc.parser.customcomponent;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.TableLayout;
import android.widget.TextView;

import com.phtt.telemedhc.R;
import com.phtt.telemedhc.parser.NotificationsDisplay;
import com.phtt.telemedhc.parser.QuestionEntity;


public class CustomTextView extends TextView implements OnClickListener, OnLongClickListener {
    TableLayout.LayoutParams param;
    Context mContext;
    NotificationsDisplay notify;

    public CustomTextView(Context context, IEvent listener, String id, int index) {
        super(context);

        mContext = context;
        notify = new NotificationsDisplay(context);

        param = new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT, 0.5f);
        setLayoutParams(param);
//		ViewUtils.setTypeFace(mContext, this, ViewUtils.MEDIUM_NORMAL_SIZE, Typeface.NORMAL);

        setListenr(listener);
        setQuestionID(id);
        setOnLongClickListener(this);
        setIndex(index);

    }

    public void initialize() {
        if (this.getText().toString().contains("**")) {
            String arse = this.getText().toString();

            int apple = arse.indexOf("**");
            String next = "<font color='#FF0000'>" + arse.substring(apple) + "</font>";
            this.setText(Html.fromHtml(arse.substring(0, apple) + next));
        }
        this.setTextColor(Color.WHITE);
    }


    int index;

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }


    String questionID;

    public String getQuestionID() {
        return questionID;
    }

    public void setQuestionID(String questionID) {
        this.questionID = questionID;
    }


    @Override
    public void onClick(View v) {
        // TODO Auto-generated method stub

    }

    IEvent listenr;

    public IEvent getListenr() {
        return listenr;
    }

    public void setListenr(IEvent listenr) {
        this.listenr = listenr;
    }

    QuestionEntity questionEntity;

    public void setRequiredData(QuestionEntity data) {
        this.questionEntity = data;
    }

    public QuestionEntity getRequiredData() {
        return questionEntity;
    }

    @Override
    public boolean onLongClick(View v) {

        if (getRequiredData() != null) {
            if (getRequiredData().getHint() != null && !getRequiredData().getHint().equals("")) {
                showHintDialogBox(getRequiredData().getHint());
            } else {
                showNoHintPopup();
            }
        } else {
            showNoHintPopup();
        }
        return true;
    }

    /**
     * this will display no hint pop up
     */
    public void showNoHintPopup() {
        notify.customToast(mContext, "info", "No hint is available for this question.");
        //ApplicationController.getInstance().showCustomToast("No hint is available for this question.");
    }

    TextView contDialogMsg;
    Button contDialogbtn1, contDialogbtn2, contDialogBtnClose;

    /**
     * this will show the dialog box with hint text
     */
    public void showHintDialogBox(String mes) {
        final Dialog contDialog;
//		contDialog = new Dialog(mContext *//*R.style.HintThemeWithCorners*//* );
//		contDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
//		contDialog.setContentView(R.layout.hintdialog);
//		contDialog.setCancelable(true);
        contDialog = new Dialog(mContext);
        contDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        contDialog.setContentView(R.layout.hintdialog);
        contDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        contDialog.setCancelable(false);

        //ImageView iv = (ImageView)findViewById(R.id.img);
        contDialogMsg = (TextView) contDialog.findViewById(R.id.DialogMsg);
//		contDialogMsg.setTextSize(20);
        contDialogbtn1 = (Button) contDialog
                .findViewById(R.id.customDialogbtn1);
        contDialogBtnClose = (Button) contDialog
                .findViewById(R.id.customDialogbtnClose);
        contDialogMsg.setText(mes);
        contDialogbtn1.setText("OK");
        contDialog.show();
        contDialogbtn1.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                contDialog.dismiss();
            }
        });


        contDialogBtnClose.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                contDialog.dismiss();
            }
        });

    }
}
