package com.phtt.telemedhc.parser.jsonparser;


import android.content.Context;

import com.phtt.telemedhc.database.DatabaseManager;
import com.phtt.telemedhc.database.DbConstant;
import com.phtt.telemedhc.models.ApplicationDataModel;
import com.phtt.telemedhc.models.AppointmentRequest;
import com.phtt.telemedhc.models.ClinicData;
import com.phtt.telemedhc.models.DiagnosticData;
import com.phtt.telemedhc.models.DoctorData;
import com.phtt.telemedhc.models.LoginData;
import com.phtt.telemedhc.models.RegistrationData;
import com.phtt.telemedhc.models.ReportData;
import com.phtt.telemedhc.models.ServicesData;
import com.phtt.telemedhc.models.SlotDateListData;
import com.phtt.telemedhc.models.TimeSlot;
import com.phtt.telemedhc.models.UploadResponseData;
import com.phtt.telemedhc.network.NetworkConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

/**
 * Created by Kewal on 05-10-2015.
 * This class will parse the all web service response returned form server
 * and process further.
 */
public class JsonParser {

    private JSONObject jsonObject = null;

    /**
     * This method will parse the login json data
     * and return parsed data to caller
     *
     * @param jsonData
     * @return LoginData model object
     */
    public LoginData parseLoginData(String jsonData) {
        LoginData model = null;

        try {
            jsonObject = new JSONObject(jsonData);
            if (null != jsonObject) {
                model = new LoginData();
                JSONObject loginDataObject = jsonObject.getJSONObject(JsonParserConstant.PARSER_KEY_HEADER);
                model.setStatus(loginDataObject.getString(JsonParserConstant.PARSER_KEY_STATUS));
                model.setMessage(loginDataObject.getString(JsonParserConstant.PARSER_KEY_MESSAGE));
                model.setPatientLid(loginDataObject.getString(JsonParserConstant.PARSER_KEY_PATIENT_LID));
                model.setPatientGid(loginDataObject.getString(JsonParserConstant.PARSER_KEY_PATIENT_GID));

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }


        return model;

    }

    /**
     * This method will parse registration data returned form server
     *
     * @param jsonData
     * @return RegistrationData model
     */
    public RegistrationData parserRegistrationData(Context mContext, String jsonData) {

        RegistrationData model = null;
        try {
            jsonObject = new JSONObject(jsonData);
            if (null != jsonObject) {
                model = new RegistrationData();
                JSONObject resultObj = jsonObject.getJSONObject(JsonParserConstant.PARSER_KEY_HEADER);
                if (null != resultObj && resultObj.length() > 0) {
                    Iterator<String> testId = resultObj.keys();
                    while (testId.hasNext()) {
                        String id = testId.next();
                        JSONArray testNameArr = resultObj.getJSONArray(id);
                        JSONObject json_obj = testNameArr.getJSONObject(0);
                        if (null != json_obj && json_obj.length() > 0) {
                            String status = json_obj.getString(JsonParserConstant.PARSER_KEY_STATUS);
                            String message = json_obj.getString(JsonParserConstant.PARSER_KEY_MESSAGE);
                            String lid = json_obj.getString(JsonParserConstant.PARSER_KEY_PATIENT_LID);
                            String gid = json_obj.getString(JsonParserConstant.PARSER_KEY_PATIENT_GID);
                            model.setMessage(message);
                            model.setStatus(status);
                            model.setPatientGid(gid);
                            model.setPatientLid(lid);

                        }
                        if (model.getStatus().equalsIgnoreCase(NetworkConstants.MESSAGE_SUCCESS)) {
                            DatabaseManager.getInstance().updateRegistration(mContext, DbConstant.URI_TABLE_REGISTRATION, model);
                        }
                        ApplicationDataModel.getInstance().setPatientGid(model.getPatientGid());
                        ApplicationDataModel.getInstance().setPatientLid(model.getPatientLid());
                    }

                }
               /* model.setStatus(loginDataObject.getString(JsonParserConstant.PARSER_KEY_STATUS));
                model.setMessage(loginDataObject.getString(JsonParserConstant.PARSER_KEY_MESSAGE));
                model.setPatientLid(loginDataObject.getString(JsonParserConstant.PARSER_KEY_PATIENT_LID));
                model.setPatientGid(loginDataObject.getString(JsonParserConstant.PARSER_KEY_PATIENT_GID));
                if (model.getStatus().equalsIgnoreCase(NetworkConstants.MESSAGE_SUCCESS)) {
                    DatabaseManager.getInstance().updateRegistration(mContext, DbConstant.URI_TABLE_REGISTRATION, model);
                }
                ApplicationDataModel.getInstance().setPatientGid(model.getPatientGid());
                ApplicationDataModel.getInstance().setPatientLid(model.getPatientLid());*/
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return model;
    }

    public synchronized void parseSymptomData(String jsonData, Context mContext) {

        try {
            JSONObject jsonObj = new JSONObject(jsonData);
            if (null != jsonObj && jsonObj.length() > 0) {

                JSONObject resultObj = jsonObj.getJSONObject(JsonParserConstant.PARSER_KEY_HEADER);

                if (null != resultObj && resultObj.length() > 0) {
                    Iterator<String> testId = resultObj.keys();
                    while (testId.hasNext()) {
                        String id = testId.next();
                        JSONObject testObj = resultObj.getJSONObject(id);
                        Iterator<String> tableName = testObj.keys();
                        while (tableName.hasNext()) {
                            String name = tableName.next();
                            JSONArray testNameArr = testObj.getJSONArray(name);
                            JSONObject json_obj = testNameArr.getJSONObject(0);

                            if (null != json_obj && json_obj.length() > 0) {
                                String status = json_obj.getString(JsonParserConstant.PARSER_KEY_STATUS);
                                String upFlag = null;
                                if (status != null) {
                                    if (status.equalsIgnoreCase("Success")) {
                                        upFlag = "1";
                                    } else {
                                        upFlag = "0";
                                    }
                                }
                                DatabaseManager.getInstance().updateUploadStatus(mContext, DbConstant.C_U_T_SYMPTONS, upFlag, id);
                            }
                        }

                    }

                }
            }


        } catch (
                JSONException e
                )

        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }


    public synchronized void parseImageResponse(String jsonData, Context mContext) {

        try {
            JSONObject jsonObj = new JSONObject(jsonData);
            if (null != jsonObj && jsonObj.length() > 0) {

                JSONObject resultObj = jsonObj.getJSONObject(JsonParserConstant.PARSER_KEY_HEADER);

                String status = resultObj.getString(JsonParserConstant.PARSER_KEY_STATUS);
                String id = resultObj.getString("patient_lid");
               /* String upFlag = null;
                if (status != null) {
                    if (status.equalsIgnoreCase("Success")) {
                        upFlag = "1";
                    } else {
                        upFlag = "0";
                    }
                }
                DatabaseManager.getInstance().updateUploadStatus(mContext, DbConstant.URI_IMAGES, upFlag, id);*/
            }

        } catch (
                JSONException e
                )

        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }


    public UploadResponseData parserUploadResponseData(String jsonData) {

        UploadResponseData model = null;
        try {
            jsonObject = new JSONObject(jsonData);
            if (null != jsonObject) {
                model = new UploadResponseData();
                JSONObject resultObj = jsonObject.getJSONObject(JsonParserConstant.PARSER_KEY_HEADER);
                if (null != resultObj && resultObj.length() > 0) {
                    Iterator<String> testId = resultObj.keys();
                    // hashmap consist lid as key and gid as value
                    HashMap lid_gid = new HashMap();
                    while (testId.hasNext()) {
                        String id = testId.next();
                        JSONArray testNameArr = resultObj.getJSONArray(id);
                        JSONObject json_obj = testNameArr.getJSONObject(0);
                        if (null != json_obj && json_obj.length() > 0) {
                            String status = json_obj.getString(JsonParserConstant.PARSER_KEY_STATUS);
                            String message = json_obj.getString(JsonParserConstant.PARSER_KEY_MESSAGE);
                            String lid = json_obj.getString(JsonParserConstant.PARSER_KEY_PATIENT_LID);
                            String gid = json_obj.getString(JsonParserConstant.PARSER_KEY_PATIENT_GID);

/*                if (json_array != null && json_array.length() > 0) {
                    HashMap lid_gid = new HashMap();
                    for (int i = 0; i < json_array.length(); i++) {
                        JSONObject json_obj = json_array.optJSONObject(i);
                        Iterator<String> iter = json_obj.keys();
                        while (iter.hasNext()) {
                            String gid = "";
                            String lid = iter.next();*/
                           /* try {
                                gid = (String) json_obj.get(lid);
                            } catch (JSONException e) {
                                // Something went wrong!
                            }
*/

                            lid_gid.put(lid, gid);
                            model.setMessage(message);
                            model.setStatus(status);
                        }

                        model.setLid_gid(lid_gid);
                    }

                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return model;
    }


//    /**
//     * This method will parse forgot password data returned form server
//     * @param jsonData
//     * @return ForgotPasswordData model
//     */
//    public ForgotPasswordData parserForgotPasswordData(String jsonData){
//
//        ForgotPasswordData model = null;
//        try {
//            jsonObject = new JSONObject(jsonData);
//            if(null!=jsonObject){
//                model = new ForgotPasswordData();
//                JSONObject loginDataObject = jsonObject.getJSONObject(JsonParserConstant.PARSER_KEY_HEADER);
//                model.setStatus(loginDataObject.getString(JsonParserConstant.PARSER_KEY_STATUS));
//                model.setMessage(loginDataObject.getString(JsonParserConstant.PARSER_KEY_MESSAGE));
//
//            }
//
//        }catch (JSONException e){
//            e.printStackTrace();
//        }
//
//        return model;
//    }
//


    public JsonParser() {
        super();
    }


    /**
     * parse clinic images path
     *
     * @param jsonObject
     * @return
     * @throws Exception
     */
    private String[] parseClinicImages(JSONObject jsonObject) {
        String imgPath[] = null;

        try {
            JSONArray imageArray = jsonObject.getJSONArray(JsonParserConstant.PARSER_KEY_CLINIC_IMAGES);
            if (null != imageArray && imageArray.length() > 0) {
                int len = imageArray.length();
                imgPath = new String[len];
                for (int i = 0; i < len; i++) {
                    imgPath[i] = imageArray.getString(i);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return imgPath;
    }

    /**
     * parse clinic video path data
     *
     * @param jsonObject
     * @return
     */
    private String[] parseClinicVideo(JSONObject jsonObject) {

        String videoPath[] = null;
        try {

            JSONArray videoArray = jsonObject.getJSONArray(JsonParserConstant.PARSER_KEY_CLINIC_VIDEOS);
            if (null != videoArray && videoArray.length() > 0) {
                int len = videoArray.length();
                videoPath = new String[len];
                for (int i = 0; i < len; i++) {
                    videoPath[i] = videoArray.getString(i);
                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return videoPath;
    }


    /**
     * This method will parse clinic/doctor search data returned form server
     *
     * @param jsonData
     * @return ClinicData model
     */
    public ArrayList<ClinicData> parserClinicSearchData(String jsonData) {

        ArrayList<ClinicData> clinicList = null;
        try {
            jsonObject = new JSONObject(jsonData);
            if (null != jsonObject) {

                clinicList = new ArrayList<>();
                JSONObject responseObject = jsonObject.getJSONObject(JsonParserConstant.PARSER_KEY_HEADER);

                JSONArray clinicDataArray = responseObject.getJSONArray(JsonParserConstant.PARSER_KEY_CLINIC_DATA);

                for (int i = 0; i < clinicDataArray.length(); i++) {


                    ClinicData model = new ClinicData();

                    JSONObject clinicDataObject = clinicDataArray.getJSONObject(i);

                    model.setId(clinicDataObject.getString(JsonParserConstant.PARSER_KEY_CLINIC_ID));
                    model.setName(clinicDataObject.getString(JsonParserConstant.PARSER_KEY_CLINIC_NAME));
                    model.setAddress(clinicDataObject.getString(JsonParserConstant.PARSER_KEY_CLINIC_ADDRESS));
                    model.setStatus(clinicDataObject.getString(JsonParserConstant.PARSER_KEY_CLINIC_STATUS));
                    model.setLat(clinicDataObject.getString(JsonParserConstant.PARSER_KEY_CLINIC_LATITUDE));
                    model.setLon(clinicDataObject.getString(JsonParserConstant.PARSER_KEY_CLINIC_LONGITUDE));
                    model.setTiming(clinicDataObject.getString(JsonParserConstant.PARSER_KEY_CLINIC_TIMING));
                    model.setPhone(clinicDataObject.getString(JsonParserConstant.PARSER_KEY_CLINIC_PHONE));

                    // parse clinic images and set to model
                    model.setImagePath(parseClinicImages(clinicDataObject));
                    // parse clinic videos and set to model
                    model.setVideoPath(parseClinicVideo(clinicDataObject));

                    // parse available service data
                    model.setServiceList(parseServiceData(clinicDataObject));

                    // parse doctor data and set list to model
                    model.setDoctorDataList(parseDoctorData(clinicDataObject));

                    clinicList.add(model);
                }

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return clinicList;
    }


    /**
     * parse doctor data
     *
     * @param jsonObject
     * @return ArrayList<DoctorData>
     */
    private ArrayList<DoctorData> parseDoctorData(JSONObject jsonObject) {

        ArrayList<DoctorData> docList = null;
        DoctorData docModel = null;
        try {

            JSONArray docDataArray = jsonObject.optJSONArray(JsonParserConstant.PARSER_KEY_DOCTOR_DATA);
            if (null != docDataArray && docDataArray.length() > 0) {
                docList = new ArrayList<DoctorData>();

                for (int i = 0; i < docDataArray.length(); i++) {
                    docModel = new DoctorData();
                    JSONObject docDataObject = docDataArray.optJSONObject(i);
                    docModel.setId(docDataObject.optString(JsonParserConstant.PARSER_KEY_DOCTOR_ID));
                    docModel.setName(docDataObject.optString(JsonParserConstant.PARSER_KEY_DOCTOR_NAME));
                    docModel.setQualification(docDataObject.optString(JsonParserConstant.PARSER_KEY_DOCTOR_DEGREE));
                    docModel.setExperience(docDataObject.optString(JsonParserConstant.PARSER_KEY_DOCTOR_EXPERIENCE));
                    docModel.setMobile(docDataObject.optString(JsonParserConstant.PARSER_KEY_DOCTOR_MOBILE));
                    docModel.setProfilePic(docDataObject.optString(JsonParserConstant.PARSER_KEY_DOCTOR_PROFILE_PIC));
                    docModel.setAssociation(docDataObject.optString(JsonParserConstant.PARSER_KEY_DOCTOR_ASSOCIATION));
                    docModel.setAvailability(docDataObject.optString(JsonParserConstant.PARSER_KEY_DOCTOR_AVAILABILITY));
                    docModel.setFees(docDataObject.optString(JsonParserConstant.PARSER_KEY_DOCTOR_FEES));
                    docModel.setTiming(docDataObject.optString(JsonParserConstant.PARSER_KEY_DOCTOR_TIMING));
                    docModel.setSpeciality(docDataObject.optString(JsonParserConstant.PARSER_KEY_DOCTOR_SPECIALITY));

                    docList.add(docModel);

                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return docList;
    }


    /**
     * parse services available at clinic
     *
     * @param jsonObject
     * @return
     */
    private ArrayList<ServicesData> parseServiceData(JSONObject jsonObject) {
        ArrayList<ServicesData> serviceList = null;
        ServicesData model = null;
        try {

            JSONArray serviceArray = jsonObject.getJSONArray(JsonParserConstant.PARSER_KEY_SERVICES_DATA);
            if (null != serviceArray && serviceArray.length() > 0) {

                serviceList = new ArrayList<ServicesData>();

                for (int i = 0; i < serviceArray.length(); i++) {
                    model = new ServicesData();
                    JSONObject serviceObject = serviceArray.optJSONObject(i);
                    model.setServiceID(serviceObject.optString(JsonParserConstant.PARSER_KEY_SERVICE_ID));
                    model.setServiceName(serviceObject.optString(JsonParserConstant.PARSER_KEY_SERVICE_NAME));
                    model.setServiceDescription(serviceObject.optString(JsonParserConstant.PARSER_KEY_SERVICE_DESCRIPTION));
                    serviceList.add(model);
                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return serviceList;
    }

    /**
     * parse time slot data response
     *
     * @param jsonData
     * @return model
     */
    public ArrayList<SlotDateListData> parseTimeSlotData(String jsonData) {

        SlotDateListData model = null;
        ArrayList<TimeSlot> slotList = null;
        ArrayList<SlotDateListData> slotDayListData = null;

        try {
            jsonObject = new JSONObject(jsonData);
            if (null != jsonObject) {

                slotDayListData = new ArrayList<SlotDateListData>();
                JSONObject apptDataObject = jsonObject.optJSONObject(JsonParserConstant.PARSER_KEY_HEADER);
                JSONObject slotDataObject = apptDataObject.optJSONObject(JsonParserConstant.PARSER_KEY_TIME_SLOT_DATA);
                if (null != slotDataObject && slotDataObject.length() > 0) {

                    Iterator<String> arrKeys = slotDataObject.keys();
                    while (arrKeys.hasNext()) {

                        model = new SlotDateListData();
                        model.setStatus(apptDataObject.optString(JsonParserConstant.PARSER_KEY_STATUS));
                        model.setMessage(apptDataObject.optString(JsonParserConstant.PARSER_KEY_MESSAGE));

                        String indexKey = arrKeys.next();
                        JSONArray slotArrayObject = slotDataObject.optJSONArray(indexKey);

                        // parse time slot time data
                        slotList = parseSlots(slotArrayObject, indexKey);

                        model.setSlotList(slotList);
                        slotDayListData.add(model);
                    }
                }
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return slotDayListData;
    }


    /**
     * parse slot data
     *
     * @param slotArrayObject
     * @return ArrayList<TimeSlot> object
     */
    private ArrayList<TimeSlot> parseSlots(JSONArray slotArrayObject, String date) {

        ArrayList<TimeSlot> slotList = null;
        try {
            if (null != slotArrayObject && slotArrayObject.length() > 0) {

                slotList = new ArrayList<TimeSlot>();
                for (int i = 0; i < slotArrayObject.length(); i++) {
                    TimeSlot dataModel = new TimeSlot();
                    JSONObject slotData = slotArrayObject.optJSONObject(i);
                    dataModel.setSlotId(slotData.optString(JsonParserConstant.PARSER_KEY_TIME_SLOT_ID));
                    dataModel.setTimeFormat(slotData.optString(JsonParserConstant.PARSER_KEY_TIME_FORMAT));
                    dataModel.setSlotStatus(slotData.optString(JsonParserConstant.PARSER_KEY_SLOT_STATUS));
                    dataModel.setEndTime(slotData.optString(JsonParserConstant.PARSER_KEY_END_TIME));
                    dataModel.setShift(slotData.optString(JsonParserConstant.PARSER_KEY_SHIFT));
                    dataModel.setStartTime(slotData.optString(JsonParserConstant.PARSER_KEY_START_TIME));
                    dataModel.setDate(date);
                    slotList.add(dataModel);
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return slotList;
    }


    /**
     * this method is used to parse response for appointment booking request service
     *
     * @param jsonData
     * @return model
     */
    public AppointmentRequest parseApptCreationData(String jsonData) {
        AppointmentRequest model = null;

        try {
            jsonObject = new JSONObject(jsonData);
            if (null != jsonObject) {
                model = new AppointmentRequest();
                JSONObject apptDataObject = jsonObject.optJSONObject(JsonParserConstant.PARSER_KEY_HEADER);
                model.setStatus(apptDataObject.optString(JsonParserConstant.PARSER_KEY_STATUS));
                model.setMessage(apptDataObject.optString(JsonParserConstant.PARSER_KEY_MESSAGE));
                model.setApptId(apptDataObject.optString(JsonParserConstant.PARSER_KEY_APPOINTMENT_ID));
                model.setPatientLid(apptDataObject.optString(JsonParserConstant.PARSER_KEY_PATIENT_LID));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return model;
    }

    public DiagnosticData parseDiagnosticData(String jsonData) {
        DiagnosticData diagnosticData = null;
        ArrayList diagnosticValue = null;
        try {
            jsonObject = new JSONObject(jsonData);
            if (null != jsonObject) {
                diagnosticData = new DiagnosticData();
                JSONObject diagnosticDataObject = jsonObject.optJSONObject(JsonParserConstant.PARSER_KEY_HEADER);
                diagnosticData.setStatus(diagnosticDataObject.optString(JsonParserConstant.PARSER_KEY_STATUS));
                diagnosticData.setMessage(diagnosticDataObject.optString(JsonParserConstant.PARSER_KEY_MESSAGE));

                JSONArray diag_array = diagnosticDataObject.optJSONArray(JsonParserConstant.PARSER_KEY_DIAGNOSTIC_LIST);
                if (diag_array != null && diag_array.length() > 0) {
                    diagnosticValue = new ArrayList();
                    for (int i = 0; i < diag_array.length(); i++) {
                        JSONObject json_obj = diag_array.optJSONObject(i);
                        if (json_obj != null && json_obj.length() > 0) {
                            String test_name = json_obj.optString(JsonParserConstant.PARSER_KEY_DIAGNOSTIC_KEY);
                            diagnosticValue.add(test_name);
                        }
                    }
                    diagnosticData.setDiagnostic_data(diagnosticValue);
                }

            }


        } catch (JSONException e) {
            e.printStackTrace();
        }

        return diagnosticData;
    }


    public ReportData parseReportData(String jsonData) {
        ReportData reportData = null;
        try {
            jsonObject = new JSONObject(jsonData);
            if (null != jsonObject) {
                reportData = new ReportData();
                JSONObject reportDataObject = jsonObject.optJSONObject(JsonParserConstant.PARSER_KEY_HEADER);
                reportData.setStatus(reportDataObject.optString(JsonParserConstant.PARSER_KEY_STATUS));
                reportData.setMessage(reportDataObject.optString(JsonParserConstant.PARSER_KEY_MESSAGE));
                reportData.setReport_path(reportDataObject.optString(JsonParserConstant.PARSER_KEY_REPORT_PATH));
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return reportData;
    }
}
