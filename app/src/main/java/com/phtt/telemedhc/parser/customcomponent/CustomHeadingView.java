package com.phtt.telemedhc.parser.customcomponent;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.TableLayout;
import android.widget.TextView;

import com.phtt.telemedhc.parser.QuestionEntity;


public class CustomHeadingView extends  TextView implements OnClickListener {

	TableLayout.LayoutParams param;
	Context mContext;
	
	public CustomHeadingView(Context context, IEvent listener, String id, int index) {
		super(context);
		// TODO Auto-generated constructor stub
		mContext = context;
		param = new TableLayout.LayoutParams(0,TableLayout.LayoutParams.WRAP_CONTENT,0.5f);
		setLayoutParams(param);
//		setTextSize(20);
//		ViewUtils.setTypeFace(mContext, this, ViewUtils.MEDIUM_NORMAL_SIZE, Typeface.BOLD);
		
		setListenr(listener);
		setQuestionID(id);
		setIndex(index);
		
	}
	
	
	
	public void initialize()
	{
//	  this.setText(getRequiredData().getValue());
	  listenr.onClick("heading", getQuestionID(), getIndex(), getRequiredData().getColumnname());
	  
	}

	
	int index;
	 
	public int getIndex() {
		return index;
	}
	public void setIndex(int index) {
		this.index = index;
	}


	String questionID;
	public String getQuestionID() {
		return questionID;
	}

	public void setQuestionID(String questionID) {
		this.questionID = questionID;
	}
	


	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}
	IEvent listenr;
	public IEvent getListenr() {
		return listenr;
	}
	
	public void setListenr(IEvent listenr) {
		this.listenr = listenr;
	}
	
	QuestionEntity questionEntity;
	public void setRequiredData(QuestionEntity data)
	{
		this.questionEntity = data;
	}
	
	public QuestionEntity getRequiredData()
	{
		return questionEntity;
	}



	
}
