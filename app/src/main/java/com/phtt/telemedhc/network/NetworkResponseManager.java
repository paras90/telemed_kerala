package com.phtt.telemedhc.network;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.phtt.telemedhc.models.AppointmentRequest;
import com.phtt.telemedhc.models.ClinicData;
import com.phtt.telemedhc.models.DiagnosticData;
import com.phtt.telemedhc.models.LoginData;
import com.phtt.telemedhc.models.RegistrationData;
import com.phtt.telemedhc.models.ReportData;
import com.phtt.telemedhc.models.SlotDateListData;
import com.phtt.telemedhc.models.UploadResponseData;
import com.phtt.telemedhc.parser.jsonparser.JsonParser;
import com.phtt.telemedhc.utils.INetResult;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Kewal on 06-10-2015.
 */
public class NetworkResponseManager {
    JsonParser parser;

    /**
     * process server response for json parsing
     *
     * @param mContext
     * @param requestId
     * @param response
     */
    public void processServerResponse(Context mContext, int requestId, JSONObject response, INetResult listener) {

        Intent intent = null;

        if (null != response) {

//            notify = new NotificationsDisplay(mContext);

            switch (requestId) {
                case NetworkConstants.REQUEST_ID_LOGIN:

                    parser = new JsonParser();
                    LoginData model = parser.parseLoginData(response.toString());
                    Log.v("login_response", response.toString());
                    if (null != model && model.getStatus().contains(NetworkConstants.MESSAGE_SUCCESS)) {
                        listener.onResult(true, model.getMessage(), model);
                    } else {
                        listener.onResult(false, model.getMessage(), model);
                    }

                    break;


                case NetworkConstants.REQUEST_ID_GCM_ID_UPLOAD:

                    parser = new JsonParser();
                    Log.v("gcm_response", response.toString());
                    break;

                case NetworkConstants.REQUEST_ID_REGISTRATION:

                    parser = new JsonParser();
                    RegistrationData registrationModel = parser.parserRegistrationData(mContext, response.toString());
                    try {
                        if (null != registrationModel && registrationModel.getStatus().equalsIgnoreCase(NetworkConstants.MESSAGE_SUCCESS)) {
                            listener.onResult(true, registrationModel.getMessage(), registrationModel);

                        } else {
                            listener.onResult(false, registrationModel.getMessage(), registrationModel);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }


                    break;

                case NetworkConstants.REQUEST_ID_FORGOT_PASSWORD:
                  /*  parser = new JsonParser();
                    ForgotPasswordData dataModel = parser.parserForgotPasswordData(response.toString());
                    try {
                        if (null != dataModel && dataModel.getStatus().equalsIgnoreCase(NetworkConstants.MESSAGE_SUCCESS)) {
                            listener.onResult(true, dataModel.getMessage());
                        } else {
                            listener.onResult(false, dataModel.getMessage());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
*/
                    break;


                case NetworkConstants.REQUEST_ID_SYNC_UPLOAD: {
                    parser = new JsonParser();
                    UploadResponseData sync_upload = parser.parserUploadResponseData(response.toString());
                    try {
                        if (null != sync_upload && sync_upload.getStatus().equalsIgnoreCase(NetworkConstants.MESSAGE_SUCCESS)) {
                            listener.onResult(true, sync_upload.getMessage(), sync_upload);

                        } else {
                            listener.onResult(false, sync_upload.getMessage(), sync_upload);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                }
                break;

                case NetworkConstants.REQUEST_ID_ALLIMAGE_UPLOAD: {
                    parser = new JsonParser();
                    parser.parseImageResponse(response.toString(), mContext);
                    if (null != response.toString() && !response.toString().isEmpty()) {
                        listener.onResult(true, NetworkConstants.MESSAGE_SUCCESS);
                    } else {
                        listener.onResult(false, NetworkConstants.MESSAGE_FAIL);

                    }
                  /*  UploadResponseData sync_upload=parser.parserUploadResponseData(response.toString());
                    try {
                        if (null != sync_upload && sync_upload.getStatus().equalsIgnoreCase(NetworkConstants.MESSAGE_SUCCESS)) {
                            listener.onResult(true, sync_upload.getMessage(),sync_upload);

                        } else {
                            listener.onResult(false, sync_upload.getMessage(),sync_upload);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }*/

                }
                break;

                case NetworkConstants.REQUEST_ID_SYMPTOM: {
                    parser = new JsonParser();
                    parser.parseSymptomData(response.toString(), mContext);
                    if (null != response.toString() && !response.toString().isEmpty()) {
                        listener.onResult(true, NetworkConstants.MESSAGE_SUCCESS);
                    } else {
                        listener.onResult(false, NetworkConstants.MESSAGE_FAIL);

                    }

                }
                break;

                case NetworkConstants.REQUEST_ID_SEARCH_CLINIC_DATA: {
                    parser = new JsonParser();
                    ArrayList<ClinicData> clinicDataModel = parser.parserClinicSearchData(response.toString());
                    if (clinicDataModel != null && (!clinicDataModel.isEmpty())) {
                        listener.onResult(true, "", clinicDataModel);
                    } else {
                        listener.onResult(false, "", clinicDataModel);
                    }
                }
                break;

                case NetworkConstants.REQUEST_ID_GET_APPOINTMENT_TIME_SLOT:
                    parser = new JsonParser();
                    ArrayList<SlotDateListData> time_slot_data = parser.parseTimeSlotData(response.toString());
                    if (null != time_slot_data && time_slot_data.get(0).getStatus().equalsIgnoreCase(NetworkConstants.MESSAGE_SUCCESS)) {
                        Log.d("time_slot_data", time_slot_data.toString());
                        listener.onResult(true, time_slot_data.get(0).getMessage(), time_slot_data);
                    } else {
                        listener.onResult(false, time_slot_data.get(0).getMessage(), time_slot_data);
                    }
                    break;

                case NetworkConstants.REQUEST_ID_BOOK_APPOINTMENT_TIME_SLOT:
                    parser = new JsonParser();
                    AppointmentRequest resData = parser.parseApptCreationData(response.toString());

                    if (null != resData && resData.getStatus().contains(NetworkConstants.MESSAGE_SUCCESS)) {
                        listener.onResult(true, resData.getMessage(), resData);
                    } else {
                        listener.onResult(false, resData.getMessage(), resData);
                    }

                    break;

                case NetworkConstants.REQUEST_ID_DIAGNOSTIC_LIST:
                    parser = new JsonParser();
                    DiagnosticData diagnosticData = parser.parseDiagnosticData(response.toString());
                    Log.d("processServerResponse: ", response.toString());
                    if (null != diagnosticData && diagnosticData.getStatus().equalsIgnoreCase(NetworkConstants.MESSAGE_SUCCESS)) {
                        listener.onResult(true, diagnosticData.getMessage(), diagnosticData);
                    } else {
                        listener.onResult(false, diagnosticData.getMessage(), diagnosticData);
                    }

                    break;

                case NetworkConstants.REQUEST_ID_REPORT:
                    parser = new JsonParser();
                    ReportData reportData = parser.parseReportData(response.toString());
                    if (null != reportData && reportData.getStatus().equalsIgnoreCase(NetworkConstants.MESSAGE_SUCCESS)) {
                        listener.onResult(true, reportData.getMessage(), reportData);
                    } else {
                        listener.onResult(false, reportData.getMessage(), reportData);
                    }

                    break;

                default:
                    break;

            }

        }

    }
}
