package com.phtt.telemedhc.network;

import android.graphics.Bitmap;
import android.support.v4.util.LruCache;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;

import com.phtt.telemedhc.ui.TelemedhcApplication;


/**
 * Created by Hemant on 05-10-2015.
 */
public class VolleySingleton {

    public static VolleySingleton mInstance=null;
    private RequestQueue mRequestQueue;
    private ImageLoader mImageLoader;

    private VolleySingleton(){
       mRequestQueue= Volley.newRequestQueue(TelemedhcApplication.getAppContext());
        mImageLoader=new ImageLoader(mRequestQueue,new ImageLoader.ImageCache(){

            private LruCache<String,Bitmap> cache=new LruCache<>((int)(Runtime.getRuntime().maxMemory()/1024)/8);
            @Override
            public Bitmap getBitmap(String url) {
                return cache.get(url);
            }

            @Override
            public void putBitmap(String url, Bitmap bitmap) {
                cache.put(url, bitmap);

            }
        });
    }

    public static VolleySingleton getInstance(){
        if(mInstance==null){
            mInstance=new VolleySingleton();
        }
        return  mInstance;
    }

    public RequestQueue getRequestQueue(){
        return  mRequestQueue;
    }

    public ImageLoader getImageLoader(){
        return  mImageLoader;
    }
}
