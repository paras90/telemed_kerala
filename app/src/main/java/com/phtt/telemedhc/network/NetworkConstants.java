package com.phtt.telemedhc.network;

/**
 * Created by HP on 06-10-2015.
 * This class will contain all network i/o related constants
 */
public class NetworkConstants {

    public static final int REQUEST_ID_LOGIN                                    = 1;
    public static final int REQUEST_ID_FORGOT_PASSWORD                          = 2;
    public static final int REQUEST_ID_REGISTRATION                             = 3;
    public static final int REQUEST_ID_SEARCH_CLINIC_DATA                       = 4;
    public static final int REQUEST_ID_UPLOAD_PIC_DATA                          = 5;
    public static final int REQUEST_ID_GET_APPOINTMENT_TIME_SLOT                = 6;
    public static final int REQUEST_ID_KEY_NEWS_FEED                            = 7;
    public static final int REQUEST_ID_USER_TREATMENT_DATA                      = 8;
    public static final int REQUEST_ID_BOOK_APPOINTMENT_TIME_SLOT               = 9;
    public static final int REQUEST_ID_CANCEL_APPOINTMENT_TIME_SLOT             = 10;
    public static final int REQUEST_ID_GCM_ID_UPLOAD                            = 11;
    public static final int REQUEST_ID_LOCATION_SEARCH                          = 12;
    public static final int REQUEST_ID_SYMPTOM_SEARCH                           = 13;
    public static final int REQUEST_ID_SYNC_UPLOAD                              = 14;
    public static final int REQUEST_ID_SYNC_DOWNLOAD                            = 15;
    public static final int REQUEST_ID_ALLIMAGE_UPLOAD                          = 16;
    public static final int REQUEST_ID_DIAGNOSTIC_LIST                          = 17;
    public static final int REQUEST_ID_REPORT                                   = 18;
    public static final int REQUEST_ID_SYMPTOM = 19;

    public static final String REQUEST_KEY_USERNAME                             = "username";
    public static final String REQUEST_KEY_PASSWORD                             = "password";
    public static final String REQUEST_KEY_PATIENT_LID                          = "patient_lid";
    public static final String REQUEST_KEY_PATIENT_MOBILE                       = "patient_mobile";
    public static final String REQUEST_KEY_LATITUDE                             = "latitude";
    public static final String REQUEST_KEY_LONGITUDE                            = "longitude";
    public static final String REQUEST_KEY_LOCATION_NAME                        = "location_name";
    public static final String REQUEST_KEY_SEARCH_AREA                          = "search_area_range";
    public static final String REQUEST_KEY_PAGE_INDEX                           = "page_index";
    public static final String REQUEST_KEY_SEARCH_BY                            = "search_by";
    public static final String REQUEST_KEY_UPLOAD_PIC                            = "image_path";
    public static final String MESSAGE_SUCCESS                                  = "success";
    public static final String MESSAGE_FAIL                                 = "fail";
    public static final String REQUEST_KEY_FEED_COUNT                           = "feed_count";


    // Slot Constants
    public static final String REQUEST_KEY_CLINIC_ID                             = "clinic_id";
    public static final String REQUEST_KEY_DOCTOR_ID                             = "doctor_id";
    public static final String REQUEST_KEY_DOCTOR_NAME                           = "doctor_name";
    public static final String REQUEST_KEY_APPOINTMENT_DATE                      = "appt_date";
    public static final String REQUEST_KEY_APPOINTMENT_TIME_SLOT                 = "time_slot";
    public static final String REQUEST_KEY_APPOINTMENT_ID                        = "appt_id";

    // FAB Constants
    public static final String SEARCH_BY_GYNECOLOGIST                            ="gynecologist";
    public static final String SEARCH_BY_CARDIOLOGIST                            ="cardiologist";
    public static final String SEARCH_BY_DENTIST                                 ="dentist";
    public static final String SEARCH_BY_AYURVEDA                                ="ayurveda";
    public static final String SEARCH_BY_NEUROLOGIST                             ="neurologist";
    public static final String SEARCH_BY_DERMATOLOGIST                           ="dermatologist";
    public static final String SEARCH_BY_PSYCHATRIST                             ="psychatrist";
    public static final String SEARCH_BY_GENERAL_PHYSICIAN                       ="general_physician";
    public static final String SEARCH_BY_HOMEOPATHIC                             ="homeopathic";
    public static final String SEARCH_BY_GASTRO                                  ="gastro";
    public static final String SEARCH_BY_COUGH_COLD                              ="cough and cold";


    public static final String SEARCH_AREA_RANGE                                 ="100";
    public static final String REQUEST_KEY_APPOINTMENT_CANCEL                    = "appt_id";
    public static final String SEARCH_PAGE_INDEX                                 ="1";

    // GCM Constants
    public static final String APP_NAME                                          ="app_name";
    public static final String GCM_API_KEY                                       ="api_key";
    public static final String USER_ID                                           ="user_id";
    public static final String APP_REG_ID                                        ="device_registration_id";
    public static final String APP_REG_UPDATE_ID                                 ="device_registration_id_new";
    public static final String APP_GCM_UPDATE_FLAG                               ="update_flag";

    // Location
    public static final String SEARCH_BY_LOCATION_TEXT                           ="search_key";
    public static final String SEARCH_BY_SYMPTOM_TEXT                            ="location";

}
