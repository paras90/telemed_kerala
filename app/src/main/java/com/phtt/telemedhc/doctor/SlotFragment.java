package com.phtt.telemedhc.doctor;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.phtt.telemedhc.R;
import com.phtt.telemedhc.adapter.SlotAdapter;
import com.phtt.telemedhc.fragment.BaseFragment;
import com.phtt.telemedhc.models.ClinicData;
import com.phtt.telemedhc.models.TimeSlot;

import java.util.ArrayList;

/**
 * Created by Arvind on 12-10-2015.
 */
@SuppressLint("ValidFragment")
public class SlotFragment extends BaseFragment {

    View view;
    Context mContext;
    SlotAdapter slotAdapter;
    GridView grid_view;
    ArrayList<TimeSlot> time_slot_data;
    volatile ClinicData clinic_data;

    public SlotFragment(){}

    public SlotFragment(ArrayList<TimeSlot> time_slot_data, ClinicData clinic_data) {

        this.clinic_data=clinic_data;
        this.time_slot_data=time_slot_data;
    }

    public SlotFragment getFragmentData(){

        SlotFragment slotFragment = new SlotFragment();
        Bundle args = new Bundle();
        args.putSerializable("slot_data",time_slot_data);
        slotFragment.setArguments(args);


        return slotFragment;
    }
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_slot, container, false);
        mContext=getActivity();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initViews(view);
        initData();
        setListeners();
    }

    @Override
    public void initViews(View rootView) {
        grid_view= (GridView) rootView.findViewById(R.id.slot_list);
    }

    @Override
    public void initData() {
        time_slot_data= (ArrayList<TimeSlot>) getArguments().getSerializable("slot_data");
        slotAdapter=new SlotAdapter(mContext,time_slot_data);
        grid_view.setAdapter(slotAdapter);
    }

    @Override
    public void setListeners() {


       /* slot_list.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            int previousGroup = -1;

            @Override
            public void onGroupExpand(int groupPosition) {
                if(groupPosition != previousGroup)
                    slot_list.collapseGroup(previousGroup);
                previousGroup = groupPosition;
            }
        });*/

    }


}
