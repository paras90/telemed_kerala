package com.phtt.telemedhc.doctor;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.phtt.telemedhc.R;
import com.phtt.telemedhc.fragment.BaseFragment;


/**
 * Created by Arvind on 07-10-2015.
 */


public class AllTimingFragment extends BaseFragment {
    View view;
    ImageView cross_icon;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_all_timing, container, false);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        initViews(view);
        initData();
        setListeners();
    }

    @Override
    public void initViews(View rootView) {
        cross_icon=(ImageView)rootView.findViewById(R.id.cross_icon);
    }


    @Override
    public void initData() {

    }


    @Override
    public void setListeners() {
        cross_icon.setOnClickListener(m_click);
    }


    View.OnClickListener m_click=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          getActivity().onBackPressed();
        }
    };
}
