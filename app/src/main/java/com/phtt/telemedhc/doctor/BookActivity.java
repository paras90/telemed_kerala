package com.phtt.telemedhc.doctor;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.phtt.telemedhc.R;
import com.phtt.telemedhc.models.ApplicationDataModel;
import com.phtt.telemedhc.models.ClinicData;
import com.phtt.telemedhc.models.SlotDateListData;
import com.phtt.telemedhc.network.NetworkConstants;
import com.phtt.telemedhc.network.NetworkRequestManager;
import com.phtt.telemedhc.network.VolleySingleton;
import com.phtt.telemedhc.utils.INetResult;
import com.phtt.telemedhc.utils.NotificationsDisplay;
import com.pkmmte.view.CircularImageView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;


public class BookActivity extends AppCompatActivity implements INetResult {

    private CircularImageView circular_image;
    private ImageLoader imageLoader;
    RelativeLayout doctor_data;
    ImageView previous,next;
    TextView date;
    ViewPager pager;
    PagerFragment pagerAdapter;
    INetResult listener;
    volatile ArrayList<SlotDateListData> slot_data;
    Context mContext;
    NotificationsDisplay notify;
    public ClinicData clinic_data;
    TextView doctor_name,clinic_name;
    int index;
    TextView doctor_degree,clinic_address,phone_number;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book);

        mContext=this;
        initViews();
        initData();
        setListener();
    }


    private void initViews(){
        circular_image= (CircularImageView) findViewById(R.id.profile_pic);
        doctor_data=(RelativeLayout)findViewById(R.id.profile_layout);
        doctor_name= (TextView) findViewById(R.id.doc_name);
        doctor_degree= (TextView) findViewById(R.id.doc_degree);
        clinic_name= (TextView) findViewById(R.id.clinic_name);
        clinic_address= (TextView) findViewById(R.id.clinic_address);
        phone_number= (TextView) findViewById(R.id.phone_number);

        previous=(ImageView)findViewById(R.id.prev_arrow);
        next=(ImageView)findViewById(R.id.next_arrow);
        pager=(ViewPager)findViewById(R.id.pager);
        date=(TextView)findViewById(R.id.date);
        notify = new NotificationsDisplay(mContext);
    }

    private void  initData(){
        listener= this;
        imageLoader= VolleySingleton.getInstance().getImageLoader();
        Bundle clinic_data_bundle=getIntent().getExtras();
        if(clinic_data_bundle!=null) {
            clinic_data = ApplicationDataModel.getInstance().getClinicData();//(ClinicData) clinic_data_bundle.getSerializable("clinic_data");
            Log.d("clinic_data", clinic_data.toString());
            doctor_name.setText(clinic_data.getDoctorDataList().get(0).getName());
            if(clinic_data.getDoctorDataList().get(0).getQualification()!=null &&
                    !(clinic_data.getDoctorDataList().get(0).getQualification()).isEmpty())
            doctor_degree.setText("(" +clinic_data.getDoctorDataList().get(0).getQualification() +")");

            clinic_name.setText(clinic_data.getName());
            clinic_address.setText(clinic_data.getAddress());
            phone_number.setText(clinic_data.getPhone());

            if(clinic_data.getDoctorDataList().get(0).getProfilePic()!=null) {
            imageLoader.get(clinic_data.getDoctorDataList().get(0).getProfilePic(), new ImageLoader.ImageListener() {
                @Override
                public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                    if (response.getBitmap() != null) {
                        circular_image.setImageBitmap(response.getBitmap());
                    }else{
                        circular_image.setImageResource(R.drawable.default_doctor_profile);
                    }
                }

                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("image_load_error", "volley_image_loader");
                    circular_image.setImageResource(R.drawable.default_doctor_profile);
                }
            });}else{
                circular_image.setImageResource(R.drawable.default_doctor_profile);
            }
        }

        //ApplicationDataModel.getInstance().setPatientLid(Utils.getInstance().generateId(mContext));
        NetworkRequestManager time_slot_request= new NetworkRequestManager(mContext,listener);
        Bundle bundle = new Bundle();
        bundle.putString(NetworkConstants.REQUEST_KEY_PATIENT_LID, ApplicationDataModel.getInstance().getPatientLid());
        bundle.putString(NetworkConstants.REQUEST_KEY_CLINIC_ID, ""+clinic_data.getId());
        bundle.putString(NetworkConstants.REQUEST_KEY_DOCTOR_ID, ""+clinic_data.getDoctorDataList().get(0).getId());
        //bundle.putString(NetworkConstants.REQUEST_KEY_APPOINTMENT_DATE, "");
        time_slot_request.generateServiceRequestParams(NetworkConstants.REQUEST_ID_GET_APPOINTMENT_TIME_SLOT, bundle);
    }

    private void setListener() {
       // doctor_data.setOnClickListener(m_click);
        previous.setOnClickListener(m_click);
        next.setOnClickListener(m_click);
    }




    View.OnClickListener m_click=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(v==doctor_data) {
                Intent intent = new Intent(BookActivity.this, DoctorDetailActivity.class);
                startActivity(intent);
                finish();
            } else if(v==next){
                if(slot_data!=null && slot_data.size() >0) {
                    if ((pager.getCurrentItem() + 1) < slot_data.size()) {
                        date.setText(slot_data.get(pager.getCurrentItem() + 1).getSlotList().get(0).getDate());
                        pager.setCurrentItem(pager.getCurrentItem() + 1);
                    }
                }

            }else if(v==previous){
                if(slot_data!=null && slot_data.size()>0) {
                    if (pager.getCurrentItem() > 0) {
                        date.setText(slot_data.get(pager.getCurrentItem() - 1).getSlotList().get(0).getDate());
                        pager.setCurrentItem(pager.getCurrentItem() - 1);
                    }
                }
            }
        }
    };

    @Override
    public void onResult(boolean resultFlag, String message) {}

    @Override
    public void onResult(boolean resultFlag, String message, ArrayList<?> dataList) {

        if(resultFlag){
            slot_data= (ArrayList<SlotDateListData>) dataList;
            pagerAdapter = new PagerFragment(getSupportFragmentManager());
            pager.setAdapter(pagerAdapter);
            pagerAdapter.sortdateAscendingOrder();
            date.setText(slot_data.get(0).getSlotList().get(0).getDate());

            pager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {}

                @Override
                public void onPageSelected(int position) {
                    date.setText(slot_data.get(position).getSlotList().get(0).getDate());
                }
                @Override
                public void onPageScrollStateChanged(int state) {}
            });
        }else{
            notify.customToast(mContext,mContext.getResources().getString(R.string.info), message);
        }
    }

    @Override
    public void onResult(boolean resultFlag, String message, Object data) {}


    private class PagerFragment extends FragmentPagerAdapter {

        public PagerFragment(FragmentManager fm) {
            super(fm);
        }


        @Override
        public Fragment getItem(int position) {
            return new SlotFragment(slot_data.get(position).getSlotList(),clinic_data).getFragmentData();
        }

        @Override
        public int getCount() {
            return slot_data.size();
        }


        public void sortdateAscendingOrder(){
            Comparator<SlotDateListData> dateComparator=new Comparator<SlotDateListData>() {
                @Override
                public int compare(SlotDateListData object1, SlotDateListData object2) {
                    return object1.getSlotList().get(0).getDate().compareToIgnoreCase(object2.getSlotList().get(0).getDate());
                }
            };
            Collections.sort(slot_data,dateComparator);
        }
    }
}
