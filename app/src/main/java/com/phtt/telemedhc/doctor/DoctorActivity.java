package com.phtt.telemedhc.doctor;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;

import com.android.volley.toolbox.ImageLoader;
import com.phtt.telemedhc.R;
import com.phtt.telemedhc.adapter.SearchResultAdapter;
import com.phtt.telemedhc.models.ApplicationDataModel;
import com.phtt.telemedhc.models.ClinicData;
import com.phtt.telemedhc.network.NetworkConstants;
import com.phtt.telemedhc.network.NetworkRequestManager;
import com.phtt.telemedhc.network.VolleySingleton;
import com.phtt.telemedhc.ui.ActivityManagerForFinish;
import com.phtt.telemedhc.utils.INetResult;
import com.phtt.telemedhc.utils.NotificationsDisplay;
import com.phtt.telemedhc.utils.Utils;

import java.util.ArrayList;

public class DoctorActivity extends AppCompatActivity implements INetResult{

    ImageButton back_btn, help_btn, exit_btn;
    private Context mContext;
    NotificationsDisplay notify;
    private INetResult listener;
    ArrayList<ClinicData> clinic_data;
    RecyclerView recyclerView;
    ArrayList<ClinicData> search_result_data;
    SearchResultAdapter searchResultAdapter;
    ImageLoader imageLoader;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor);

        mContext = this;
        listener=this;
        notify=new NotificationsDisplay(mContext);
        imageLoader = VolleySingleton.getInstance().getImageLoader();

        ActivityManagerForFinish.getInstance().addActivity(this);

        initViews();
        initData();
        setListeners();
    }

    private void initViews() {
        recyclerView=(RecyclerView)findViewById(R.id.recyler_view);
        back_btn = (ImageButton) findViewById(R.id.back);
        help_btn = (ImageButton) findViewById(R.id.help);
        exit_btn = (ImageButton) findViewById(R.id.exit);
    }

    private void initData() {

        Bundle bundle = new Bundle();
        bundle.putString(NetworkConstants.REQUEST_KEY_PATIENT_LID, ApplicationDataModel.getInstance().getPatientLid());
        bundle.putString(NetworkConstants.REQUEST_KEY_LATITUDE,"28.528510");
        bundle.putString(NetworkConstants.REQUEST_KEY_LONGITUDE,"77.278240");
        //bundle.putString(NetworkConstants.REQUEST_KEY_LOCATION_NAME, ""+current_latlong.getCity_name());
        bundle.putString(NetworkConstants.REQUEST_KEY_SEARCH_AREA,NetworkConstants.SEARCH_AREA_RANGE);
        bundle.putString(NetworkConstants.REQUEST_KEY_SEARCH_BY, NetworkConstants.SEARCH_BY_COUGH_COLD);
        bundle.putString(NetworkConstants.REQUEST_KEY_PAGE_INDEX, NetworkConstants.SEARCH_PAGE_INDEX);
        NetworkRequestManager nrm = new NetworkRequestManager(this, listener);
        nrm.generateServiceRequestParams(NetworkConstants.REQUEST_ID_SEARCH_CLINIC_DATA, bundle);

    }

    private void setListeners() {
        back_btn.setOnClickListener(m_click);
        help_btn.setOnClickListener(m_click);
        exit_btn.setOnClickListener(m_click);
    }

    View.OnClickListener m_click = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (v == back_btn) {
                  finish();
            } else if (v == help_btn) {
                try {
                    int engDrawableId = R.drawable.home_help;
                    Utils.getInstance().helpDialog(mContext, engDrawableId);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (v == exit_btn) {
                Utils.getInstance().exitAlert(mContext);
            }
        }
    };

    @Override
    public void onResult(boolean resultFlag, String message) {

    }

    @Override
    public void onResult(boolean resultFlag, String message, ArrayList<?> dataList) {
        if (resultFlag) {
            clinic_data = (ArrayList<ClinicData>) dataList;
            if(clinic_data!=null && clinic_data.size()>0) {
                searchResultAdapter = new SearchResultAdapter(this, clinic_data, imageLoader);
                recyclerView.setAdapter(searchResultAdapter);
                recyclerView.setLayoutManager(new LinearLayoutManager(this));
            }
        } else {
            notify.customToast(mContext, getResources().getString(R.string.info), message);
        }

    }

    @Override
    public void onResult(boolean resultFlag, String message, Object data) {

    }
}
