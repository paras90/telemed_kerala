package com.phtt.telemedhc.doctor;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.phtt.telemedhc.R;
import com.phtt.telemedhc.database.DbConstant;
import com.phtt.telemedhc.models.ApplicationDataModel;
import com.phtt.telemedhc.models.AppointmentRequest;
import com.phtt.telemedhc.models.ClinicData;
import com.phtt.telemedhc.network.NetworkConstants;
import com.phtt.telemedhc.network.NetworkRequestManager;
import com.phtt.telemedhc.network.VolleySingleton;
import com.phtt.telemedhc.utils.AppointmentConstants;
import com.phtt.telemedhc.utils.INetResult;
import com.phtt.telemedhc.utils.NotificationsDisplay;
import com.pkmmte.view.CircularImageView;

import java.util.ArrayList;
import java.util.HashMap;

public class SlotBookActivity extends AppCompatActivity implements INetResult {

    ClinicData clinic_data;
    TextView doctor_name,clinic_name;
    String time_slot,apptDate;
    RelativeLayout book_layout;
    Context mContext;
    INetResult listener;
    NotificationsDisplay notify;
    AppointmentRequest booked_data;
    TextView doctor_degree,clinic_address,phone_number;
    private ImageLoader imageLoader;
    private CircularImageView circular_image;
    TextView appointment_date,slot_time;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slot_book);

        mContext=this;
        listener=this;

        initViews();
        initData();
        setListeners();
        notify = new NotificationsDisplay(mContext);

    }

    private void initViews() {

        circular_image= (CircularImageView) findViewById(R.id.profile_pic);
        doctor_name= (TextView) findViewById(R.id.doc_name);
        doctor_degree= (TextView) findViewById(R.id.doc_degree);
        clinic_name= (TextView) findViewById(R.id.clinic_name);
        clinic_address= (TextView) findViewById(R.id.clinic_address);
        phone_number= (TextView) findViewById(R.id.phone_number);
        appointment_date= (TextView) findViewById(R.id.appointment_date);
        slot_time= (TextView) findViewById(R.id.slot_time);

        book_layout= (RelativeLayout) findViewById(R.id.book_layout);
    }

    private void initData() {
        imageLoader= VolleySingleton.getInstance().getImageLoader();
        Bundle clinic_data_bundle=getIntent().getExtras();
        if(clinic_data_bundle!=null) {
            clinic_data = ApplicationDataModel.getInstance().getClinicData();
            //(ClinicData) clinic_data_bundle.getSerializable("clinic_data");
            time_slot=clinic_data_bundle.getString("time_slot");
            apptDate = clinic_data_bundle.getString("appt_date");
            appointment_date.setText(apptDate);
            slot_time.setText(time_slot);

            doctor_name.setText(clinic_data.getDoctorDataList().get(0).getName());
            if(clinic_data.getDoctorDataList().get(0).getQualification()!=null &&
                    !(clinic_data.getDoctorDataList().get(0).getQualification()).isEmpty())
                doctor_degree.setText("(" +clinic_data.getDoctorDataList().get(0).getQualification() +")");
            clinic_name.setText(clinic_data.getName());
            clinic_address.setText(clinic_data.getAddress());
            phone_number.setText(clinic_data.getPhone());
            if(clinic_data.getDoctorDataList().get(0).getProfilePic()!=null) {
                imageLoader.get(clinic_data.getDoctorDataList().get(0).getProfilePic(), new ImageLoader.ImageListener() {
                    @Override
                    public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                        if (response.getBitmap() != null) {
                            circular_image.setImageBitmap(response.getBitmap());
                        }else{
                            circular_image.setImageResource(R.drawable.default_doctor_profile);
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("image_load_error", "volley_image_loader");
                        circular_image.setImageResource(R.drawable.default_doctor_profile);
                    }
                });}else{
                circular_image.setImageResource(R.drawable.default_doctor_profile);
            }


        }

    }

    private void setListeners() {
        book_layout.setOnClickListener(m_click);
    }

    View.OnClickListener m_click=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(v==book_layout){
                NetworkRequestManager time_slot_request= new NetworkRequestManager(mContext,listener);
                Bundle bundle = new Bundle();
                bundle.putString(NetworkConstants.REQUEST_KEY_PATIENT_LID, ApplicationDataModel.getInstance().getPatientLid());
                bundle.putString(NetworkConstants.REQUEST_KEY_CLINIC_ID, ""+clinic_data.getId());
                bundle.putString(NetworkConstants.REQUEST_KEY_DOCTOR_NAME, ""+clinic_data.getDoctorDataList().get(0).getName());
                bundle.putString(NetworkConstants.REQUEST_KEY_DOCTOR_ID, ""+clinic_data.getDoctorDataList().get(0).getId());
                bundle.putString(NetworkConstants.REQUEST_KEY_APPOINTMENT_DATE, ""+apptDate);
                bundle.putString(NetworkConstants.REQUEST_KEY_APPOINTMENT_TIME_SLOT,time_slot);
                time_slot_request.generateServiceRequestParams(NetworkConstants.REQUEST_ID_BOOK_APPOINTMENT_TIME_SLOT, bundle);
            }

        }
    };



    @Override
    public void onResult(boolean resultFlag, String message) {}

    @Override
    public void onResult(boolean resultFlag, String message, ArrayList<?> dataList) {}


    @Override
    public void onResult(boolean resultFlag, String message, Object data) {
        if(resultFlag){
            booked_data= (AppointmentRequest) data;
            Log.d("booked_data",booked_data.toString());
            HashMap<String,String> values = new HashMap<String,String>();
//            ApplicationDataModel.getInstance().setPatientLid(booked_data.getPatientLid());
            values.put(DbConstant.COLUMN_1, time_slot);
            values.put(DbConstant.COLUMN_2, clinic_data.getId());
            values.put(DbConstant.COLUMN_3, clinic_data.getDoctorDataList().get(0).getId());
            values.put(DbConstant.COLUMN_4, AppointmentConstants.BOOKING_STATUS_PENDING);
            values.put(DbConstant.COLUMN_5, booked_data.getApptId());
            values.put(DbConstant.COLUMN_6, ""/*fees status*/ );
            values.put(DbConstant.COLUMN_7, AppointmentConstants.VISIT_STATUS_UP_COMING );
            values.put(DbConstant.COLUMN_8, clinic_data.getName() );
            values.put(DbConstant.COLUMN_9, clinic_data.getDoctorDataList().get(0).getName() );
            values.put(DbConstant.COLUMN_10, apptDate );
            values.put(DbConstant.COLUMN_11, clinic_data.getDoctorDataList().get(0).getMobile() );
            values.put(DbConstant.COLUMN_12, clinic_data.getAddress() );
            values.put(DbConstant.COLUMN_13, clinic_data.getDoctorDataList().get(0).getQualification() );
            values.put(DbConstant.COLUMN_14, clinic_data.getDoctorDataList().get(0).getProfilePic() );
            values.put(DbConstant.COLUMN_15, clinic_data.getDoctorDataList().get(0).getAssociation() );
            values.put(DbConstant.COLUMN_16, clinic_data.getDoctorDataList().get(0).getFees() );
            values.put(DbConstant.COLUMN_17, clinic_data.getDoctorDataList().get(0).getSpeciality() );
            values.put(DbConstant.COLUMN_18, clinic_data.getDoctorDataList().get(0).getAvailability());
            values.put(DbConstant.COLUMN_19, clinic_data.getDoctorDataList().get(0).getTiming());
//            values.put(DbConstants.COLUMN_7, "current status ");
           // DatabaseManager.getInstance().insertAppointmentData(mContext, DbConstants.URI_TABLE_APPOINTMENT, values);
           // notify.customToast(mContext, mContext.getResources().getString(R.string.info), message);

          /*  Intent intent = new Intent(mContext, MyAppointment.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);*/
            finish();

        }else{
            notify.customToast(mContext,mContext.getResources().getString(R.string.info), message);
        }

    }
}
