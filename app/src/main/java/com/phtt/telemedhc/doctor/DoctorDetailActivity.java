package com.phtt.telemedhc.doctor;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import com.phtt.telemedhc.R;
import com.phtt.telemedhc.models.ApplicationDataModel;
import com.phtt.telemedhc.models.AppointmentRequest;
import com.phtt.telemedhc.models.ClinicData;
import com.phtt.telemedhc.network.NetworkConstants;
import com.phtt.telemedhc.network.NetworkRequestManager;
import com.phtt.telemedhc.utils.Constants;
import com.phtt.telemedhc.utils.INetResult;
import com.phtt.telemedhc.utils.NotificationsDisplay;
import com.phtt.telemedhc.utils.Utils;
import java.util.ArrayList;


public class DoctorDetailActivity extends AppCompatActivity implements INetResult {

    RelativeLayout call_book_layout;
    ClinicData clinic_data;
    Context mContext;
    INetResult listener;
    AppointmentRequest booked_data;
    private NotificationsDisplay notify;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doctor_detail);

        initViews();
        initData();
        setListeners();

        mContext=this;
        listener=this;
        notify = new NotificationsDisplay(mContext);

        Bundle bundle=getIntent().getExtras();
        if(bundle!=null) {
            clinic_data = (ClinicData) bundle.getSerializable("clinic_data");
            Log.d("clinic_data", clinic_data.toString());
            ApplicationDataModel.getInstance().setClinicData(clinic_data);
        }

        DoctorDetailFragment doctorDetailFragment=new DoctorDetailFragment();
        doctorDetailFragment.setArguments(bundle);
        pushFragment(Constants.DOCTOR_DETAIL, doctorDetailFragment, false, true);

    }

    public void initViews(){
        call_book_layout=(RelativeLayout)findViewById(R.id.call_layout);
    }

    public void initData(){

    }

    public void setListeners(){
        call_book_layout.setOnClickListener(m_click);
    }

    View.OnClickListener m_click=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          /*  Intent intent=new Intent(DoctorDetailActivity.this,BookActivity.class);
            Bundle bundle=new Bundle();
            bundle.putSerializable("clinic_data",clinic_data);
            intent.putExtras(bundle);
            startActivity(intent);*/

            NetworkRequestManager time_slot_request= new NetworkRequestManager(mContext,listener);
            Bundle bundle = new Bundle();
            bundle.putString(NetworkConstants.REQUEST_KEY_PATIENT_LID, ApplicationDataModel.getInstance().getPatientLid());
            bundle.putString(NetworkConstants.REQUEST_KEY_CLINIC_ID, ""+clinic_data.getId());
            bundle.putString(NetworkConstants.REQUEST_KEY_DOCTOR_NAME, ""+clinic_data.getDoctorDataList().get(0).getName());
            bundle.putString(NetworkConstants.REQUEST_KEY_DOCTOR_ID, ""+clinic_data.getDoctorDataList().get(0).getId());
            bundle.putString(NetworkConstants.REQUEST_KEY_APPOINTMENT_DATE, ""+ Utils.getInstance().getDate());
            bundle.putString(NetworkConstants.REQUEST_KEY_APPOINTMENT_TIME_SLOT,""+Utils.getInstance().getTimeStamp());
            time_slot_request.generateServiceRequestParams(NetworkConstants.REQUEST_ID_BOOK_APPOINTMENT_TIME_SLOT, bundle);
        }
    };



    public void pushFragment(String tag, Fragment fragment,
                             boolean shouldAnimate, boolean shouldAdd) {

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction ft = manager.beginTransaction();
        if (shouldAnimate) {

        }
         ft.replace(R.id.container, fragment);
         ft.addToBackStack(null);
         //ft.commit();
         ft.commitAllowingStateLoss();
    }

    @Override
    public void onBackPressed() {
        if(getSupportFragmentManager().getBackStackEntryCount()==1){
            this.finish();
        }else{
            getSupportFragmentManager().popBackStack();
        }
    }

    @Override
    public void onResult(boolean resultFlag, String message) {}

    @Override
    public void onResult(boolean resultFlag, String message, ArrayList<?> dataList) {

    }

    @Override
    public void onResult(boolean resultFlag, String message, Object data) {
        if(resultFlag) {
           /* booked_data = (AppointmentRequest) data;
            Log.d("booked_data", booked_data.toString());*/
            notify.customToast(mContext, mContext.getResources().getString(R.string.info), message);
            DoctorDetailActivity.this.finish();
        }

    }
}
