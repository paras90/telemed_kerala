package com.phtt.telemedhc.doctor;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.phtt.telemedhc.R;
import com.phtt.telemedhc.adapter.ImagePagerAdapter;
import com.phtt.telemedhc.fragment.BaseFragment;
import com.phtt.telemedhc.models.ClinicData;
import com.phtt.telemedhc.network.VolleySingleton;
import com.phtt.telemedhc.utils.Constants;
import com.pkmmte.view.CircularImageView;
import com.viewpagerindicator.CirclePageIndicator;

import java.lang.reflect.Field;

/**
 * Created by Arvind on 07-10-2015.
 */
public class DoctorDetailFragment extends BaseFragment implements OnMapReadyCallback {

    Toolbar toolbar;
    AutoScrollViewPager pager;
    CollapsingToolbarLayout collapsingToolbarLayout;
    GoogleMap googleMap;
    View long_content;
    View profile_layout;
    TextView doctor_name;
    TextView doctor_degree;
    TextView doctor_specialization,doctor_experience,doctor_fees;
    TextView all_timing_text;
    ImagePagerAdapter pageradpter;
    CirclePageIndicator circlePageIndicator;
    CircularImageView circular_image;
    ImageLoader imageLoader;
    TextView open_close_text,timing_text;
    TextView clinic_name,clinic_address,phone_number;
    View view;
    ClinicData clinic_data;
    SupportMapFragment supportMapFragment;
    LinearLayout services_layout;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_doctor_detail, container, false);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initViews(view);
        initData();
        setListeners();

    }

    @Override
    public void initViews(View rootView) {
        toolbar = (Toolbar) rootView.findViewById(R.id.toolbar);
        long_content= rootView.findViewById(R.id.long_content);
        profile_layout= rootView.findViewById(R.id.profile_layout);
        pager = (AutoScrollViewPager)  rootView.findViewById(R.id.pager);
        circlePageIndicator= (CirclePageIndicator)  rootView.findViewById(R.id.indicator);

        collapsingToolbarLayout = (CollapsingToolbarLayout)  rootView.findViewById(R.id.collapsingToolbarLayout);

        // doctor view ids
        doctor_name= (TextView) profile_layout.findViewById(R.id.doctor_name);
        doctor_degree= (TextView) profile_layout.findViewById(R.id.degree);
        doctor_specialization= (TextView) profile_layout.findViewById(R.id.specialization);
        circular_image= (CircularImageView) profile_layout.findViewById(R.id.profile_pic);
        doctor_experience= (TextView) long_content.findViewById(R.id.doctor_experience);
        doctor_fees= (TextView) long_content.findViewById(R.id.doctor_fees);

        // clinic view ids
        open_close_text= (TextView) long_content.findViewById(R.id.open_close_text);
        timing_text= (TextView) long_content.findViewById(R.id.timing_text);
        clinic_name= (TextView) long_content.findViewById(R.id.clinic_name);
        clinic_address= (TextView) long_content.findViewById(R.id.clinic_address);
        phone_number= (TextView) long_content.findViewById(R.id.phone_number);
        //map_container= (FrameLayout) long_content.findViewById(R.id.map_container);


        all_timing_text=(TextView)long_content.findViewById(R.id.all_timing);
       // services_list= (ListView) long_content.findViewById(R.id.services_list);
        services_layout= (LinearLayout) long_content.findViewById(R.id.services_layout);
        // booking_text=(TextView)long_content.findViewById(R.id.book);

        ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowHomeEnabled(true);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    @Override
    public void initData() {
        imageLoader= VolleySingleton.getInstance().getImageLoader();

        if (!isGooglePlayServicesAvailable()) {
            getActivity().finish();
        }

        supportMapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        supportMapFragment.getMapAsync(this);

        Bundle bundle=getArguments();
        if(bundle!=null) {
            //clinic_data = ApplicationDataModel.getInstance().getClinicData();//(ClinicData) bundle.getSerializable("clinic_data");
            clinic_data= (ClinicData) bundle.getSerializable("clinic_data");
            setupPager();
            Log.d("clinic_data", clinic_data.toString());

            if(clinic_data.getDoctorDataList().get(0).getProfilePic()!=null) {
                imageLoader.get(clinic_data.getDoctorDataList().get(0).getProfilePic(), new ImageLoader.ImageListener() {
                    @Override
                    public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                        if (response.getBitmap() != null) {
                            circular_image.setImageBitmap(response.getBitmap());
                        }else{
                            circular_image.setImageResource(R.drawable.default_doctor_profile);
                        }
                    }

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("image_load_error", "volley_image_loader");
                        circular_image.setImageResource(R.drawable.default_doctor_profile);
                    }
                });
            }else{
                circular_image.setImageResource(R.drawable.default_doctor_profile);
            }


            ((AppCompatActivity) getActivity()).getSupportActionBar().setTitle(clinic_data.getDoctorDataList().get(0).getName());
            doctor_name.setText(clinic_data.getDoctorDataList().get(0).getName());
            if(clinic_data.getDoctorDataList().get(0).getQualification()!=null &&
                    !(clinic_data.getDoctorDataList().get(0).getQualification()).isEmpty())
            doctor_degree.setText("(" +clinic_data.getDoctorDataList().get(0).getQualification() +")");
            doctor_specialization.setText(clinic_data.getDoctorDataList().get(0).getSpeciality());
            if(clinic_data.getDoctorDataList().get(0).getExperience()!=null &&
                    !(clinic_data.getDoctorDataList().get(0).getExperience()).isEmpty())
            doctor_experience.setText(clinic_data.getDoctorDataList().get(0).getExperience() + " Years");

            doctor_fees.setText(clinic_data.getDoctorDataList().get(0).getFees());

            open_close_text.setText(clinic_data.getStatus());
            timing_text.setText(clinic_data.getTiming());
            clinic_name.setText(clinic_data.getName());
            clinic_address.setText(clinic_data.getAddress());
            phone_number.setText(clinic_data.getPhone());

           /* servicesAdapter=new AllServicesAdapter(getActivity(),clinic_data.getServiceList());
            services_list.setAdapter(servicesAdapter);
            Utility.setListViewHeightBasedOnChildren(services_list);*/
            if(clinic_data.getServiceList()!=null){
                for(int i=0;i<clinic_data.getServiceList().size();i++) {
                    TextView serviceText = new TextView(getActivity());
                    serviceText.setText(clinic_data.getServiceList().get(i).getServiceName());
                    LinearLayout.LayoutParams layoutParams=new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                    serviceText.setLayoutParams(layoutParams);
                    services_layout.addView(serviceText);
                }
            }

        }




    }

    @Override
    public void setListeners() {
        all_timing_text.setOnClickListener(m_click);
    }

    private boolean isGooglePlayServicesAvailable() {
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
        if (ConnectionResult.SUCCESS == status) {
            return true;
        } else {
            GooglePlayServicesUtil.getErrorDialog(status, getActivity(), 0).show();
            return false;
        }
    }



    private void setupPager() {
        String[] image_url=clinic_data.getImagePath();

        if(image_url!=null && image_url.length>0) {
            pageradpter = new ImagePagerAdapter(getActivity(), image_url, imageLoader);
            pager.setAdapter(pageradpter);
            pager.setInterval(2000);
            pager.startAutoScroll();
            pager.setBorderAnimation(false);
            pager.setCurrentItem(0);
            circlePageIndicator.setViewPager(pager);
        }

    }


    View.OnClickListener m_click=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(v==all_timing_text){
                ((DoctorDetailActivity)getActivity()).pushFragment(Constants.TAB_ALLTIMING,new AllTimingFragment(),false,true);
            }/*else if(v==all_services){
                ((DoctorDetailActivity)getActivity()).pushFragment(Constants.TAB_ALLSERVICES, new AllTimingFragment(), false, true);
            }*/
        }
    };


    @Override
    public void onDetach() {
        super.onDetach();
        try {
            Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);
        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        if (googleMap != null) {
            double lat,lng;

            String latitude = clinic_data.getLat();
            String longitude = clinic_data.getLon();
            try{
                lat=new Double(latitude);
                lng=new Double(longitude);
            }catch (Exception e){
                e.printStackTrace();
                Log.d("error",e.toString());
                lat=0.0;
                lng=0.0;
            }
            LatLng latLng = new LatLng(lat,lng);
            MarkerOptions markerOptions=new MarkerOptions().position(latLng);
            markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ROSE));
            googleMap.addMarker(markerOptions);
            googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            googleMap.animateCamera(CameraUpdateFactory.zoomTo(15));
        }

    }
}
