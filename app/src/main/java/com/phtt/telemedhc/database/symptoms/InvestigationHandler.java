package com.phtt.telemedhc.database.symptoms;

import java.util.List;

public class InvestigationHandler {

	private volatile String investigationID;
	private volatile String investigationType;
	private volatile String investigationName;
	private volatile String investigationDetail;
	
	
	private volatile List<String> investigationList;
	
	public String getInvestigationID() {
		return investigationID;
	}
	public void setInvestigationID(String investigationID) {
		this.investigationID = investigationID;
	}
	public String getInvestigationType() {
		return investigationType;
	}
	public void setInvestigationType(String investigationType) {
		this.investigationType = investigationType;
	}
	public String getInvestigationName() {
		return investigationName;
	}
	public void setInvestigationName(String investigationName) {
		this.investigationName = investigationName;
	}
	public String getInvestigationDetail() {
		return investigationDetail;
	}
	public void setInvestigationDetail(String investigationDetail) {
		this.investigationDetail = investigationDetail;
	}
	public List<String> getInvestigationList() {
		return investigationList;
	}
	public void setInvestigationList(List<String> investigationList) {
		this.investigationList = investigationList;
	}
	
	
}
