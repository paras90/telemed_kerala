package com.phtt.telemedhc.database;

import android.net.Uri;

public class DbConstant {


    // DATABASE NAME DECLARATION
    public static final String DATABASE_NAME = "TeleMed_HC";

    // DATABASE VERSION DECLARATION
    public static final int DATABASE_VERSION = 001;

    //DATABASE AUTHORITY
    public static final String AUTHORITY = "com.phtt.Teledatabase";


    //TABLE DECLARATION

    public static final String TABLE_LOGIN = "login";
    public static final String TABLE_REGISTRATION = "registration";
    public static final String TABLE_GPS_LOCATION = "gps_location";
    public static final String TABLE_IMAGES = "images";
    public static final String TABLE_HEALTH_REPORT = "health_data";
    public static final String TABLE_COMPLAINTS_SYMPTOM = "complaints_symptom";
    public static final String TABLE_SYMPTONS = "symptoms";
    public static final String TABLE_PATIENT_TREATMENT_FOLLOWUP = "patient_treatment_followup";


    //TABLE ID DECLARATIOM

    public static final int ID_TABLE_LOGIN = 1;
    public static final int ID_TABLE_REGISTRATION = 2;
    public static final int ID_TABLE_HEALTH_REPORT = 3;
    public final static int ID_TABLE_COMPLAINTS_SYMPTOM = 4;
    public final static int ID_TABLE_SYMPTOMS = 5;
    public final static int ID_IMAGES = 6;
    public final static int ID_TABLE_GPS_LOCATION= 7;
    public final static int ID_TABLE_PATIENT_TREATMENT_FOLLOWUP	=8;


    //CONTENT URI DECLARATION
    public static final Uri URI_TABLE_LOGIN = Uri.parse("content://" + AUTHORITY + "/" + TABLE_LOGIN);
    public static final Uri URI_TABLE_REGISTRATION = Uri.parse("content://" + AUTHORITY + "/" + TABLE_REGISTRATION);
    public static final Uri URI_TABLE_GPS_LOCATION =  Uri.parse("content://"+AUTHORITY+"/"+TABLE_GPS_LOCATION);
    public static final Uri URI_HEALTH_REPORT = Uri.parse("content://" + AUTHORITY + "/" + TABLE_HEALTH_REPORT);
    public static final Uri C_U_T_COMPLAINTS_SYMPTOM = Uri.parse("content://" + AUTHORITY + "/" + TABLE_COMPLAINTS_SYMPTOM);
    public static final Uri C_U_T_SYMPTONS = Uri.parse("content://" + AUTHORITY + "/" + TABLE_SYMPTONS);
    public static final Uri C_U_T_PATIENT_TREATMENT_FOLLOWUP = Uri.parse("content://"+AUTHORITY+"/"+TABLE_PATIENT_TREATMENT_FOLLOWUP);
    public static final Uri URI_IMAGES = Uri.parse("content://" + AUTHORITY + "/" + TABLE_IMAGES);


    //COLUMN DECLARATION

    public final static String COLUMN_1 = "col1";
    public final static String COLUMN_2 = "col2";
    public final static String COLUMN_3 = "col3";
    public final static String COLUMN_4 = "col4";
    public final static String COLUMN_5 = "col5";
    public final static String COLUMN_6 = "col6";
    public final static String COLUMN_7 = "col7";
    public final static String COLUMN_8 = "col8";
    public final static String COLUMN_9 = "col9";
    public final static String COLUMN_10 = "col10";
    public final static String COLUMN_11 = "col11";
    public final static String COLUMN_12 = "col12";
    public final static String COLUMN_13 = "col13";
    public final static String COLUMN_14 = "col14";
    public final static String COLUMN_15 = "col15";
    public final static String COLUMN_16 = "col16";
    public final static String COLUMN_17 = "col17";
    public final static String COLUMN_18 = "col18";
    public final static String COLUMN_19 = "col19";
    public final static String COLUMN_20 = "col20";
    public final static String COLUMN_21 = "col21";
    public final static String COLUMN_22 = "col22";
    public final static String COLUMN_23 = "col23";
    public final static String COLUMN_24 = "col24";
    public final static String COLUMN_25 = "col25";


    public final static String COLUMN_EDIT_KEY = "edit_flag";
    public final static String COLUMN_UPLOAD_KEY = "upload_flag";
    public static final String COLUMN_TIME = "time_stamp";
    public static final String COLUMN_DATE = "date";
    public final static String COLUMN_ID = "id";
    public final static String COLUMN_GID = "patient_gid";
    public final static String COLUMN_LID = "patient_lid";
    public final static String COLUMN_HWID = "hwid";
    public final static String COLUMN_DOC_ID = "doc_id";
    public final static String COLUMN_CLINIC_ID = "clinic_id";
    public final static String COLUMN_SUB_USER_ID = "sub_user_id";
    public static final String COLUMN_COMMON_DATE_TIME = "date_time";
    public static final String COLUMN_FOLLOWUP_CLOSE_FLAG = "followup_dismiss";

    public static final String COLUMN_TOKEN_ID = "token_id";
    public static final String COLUMN_VISIT_NO                  = "visit_no";
    public static final String COLUMN_LATITUDE                  = "latitude";
    public static final String COLUMN_LONGITUDE                 = "longitude";
    public static final String COLUMN_CELL_ID                   = "cell_id";
    public static final String COLUMN_LAC                       = "lac";


    //URI OF ALL TABLES
    public static final Uri[] ALL_URI = {
            URI_TABLE_LOGIN,
            URI_TABLE_REGISTRATION,
            URI_TABLE_GPS_LOCATION
            , URI_HEALTH_REPORT
            , URI_IMAGES
            ,C_U_T_PATIENT_TREATMENT_FOLLOWUP
    };


    //ALL TABLE NAMES
    public static final String[] ALL_TABLES = {
            TABLE_LOGIN,
            TABLE_REGISTRATION
            , TABLE_GPS_LOCATION
            , TABLE_HEALTH_REPORT
            , TABLE_IMAGES
            , TABLE_SYMPTONS
            ,TABLE_PATIENT_TREATMENT_FOLLOWUP
    };

    //ALL TABLE ID
    public static final int[] ALL_TABLE_ID = {

            ID_TABLE_LOGIN,
            ID_TABLE_REGISTRATION
            , ID_TABLE_GPS_LOCATION
            , ID_TABLE_HEALTH_REPORT
            , ID_IMAGES
            , ID_TABLE_SYMPTOMS
            ,ID_TABLE_PATIENT_TREATMENT_FOLLOWUP
    };


    //All Common coloum

    public static final String[] columnList = {

            COLUMN_1,
            COLUMN_2,
            COLUMN_3,
            COLUMN_4,
            COLUMN_5,
            COLUMN_6,
            COLUMN_7,
            COLUMN_8,
            COLUMN_9,
            COLUMN_10,
            COLUMN_11,
            COLUMN_12,
            COLUMN_13,
            COLUMN_14,
            COLUMN_15,
            COLUMN_16,
            COLUMN_17,
            COLUMN_18,
            COLUMN_19,
            COLUMN_20,
            COLUMN_21,
            COLUMN_22,
            COLUMN_23,
            COLUMN_24,
            COLUMN_25
    };


    public static final Uri[] ALL_UPLOAD_URI = {
            URI_TABLE_REGISTRATION
            , URI_HEALTH_REPORT
    };

    public static final String[] ALL_UPLOAD_TABLE = {
            TABLE_REGISTRATION
            , TABLE_HEALTH_REPORT
    };


}
