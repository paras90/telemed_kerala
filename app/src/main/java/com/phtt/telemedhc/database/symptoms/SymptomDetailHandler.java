package com.phtt.telemedhc.database.symptoms;

import java.util.List;

public class SymptomDetailHandler {

	private volatile String ailmentID;
	private volatile String ailmentName;
	private volatile String ailmentType;
	private volatile String ailmentDetail;
	
	private volatile List<InvestigationHandler> investigationList;

	public String getAilmentID() {
		return ailmentID;
	}

	public void setAilmentID(String ailmentID) {
		this.ailmentID = ailmentID;
	}

	public String getAilmentName() {
		return ailmentName;
	}

	public void setAilmentName(String ailmentName) {
		this.ailmentName = ailmentName;
	}

	public String getAilmentType() {
		return ailmentType;
	}

	public void setAilmentType(String ailmentType) {
		this.ailmentType = ailmentType;
	}

	public String getAilmentDetail() {
		return ailmentDetail;
	}

	public void setAilmentDetail(String ailmentDetail) {
		this.ailmentDetail = ailmentDetail;
	}

	public List<InvestigationHandler> getInvestigationList() {
		return investigationList;
	}

	public void setInvestigationList(List<InvestigationHandler> investigationList) {
		this.investigationList = investigationList;
	}


	@Override
	public String toString() {
		return ailmentName;
	}
}
