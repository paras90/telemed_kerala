package com.phtt.telemedhc.database.symptoms;

import java.util.ArrayList;


public class SymptomDataModel {

	 private static class SingletonHolder{
	        private static final SymptomDataModel mInstance = new SymptomDataModel();
	    }

	    public static SymptomDataModel getInstance(){
	        return SingletonHolder.mInstance;
	    }

	    
     ArrayList<SymptomDetailHandler> symptomList;


	public ArrayList<SymptomDetailHandler> getSymptomList() {
		return symptomList;
	}


	public void setSymptomList(ArrayList<SymptomDetailHandler> symptomList) {
		this.symptomList = symptomList;
	}
	    
}
