package com.phtt.telemedhc.database;

import android.annotation.TargetApi;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;

import com.phtt.telemedhc.database.symptoms.InvestigationHandler;
import com.phtt.telemedhc.database.symptoms.SymptomConstants;
import com.phtt.telemedhc.database.symptoms.SymptomDetailHandler;
import com.phtt.telemedhc.datahandlers.ApplicationModelData;
import com.phtt.telemedhc.models.ApplicationDataModel;
import com.phtt.telemedhc.models.BeneficiaryDataHandler;
import com.phtt.telemedhc.models.HomeScreenModel;
import com.phtt.telemedhc.models.LocationHandler;
import com.phtt.telemedhc.models.RegistrationData;
import com.phtt.telemedhc.models.StateDistrictData;
import com.phtt.telemedhc.parser.ParserConstant;
import com.phtt.telemedhc.parser.UriHandler;
import com.phtt.telemedhc.utils.Constants;
import com.phtt.telemedhc.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class DatabaseManager {


    private static final String TAG = "DatabaseManagesr";


    private static class SingletonHolder {
        private static final DatabaseManager INSTANCE = new DatabaseManager();
    }

    public static DatabaseManager getInstance() {
        return SingletonHolder.INSTANCE;
    }


    public synchronized void insertLogin(Context mContext, Uri contentURI, HashMap<String, String> value) {

        ContentValues values = new ContentValues();
        //DatabaseConfig.COLUMN_COMMON_VISIT_NO
        values.put(DbConstant.COLUMN_2, value.get(DbConstant.COLUMN_2));
        values.put(DbConstant.COLUMN_3, value.get(DbConstant.COLUMN_3));

        Log.v(TAG, "insertLogin, values: " + values);

        mContext.getContentResolver().insert(contentURI, values);

        values.clear();
        values = null;
        DbUtils.getInstance().copy();

    }


    public synchronized String[] getLoginValue(Context mContext, Uri contentURI, String value) {
        String list[] = null;
        Cursor cursor = null;
        try {
            //		cursor = mContext.getContentResolver().query(contentURI, null, DatabaseConfig.COLUMN_COMMON_LID+"=?",

            cursor = mContext.getContentResolver().query(contentURI, null, DbConstant.COLUMN_2 + "=?",
                    new String[]{value}, null /*DatabaseConfig.COLUMN_CHILD_GID+" ASC"*/);


            if (cursor != null && cursor.getCount() > 0) {
                cursor.moveToLast();
                list = new String[cursor.getColumnCount()];
                for (int temp = 0; temp < cursor.getColumnCount(); temp++) {

                    list[temp] = cursor.getString(temp);


                }

            }
            if (cursor != null) {
                cursor.close();
                cursor = null;
            }
        } catch (IllegalArgumentException e) {
            if (cursor != null) {
                cursor.close();
                cursor = null;
            }
            e.printStackTrace();
        }

        return list;
    }


    public synchronized String[][] getSsaQuestionValue(Context mContext, Uri contentURI, HashMap<String, String> value) {
        String list[][] = null;
        Cursor cursor = null;
        try {

            cursor = mContext.getContentResolver().query(contentURI, null,
                    DbConstant.COLUMN_LID + "=?",
                    new String[]{value.get(DbConstant.COLUMN_LID)}, null);


            if (cursor != null && cursor.getCount() > 0) {
                cursor.moveToLast();

                int count = cursor.getColumnCount() - ParserConstant.PREFILL_INDEX;
                list = new String[count][2];
                // set id to access globally
                UriHandler.getInstance().setId(cursor.getString(cursor.getColumnIndexOrThrow(DbConstant.COLUMN_ID)));

                int index = ParserConstant.PREFILL_INDEX;
                for (int temp = 0; temp < count; temp++) {

                    list[temp][0] = cursor.getColumnName(index);
                    list[temp][1] = cursor.getString(index);
                    index++;

                }

            }
            if (cursor != null) {
                cursor.close();
                cursor = null;
            }
        } catch (IllegalArgumentException e) {
            if (cursor != null) {
                cursor.close();
                cursor = null;
            }
            e.printStackTrace();
        }

        return list;
    }

    public synchronized int updateQuestionValue(Context mContext, Uri contentURI, HashMap<String, String> data, String where, String[] selection) {
        //	int ret;
        ContentValues values = new ContentValues();

        int rowupdted = 0;
        try {

            Set<String> keyset = data.keySet();

            for (String key : keyset) {

                values.put(key, data.get(key));
            }
            values.put(DbConstant.COLUMN_EDIT_KEY, "1");
            values.put(DbConstant.COLUMN_UPLOAD_KEY, "0");

            rowupdted = mContext.getContentResolver().update(contentURI, values, where, selection);

            values.clear();
            values = null;
            DbUtils.getInstance().copy();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return rowupdted;
    }

    public synchronized void insertGPS(Context mContext, String lat, String lon, String lac, String cellid) {

        ContentValues values = new ContentValues();

        values = initCommonValues(values, mContext);

        values.put(DbConstant.COLUMN_LATITUDE, lat);
        values.put(DbConstant.COLUMN_LONGITUDE, lon);
        values.put(DbConstant.COLUMN_LAC, lac);
        values.put(DbConstant.COLUMN_CELL_ID, cellid);
        values.put(DbConstant.COLUMN_EDIT_KEY, "0");
        values.put(DbConstant.COLUMN_UPLOAD_KEY, "0");


        mContext.getContentResolver().insert(DbConstant.URI_TABLE_GPS_LOCATION, values);

        values.clear();
        values = null;
        DbUtils.getInstance().copy();

    }

    public synchronized int updateRegistration(Context mContext, Uri contentURI, RegistrationData registrationModel) {
        //	int ret;
        ContentValues values = new ContentValues();

        int rowupdted = 0;
        try {
            values.put(DbConstant.COLUMN_GID, registrationModel.getPatientGid());
            values.put(DbConstant.COLUMN_UPLOAD_KEY, "1");
            String where = DbConstant.COLUMN_LID + "=?";
            String selection[] = {registrationModel.getPatientLid()};
            rowupdted = mContext.getContentResolver().update(contentURI, values, where, selection);
            values.clear();
            values = null;
            DbUtils.getInstance().copy();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return rowupdted;

    }

    public synchronized void insertQuestionValue(Context mContext, Uri contentURI, HashMap<String, String> value) {

        ContentValues values = new ContentValues();
        values = initCommonValues(values, mContext);
        Set<String> keyset = value.keySet();
        for (String key : keyset) {
            values.put(key, value.get(key));
        }
        values.put(DbConstant.COLUMN_EDIT_KEY, "0");
//        values.put(DbConstant.COLUMN_UPLOAD_KEY, "1");
        values.put(DbConstant.COLUMN_UPLOAD_KEY, "0");
        Log.v(TAG, "insertQuestionValue, values: " + values);
        mContext.getContentResolver().insert(contentURI, values);
        values.clear();
        values = null;
        //checkAndInsertTokenDetails(mContext);
        DbUtils.getInstance().copy();

    }


    private ContentValues initCommonValues(ContentValues values, final Context mContext) {

        try {
            values.put(DbConstant.COLUMN_LID, "" + ApplicationDataModel.getInstance().getPatientLid());
//            values.put(DbConstant.COLUMN_GID, "" + ApplicationDataModel.getInstance().getPatientGid());
            values.put(DbConstant.COLUMN_TIME, "" + Utils.getInstance().getTimeStamp());
            values.put(DbConstant.COLUMN_HWID, "" + ApplicationDataModel.getInstance().getHwid());


        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return values;
    }


    public ArrayList<BeneficiaryDataHandler> getPatientData(Context mContext, String hwid) {
        ArrayList<BeneficiaryDataHandler> list = null;
        String where = DbConstant.COLUMN_HWID + "=?";
        String selection[] = {hwid};
        String sortOrder = DbConstant.COLUMN_ID + " DESC";
        Cursor cursor = null;
        try {

            cursor = mContext.getContentResolver().query(DbConstant.URI_TABLE_REGISTRATION, null, where,
                    selection, sortOrder);


            if (cursor != null && cursor.getCount() > 0) {
                list = new ArrayList<BeneficiaryDataHandler>();
                while (cursor.moveToNext()) {
                    BeneficiaryDataHandler model = new BeneficiaryDataHandler();
                    model.setBeneficiaryGId(cursor.getString(cursor.getColumnIndexOrThrow(DbConstant.COLUMN_GID)));
                    model.setBeneficiarytId(cursor.getString(cursor.getColumnIndexOrThrow(DbConstant.COLUMN_LID)));
                    model.setBeneficiaryName(cursor.getString(cursor.getColumnIndexOrThrow(DbConstant.COLUMN_1)));
                    model.setImagePath(cursor.getString(cursor.getColumnIndexOrThrow(DbConstant.COLUMN_6)));
                    model.setBeneficiarySex(cursor.getString(cursor.getColumnIndexOrThrow(DbConstant.COLUMN_4)));
                    list.add(model);
                }


            }
            if (cursor != null) {
                cursor.close();
                cursor = null;
            }
        } catch (IllegalArgumentException e) {
            if (cursor != null) {
                cursor.close();
                cursor = null;
            }
            e.printStackTrace();
        }

        return list;
    }

    // Get a particular patient data

    public BeneficiaryDataHandler getSinglePatientData(Context mContext, String patient_lid) {
        BeneficiaryDataHandler patient_data = null;

        String where = DbConstant.COLUMN_LID + "=?";
        String selection[] = {patient_lid};

        Cursor cursor = null;
        try {

            cursor = mContext.getContentResolver().query(DbConstant.URI_TABLE_REGISTRATION, null, where,
                    selection, null);


            if (cursor != null && cursor.getCount() > 0) {
                while (cursor.moveToNext()) {
                    patient_data = new BeneficiaryDataHandler();
                    patient_data.setBeneficiaryGId(cursor.getString(cursor.getColumnIndexOrThrow(DbConstant.COLUMN_GID)));
                    patient_data.setBeneficiarytId(cursor.getString(cursor.getColumnIndexOrThrow(DbConstant.COLUMN_LID)));
                    patient_data.setBeneficiaryName(cursor.getString(cursor.getColumnIndexOrThrow(DbConstant.COLUMN_1)));
                    patient_data.setImagePath(cursor.getString(cursor.getColumnIndexOrThrow(DbConstant.COLUMN_6)));
                    patient_data.setBeneficiarySex(cursor.getString(cursor.getColumnIndexOrThrow(DbConstant.COLUMN_4)));
                }


            }
            if (cursor != null) {
                cursor.close();
                cursor = null;
            }
        } catch (IllegalArgumentException e) {
            if (cursor != null) {
                cursor.close();
                cursor = null;
            }
            e.printStackTrace();
        }

        return patient_data;
    }

    public synchronized int getQuestionValue(Context mContext, Uri contentURI, String where, String[] selection) {

        int count = 0;
        Cursor cursor = mContext.getContentResolver().query(contentURI, null, where, selection, null /*DatabaseConfig.COLUMN_CHILD_GID+" ASC"*/);
        //		Log.v(TAG, "readQuestionValue, cursor count: " + cursor.getCount());

        if (cursor != null && cursor.getCount() > 0) {

            count = cursor.getCount();


        }
        if (cursor != null) {
            cursor.close();
            cursor = null;
        }

        return count;
    }


    /**
     * this method read data from database and convert to json object
     *
     * @param mContext
     * @param contentURI
     * @return json object
     */
    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    public synchronized JSONObject getDataToUpload(Context mContext, Uri contentURI, String tableName) {

        JSONObject jsonObj = null;
        String where = DbConstant.COLUMN_EDIT_KEY + "=? or " + DbConstant.COLUMN_UPLOAD_KEY + "=?";
        String selectionArg[] = {"1", "1"};
        Cursor cursor = mContext.getContentResolver().query(contentURI, null, where, selectionArg, null, null);
        Log.v(TAG, "readChildDetailFromDb, cursor count: " + cursor.getCount());

        try {
            if (null != cursor && cursor.getCount() > 0) {
                jsonObj = new JSONObject();
                if (cursor.moveToFirst()) {
                    JSONArray user = new JSONArray();
                    int index = 0;
                    while (!cursor.isAfterLast()) {
                        JSONObject temp = new JSONObject();

                        try {
                            for (int i = 1; i < cursor.getColumnCount(); i++) {
                                temp.put(cursor.getColumnName(i), cursor.getString(i));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        user.put(index, temp);
                        cursor.moveToNext();
                        index++;
                    }
                    // userObj.put("test",jsonArray);
                    jsonObj.put(tableName, user);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {

            if (cursor != null) {
                cursor.close();
            }


        }
        return jsonObj;

    }

    public synchronized ArrayList<String> getAllLid(Context mContext, Uri contentURI) {

        ArrayList<String> all_id = new ArrayList<>();
        Cursor cursor = mContext.getContentResolver().query(contentURI, null, null, null, null /*DatabaseConfig.COLUMN_CHILD_GID+" ASC"*/);

        if (cursor != null && cursor.getCount() > 0) {
            int count = cursor.getCount();
            Log.d("count", "" + count);
            while (cursor.moveToNext()) {
                try {
                    all_id.add(cursor.getString(cursor.getColumnIndexOrThrow(DbConstant.COLUMN_LID)));
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                }
            }
        }
        if (cursor != null) {
            cursor.close();
            cursor = null;
        }

        return all_id;
    }


    public JSONObject getJsonToUpload(Context mContext/*, String lid*/) {
        Cursor cursor = null;
        String where = DbConstant.COLUMN_HWID + "=? AND " + DbConstant.COLUMN_UPLOAD_KEY + "=?";
        String selection[] = {ApplicationDataModel.getInstance().getHwid(), "0"};
        JSONObject userObj = new JSONObject();
        // System.out.println("get count :"+ tablename.length);

        for (int j = 0; j < DbConstant.ALL_UPLOAD_URI.length; j++) {
            try {
                cursor = DatabaseManager.getInstance().getDataUpload(mContext, DbConstant.ALL_UPLOAD_URI[j], where, selection, null);
//				     if(!DbConstant.URI_TABLE_LOGIN.equals(DbConstant.ALL_URI[j]))
                if (cursor != null && cursor.getCount() > 0) {
                    JSONArray jsonArray = new JSONArray();
                    while (cursor.moveToNext()) {
                        JSONObject user = new JSONObject();
                        try {
                            for (int i = 1; i < cursor.getColumnCount(); i++) {
                                user.put(cursor.getColumnName(i), cursor.getString(i));
                                user.put("device_id", ApplicationDataModel.getInstance().getDeviceId());
                                user.put("lat", String.valueOf(LocationHandler.getInstance().getCurrentLatitude()));
                                user.put("long", String.valueOf(LocationHandler.getInstance().getCurrentLongitude()));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        jsonArray.put(user);
//								cursor.moveToNext();
                    }
                    // userObj.put("test",jsonArray);
                    userObj.put(DbConstant.ALL_UPLOAD_TABLE[j], jsonArray);
                }

            } catch (Exception e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } finally {

                if (cursor != null) {
                    cursor.close();
                }
            }


        }

        return userObj;
    }


    public synchronized Set<Map<String, String>> getUserSymptoms(Context mContext) {

        Set<Map<String, String>> list = new HashSet<>();

        //		String Where = DbConstants.COLUMN_PATIENT_LID + " = ?";
        //		String[] Selection = {ApplicationModelData.getInstance().getmPatientInfoDataHandler().getPatientId()};
        String where = DbConstant.COLUMN_LID + "=?";
        String selection[] = {ApplicationDataModel.getInstance().getPatientLid()};

        try {
            Cursor cursor = mContext.getContentResolver().query(DbConstant.C_U_T_SYMPTONS, null, where, selection, null);

            if (cursor != null && cursor.getCount() > 0) {

                while (cursor.moveToNext()) {
                    try {
                        Map<String, String> handler = new HashMap<String, String>();

                        handler.put(DbConstant.COLUMN_1, cursor.getString(cursor.getColumnIndex(DbConstant.COLUMN_1)));
                        handler.put(DbConstant.COLUMN_2, cursor.getString(cursor.getColumnIndex(DbConstant.COLUMN_2)));
                        handler.put(DbConstant.COLUMN_3, cursor.getString(cursor.getColumnIndex(DbConstant.COLUMN_3)));
                        handler.put(DbConstant.COLUMN_4, cursor.getString(cursor.getColumnIndex(DbConstant.COLUMN_4)));
                        handler.put(DbConstant.COLUMN_5, cursor.getString(cursor.getColumnIndex(DbConstant.COLUMN_5)));

                        list.add(handler);
                    } catch (IllegalArgumentException e) {

                    }
                }
            }

            cursor.close();
        } catch (Exception E) {
            E.printStackTrace();
        }


        return list;
    }

    private Cursor getDataUpload(Context mContext, Uri uri, String where, String[] selection, Object o) {

        Cursor cursor = mContext.getContentResolver().query(uri, null, where/*"child_gid = ?"*/, selection, null);
//          Log.v(TAG, "readChildDetailFromDb, cursor count: " + cursor.getCount());
        return cursor;
    }


    public synchronized String getBPData(String lid) {
        String result = "";

        File file = new File(Environment.getExternalStorageDirectory() + "/swasthyaslate/swasthv2");// Initialize file object to the external Registration Regdatabase path
        SQLiteDatabase db = SQLiteDatabase.openOrCreateDatabase(file, null);// open Regdatabase

        if (file.exists()) {

            Cursor cursor = null;
//			String q = "SELECT sys, dia FROM Blood_Pressure WHERE gid='" + lid + "'";
            cursor = db.query("Blood_Pressure", new String[]{"sys", "dia"}, "gid=?", new String[]{lid}, null, null, null);

            if (null != cursor && cursor.getCount() > 0) {
                cursor.moveToLast();
                String sys = cursor.getString(0);
                String dia = cursor.getString(1);
                result = sys + "/" + dia;
            }

        }
        return result;

    }

    public synchronized String getPulseData(String lid) {
        String result = "";

        File file = new File(Environment.getExternalStorageDirectory() + "/swasthyaslate/swasthv2");// Initialize file object to the external Registration Regdatabase path
        SQLiteDatabase db = SQLiteDatabase.openOrCreateDatabase(file, null);// open Regdatabase

        if (file.exists()) {

            Cursor cursor = null;
//			String q = "SELECT sys, dia FROM Blood_Pressure WHERE gid='" + lid + "'";
            cursor = db.query("Pulse_Oximeter", new String[]{"avg"}, "gid=?", new String[]{lid}, null, null, null);

            if (null != cursor && cursor.getCount() > 0) {
                cursor.moveToLast();
                result = cursor.getString(0);
//				String dia = cursor.getString(1);
//				result = sys + "/" + dia;
            }

        }
        return result;

    }

    public synchronized String getBloodGroupData(String lid) {
        String result = "";

        File file = new File(Environment.getExternalStorageDirectory() + "/swasthyaslate/swasthv2");// Initialize file object to the external Registration Regdatabase path
        SQLiteDatabase db = SQLiteDatabase.openOrCreateDatabase(file, null);// open Regdatabase

        if (file.exists()) {

            Cursor cursor = null;
//			String q = "SELECT sys, dia FROM Blood_Pressure WHERE gid='" + lid + "'";
            cursor = db.query("Blood_Grouping_And_Typing", new String[]{"machine_result", "user_input_test_result"}, "gid=?", new String[]{lid}, null, null, null);

            if (null != cursor && cursor.getCount() > 0) {
                cursor.moveToLast();
                String r1 = cursor.getString(0);
                String r2 = cursor.getString(1);
                if (r2 == null || r2.isEmpty()) {
                    result = r1;
                } else {
                    result = r2;
                }
//				result = sys + "/" + dia;
            }

        }
        return result;

    }

    public synchronized String getBloodGlucoseData(String lid) {
        String result = "";

        File file = new File(Environment.getExternalStorageDirectory() + "/swasthyaslate/swasthv2");// Initialize file object to the external Registration Regdatabase path
        SQLiteDatabase db = SQLiteDatabase.openOrCreateDatabase(file, null);// open Regdatabase

        if (file.exists()) {

            Cursor cursor = null;
//			String q = "SELECT sys, dia FROM Blood_Pressure WHERE gid='" + lid + "'";
            cursor = db.query("Blood_Glucose", new String[]{"value"}, "gid=?", new String[]{lid}, null, null, null);

            if (null != cursor && cursor.getCount() > 0) {
                cursor.moveToLast();
                result = cursor.getString(0);

            }

        }
        return result;

    }


    public synchronized String getHaemoglobinData(String lid) {
        String result = "";

        File file = new File(Environment.getExternalStorageDirectory() + "/swasthyaslate/swasthv2");// Initialize file object to the external Registration Regdatabase path
        SQLiteDatabase db = SQLiteDatabase.openOrCreateDatabase(file, null);// open Regdatabase

        if (file.exists()) {

            Cursor cursor = null;
//			String q = "SELECT sys, dia FROM Blood_Pressure WHERE gid='" + lid + "'";
            cursor = db.query("Haemoglobin", new String[]{"machine_result"}, "gid=?", new String[]{lid}, null, null, null);

            if (null != cursor && cursor.getCount() > 0) {
                cursor.moveToLast();
                result = cursor.getString(0);
//				String dia = cursor.getString(1);
//				result = sys + "/" + dia;
            }

        }
        return result;

    }

    public synchronized String getUrineProteinData(String lid) {
        String result = "";

        File file = new File(Environment.getExternalStorageDirectory() + "/swasthyaslate/swasthv2");// Initialize file object to the external Registration Regdatabase path
        SQLiteDatabase db = SQLiteDatabase.openOrCreateDatabase(file, null);// open Regdatabase

        if (file.exists()) {

            Cursor cursor = null;
//			String q = "SELECT sys, dia FROM Blood_Pressure WHERE gid='" + lid + "'";
            cursor = db.query("Urine_Sugar", new String[]{"machine_result", "user_input_test_result"}, "gid=?", new String[]{lid}, null, null, null);

            if (null != cursor && cursor.getCount() > 0) {
                cursor.moveToLast();
                String r1 = cursor.getString(0);
                String r2 = cursor.getString(1);
                if (r2 == null || r2.isEmpty()) {
                    result = r1;
                } else {
                    result = r2;
                }
            }

        }
        return result;

    }

    public synchronized String getMaleriaData(String lid) {
        String result = "";

        File file = new File(Environment.getExternalStorageDirectory() + "/swasthyaslate/swasthv2");// Initialize file object to the external Registration Regdatabase path
        SQLiteDatabase db = SQLiteDatabase.openOrCreateDatabase(file, null);// open Regdatabase

        if (file.exists()) {

            Cursor cursor = null;
//			String q = "SELECT sys, dia FROM Blood_Pressure WHERE gid='" + lid + "'";

            cursor = db.query("Malaria", new String[]{"machine_result", "user_input_test_result"}, "gid=?", new String[]{lid}, null, null, null);

            if (null != cursor && cursor.getCount() > 0) {
                cursor.moveToLast();
                String r1 = cursor.getString(0);//machine_result
                String r2 = cursor.getString(1);//user_input_test_result
                if (r2 == null || r2.isEmpty()) {
                    result = r1;
                } else {
                    result = r2;
                }
            }

        }
        return result;

    }

    public synchronized String getHepatitisBData(String lid) {
        String result = "";

        File file = new File(Environment.getExternalStorageDirectory() + "/swasthyaslate/swasthv2");// Initialize file object to the external Registration Regdatabase path
        SQLiteDatabase db = SQLiteDatabase.openOrCreateDatabase(file, null);// open Regdatabase

        if (file.exists()) {

            Cursor cursor = null;
//			String q = "SELECT sys, dia FROM Blood_Pressure WHERE gid='" + lid + "'";
            cursor = db.query("Hepatitis_B_Virus", new String[]{"machine_result", "user_input_test_result"}, "gid=?", new String[]{lid}, null, null, null);

            if (null != cursor && cursor.getCount() > 0) {
                cursor.moveToLast();
                String r1 = cursor.getString(0);
                String r2 = cursor.getString(1);

                if (r2 == null || r2.isEmpty()) {
                    result = r1;
                } else {
                    result = r2;
                }
            }

        }
        return result;

    }

    public synchronized String getTyphoidData(String lid) {
        String result = "";

        File file = new File(Environment.getExternalStorageDirectory() + "/swasthyaslate/swasthv2");// Initialize file object to the external Registration Regdatabase path
        SQLiteDatabase db = SQLiteDatabase.openOrCreateDatabase(file, null);// open Regdatabase

        if (file.exists()) {

            Cursor cursor = null;
//			String q = "SELECT sys, dia FROM Blood_Pressure WHERE gid='" + lid + "'";
            cursor = db.query("Onsite_Typhoid", new String[]{"machine_result", "user_input_test_result"}, "gid=?", new String[]{lid}, null, null, null);

            if (null != cursor && cursor.getCount() > 0) {
                cursor.moveToLast();
                String r1 = cursor.getString(0);
                String r2 = cursor.getString(1);

                if (r2 == null || r2.isEmpty()) {
                    result = r1;
                } else {
                    result = r2;
                }
            }

        }
        return result;

    }

    public synchronized String getPregnancyData(String lid) {
        String result = "";

        File file = new File(Environment.getExternalStorageDirectory() + "/swasthyaslate/swasthv2");// Initialize file object to the external Registration Regdatabase path
        SQLiteDatabase db = SQLiteDatabase.openOrCreateDatabase(file, null);// open Regdatabase

        if (file.exists()) {

            Cursor cursor = null;
//			String q = "SELECT sys, dia FROM Blood_Pressure WHERE gid='" + lid + "'";
            cursor = db.query("Pregnancy_HCG", new String[]{"machine_result", "user_input_test_result"}, "gid=?", new String[]{lid}, null, null, null);

            if (null != cursor && cursor.getCount() > 0) {
                cursor.moveToLast();
                String r1 = cursor.getString(0);
                String r2 = cursor.getString(1);

                if (r2 == null || r2.isEmpty()) {
                    result = r1;
                } else {
                    result = r2;
                }
            }

        }
        return result;

    }

    public synchronized String getSyphilisData(String lid) {
        String result = "";

        File file = new File(Environment.getExternalStorageDirectory() + "/swasthyaslate/swasthv2");// Initialize file object to the external Registration Regdatabase path
        SQLiteDatabase db = SQLiteDatabase.openOrCreateDatabase(file, null);// open Regdatabase

        if (file.exists()) {

            Cursor cursor = null;
//			String q = "SELECT sys, dia FROM Blood_Pressure WHERE gid='" + lid + "'";
            cursor = db.query("Syphillis", new String[]{"machine_result", "user_input_test_result"}, "gid=?", new String[]{lid}, null, null, null);

            if (null != cursor && cursor.getCount() > 0) {
                cursor.moveToLast();
                String r1 = cursor.getString(0);
                String r2 = cursor.getString(1);

                if (r2 == null || r2.isEmpty()) {
                    result = r1;
                } else {
                    result = r2;
                }
            }

        }
        return result;

    }

    public synchronized String getTemperatureData(String lid) {
        String result = "";

        File file = new File(Environment.getExternalStorageDirectory() + "/swasthyaslate/swasthv2");// Initialize file object to the external Registration Regdatabase path
        SQLiteDatabase db = SQLiteDatabase.openOrCreateDatabase(file, null);// open Regdatabase

        if (file.exists()) {

            Cursor cursor = null;
//			String q = "SELECT sys, dia FROM Blood_Pressure WHERE gid='" + lid + "'";
            cursor = db.query("Temperature", new String[]{"value"}, "gid=?", new String[]{lid}, null, null, null);

            if (null != cursor && cursor.getCount() > 0) {
                cursor.moveToLast();
                String r1 = cursor.getString(0);

                if (r1 != null) {
                    result = r1;
                }
            }

        }
        return result;

    }


    public synchronized void updateReportValue(Context mContext, Uri contentURI, String lid) {
        //	int ret;
        ContentValues values = new ContentValues();
        int rowupdted = 0;

        try {
            values.put(DbConstant.COLUMN_EDIT_KEY, "0");
            values.put(DbConstant.COLUMN_UPLOAD_KEY, "1");
            String where = DbConstant.COLUMN_LID + "=?";
            String selection[] = {lid};
            rowupdted = mContext.getContentResolver().update(contentURI, values, where, selection);
            values.clear();
            values = null;
            DbUtils.getInstance().copy();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
//		return rowupdted;
    }

    public synchronized String getPicPath(Context mContext, Uri contentURI, String lid) {
        String list = null;
        Cursor cursor = null;
        try {

            cursor = mContext.getContentResolver().query(contentURI, new String[]{DbConstant.COLUMN_6}, DbConstant.COLUMN_LID + "=?",
                    new String[]{lid}, null);


            if (cursor != null && cursor.getCount() > 0) {
                cursor.moveToLast();
                list = cursor.getString(0);
            }
            if (cursor != null) {
                cursor.close();
                cursor = null;
            }
        } catch (IllegalArgumentException e) {
            if (cursor != null) {
                cursor.close();
                cursor = null;
            }
            e.printStackTrace();
        }

        return list;
    }


    public synchronized int updatePatientData(Context mContext, Uri contentURI, String lid, String gid) {
        //	int ret;
        ContentValues values = new ContentValues();
        int rowupdted = 0;

        try {
            values.put(DbConstant.COLUMN_GID, gid);
            values.put(DbConstant.COLUMN_UPLOAD_KEY, "1");
            String where = DbConstant.COLUMN_LID + "=?";
            String selection[] = {lid};
            rowupdted = mContext.getContentResolver().update(contentURI, values, where, selection);
            values.clear();
            values = null;
            DbUtils.getInstance().copy();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return rowupdted;
    }

    /**
     * this method returns the all symptom list
     *
     * @return
     */
    public ArrayList<SymptomDetailHandler> getSymptomsListData() {

        ArrayList<SymptomDetailHandler> dataList = new ArrayList<SymptomDetailHandler>();

        File file = new File(Environment.getExternalStorageDirectory() + "/" + ParserConstant.DIRECTORY_PATH + "symptom_ailment");// Initialize file object to the external Registration Regdatabase path
        SQLiteDatabase db = SQLiteDatabase.openOrCreateDatabase(file, null);// open Regdatabase

        if (file.exists()) {

            Cursor cursor = null;
            String q = "SELECT * FROM '" + SymptomConstants.TABLE_SYMPTOMS_MASTER + "'";

            try {

                cursor = db.rawQuery(q, null);

                if ((cursor != null) && (cursor.getCount() > 0)) {
                    //					cursor.moveToFirst();//move cursor to the start
                    while (cursor.moveToNext()) {

                        SymptomDetailHandler handler = new SymptomDetailHandler();
                        handler.setAilmentID(cursor.getString(cursor.getColumnIndexOrThrow(SymptomConstants.SYMPTOM_ID)));
                        handler.setAilmentName(cursor.getString(cursor.getColumnIndexOrThrow(SymptomConstants.SYMPTOM_NAME)));

                        String symptomid = handler.getAilmentID();

                        String q1 = "SELECT * FROM " + SymptomConstants.TABLE_SYMPTOMS_DETAIL + " where ailment_id=?";
                        Cursor cursor1 = db.rawQuery(q1, new String[]{symptomid});

                        if (cursor1 != null && cursor1.getCount() > 0) {

                            cursor1.moveToFirst();

                            while (cursor1.moveToNext()) {

                                handler.setAilmentType(cursor1.getString(cursor1.getColumnIndexOrThrow(SymptomConstants.SYMPTOM_DETAIL_AILMENT_CAT)));
                                handler.setAilmentDetail(cursor1.getString(cursor1.getColumnIndexOrThrow(SymptomConstants.SYMPTOM_DETAIL_AILMENT_DETAIL)));

                            }
                            cursor1.close();
                        }

                        String q2 = "SELECT * FROM " + SymptomConstants.TABLE_INVESTIGATION_MASTER + " where ailment_id=?";
                        Cursor cursor2 = db.rawQuery(q2, new String[]{symptomid});

                        if (cursor2 != null && cursor2.getCount() > 0) {

                            List<InvestigationHandler> list = new ArrayList<InvestigationHandler>();
                            //							cursor2.moveToFirst();

                            while (cursor2.moveToNext()) {

                                InvestigationHandler handler1 = new InvestigationHandler();

                                handler1.setInvestigationID(cursor2.getString(cursor2.getColumnIndexOrThrow(SymptomConstants.INVESTIGATION_ID)));
                                handler1.setInvestigationName(cursor2.getString(cursor2.getColumnIndexOrThrow(SymptomConstants.INVESTIGATIN_NAME)));
                                handler1.setInvestigationType(cursor2.getString(cursor2.getColumnIndexOrThrow(SymptomConstants.INVESTIGATIN_CATEGORY)));

                                String q3 = "SELECT * FROM " + SymptomConstants.TABLE_INVESTIGATION_DETAIL + " where ailment_id=? and investigation_name_id=?";
                                Cursor cursor3 = db.rawQuery(q3, new String[]{symptomid, handler1.getInvestigationID()});

                                if (cursor3 != null && cursor3.getCount() > 0) {

                                    //									cursor3.moveToFirst();
                                    List<String> invList = new ArrayList<String>();

                                    while (cursor3.moveToNext()) {


                                        String invDetail = cursor3.getString(cursor3.getColumnIndexOrThrow(SymptomConstants.INVESTIGATIN_DETAIL));

                                        //										handler1.setInvestigationDetail(cursor3.getString(cursor3.getColumnIndexOrThrow(SymptomConstants.SYMPTOM_DETAIL_AILMENT_CAT)));
                                        invList.add(invDetail);

                                    }
                                    handler1.setInvestigationList(invList);
                                    cursor3.close();
                                }

                                list.add(handler1);

                            }

                            cursor2.close();
                            handler.setInvestigationList(list);

                        }

                        dataList.add(handler);
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                if (cursor != null)
                    cursor.close();
                db.close();
            }


        }

        return dataList;
    }

    /**
     * this is generic method to retrieve the list of data for autofill
     *
     * @param mContext
     * @param contentURI
     * @param columnList
     * @return
     */
    public ArrayList<String> getAutofillDataList(Context mContext, Uri contentURI, String[] columnList) {

        ArrayList<String> list = null;
        Cursor cursor = null;
        try {
            cursor = mContext.getContentResolver().query(contentURI, columnList, DbConstant.COLUMN_LID + "=?",
                    new String[]{ApplicationModelData.getInstance().getmPatientInfoDataHandler().getPatientId()}, null);

            if (cursor != null && cursor.getCount() > 0) {

                list = new ArrayList<String>();

                while (cursor.moveToNext()) {

                    String data = cursor.getString(0);
                    if (null != data && !data.equalsIgnoreCase("null")) {
                        list.add(data);
                    }

                }


            }

            if (cursor != null) {
                cursor.close();
                cursor = null;
            }

        } catch (IllegalArgumentException e) {
            if (cursor != null) {
                cursor.close();
                cursor = null;
            }
            e.printStackTrace();
        }


        return list;
    }


    public synchronized void insertImages(Context mContext, Uri contentURI, String type, String side, String userPicName) {

        ContentValues values = new ContentValues();
        values = initCommonValues(values, mContext);

        values.put(DbConstant.COLUMN_EDIT_KEY, "0");
//        values.put(DbConstant.COLUMN_UPLOAD_KEY, "1");
        values.put(DbConstant.COLUMN_UPLOAD_KEY, "0");

        values.put(DbConstant.COLUMN_2, type);
        values.put(DbConstant.COLUMN_3, side);
        values.put(DbConstant.COLUMN_4, userPicName);

        Log.v(TAG, "insertLogin, values: " + values);

        mContext.getContentResolver().insert(contentURI, values);

        values.clear();
        values = null;
        DbUtils.getInstance().copy();

    }


    public synchronized void deleteOthersImages(Context mContext, Uri contentURI, String type, String subtype, String user_pic, String patient_lid) {
        String where;
        String selectionArg[];
        if (subtype != null) {
            where = DbConstant.COLUMN_2 + "=? and " + DbConstant.COLUMN_LID + "=? and " + DbConstant.COLUMN_3 + "=? and " + DbConstant.COLUMN_4 + "=?";
            selectionArg = new String[]{type, patient_lid, subtype, user_pic};
        } else {
            where = DbConstant.COLUMN_2 + "=? and " + DbConstant.COLUMN_LID + "=? and " + DbConstant.COLUMN_4 + "=?";
            selectionArg = new String[]{type, patient_lid, user_pic};
        }

        mContext.getContentResolver().delete(contentURI, where, selectionArg);
        DbUtils.getInstance().copy();

    }

    public synchronized ArrayList<String> getAllLImagesPath(Context mContext, Uri contentURI, String type, String subtype, String patient_lid) {

        ArrayList<String> all_images = new ArrayList<>();
        String where;
        String selectionArg[];
        if (subtype != null) {

            where = DbConstant.COLUMN_2 + "=? and " + DbConstant.COLUMN_LID + "=? and " + DbConstant.COLUMN_3 + "=?";
            selectionArg = new String[]{type, patient_lid, subtype};
        } else {
            where = DbConstant.COLUMN_2 + "=? and " + DbConstant.COLUMN_LID + "=?";
            selectionArg = new String[]{type, patient_lid};
        }

        Cursor cursor = mContext.getContentResolver().query(contentURI, null, where, selectionArg, null, null);

        if (cursor != null && cursor.getCount() > 0) {
            int count = cursor.getCount();
            Log.d("count", "" + count);
            while (cursor.moveToNext()) {
                try {

                    all_images.add(cursor.getString(cursor.getColumnIndexOrThrow(DbConstant.COLUMN_4)));
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                }
            }
        }
        if (cursor != null) {
            cursor.close();
            cursor = null;
        }

        return all_images;
    }

    public synchronized JSONObject getAllImagesToUpload(Context mContext, String lid) {
        Cursor cursor = null;
        String where = DbConstant.COLUMN_LID + "=? AND " + DbConstant.COLUMN_UPLOAD_KEY + "=?";
//        String selection[] = {lid, "1"};
        String selection[] = {lid, "0"};
        JSONObject userObj = new JSONObject();

        try {
            cursor = DatabaseManager.getInstance().getDataUpload(mContext, DbConstant.URI_IMAGES, where, selection, null);
//				     if(!DbConstant.URI_TABLE_LOGIN.equals(DbConstant.ALL_URI[j]))
            if (cursor != null && cursor.getCount() > 0) {
                JSONArray jsonArray = new JSONArray();
                while (cursor.moveToNext()) {
                    JSONObject user = new JSONObject();
                    try {
                        for (int i = 1; i < cursor.getColumnCount(); i++) {
                            user.put(cursor.getColumnName(i), cursor.getString(i));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    jsonArray.put(user);
                }
                userObj.put(DbConstant.TABLE_IMAGES, jsonArray);
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {

            if (cursor != null) {
                cursor.close();
            }
        }

        return userObj;
    }


    public synchronized JSONObject getSingleTableJson(Context mContext, String lid, Uri tableUri, String tableName) {
        Cursor cursor = null;

        //// TODO: 18/01/16 check if upload_key has to change to 0
        String where = /*DbConstant.COLUMN_LID + "=? AND " + */DbConstant.COLUMN_UPLOAD_KEY + "=?";
//        String selection[] = {lid, "1"};
        String selection[] = {/*lid, */"0"};
        JSONObject userObj = new JSONObject();

        try {
            cursor = DatabaseManager.getInstance().getDataUpload(mContext, tableUri, where, selection, null);
//				     if(!DbConstant.URI_TABLE_LOGIN.equals(DbConstant.ALL_URI[j]))
            if (cursor != null && cursor.getCount() > 0) {
                JSONArray jsonArray = new JSONArray();
                while (cursor.moveToNext()) {
                    JSONObject user = new JSONObject();
                    try {
                        for (int i = 1; i < cursor.getColumnCount(); i++) {
                            user.put(cursor.getColumnName(i), cursor.getString(i));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    jsonArray.put(user);
                }
                userObj.put(tableName, jsonArray);
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {

            if (cursor != null) {
                cursor.close();
            }
        }

        return userObj;
    }


    public synchronized ArrayList<String> getAllLImagesPathToUpload(Context mContext, Uri contentURI, String patient_lid) {
        ArrayList<String> all_images = new ArrayList<>();

        String where = DbConstant.COLUMN_LID + "=? AND " + DbConstant.COLUMN_UPLOAD_KEY + "=?";
//        String selection[] = {patient_lid, "1"};
        String selection[] = {patient_lid, "0"};

        Cursor cursor = mContext.getContentResolver().query(contentURI, null, where, selection, null, null);

        if (cursor != null && cursor.getCount() > 0) {
            int count = cursor.getCount();
            Log.d("count", "" + count);
            while (cursor.moveToNext()) {
                try {

                    all_images.add(cursor.getString(cursor.getColumnIndexOrThrow(DbConstant.COLUMN_4)));
                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                }
            }
        }
        if (cursor != null) {
            cursor.close();
            cursor = null;
        }

        return all_images;
    }


    public synchronized void updateAllImages(Context mContext, Uri contentURI, String patient_lid) {
        ContentValues values = new ContentValues();
        int rowupdted = 0;

        try {
            values.put(DbConstant.COLUMN_EDIT_KEY, "0");
//            values.put(DbConstant.COLUMN_UPLOAD_KEY, "0");
            values.put(DbConstant.COLUMN_UPLOAD_KEY, "1");
            String where = DbConstant.COLUMN_LID + "=?";
            String selection[] = {patient_lid};
            rowupdted = mContext.getContentResolver().update(contentURI, values, where, selection);
            values.clear();
            values = null;
            DbUtils.getInstance().copy();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }


    /**
     * this method returns the all state and district list
     *
     * @return
     */
    @SuppressWarnings("null")
    public ArrayList<StateDistrictData> getStateListData(Context ctx) {

        ArrayList<StateDistrictData> dataList = new ArrayList<StateDistrictData>();

        File file = new File(Environment.getExternalStorageDirectory() + "/" + ParserConstant.DIRECTORY_PATH + "statedistrictsdetails");// Initialize file object to the external Registration Regdatabase path
        SQLiteDatabase db = SQLiteDatabase.openOrCreateDatabase(file, null);// open Regdatabase

        if (file.exists()) {

            Cursor cursor = null;
            String q = "SELECT statename, statecode FROM statedistrict group by statename ";

            try {

                cursor = db.rawQuery(q, null);

                if ((cursor != null) && (cursor.getCount() > 0)) {
                    //					cursor.moveToFirst();//move cursor to the start
                    while (cursor.moveToNext()) {

                        StateDistrictData handler = new StateDistrictData();
                        handler.setStateCode(cursor.getString(cursor.getColumnIndexOrThrow(Constants.STATE_CODE)));
                        handler.setStateName(cursor.getString(cursor.getColumnIndexOrThrow(Constants.STATE_NAME)));
                        dataList.add(handler);
                    }

                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                cursor.close();
                db.close();
            }


        }

        return dataList;
    }

    public ArrayList<StateDistrictData> getDistrictListData(Context ctx, String symptomid) {

        ArrayList<StateDistrictData> dataList = new ArrayList<StateDistrictData>();

        File file = new File(Environment.getExternalStorageDirectory() + "/" + ParserConstant.DIRECTORY_PATH + "statedistrictsdetails");// Initialize file object to the external Registration Regdatabase path
        SQLiteDatabase db = SQLiteDatabase.openOrCreateDatabase(file, null);// open Regdatabase

        if (file.exists()) {

            Cursor cursor = null;
            String q = "SELECT * FROM " + Constants.TABLE_STATE_DISTRICT + " where statecode='" + symptomid + "' order by districtname asc";

            try {

                cursor = db.rawQuery(q, null);

                if ((cursor != null) && (cursor.getCount() > 0)) {
                    //					cursor.moveToFirst();//move cursor to the start
                    while (cursor.moveToNext()) {

                        StateDistrictData handler = new StateDistrictData();
                        handler.setDistrictCode(cursor.getString(cursor.getColumnIndexOrThrow(Constants.DISTRICT_CODE)));
                        handler.setDistrictName(cursor.getString(cursor.getColumnIndexOrThrow(Constants.DISTRICT_NAME)));
                        dataList.add(handler);

                    }


                }

            } catch (Exception e) {
                e.printStackTrace();
            } finally

            {
                cursor.close();
                db.close();
            }


        }

        return dataList;
    }

    /**
     * get the patient list to upload
     *
     * @param mContext
     * @return
     */
    public synchronized String[] getPatientListToUpload(Context mContext) {
        String[] lidList = null;
        Cursor cursor = null;

        String where = DbConstant.COLUMN_UPLOAD_KEY + "=?";
        String selection[] = {"0"};

        Log.v(TAG, "where : " + where + " selection : " + selection);
        try {

            cursor = mContext.getContentResolver().query(DbConstant.URI_TABLE_REGISTRATION, null, where, selection, null);

            Log.v(TAG, "cursor.getCount() : " + cursor.getCount());

            if (cursor != null && cursor.getCount() > 0) {
                int cursorLength = cursor.getCount();
                lidList = new String[cursorLength];

                cursor.moveToFirst();

                for (int i = 0; i < cursorLength; i++) {

                    String gidRet = cursor.getString(cursor.getColumnIndexOrThrow(DbConstant.COLUMN_LID));
                    Log.v(TAG, "gid to upload : " + gidRet);

                    lidList[i] = gidRet;

                    cursor.moveToNext();
                }

            }


        } catch (IllegalArgumentException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } finally {

            if (cursor != null) {
                cursor.close();
            }

        }
        return lidList;
    }

    public synchronized Bundle getPatientToUpload(Context mContext, Uri contentURI) {

        Bundle bundle = new Bundle();
        String where;
        String selectionArg[];
        Cursor cursor = mContext.getContentResolver().query(contentURI, null, null, null, null, null);

        if (cursor != null && cursor.getCount() > 0) {
            int count = cursor.getCount();
            Log.d("count", "" + count);
            while (cursor.moveToNext()) {
                try {

                    bundle.putString(DbConstant.COLUMN_UPLOAD_KEY, cursor.getString(cursor.getColumnIndexOrThrow(DbConstant.COLUMN_UPLOAD_KEY)));
                    bundle.putString(DbConstant.COLUMN_1, cursor.getString(cursor.getColumnIndexOrThrow(DbConstant.COLUMN_1)));
                    bundle.putString(DbConstant.COLUMN_2, cursor.getString(cursor.getColumnIndexOrThrow(DbConstant.COLUMN_2)));
                    bundle.putString(DbConstant.COLUMN_3, cursor.getString(cursor.getColumnIndexOrThrow(DbConstant.COLUMN_3)));
                    bundle.putString(DbConstant.COLUMN_4, cursor.getString(cursor.getColumnIndexOrThrow(DbConstant.COLUMN_4)));
                    bundle.putString(DbConstant.COLUMN_5, cursor.getString(cursor.getColumnIndexOrThrow(DbConstant.COLUMN_5)));
                    bundle.putString(DbConstant.COLUMN_6, cursor.getString(cursor.getColumnIndexOrThrow(DbConstant.COLUMN_6)));
                    bundle.putString(DbConstant.COLUMN_7, cursor.getString(cursor.getColumnIndexOrThrow(DbConstant.COLUMN_7)));
                    bundle.putString(DbConstant.COLUMN_8, cursor.getString(cursor.getColumnIndexOrThrow(DbConstant.COLUMN_8)));
                    bundle.putString(DbConstant.COLUMN_HWID, cursor.getString(cursor.getColumnIndexOrThrow(DbConstant.COLUMN_HWID)));
                    bundle.putString(DbConstant.COLUMN_LID, cursor.getString(cursor.getColumnIndexOrThrow(DbConstant.COLUMN_LID)));
                    bundle.putString(DbConstant.COLUMN_GID, cursor.getString(cursor.getColumnIndexOrThrow(DbConstant.COLUMN_GID)));

                } catch (IllegalArgumentException e) {
                    e.printStackTrace();
                }
            }
        }
        if (cursor != null) {
            cursor.close();
            cursor = null;
        }

        return bundle;
    }

    /**
     * update upload key in table after data upload
     *
     * @param mContext
     * @param contentURI
     * @param value
     */
    public synchronized int updateUploadStatus(Context mContext, Uri contentURI, String value, String lid) {

        int rowupdted = 0;

        ContentValues values = new ContentValues();
        String where = DbConstant.COLUMN_LID + "=? AND " + DbConstant.COLUMN_UPLOAD_KEY + "=?";
        String selection[] = {lid, "0"};
        try {
            values.put(DbConstant.COLUMN_UPLOAD_KEY, value);
            Log.v(TAG, "Update key, values: " + values);
            rowupdted = mContext.getContentResolver().update(contentURI, values, where, selection);
            values.clear();
            values = null;
            DbUtils.getInstance().copy();
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return rowupdted;
    }

}
