package com.phtt.telemedhc.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {
	
	    private static String TAG = "DatabaseHelper";


	    public  DatabaseHelper(Context mContext){
	        super(mContext,DbConstant.DATABASE_NAME, null,DbConstant.DATABASE_VERSION);
	    }


		@Override
		public void onCreate(SQLiteDatabase db) {
			// TODO Auto-generated method stub
			String allCalls =  DbConstant.COLUMN_VISIT_NO + " TEXT, " + DbConstant.COLUMN_TOKEN_ID + " TEXT, " +DbConstant.COLUMN_1 + " TEXT, "+ DbConstant.COLUMN_2 + " TEXT, " + DbConstant.COLUMN_3 + " TEXT, "
					+ DbConstant.COLUMN_4 + " TEXT, " + DbConstant.COLUMN_5 + " TEXT, " + DbConstant.COLUMN_6 
					+ " TEXT," + DbConstant.COLUMN_7 + " TEXT," +DbConstant.COLUMN_8 + " TEXT," +DbConstant.COLUMN_9
					+ " TEXT," +  DbConstant.COLUMN_10 + " TEXT," +  DbConstant.COLUMN_11 + " TEXT," + DbConstant.COLUMN_12
					+ " TEXT," + DbConstant.COLUMN_13 + " TEXT," + DbConstant.COLUMN_14 + " TEXT," + DbConstant.COLUMN_15
					+ " TEXT," + DbConstant.COLUMN_16 + " TEXT," + DbConstant.COLUMN_17 + " TEXT," + DbConstant.COLUMN_18
					+ " TEXT," + DbConstant.COLUMN_19 + " TEXT," + DbConstant.COLUMN_20 + " TEXT," +DbConstant.COLUMN_21 + " TEXT,"
					+ DbConstant.COLUMN_22 + " TEXT," + DbConstant.COLUMN_23+ " TEXT," + DbConstant.COLUMN_24 + " TEXT,"
					+ DbConstant.COLUMN_25 + " TEXT";
			
			// TABLE LOGIN
			db.execSQL("create table " + DbConstant.TABLE_LOGIN + "(" + DbConstant.COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
					+ DbConstant.COLUMN_GID + " TEXT, " + DbConstant.COLUMN_LID + " TEXT, " + DbConstant.COLUMN_TIME + " TEXT,"
					+ DbConstant.COLUMN_EDIT_KEY + " TEXT," + DbConstant.COLUMN_UPLOAD_KEY + " TEXT," + allCalls + ");");

			// TABLE REGISTRATION
			db.execSQL("create table " + DbConstant.TABLE_REGISTRATION + "(" + DbConstant.COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
					+ DbConstant.COLUMN_GID + " TEXT, " + DbConstant.COLUMN_LID + " TEXT, " + DbConstant.COLUMN_TIME + " TEXT,"
					+ DbConstant.COLUMN_HWID + " TEXT, "+ DbConstant.COLUMN_EDIT_KEY + " TEXT," + DbConstant.COLUMN_UPLOAD_KEY + " TEXT," + allCalls + ");");

			// TABLE_GPS_LOCATION
			db.execSQL("create table " + DbConstant.TABLE_GPS_LOCATION + "(" + DbConstant.COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
					+ DbConstant.COLUMN_GID + " TEXT, " + DbConstant.COLUMN_LID + " TEXT, " + DbConstant.COLUMN_TIME + " TEXT," + DbConstant.COLUMN_EDIT_KEY + " TEXT," + DbConstant.COLUMN_UPLOAD_KEY + " TEXT,"
					+ DbConstant.COLUMN_HWID + " TEXT, "+ DbConstant.COLUMN_LATITUDE + " TEXT, " + DbConstant.COLUMN_LONGITUDE + " TEXT, " + DbConstant.COLUMN_CELL_ID + " TEXT, " + DbConstant.COLUMN_LAC + " TEXT, " + allCalls + ");");
			// TABLE HEALTH REPORT
			db.execSQL("create table " + DbConstant.TABLE_HEALTH_REPORT + "(" + DbConstant.COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
					+ DbConstant.COLUMN_GID + " TEXT, " + DbConstant.COLUMN_LID + " TEXT, " + DbConstant.COLUMN_TIME + " TEXT,"
					+ DbConstant.COLUMN_HWID + " TEXT, "+ DbConstant.COLUMN_EDIT_KEY + " TEXT," + DbConstant.COLUMN_UPLOAD_KEY + " TEXT," + allCalls + ");");

			// TABLE IMAGES
			db.execSQL("create table " + DbConstant.TABLE_IMAGES + "(" + DbConstant.COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
					+ DbConstant.COLUMN_GID + " TEXT, " + DbConstant.COLUMN_LID + " TEXT, " + DbConstant.COLUMN_TIME + " TEXT,"
					+ DbConstant.COLUMN_HWID + " TEXT, "+ DbConstant.COLUMN_EDIT_KEY + " TEXT," + DbConstant.COLUMN_UPLOAD_KEY + " TEXT," + allCalls + ");");


			// TABLE Symptoms
			db.execSQL("create table " + DbConstant.TABLE_SYMPTONS + "(" + DbConstant.COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
					+ DbConstant.COLUMN_GID + " TEXT, " + DbConstant.COLUMN_LID + " TEXT, " + DbConstant.COLUMN_TIME + " TEXT,"
					+ DbConstant.COLUMN_HWID + " TEXT, "+ DbConstant.COLUMN_EDIT_KEY + " TEXT," + DbConstant.COLUMN_UPLOAD_KEY + " TEXT," + allCalls + ");");


			//TABLE_PATIENT_TREATMENT_FOLLOWUP
			db.execSQL("create table " + DbConstant.TABLE_PATIENT_TREATMENT_FOLLOWUP + "(" + DbConstant.COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
					+ DbConstant.COLUMN_LID + " TEXT, " + DbConstant.COLUMN_DOC_ID + " TEXT , " + DbConstant.COLUMN_CLINIC_ID + " TEXT, " + DbConstant.COLUMN_SUB_USER_ID + " TEXT, "
					+ DbConstant.COLUMN_DATE + " TEXT, "+DbConstant.COLUMN_COMMON_DATE_TIME+ " TEXT," + DbConstant.COLUMN_FOLLOWUP_CLOSE_FLAG + " TEXT,"
					+ DbConstant.COLUMN_TIME  +" TEXT,"+DbConstant.COLUMN_EDIT_KEY+ " TEXT," +DbConstant.COLUMN_UPLOAD_KEY + " TEXT," +allCalls+ ");");



		}


		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			// TODO Auto-generated method stub
			
		}

}
