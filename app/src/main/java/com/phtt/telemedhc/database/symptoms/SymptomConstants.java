package com.phtt.telemedhc.database.symptoms;

public class SymptomConstants {

	
	public static final String SYMPTOM_ID 					= "id";
	public static final String SYMPTOM_NAME 				= "ailment_name";
//	public static final String SYMPTOM_DETAIL_ID 			= "id";
	public static final String SYMPTOM_DETAIL_AILMENT_ID 	= "ailment_id";
	public static final String SYMPTOM_DETAIL_AILMENT_CAT 	= "ailment_category_name";
	public static final String SYMPTOM_DETAIL_AILMENT_DETAIL= "ailment_symptom_details";
	public static final String INVESTIGATION_ID 			= "id";
	public static final String INVESTIGATIN_CATEGORY	 	= "investigation_cat_name";
	public static final String INVESTIGATIN_NAME	 		= "investigation_name";
	
	public static final String INVESTIGATIN_DETAIL_ID	 	= "investigation_name_id";
	public static final String INVESTIGATIN_DETAIL	 		= "investigation_details";
	
	
	public static final String TABLE_SYMPTOMS_MASTER        =  "ailment_master";
	public static final String TABLE_SYMPTOMS_DETAIL        =  "ailment_symptom_details";
	public static final String TABLE_INVESTIGATION_MASTER   =  "investigation_name";
	public static final String TABLE_INVESTIGATION_DETAIL   =  "investigation_details";
	
	
}
