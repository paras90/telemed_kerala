package com.phfi.model;

import java.io.Serializable;

public class MotherInformationModel implements Serializable {

    public static final String MOTHER_PARSE_DATA = "mother_parse_data";
    private static final long serialVersionUID = 1L;

    private String firstName = "";
    private String gloablId = "";
    private String localId = "";
    private String imagePath = "";
    private String age = "";


    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getGloablId() {
        return gloablId;
    }

    public void setGloablId(String gloablId) {
        this.gloablId = gloablId;
    }

    public String getLocalId() {
        return localId;
    }

    public void setLocalId(String localId) {
        this.localId = localId;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }


}
