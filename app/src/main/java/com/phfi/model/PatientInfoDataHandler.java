package com.phfi.model;

import java.io.Serializable;


public class PatientInfoDataHandler implements Serializable {

    private static final long serialVersionUID = 1L;

    private String PatientGlobalId = "";

    private String PatientId = "";

    public String getPatientSex() {
        return PatientSex;
    }

    public void setPatientSex(String patientSex) {
        PatientSex = patientSex;
    }

    private String PatientSex = "";

    public String getHWId() {
        return HWId;
    }

    public void setHWId(String HWId) {
        this.HWId = HWId;
    }

    private String HWId = "";

    private String FirstName = "";

    private String LastName = "";

    public String getGloablId() {
        return gloablId;
    }

    public void setGloablId(String gloablId) {
        this.gloablId = gloablId;
    }

    public String getLocalId() {
        return localId;
    }

    public void setLocalId(String localId) {
        this.localId = localId;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    private String gloablId = "";
    private String localId = "";
    private String imagePath = "";

    // TODO: need to add something here


    public String getPatientId() {

        return PatientId;

    }

    public void setPatientId(String patientId) {

        PatientId = patientId;

    }


    public String getFirstName() {

        return FirstName;

    }

    public void setFirstName(String firstName) {

        FirstName = firstName;

    }

    public String getLastName() {

        return LastName;

    }

    public void setLastName(String lastName) {

        LastName = lastName;

    }

    public String getPatientGlobalId() {

        return PatientGlobalId;

    }

    public void setPatientGlobalId(String patientGlobalId) {

        PatientGlobalId = patientGlobalId;

    }

}
